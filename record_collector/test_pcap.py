#!/usr/bin/env python
import sys
import pcap
import dpkt
import time
from sip_parser.sip_message import SipMessage
from sip_parser.sdp_message import SdpMessage
from sip_parser.exceptions import SipParseError
import record_pb2
import textwrap
import zmq
import multiprocessing
import socket
import loguru
import json
import datetime as dt


LOGGER = loguru.logger

def prepare_msg(msg):
    # Message lines must be CRLF-terminated and not indented
    return textwrap.dedent(msg.strip())


class SipSession:
    def __init__(self):
        self.call_id = ''
        self.from_user = ''
        self.from_addr = ''
        self.to_user = ''
        self.pkt_cnt = 0
        self.invite_resp_code = 0

    def __repr__(self):
        return 'call_id: {self.call_id}\nto_user: {self.to_user}\npkt_cnt: {self.pkt_cnt}'


class SipSniffer:
    def __init__(self):
        self.sip_map = dict()
        pass

    def send_event(self, sip_msg):
        # TODO: sip/sdp 에서 적당한 값을 record.proto format으로 변환하여 zeromq로 전송한다
        pass

    def send_voice(self, rtp):
        # TODO: rtp 에서 적당한 값을 record.proto format으로 변환하여 zeromq로 전송한다
        pass

    def run(self):
        ctx = zmq.Context()
        push_sock = ctx.socket(zmq.PUSH)
        push_sock.connect("tcp://127.0.0.1:5559")
        #push_sock.connect("ipc://frecord_q.ipc")
        #push_sock.connect("tcp://127.0.0.1:5560")

        # file name or nic name
        sniffer = pcap.pcap('packet_voice1_00001_20201116145307.pcap')
        pack_cnt = 0
        st = time.time()
        for ts, pkt in sniffer:
            pack_cnt += 1
            push_sock.send_pyobj(pkt)
            continue

        elapsed = time.time() - st
        print(pack_cnt / elapsed, "packets per second,", pack_cnt)
        for k in self.sip_map:
            print(self.sip_map[k])
        push_sock.send_pyobj('exit')


def handle_packet(trace_sip):
    record_msg2 = record_pb2.Record()
    trace_sip = False
    sip_logger = LOGGER.bind(name='SIP')
    sip_map = dict()
    #ctx = zmq.Context(io_threads=4)
    ctx = zmq.Context(io_threads=4)
    pull_sock = ctx.socket(zmq.PULL)
    pull_sock.connect("tcp://127.0.0.1:5560")
    #pull_sock.connect("ipc://brecord_q.ipc")

    record_sock = ctx.socket(zmq.PUSH)
    record_sock.connect("tcp://127.0.0.1:5561")
    #record_sock.connect("ipc:///tmp/test.ipc")

    pkt_cnt = 0
    st = time.time()
    while True:
        pkt = pull_sock.recv_pyobj()
        pkt_cnt += 1
        if pkt == 'exit':
            print(pkt_cnt)
            break

        eth = dpkt.ethernet.Ethernet(pkt)
        if not isinstance(eth.data, dpkt.ip.IP):
            # print('Non IP Packet type not supported %s\n' % eth.data.__class__.__name__)
            continue

        # Now grab the data within the Ethernet frame (the IP packet)
        ip = eth.data
        if isinstance(ip.data, dpkt.udp.UDP):
            udp = ip.data
            if udp.sport == 5060 or udp.dport == 5060:
                try:
                    sip_msg = SipMessage.from_string(udp.data.decode('utf-8'))
                    call_id = sip_msg.headers['call-id']
                    cseq = sip_msg.headers['cseq']
                    if trace_sip:
                        sip_f = open('sip_log/' + call_id + '.log', 'a')
                        sip_f.write('%s : ---------- GET MESSAGE ----------\n%s\n' % (dt.datetime.today(), udp.data.decode('utf-8')))
                        sip_f.close()

                    # 이미 존재하는 CALL-ID 인 경우 정보 업데이트
                    if call_id in sip_map:
                        # get sip session
                        ss = sip_map[call_id]
                        ss.pkt_cnt += 1
                        # INVITE
                        if sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'INVITE':
                            # SDP가 존재하는 경우 SDP 정보 refresh
                            if len(sip_msg.content) > 0:
                                sdp_msg = SdpMessage.from_string(prepare_msg(sip_msg.content))
                                print(sip_msg.content)
                                host = sdp_msg.session_description_fields["c"].connection_address
                                port = sdp_msg.media_descriptions[0].media.port
                                ss.from_addr = '{host}:{port}'

                                # send protobuf over zeromq
                                record_msg = record_pb2.Record()
                                record_msg.event.type = record_pb2.Event.EventType.PREPARE
                                record_msg.event.record_key = call_id
                                record_msg.event.voice_key = ss.from_addr
                                record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)

                        # BYE 인 경우 삭제
                        elif sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'BYE':
                            # print('got invite response:', call_id)
                            record_msg = record_pb2.Record()
                            record_msg.event.type = record_pb2.Event.EventType.STOP
                            record_msg.event.record_key = call_id
                            record_msg.event.voice_key = ss.from_addr
                            record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)

                            del sip_map[call_id]
                            #print(record_msg)

                        elif sip_msg.type == SipMessage.TYPE_RESPONSE and cseq['method'] == 'INVITE':
                            ss.invite_resp_code = sip_msg.status

                        elif sip_msg.type == SipMessage.TYPE_REQUEST and cseq['method'] == 'ACK':
                            if ss.invite_resp_code == 200:
                                # print('got invite response:', call_id)
                                record_msg = record_pb2.Record()
                                record_msg.event.type = record_pb2.Event.EventType.START
                                record_msg.event.record_key = call_id
                                record_msg.event.voice_key = ss.from_addr
                                record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
                            #print(record_msg)

                    # CALL-ID 가 없는 경우, INVITE가 온 경우
                    elif sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'INVITE':
                        #sip_msg.debug_print()
                        ss = SipSession()
                        ss.call_id = call_id
                        ss.to_uri = sip_msg.headers['to']['uri']
                        ss.pkt_cnt = 1
                        #pn = to_uri.split(':')[1].split('@')[0]
                        #print(f'phone number = {pn}')

                        if sip_msg.headers['content-type'] == 'application/sdp' and len(sip_msg.content) > 0:
                            sdp_msg = SdpMessage.from_string(prepare_msg(sip_msg.content))
                            host = sdp_msg.session_description_fields["c"].connection_address
                            port = sdp_msg.media_descriptions[0].media.port
                            ss.from_addr = '{host}:{port}'

                            record_msg = record_pb2.Record()
                            record_msg.event.type = record_pb2.Event.EventType.PREPARE
                            record_msg.event.record_key = call_id
                            record_msg.event.voice_key = ss.from_addr
                            record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)

                        sip_map[call_id] = ss

                    # TODO:
                    # sip_map 에 있는지 검색, 없으면서 invite request인 경우는 생성
                    # invite 에 대한 ack시에 start event 전송
                    # bye경우 bye event전송 후 sip_map에서 삭제
                except SipParseError as e:
                    pass
                except Exception as e:
                    print('Exception :', type(e), e)
                    print(udp.data.decode('utf-8'))
                    pass
            else:
                # TODO
                # RTP 패킷인지 확인
                # udp src ip/port와 dst ip/port, RTP payload type 확인하여 payload와 함께 전송
                rtp = dpkt.rtp.RTP(udp.data)
                if rtp.version == 2:
                    # if len(rtp) != 172:
                    if rtp.pt != 0:
                        pass
                        # print('this may be rtcp: ', udp.data, udp.sport, udp.dport)
                    else:
                        #record_msg = record_pb2.Record()
                        record_msg2.voice.channel_no = -1
                        src = "%s:%d" % (socket.inet_ntop(socket.AF_INET, ip.src), udp.sport)
                        dst = "%s:%d" % (socket.inet_ntop(socket.AF_INET, ip.dst), udp.dport)
                        #record_msg2.voice.voice_key_candidates.extend([src, dst])
                        record_msg2.voice.payload_type = 0
                        record_msg2.voice.payload = udp.data
                        #continue
                        # print(record_msg)
                        #buf = record_msg.SerializeToString()
                        #continue
                        #record_sock.send(record_msg.SerializeToString(), 0)
                        record_sock.send(record_msg2.SerializeToString(), zmq.NOBLOCK)
                else:
                    # print('this is not sip or rtp', udp.data)
                    pass

    elapsed = time.time() - st
    print(pkt_cnt / elapsed, "packets per second (handle_packet),", pkt_cnt)
    for k in sip_map:
        print(sip_map[k])


def make_filter(name):
    def filter(record):
        return record["extra"].get("name") == name
    return filter


if __name__ == '__main__':
    log_format = '[{time:YYYY-MM-DD HH:mm:ss.SSS}] [{process: >5}] [{level.name:>5}] <level>{message}</level>'
    loguru.logger.configure(
        handlers=[
            # dict(sink=sys.stdout, format=log_format, colorize=True),
            dict(sink=sys.stdout, colorize=True, filter=make_filter('main')),
            dict(sink='sip.log',
                 format=log_format,
                 colorize=False,
                 filter=make_filter('SIP')
            ),
            # dict(sink=config['LOG']['directory'] + '/list.log',
            #      format=log_format,
            #      enqueue=True,
            #      level=config['LOG']['level'].upper(),
            #      # serialize=True,
            #      rotation='%s MB' % config['LOG']['max_mbytes'],
            #      retention=int(config['LOG']['backup_count']),
            #      filter=make_filter("SIP")
            #      )
        ],
    )

    #manager = multiprocessing.Manager()
    #rq = manager.Queue(-1)
    p = multiprocessing.Process(target=handle_packet, args=(True, ))
    p.daemon = True
    p.start()

    SipSniffer().run()
    p.join()
