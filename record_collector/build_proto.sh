#!/bin/sh
SRC_DIR=../protos
DST_DIR=.
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=cpp
#protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/record.proto
/home/minds/.virtualenvs/venv_vad/bin/python -m grpc_tools.protoc -I ${SRC_DIR} --python_out=. --grpc_python_out=. ${SRC_DIR}/record.proto
