import zmq

#ctx = zmq.Context(io_threads=4)
ctx = zmq.Context()

def run_proxy(fport=5559, bport=5560):
  fsock, bsock = ctx.socket(zmq.PULL), ctx.socket(zmq.PUSH)
  fsock.bind(f"tcp://*:{fport}")
  bsock.bind(f"tcp://*:{bport}")
  #fsock.bind(f"ipc://frecord_q.ipc")
  #bsock.bind(f"ipc://brecord_q.ipc")
  # start proxy with 2 sockets
  zmq.proxy(fsock, bsock)

run_proxy()
