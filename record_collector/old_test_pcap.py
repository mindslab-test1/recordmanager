#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pcap
import dpkt
import time
import os
import sys
sys.path.append('/DATA/maum/capd_install/RecordManager-develop/record_collector/sip-parser-master/build/lib')
from sip_parser.sip_message import SipMessage
from sip_parser.sdp_message import SdpMessage


class SipSession:
    def __init__(self):
        self.call_id = ''
        self.from_user = ''
        self.to_user = ''


class SipSniffer:
    def __init__(self):
        self.sip_map = dict()
        pass

    def send_event(self, sip_msg):
        # TODO: sip/sdp 에서 적당한 값을 record.proto format으로 변환하여 zeromq로 전송한다
        pass

    def send_voice(self, rtp):
        # TODO: rtp 에서 적당한 값을 record.proto format으로 변환하여 zeromq로 전송한다
        pass

    def run(self):
        # file name or nic name
        sniffer = pcap.pcap('myfile.pcap')
        pack_cnt = 0
        st = time.time()
        for ts, pkt in sniffer:
            pack_cnt += 1
            eth = dpkt.ethernet.Ethernet(pkt)
            if not isinstance(eth.data, dpkt.ip.IP):
                print('Non IP Packet type not supported %s\n' % eth.data.__class__.__name__)
                continue
            # Now grab the data within the Ethernet frame (the IP packet)
            ip = eth.data
            if isinstance(ip.data, dpkt.udp.UDP):
                udp = ip.data
                if udp.sport == 5060 or udp.dport == 5060:
                    try:
                        sip_msg = SipMessage.from_string(udp.data.decode('utf-8'))
                        #sip_msg.debug_print()
                        call_id = sip_msg.headers['call-id']
                        print(call_id)
                        # TODO:
                        # sip_map 에 있는지 검색, 없으면서 invite request인 경우는 생성
                        # invite 에 대한 ack시에 start event 전송
                        # bye경우 bye event전송 후 sip_map에서 삭제
                    except Exception as e:
                        pass
                else:
                    # TODO
                    # RTP 패킷인지 확인
                    # udp src ip/port와 dst ip/port, RTP payload type 확인하여 payload와 함께 전송
                    pass

        elapsed = time.time() - st
        print(pack_cnt / elapsed, "packets per second,", pack_cnt)


if __name__ == '__main__':
    SipSniffer().run()
