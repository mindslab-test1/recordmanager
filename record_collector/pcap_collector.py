#!/usr/bin/env python
import sys
import pcap
import time
from sip_parser.sip_message import SipMessage
from sip_parser.sdp_message import SdpMessage
from sip_parser.exceptions import SipParseError
import record_pb2
import textwrap
import zmq
import multiprocessing
import socket
import loguru
import json
import datetime as dt
import struct
import os
import argparse
import configparser
import audioop


LOGGER = loguru.logger

def prepare_msg(msg):
    # Message lines must be CRLF-terminated and not indented
    #return textwrap.dedent(msg.strip())
    # 2021-09-02 by shinwc
    # The order of 'c' line and 'm' line in SDP body is important when MicroSIP client is used
    # 'm' line should be prior to 'c' line to avoid 'c' KeyError
    # 'b' line should be removed to avoid 'b' KeyError
    lines = textwrap.dedent(msg).split('\n')
    m_idx = [i for i, x in enumerate(lines) if x.startswith('m')]
    c_idx = [i for i, x in enumerate(lines) if x.startswith('c')]
    if c_idx[0] > m_idx[0]:
        lines[m_idx[0]], lines[c_idx[0]] = lines[c_idx[0]], lines[m_idx[0]]
    msg = '\n'.join(x for x in lines if not x.startswith('b')).strip()
    #print(msg)
    return msg


class SipSession:
    def __init__(self):
        self.call_id = ''
        self.from_user = ''
        self.from_addr = ''
        self.to_user = ''
        self.pkt_cnt = 0
        self.invite_resp_code = 0

    def __repr__(self):
        return f'call_id: {self.call_id}\nto_user: {self.to_user}\npkt_cnt: {self.pkt_cnt}'


class PcapCollector:
    def __init__(self, config):
        self.config = config
        self.trace_sip = True if config['common']['trace_sip'] == 'true' else False
        self.sip_map = dict()

        # For SIP Event send
        self.ctx = zmq.Context()
        self.record_sock = self.ctx.socket(zmq.PUSH)
        self.record_sock.connect(config['common']['record_manager_addr'])

        self.rtp_sock = list()
        self.worker_cnt = int(config['common']['worker_cnt'])
        self.worker_start_port = int(config['common']['worker_start_port'])

        self.start_time = None
        self.pkt_cnt = 0
        #self.extensions = config['extension']['lines']
        self.extensions = json.loads(config.get('extension', 'lines'))
        LOGGER.info(f'extensions: {self.extensions}')

    def get_udp(self, packet):
        '''
        Return None or (s_addr, d_addr, sport, dport, udp)
        '''
        eth_length = 14
        eth_header = packet[:eth_length]
        eth = struct.unpack('!6s6sH' , eth_header)
        eth_protocol = socket.ntohs(eth[2])

        if eth_protocol == 8 :
            #Parse IP header
            #take first 20 characters for the ip header
            ip_header = packet[eth_length:20+eth_length]
        
            #now unpack them :)
            iph = struct.unpack('!BBHHHBBH4s4s' , ip_header)

            version_ihl = iph[0]
            version = version_ihl >> 4
            ihl = version_ihl & 0xF

            iph_length = ihl * 4

            ttl = iph[5]
            protocol = iph[6]
            s_addr = socket.inet_ntoa(iph[8]);
            d_addr = socket.inet_ntoa(iph[9]);

            if protocol == 17 :
                u = iph_length + eth_length
                udph_length = 8
                udp_header = packet[u:u+8]

                #now unpack them :)
                udph = struct.unpack('!HHHH' , udp_header)

                sport = udph[0]
                dport = udph[1]
                length = udph[2]
                checksum = udph[3]
                
                #print('Source Port : ' + str(sport) + ' Dest Port : ' + str(dport) + ' Length : ' + str(length) + ' Checksum : ' + str(checksum))
                
                h_size = eth_length + iph_length + udph_length
                data_size = len(packet) - h_size
                
                #get data from the packet
                data = packet[h_size:]

                return (s_addr, d_addr, sport, dport, data)
        return None

    def print_statistics(self):
        elapsed = time.time() - self.start_time
        LOGGER.info(f"{self.pkt_cnt / elapsed} packets per second, total received {self.pkt_cnt}")
        self.pkt_cnt = 0
        self.start_time = None

    def run(self):
        plist = []
        init_start = None
        init_cnt = 0

        for i in range(self.worker_cnt):
            internal = self.ctx.socket(zmq.PUSH)
            internal.bind(f"tcp://0.0.0.0:{self.worker_start_port+i}")
            self.rtp_sock.append(internal)

            p = multiprocessing.Process(target=self.run_rtp, args=(i,))
            p.start()
            plist.append(p)

        # file name or nic name
        sniffer = pcap.pcap(self.config['pcap']['device'])

        for ts, pkt in sniffer:
            if self.start_time is None:
                self.start_time = time.time()
            if init_start is None:
                init_start = time.time()
            self.pkt_cnt += 1
            init_cnt += 1
            # if pkt == 'exit':
                # self.print_statistics()
                # break

            udp_info = self.get_udp(pkt)
            if udp_info:
                s_addr, d_addr, sport, dport, data = udp_info
                if sport == 5060 or dport == 5060:
                    try:
                        sip_msg = SipMessage.from_string(data.decode('utf-8'))
                        call_id = sip_msg.headers['call-id']
                        cseq = sip_msg.headers['cseq']
                        if self.trace_sip:
                            sip_f = open('sip_log/' + call_id + '.log', 'a')
                            sip_f.write('%s : ---------- GET MESSAGE ----------\n%s\n' % (dt.datetime.today(), data.decode('utf-8')))
                            sip_f.close()

                        # 이미 존재하는 CALL-ID 인 경우 정보 업데이트
                        if call_id in self.sip_map:
                            # get sip session
                            ss = self.sip_map[call_id]
                            ss.pkt_cnt += 1
                            # INVITE
                            if sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'INVITE':
                                # SDP가 존재하는 경우 SDP 정보 refresh
                                if len(sip_msg.content) > 0:
                                    sdp_msg = SdpMessage.from_string(prepare_msg(sip_msg.content))
                                    host = sdp_msg.session_description_fields["c"].connection_address
                                    port = sdp_msg.media_descriptions[0].media.port
                                    ss.from_addr = f'{host}:{port}'

                                    # send protobuf over zeromq
                                    record_msg = record_pb2.Record()
                                    record_msg.event.type = record_pb2.Event.EventType.PREPARE
                                    record_msg.event.record_key = call_id
                                    record_msg.event.voice_key = ss.from_addr
                                    record_msg.event.from_user = ss.from_user
                                    self.record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
                                    LOGGER.debug('SIP REQUEST INVITE (exist) CALL-ID: [%s], MEDIA ADDR: [%s]' % (call_id, ss.from_addr))

                            # BYE 인 경우 삭제
                            elif sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'BYE':
                                # print('got invite response:', call_id)
                                record_msg = record_pb2.Record()
                                record_msg.event.type = record_pb2.Event.EventType.STOP
                                record_msg.event.record_key = call_id
                                record_msg.event.voice_key = ss.from_addr
                                record_msg.event.from_user = ss.from_user
                                self.record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
                                LOGGER.debug('SIP REQUEST BYE CALL-ID: [%s]' % (call_id))

                                del self.sip_map[call_id]
                                #print(record_msg)

                            elif sip_msg.type == SipMessage.TYPE_RESPONSE and cseq['method'] == 'INVITE':
                                ss.invite_resp_code = sip_msg.status
                                LOGGER.debug('SIP RESPONSE INVITE (%d) CALL-ID: [%s]' % (sip_msg.status, call_id))

                            elif sip_msg.type == SipMessage.TYPE_REQUEST and cseq['method'] == 'ACK':
                                if ss.invite_resp_code == 200:
                                    # print('got invite response:', call_id)
                                    record_msg = record_pb2.Record()
                                    record_msg.event.type = record_pb2.Event.EventType.START
                                    record_msg.event.record_key = call_id
                                    record_msg.event.voice_key = ss.from_addr
                                    record_msg.event.from_user = ss.from_user
                                    self.record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
                                LOGGER.debug('SIP REQUEST ACK (%d) CALL-ID: [%s]' % (ss.invite_resp_code, call_id))
                                #print(record_msg)

                        # CALL-ID 가 없는 경우, INVITE가 처음 온 경우
                        elif sip_msg.type == SipMessage.TYPE_REQUEST and sip_msg.method == 'INVITE':
                            #sip_msg.debug_print()
                            ss = SipSession()
                            ss.call_id = call_id
                            ss.to_uri = sip_msg.headers['to']['uri']
                            ss.from_user = sip_msg.headers['from']['uri']
                            #ss.from_user = sip_msg.headers['from']['name']
                            #print('from:', ss.from_user)
                            ss.pkt_cnt = 1
                            #pn = ss.to_uri.split(':')[1].split('@')[0]
                            ss.from_user = ss.from_user.split(':')[1].split('@')[0]
                            if not ss.from_user in self.extensions:
                                LOGGER.info(f'line number: {ext} is in the extension list')
                                continue;

                            if sip_msg.headers['content-type'] == 'application/sdp' and len(sip_msg.content) > 0:
                                sdp_msg = SdpMessage.from_string(prepare_msg(sip_msg.content))
                                host = sdp_msg.session_description_fields["c"].connection_address
                                port = sdp_msg.media_descriptions[0].media.port
                                ss.from_addr = f'{host}:{port}'

                                record_msg = record_pb2.Record()
                                record_msg.event.type = record_pb2.Event.EventType.PREPARE
                                record_msg.event.record_key = call_id
                                record_msg.event.voice_key = ss.from_addr
                                record_msg.event.from_user = ss.from_user
                                self.record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
                                LOGGER.debug('SIP REQUEST INVITE (new  ) CALL-ID: [%s], MEDIA ADDR: [%s]' % (call_id, ss.from_addr))

                            self.sip_map[call_id] = ss

                    except SipParseError as e:
                        pass
                    except Exception as e:
                        print('Exception :', type(e), e)
                        print(data.decode('utf-8'))
                        pass
                else:
                    #continue
                    worker_index = sport % self.worker_cnt
                    self.rtp_sock[worker_index].send_pyobj(udp_info)
                    #self.handle_rtp(s_addr, d_addr, sport, dport, data, self.record_sock)

        for i in range(self.worker_cnt):
            self.rtp_sock[i].send_pyobj('exit')
        for p in plist:
            p.join()
        elapsed = time.time() - init_start
        LOGGER.info(f'{init_cnt / elapsed} packets per second, total received packet: {init_cnt}')

    def run_rtp(self, index):
        port = self.worker_start_port + index
        ctx = zmq.Context()
        rtp_sock = ctx.socket(zmq.PULL)
        rtp_sock.connect(f"tcp://127.0.0.1:{port}")
        
        record_sock = ctx.socket(zmq.PUSH)
        record_sock.connect(config['common']['record_manager_addr'])
        
        while True:
            data = rtp_sock.recv_pyobj()
            if data == 'exit':
                LOGGER.info(f'RTP Worker {index} stop')
                break
            s_addr, d_addr, sport, dport, packet = data
            try:
                self.handle_rtp(s_addr, d_addr, sport, dport, packet, record_sock)                
            except Exception as e:
                pass
            

    def handle_rtp(self, s_addr, d_addr, sport, dport, packet, record_sock):
        e = struct.unpack(">BBHII",packet[:12])
        
        if (e[0]>>6) != 2:       # check version is 2
            return None
        # TODO: rtcp 체크

        # ignore padding bit atm
        
        hasPadding   = e[0] & 0x20
        hasExtension = e[0] & 0x10
        numCSRCs     = e[0] & 0x0f
        hasMarker    = e[1] & 0x80
        payloadType  = e[1] & 0x7f
        seqnum       = e[2]
        timestamp    = e[3]
        ssrc         = e[4]
        
        i=12
        if numCSRCs:
            csrcs = struct.unpack(">"+str(numCSRCs)+"I", packet[i:i+4*numCSRCs])
            i=i+4*numCSRCs
        else:
            csrcs = []
            
        if hasExtension:
            ehdr, length = struct(">2sH",packet[i:i+4])
            epayload = packet[i+4:i+4+length]
            extension = (ehdr,epayload)
            i=i+4+length
        else:
            extension = None
        
        # now work out how much padding needs stripping, if at all
        end = len(packet)
        if hasPadding:
            amount = ord(packet[-1])
            end = end - amount
            
        payload = packet[i:end]

        # send protobuf
        record_msg = record_pb2.Record()
        record_msg.voice.channel_no = -1
        src = s_addr + ":" + str(sport)
        dst = d_addr + ":" + str(dport)

        record_msg.voice.voice_key_candidates.extend([src, dst])
        #if hasMarker == 128:
        #    LOGGER.debug(f'handle_rtp voice_key_candidates: {record_msg.voice.voice_key_candidates} hasMarker {hasMarker}')
        record_msg.voice.payload_type = 0
        if payloadType == 8:
            #LOGGER.debug(f'handle_rtp payloadType:{payloadType}, PCMA')
            npayload = audioop.alaw2lin(payload, 2)
        elif payloadType == 0:
            #LOGGER.debug(f'handle_rtp payloadType:{payloadType}, PCMU')
            npayload = audioop.ulaw2lin(payload, 2)
        elif payloadType == 13:
            LOGGER.debug(f'handle_rtp payloadType:{payloadType}, CN')
        else:
            npayload = payload
        record_msg.voice.payload = npayload
        buf = record_msg.SerializeToString()
        #return
        record_sock.send(buf, zmq.NOBLOCK)
        #self.record_sock.send(record_msg.SerializeToString(), zmq.NOBLOCK)
        
        # return ( seqnum,
                 # { 'payloadtype' : payloadType,
                   # 'payload'     : payload,
                   # 'timestamp'   : timestamp,
                   # 'ssrc'        : ssrc,
                   # 'extension'   : extension,
                   # 'csrcs'       : csrcs,
                   # 'marker'      : hasMarker,
                 # }
               # )


def make_filter(name):
    def filter(record):
        return record["extra"].get("name") == name
    return filter


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pcap Collector')
    parser.add_argument('-v', '--version', action='store_true', help='print version')
    parser.add_argument('-c', '--cfg', nargs='?',
                        default='pcap_collector.cfg', metavar='CFG',
                        help='set config file')
    args = parser.parse_args()

    config = configparser.ConfigParser(os.environ)
    config.read(args.cfg)

    log_format = '[{time:YYYY-MM-DD HH:mm:ss.SSS}] [{process: >5}] [{level.name:>5}] <level>{message}</level>'
    loguru.logger.configure(
        handlers=[
            dict(sink=sys.stdout, format=log_format, colorize=True),
            # dict(sink=sys.stdout, colorize=True, filter=make_filter('main')),
            # dict(sink='sip.log',
                 # format=log_format,
                 # colorize=False,
                 # filter=make_filter('SIP')
            # ),
            # dict(sink=config['LOG']['directory'] + '/list.log',
            #      format=log_format,
            #      enqueue=True,
            #      level=config['LOG']['level'].upper(),
            #      # serialize=True,
            #      rotation='%s MB' % config['LOG']['max_mbytes'],
            #      retention=int(config['LOG']['backup_count']),
            #      filter=make_filter("SIP")
            #      )
        ],
    )

    collector = PcapCollector(config)
    collector.run()
