#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-14, modification: 0000-00-00"

###########
# imports #
###########
import sys
import time
import json
import signal
import socket
import requests
import traceback
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class RecMapDaemon(object):
    def __init__(self, log):
        self.conf = config.RecMapConfig
        self.logger = log
        self.host_name = socket.gethostname()
        self.db = self.connect_db('ORACLE', config.OracleConfig)

    def set_sig_handler(self):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        if frame:
            pass
        if sig == signal.SIGHUP:
            return
        if sig == signal.SIGTERM or sig == signal.SIGINT:
            if self.db:
                self.db.conn.commit()
            self.logger.info('stopped by interrupt')
            sys.exit(0)

    def connect_db(self, db_type, db_conf):
        db = ''
        flag = True
        while flag:
            try:
                self.logger.info('Try connecting to {0} DB ...'.format(db_type))
                if db_type.upper() == 'ORACLE':
                    db = db_connection.Oracle(db_conf, failover=True, service_name=True)
                    flag = False
                elif db_type.upper() == 'MSSQL':
                    db = db_connection.MSSQL(db_conf)
                    flag = False
                elif db_type.upper() == 'MYSQL':
                    db = db_connection.MYSQL(db_conf)
                    flag = False
                else:
                    raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
                self.logger.info('Success connect to {0} DB'.format(db_type))
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(60)
        return db

    def make_job_list(self, target_date):
        result = util.select_rec_mapping_target(self.db, target_date)
        if not result:
            return list()
        return result

    def run(self):
        try:
            self.logger.info('[START] Record mapping daemon process started')
            self.set_sig_handler()
            while True:
                # Make job list
                try:
                    target_date = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
                    job_list = self.make_job_list(target_date)
                    self.logger.info("Record mapping target count= {0}".format(len(job_list)))
                except Exception:
                    self.logger.error(traceback.format_exc())
                    time.sleep(10)
                    continue
                # Execute mapping process
                for job in job_list:
                    """
                    job(
                        REC_KEY             녹취KEY
                        STT_REC_ID          STT녹취ID(SIP callid)
                        REC_ST_DT           녹시작일자(YYYY-MM-DD)
                    )
                    """
                    try:
                        data = {
                            "cmd": "06", "id": "", "med": "", "def": "",
                            "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
                            "data": [
                                {
                                    "recdate": job['REC_ST_DT'], "callid": job['STT_REC_ID']
                                }
                            ]
                        }
                        self.logger.info('-' * 100)
                        log_str = "--> [TRYING] REC_KEY= {0}, REC_ST_DT= {1}, STT_REC_ID(SIP callid)= {2}".format(
                            job['REC_KEY'], job['REC_ST_DT'], job['STT_REC_ID'])
                        self.logger.info(log_str)
                        result = requests.post(self.conf.host, json=data)
                        json_result = json.loads(result.text)
                        self.logger.info("--> {0}".format(json_result))
                        for item in json_result['data']:
                            rec_file_nm = item['recfilenm'].strip()
                            if not rec_file_nm:
                                util.update_rec_retry_yn(self.db, job['REC_KEY'], job['REC_ST_DT'])
                                self.logger.info("--> [ SKIP ] No data. Update REC_RTY_TRAN_YN")
                                continue
                            util.update_rec_mapping(
                                self.db, job['REC_KEY'], job['REC_ST_DT'], rec_file_nm, self.host_name)
                            self.logger.info("--> [ DONE ] Update REC_FILE_NM= {0}".format(rec_file_nm))
                    except Exception:
                        self.logger.error(traceback.format_exc())
                        log_str = "--> [ FAIL ] REC_KEY= {0}, REC_ST_DT= {1}, STT_REC_ID(SIP callid)= {2}".format(
                            job['REC_KEY'], job['REC_ST_DT'], job['STT_REC_ID'])
                        self.logger.info(log_str)
                        time.sleep(10)
                        continue
                time.sleep(self.conf.pro_interval)
        except KeyboardInterrupt:
            self.logger.info('stopped by interrupt')
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            if self.db:
                self.db.disconnect()
            self.logger.info('[E N D] Record mapping daemon process stopped')
            raise Exception


########
# main #
########
def main():
    """
    This is a program that Record mapping Daemon process
    """
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.RecMapConfig.logger_name,
        log_dir_path=config.RecMapConfig.log_dir_path,
        log_file_name=config.RecMapConfig.log_file_name,
        backup_count=config.RecMapConfig.backup_count,
        log_level=config.RecMapConfig.log_level
    )
    rec_mapping_daemon = RecMapDaemon(log)
    while True:
        try:
            rec_mapping_daemon.run()
        except Exception:
            raise Exception
        finally:
            time.sleep(1)


if __name__ == '__main__':
    main()
