#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-24, modification: 0000-00-00"

###########
# imports #
###########
import sys
import time
import signal
import socket
import datetime
import traceback
import multiprocessing
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class JPDaemon(object):
    def __init__(self):
        self.conf = config.JPDaemonConfig
        # Set logger
        self.logger = logger.get_timed_rotating_logger(
            logger_name=self.conf.logger_name,
            log_dir_path=self.conf.log_dir_path,
            log_file_name=self.conf.log_file_name,
            backup_count=self.conf.backup_count,
            log_level=self.conf.log_level
        )
        self.db = self.connect_db('ORACLE', config.OracleConfig)

    def set_sig_handler(self):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        if frame:
            pass
        if sig == signal.SIGHUP:
            return
        if sig == signal.SIGTERM or sig == signal.SIGINT:
            if self.db:
                self.db.conn.commit()
            self.logger.info('stopped by interrupt')
            sys.exit(0)

    def connect_db(self, db_type, db_conf):
        db = ''
        flag = True
        while flag:
            try:
                self.logger.info('Try connecting to {0} DB ...'.format(db_type))
                if db_type.upper() == 'ORACLE':
                    db = db_connection.Oracle(db_conf, failover=True, service_name=True)
                    flag = False
                elif db_type.upper() == 'MSSQL':
                    db = db_connection.MSSQL(db_conf)
                    flag = False
                elif db_type.upper() == 'MYSQL':
                    db = db_connection.MYSQL(db_conf)
                    flag = False
                else:
                    raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
                self.logger.info('Success connect to {0} DB'.format(db_type))
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(60)
        return db

    def make_job_list(self):
        output_list = list()
        overlap_check_dict = dict()
        result = util.select_jp_target(self.db)
        if not result:
            return list()
        for item in result:
            if item['TL_CSMR_NO'] in overlap_check_dict:
                continue
            output_list.append(item)
            overlap_check_dict[item['TL_CSMR_NO']] = True
        return output_list

    def run(self):
        try:
            self.logger.info('[START] JoinPlan TA daemon process started')
            self.set_sig_handler()
            pid_list = list()
            while True:
                # TA engine reset time (23:50 ~ 23:59)
                now_time = datetime.datetime.now().time()
                open_time = datetime.time(23, 50, 0)
                closed_time = datetime.time(23, 59, 59)
                if open_time < now_time < closed_time:
                    self.logger.info('TA engine reset time (23:50 ~ 00:00)')
                    time.sleep(30)
                    continue
                # Make job list
                try:
                    result_list = self.make_job_list()
                except Exception:
                    self.logger.error(traceback.format_exc())
                    time.sleep(10)
                    continue
                # Check child process
                for pid in pid_list[:]:
                    if not pid.is_alive():
                        pid_list.remove(pid)
                # Execute JoinPlan process
                for job in result_list[:self.conf.prc_max_limit]:
                    try:
                        if len(pid_list) >= self.conf.prc_max_limit:
                            self.logger.info('Processing Count is MAX ...')
                            time.sleep(10)
                            break
                        """
                        job(
                            TL_CSMR_NO     고객관리번호(T)
                            PROD_CD        상품코드
                            PROD_NM        상품명
                            REC_KEY        녹취KEY
                            REC_ID         녹취ID
                            REC_FILE_NM    녹취파일명
                            REC_ST_DT      녹취시작일자
                            JP_REQ_TM      가입설계요청일자
                            BROF_CD        지점코드
                            EMP_NO         사원번호
                            SAVE_DTTM      저장일자
                            SAVE_TIME      저장시간
                            PROD_CAT       보종구분
                            PLN_CD         플랜코드
                        )
                        """
                        p = multiprocessing.Process(
                            target=do_task,
                            args=(
                                job,
                            )
                        )
                        p.daemon = None
                        pid_list.append(p)
                        p.start()
                        # Update JoinPlan status
                        util.update_jp_status(
                            db=self.db,
                            host_nm=socket.gethostname(),
                            prog_stat_cd='31', # JP대기
                            prog_stat_dtl_cd=None,
                            updated_tm=datetime.datetime.fromtimestamp(time.time()),
                            tl_csmr_no=job['TL_CSMR_NO'],
                            rec_id=job['REC_ID'],
                            rec_file_nm=job['REC_FILE_NM']
                        )
                        log_str_list = list()
                        log_str_list.append("Execute [TL_CSMR_NO(고객관리번호(T))= {0},".format(job['TL_CSMR_NO']))
                        log_str_list.append("JP_REQ_TM(가입설계요청일자)= {0},".format(job['JP_REQ_TM']))
                        log_str_list.append("REC_ST_DT(녹취시작일자)= {0},".format(job['REC_ST_DT']))
                        log_str_list.append("REC_ID(녹취ID)= {0},".format(job['REC_ID']))
                        log_str_list.append("REC_FILE_NM(녹취파일명)= {0},".format(job['REC_FILE_NM']))
                        log_str_list.append("PROD_CAT(보종구분)= {0},".format(job['PROD_CAT']))
                        log_str_list.append("PLN_CD(플랜코드)= {0}]".format(job['PLN_CD']))
                        self.logger.info(' '.join(log_str_list))
                        time.sleep(self.conf.prc_interval)
                    except Exception:
                        self.logger.error(traceback.format_exc())
                        log_str_list = list()
                        log_str_list.append("Execute [TL_CSMR_NO(고객관리번호(T))= {0},".format(job['TL_CSMR_NO']))
                        log_str_list.append("JP_REQ_TM(가입설계요청일자)= {0},".format(job['JP_REQ_TM']))
                        log_str_list.append("REC_ST_DT(녹취시작일자)= {0},".format(job['REC_ST_DT']))
                        log_str_list.append("REC_ID(녹취ID)= {0},".format(job['REC_ID']))
                        log_str_list.append("REC_FILE_NM(녹취파일명)= {0},".format(job['REC_FILE_NM']))
                        log_str_list.append("PROD_CAT(보종구분)= {0},".format(job['PROD_CAT']))
                        log_str_list.append("PLN_CD(플랜코드)= {0}]".format(job['PLN_CD']))
                        self.logger.info(' '.join(log_str_list))
                        continue
                self.db.conn.commit()
                time.sleep(5)
        except KeyboardInterrupt:
            self.logger.info('stopped by interrupt')
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            if self.db:
                self.db.disconnect()
            self.logger.info('[E N D] JoinPlan TA daemon process stopped')


#######
# def #
#######
def do_task(job):
    """
    Process execute jp_process.py
    @param      job:        Job
    """
    import jp_process
    reload(jp_process)
    jp_process.main(job)


def main():
    """
    This is a program that JoinPlan TA Daemon process
    """
    try:
        jp_daemon = JPDaemon()
        jp_daemon.run()
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    main()
