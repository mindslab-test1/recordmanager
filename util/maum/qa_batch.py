#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-24, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import socket
import argparse
import traceback
import collections
from cfg import config
from lib import db_connection, libpcpython, logger, util
from datetime import datetime, timedelta


###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#############
# constants #
#############
PETRA_SID = str()
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:        Logger Object
    """
    global PETRA_SID
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    PETRA_SID = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))


def pro_stt00003(log, db, host_nm, json_data):
    """
    Process STT00003 청약정보전달
    @param     log:            Logger Object
    @param     db:             DB object
    @param     host_nm:        Host name
    @param     json_data:      Json data
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    log.info("[STT00003 청약 --> NSPL_NO(계약번호)= {0}, TMS_INFO(회차정보)= {1}]".format(
        json_data['body']['NSPL_NO'].strip(),
        json_data['body']['TMS_INFO'].strip()
    ))
    flag = False
    err_msg = ""
    tms_file_cnt = 0
    rec_overlap_check = dict()
    json_body = json_data['body']
    for item in json_body['REC_INFO']:
        # 1: 'C', 2: 'R', 3: 'S', 4: 'E'
        rec_file_nm = item['REC_FILE_NM'].strip()
        if not rec_file_nm:
            continue
        rec_biz_cd = item['REC_BIZ_CD'].strip()
        if rec_file_nm in rec_overlap_check:
            if rec_overlap_check[rec_file_nm][0] == 'C':
                continue
            elif rec_overlap_check[rec_file_nm][0] == 'R':
                if rec_biz_cd in ('C', ):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
            elif rec_overlap_check[rec_file_nm][0] == 'S':
                if rec_biz_cd in ('C', 'R'):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
            else:
                if rec_biz_cd in ('C', 'R', 'S'):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
        else:
            rec_overlap_check[rec_file_nm] = [rec_biz_cd, True]
    for _ in range(0, 3):
        try:
            nspl_no = json_body['NSPL_NO'].strip()
            tms_info = json_body['TMS_INFO'].strip()
            prod_cd = json_body['PROD_CD'].strip()
            prod_nm = json_body['PROD_NM'].strip()
            ip_csmr_no = json_body['IP_CSMR_NO'].strip()
            tl_csmr_no = json_body['TL_CSMR_NO'].strip()
            csmr_nm = json_body['CSMR_NM'].strip()
            pnm = json_body['PNM'].strip()
            brof_cd = json_body['BROF_CD'].strip()
            emp_no = json_body['SAES_ENMO'].strip()
            emp_nm = json_body['SAES_EMNM'].strip()
            contract_dt = json_body['CONTRACT_DT'].strip()
            rec_info = json_body['REC_INFO']
            g_home_tno = json_body.get('G_HOME_TNO')
            g_offc_tno = json_body.get('G_OFFC_TNO')
            g_move_tno = json_body.get('G_MOVE_TNO')
            p_home_tno = json_body.get('P_HOME_TNO')
            p_offc_tno = json_body.get('P_OFFC_TNO')
            p_move_tno = json_body.get('P_MOVE_TNO')
            brof_nm = util.select_brof_nm(db, brof_cd)
            if not brof_nm:
                brof_nm = None
            org_info = util.select_org_info(db, brof_cd)
            user_info = util.select_user_info(db, emp_no)
            cal_info_list = list()
            cal_rel_bind_list = list()
            for item in rec_info:
                rec_file_nm = item['REC_FILE_NM'].strip()
                if not rec_file_nm:
                    continue
                csmr_tlno = item['CSMR_TLNO'].strip() if item['CSMR_TLNO'].strip() else ''
                if csmr_tlno == g_home_tno:
                    tl_cd = '11'
                elif csmr_tlno == g_offc_tno:
                    tl_cd = '12'
                elif csmr_tlno == g_move_tno:
                    tl_cd = '13'
                elif csmr_tlno == p_home_tno:
                    tl_cd = '21'
                elif csmr_tlno == p_offc_tno:
                    tl_cd = '22'
                elif csmr_tlno == p_move_tno:
                    tl_cd = '23'
                else:
                    tl_cd = item['TL_CD'].strip() if item['TL_CD'].strip() else ''
                rec_biz_cd =  item['REC_BIZ_CD'].strip()
                rec_st_dt = item['REC_ST_DT'].replace('/', '').replace('-', '').strip()
                # QA_CTR_CAL_REL_TB(계약콜관계테이블)
                exists_yn = util.select_qa_cal_rel_existed(db, nspl_no, tms_info)
                if exists_yn:
                    util.delete_qa_cal_rel(db, nspl_no, tms_info)
                # NSPL_NO(계약번호) 기준 다른 회차에 동일한 녹취 파일 존재 확인
                rec_exists_yn = util.select_qa_cal_rel_rec_existed(db, nspl_no, brof_cd, rec_file_nm)
                if rec_exists_yn:
                    continue
                # 같은 회차에 중복된 녹취 파일 제거
                if rec_overlap_check[rec_file_nm][0] != rec_biz_cd:
                    continue
                if not rec_overlap_check[rec_file_nm][1]:
                    continue
                rec_overlap_check[rec_file_nm][1] = False
                tms_file_cnt += 1
                cal_rel_bind = (
                    nspl_no,
                    tms_info,
                    brof_cd,
                    rec_file_nm,
                    rec_biz_cd,
                    host_nm,
                    datetime.fromtimestamp(time.time()),
                    host_nm,
                    datetime.fromtimestamp(time.time())
                )
                cal_rel_bind_list.append(cal_rel_bind)
                results = util.select_qa_cal_existed(db, brof_cd, rec_file_nm)
                if results:
                    util.update_qa_cal_info(db, brof_cd, rec_file_nm, rec_biz_cd, tl_cd)
                    for result in results:
                        if result['PROG_STAT_CD'] == '14':
                            cal_info_bind = (
                                "{0}!@#${1}".format(brof_cd, rec_file_nm),
                                brof_cd,
                                rec_file_nm,
                                rec_biz_cd,
                                '10',
                                "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                                os.path.splitext(rec_file_nm)[1][1:],
                                'N',
                                1,
                                tl_csmr_no,
                                csmr_nm,
                                org_info.get('SAES_CD'),
                                org_info.get('SAES_NM'),
                                org_info.get('HQT_CD'),
                                org_info.get('HQT_NM'),
                                brof_cd,
                                brof_nm,
                                user_info.get('DET_CD'),
                                org_info.get('CHN_CD'),
                                org_info.get('CHN_NM'),
                                emp_no,
                                emp_nm,
                                user_info.get('ADMIN_TLNO'),
                                csmr_tlno,
                                tl_cd,
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                brof_cd,
                                rec_file_nm
                            )
                            cal_info_list.append(cal_info_bind)
                            break
                else:
                    cal_info_bind = (
                        "{0}!@#${1}".format(brof_cd, rec_file_nm),
                        brof_cd,
                        rec_file_nm,
                        rec_biz_cd,
                        '10',
                        "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                        os.path.splitext(rec_file_nm)[1][1:],
                        'N',
                        1,
                        tl_csmr_no,
                        csmr_nm,
                        org_info.get('SAES_CD'),
                        org_info.get('SAES_NM'),
                        org_info.get('HQT_CD'),
                        org_info.get('HQT_NM'),
                        brof_cd,
                        brof_nm,
                        user_info.get('DET_CD'),
                        org_info.get('CHN_CD'),
                        org_info.get('CHN_NM'),
                        emp_no,
                        emp_nm,
                        user_info.get('ADMIN_TLNO'),
                        csmr_tlno,
                        tl_cd,
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        brof_cd,
                        rec_file_nm
                    )
                    cal_info_list.append(cal_info_bind)
            # QA_CAL_INFO_TB(콜정보테이블)
            if cal_info_list:
                util.insert_qa_cal_info(db, cal_info_list)
            # QA_CTR_CAL_REL_TB(계약콜관계테이블)
            if cal_rel_bind_list:
                util.insert_qa_cal_rel(db, cal_rel_bind_list)
            # QA_CTR_LIST_TB(계약리스트테이블)
            ctr_list_bind = (
                nspl_no,
                tms_info,
                tms_file_cnt,
                prod_cd,
                prod_nm,
                ip_csmr_no,
                tl_csmr_no,
                csmr_nm,
                pnm,
                brof_cd,
                brof_nm,
                emp_no,
                emp_nm,
                contract_dt,
                1,
                datetime.fromtimestamp(time.time()),
                '20',
                host_nm,
                datetime.fromtimestamp(time.time()),
                host_nm,
                datetime.fromtimestamp(time.time())
            )
            exists_yn = util.select_qa_target_existed(db, nspl_no, tms_info)
            if exists_yn:
                util.delete_qa_target(db, nspl_no, tms_info)
            util.insert_qa_target(db, ctr_list_bind)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            time.sleep(0.1)
    if not flag:
        output = {"CODE": -1, "MSG": err_msg}
        json_data['body'].update(output)
        log.error("[FAIL][STT00003] NSPL_NO(계약번호)= {0}, TMS_INFO(회차정보)= {1}".format(
            json_body.get('NSPL_NO').strip(), json_body.get('TMS_INFO').strip()))
        return json_data
    output = {"CODE": 0, "MSG": "Success, NSPL_NO(계약번호)={0}, TMS_INFO(회차정보)={1}".format(
        json_body.get('NSPL_NO').strip(), json_body.get('TMS_INFO').strip())}
    json_data['body'].update(output)
    return json_data


def main(args):
    """
    This program that
    @param      args:    Arguments
    """
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.QABatchConfig.logger_name,
        log_dir_path=config.QABatchConfig.log_dir_path,
        log_file_name=config.QABatchConfig.log_file_name,
        backup_count=config.QABatchConfig.backup_count,
        log_level=config.QABatchConfig.log_level
    )
    log.info("[START] Execute STT00003 Batch")
    if args.wait > 0:
        log.info("Waiting ... ({0}/s)".format(args.wait))
        time.sleep(args.wait)
    # Connect DB and Load Petra cipher API
    try:
        db_obj = connect_db(log, 'ORACLE', config.OracleConfig)
        load_petra_api(log)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    try:
        if not args.target_file_path:
            args.target_file_path = config.QABatchConfig.target_file_path
        if not os.path.exists(args.target_file_path):
            log.error("Target file is not exists [{0}]".format(args.target_file_path))
            sys.exit(1)
        target_file = open(args.target_file_path)
        total_dict = dict()
        for line in target_file:
            try:
                line_list = line.split('|')
                nspl_no = line_list[0]         # 계약번호
                tms_info = line_list[1]        # 보안회차정보
                csmr_tlno = line_list[21]
                g_home_tno = line_list[12]
                g_offc_tno = line_list[13]
                g_move_tno = line_list[14]
                p_home_tno = line_list[15]
                p_offc_tno = line_list[16]
                p_move_tno = line_list[17]
                if csmr_tlno == g_home_tno:
                    tl_cd = '11'
                elif csmr_tlno == g_offc_tno:
                    tl_cd = '12'
                elif csmr_tlno == g_move_tno:
                    tl_cd = '13'
                elif csmr_tlno == p_home_tno:
                    tl_cd = '21'
                elif csmr_tlno == p_offc_tno:
                    tl_cd = '22'
                elif csmr_tlno == p_move_tno:
                    tl_cd = '23'
                else:
                    tl_cd = line_list[22]
                if len(line_list[18]) < 1:
                    log.error('REC_FILE_NM length is Zero')
                    log.error('line error -> {0}'.format(line))
                    continue
                key = '{0}_{1}'.format(nspl_no, tms_info)
                if key not in total_dict:
                    total_dict[key] = {
                        'body': {
                            'NSPL_NO': nspl_no,             # 계약번호
                            'TMS_INFO': tms_info,           # 보안회차정보
                            'PROD_CD': line_list[2],        # 대표상품코드
                            'PROD_NM': line_list[3],        # 상품명
                            'IP_CSMR_NO': line_list[4],     # 고객관리번호(I)
                            'TL_CSMR_NO': line_list[5],     # 고객관리번호(T)
                            'CSMR_NM': line_list[6],        # 고객성명
                            'PNM': line_list[7],            # 계약자명
                            'BROF_CD': line_list[8],        # 지점코드
                            'SAES_ENMO': line_list[9],      # 모집사원번호
                            'SAES_EMNM': line_list[10],     # 상담원명
                            'CONTRACT_DT': line_list[11],   # 청약일자
                            'G_HOME_TNO': line_list[12],    # 계약자(자택) 전화번호
                            'G_OFFC_TNO': line_list[13],    # 계약자(직장) 전화번호
                            'G_MOVE_TNO': line_list[14],    # 계약자(HP) 전화번호
                            'P_HOME_TNO': line_list[15],    # 피보험자(자택) 전화번호
                            'P_OFFC_TNO': line_list[16],    # 피보험자(직장) 전화번호
                            'P_MOVE_TNO': line_list[17],    # 피보험자(HP) 전화번호
                            'REC_INFO': list()
                        }
                    }
                total_dict[key]['body']['REC_INFO'].append({
                    'REC_FILE_NM': line_list[18],   # 녹취파일명
                    'REC_BIZ_CD': line_list[19],    # 녹취업무코드
                    'REC_ST_DT': line_list[20],     # 녹취시작일자
                    'CSMR_TLNO': csmr_tlno,         # 고객전화번호(암호화)
                    'TL_CD': tl_cd                  # 전화번호
                })
            except Exception:
                log.error('line error -> {0}'.format(line))
                continue
        for key, json_data in collections.OrderedDict(sorted(total_dict.items())).items():
            nspl_no = json_data['body']['NSPL_NO']
            tms_info = json_data['body']['TMS_INFO']
            exists_yn = util.select_qa_target_existed(db_obj, nspl_no, tms_info)
            if exists_yn:
                log.info('already [nspl_no: {0}, tms_info: {1}]'.format(nspl_no, tms_info))
                continue
            pro_stt00003(log, db_obj, socket.gethostname(), json_data)
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', action='store', dest='target_file_path', required=False, type=str,
                        default=str(),
                        help='Input target file path')
    parser.add_argument('-w', action='store', dest='wait', required=False, type=int,
                        default=0,
                        help='Waiting time to run [default= 0]')
    arguments = parser.parse_args()
    main(arguments)
