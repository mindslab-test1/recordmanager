#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-30, modification: 0000-00-00"

###########
# imports #
###########
import ssl
import json
import time
import socket
import websocket
import traceback
import tornado.web
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.websocket
import tornado.httpserver
from datetime import datetime, timedelta
from tornado.options import define, options
from cfg import config
from lib import logger, util, py3_db_connection

#############
# constants #
#############
define("port", default=int(config.WsSvrConfig.port), help="run on the given port", type=int)
DB = py3_db_connection.Oracle(config.OracleConfig, failover=True, service_name=True)
LOGGER = logger.get_timed_rotating_logger(
    logger_name=config.WsSvrConfig.logger_name,
    log_dir_path=config.WsSvrConfig.log_dir_path,
    log_file_name=config.WsSvrConfig.log_file_name,
    backup_count=config.WsSvrConfig.backup_count,
    log_level=config.WsSvrConfig.log_level
)


#########
# class #
#########
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/callsocket", CallSocketHandler)]
        settings = dict(debug=True)
        super(Application, self).__init__(handlers, **settings)


class CallSocketHandler(tornado.websocket.WebSocketHandler):
    # Key: agent, value: socketHandler
    waiters = set()
    sessions = dict()       # STT 이벤트 Subscribers
    qa_sessions = dict()    # QA 이벤트 Subscribers
    agents = dict()
    qa_agents = dict()
    call_event_subscribers = set()
    def check_origin(self, origin):
        return True

    def open(self):
        self.agent = None
        LOGGER.info("Client connected")
        CallSocketHandler.waiters.add(self)

    def on_close(self):
        LOGGER.info("on_close")
        if self.agent and self.agent in CallSocketHandler.sessions:
            CallSocketHandler.sessions[self.agent].remove(self)
            LOGGER.info("unsubscribe {0}".format(self.agent))
        if self.agent and self.agent in CallSocketHandler.qa_sessions:
            CallSocketHandler.qa_sessions[self.agent].remove(self)
            LOGGER.info("QA unsubscribe {0}".format(self.agent))
        if self in CallSocketHandler.call_event_subscribers:
            CallSocketHandler.call_event_subscribers.remove(self)
        CallSocketHandler.waiters.remove(self)

    def on_message(self, message):
        try:
            # LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
            data = json.loads(message)
            data = dict((k.lower(), v) for k, v in data.items())
            event_type = data['eventtype']
            if event_type == 'STT':
                CallSocketHandler.process_stt(self, message, data)
            elif event_type == 'CALL':
                LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
                self.process_call_event(message, data)
            elif event_type == 'QA':
                CallSocketHandler.process_qa(self, message, data)
            else:
                LOGGER.error('unknown event type: {}'.format(event_type))
        except KeyError as e:
            LOGGER.error("there is no event type: {}".format(e))
            LOGGER.error("recv message: %s" % message)
        except ValueError as e:
            LOGGER.error("json error: {}".format(e))
            LOGGER.error("recv message: %s" % message)
        except Exception:
            LOGGER.error(traceback.format_exc())
        return

    @staticmethod
    def qa_open_start_event(agent, event):
        results = util.select_real_qa_results(DB, agent)
        output = {
            "EventType": "QA",
            "Event": event,
            "Agent": agent,
            "QaResult": list()
        }
        for item in results:
            output['QaResult'].append(
                {
                    "TL_CSMR_NO": item['TL_CSMR_NO'],
                    "CSMR_NM": item['CSMR_NM'],
                    "CALL_ID": item['REC_KEY'],
                    "CHK_APP_ORD": item['CHK_APP_ORD'],
                    "CHK_ITM_CD": item['CHK_ITM_CD'],
                    "SCRP_MCTG_NM": item['SCRP_MCTG_NM'],
                    "SCRP_SCTG_NM": item['SCRP_SCTG_NM'],
                    "PSN_JUG_CAT": item['PSN_JUG_CAT'],
                    "DTCT_RST": item['DTCT_RST']
                }
            )
        return output

    @classmethod
    def process_qa(cls, handler, message, data):
        agent = data['agent']
        event = data['event']
        if agent not in cls.qa_sessions:
            cls.qa_sessions[agent] = set()
        if event == 'subscribe':
            LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
            if handler not in cls.qa_sessions[agent]:
                cls.qa_sessions[agent].add(handler)
                handler.agent = agent
            else:
                LOGGER.info('Host ({0}) already QA subscribed agent - {1}'.format(
                    str(handler.request.remote_ip), str(agent)))
            if agent in cls.qa_agents:
                output = CallSocketHandler.qa_open_start_event(agent, "open")
                for client in cls.qa_sessions[agent]:
                    try:
                        client.write_message(json.dumps(output, ensure_ascii=False))
                    except Exception:
                        LOGGER.error("Error sending message", exc_info=True)
                    LOGGER.info('send agent list: %s' % json.dumps(output, ensure_ascii=False))
        elif event in ('result', 'start'):
            LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
            if event == 'start':
                output = CallSocketHandler.qa_open_start_event(agent, "start")
                message = json.dumps(output, ensure_ascii=False)
            for client in cls.qa_sessions[agent]:
                try:
                    client.write_message(message)
                except Exception:
                    LOGGER.error("Error sending message", exc_info=True)
                LOGGER.info('send qa result: %s' % message)
            cls.qa_agents[agent] = True

    def process_call_event(self, message, data):
        if data['event'] == 'subscribe':
            if self not in CallSocketHandler.call_event_subscribers:
                CallSocketHandler.call_event_subscribers.add(self)
                LOGGER.info('Host (%s) subscribe Call Event' % self.request.remote_ip)
                CallSocketHandler.agents = {
                    agent: exp_time for agent, exp_time in CallSocketHandler.agents.items() if time.time() - exp_time < 300
                }
                output = {
                    "EventType": "CALL",
                    "Event": "dashboard",
                    "AgentList": list(CallSocketHandler.agents.keys())
                }
                LOGGER.info(json.dumps(output, ensure_ascii=False))
                self.write_message(json.dumps(output, ensure_ascii=False))
        elif data['event'] in ('status', ):
            if data['status'] != 'CALLING':
                if data['status'] == 'DISCONNECTED':
                    if data['agent'] in CallSocketHandler.agents:
                        del CallSocketHandler.agents[data['agent']]
                else:
                    if data['agent'] not in CallSocketHandler.agents:
                        CallSocketHandler.agents[data['agent']] = time.time()
                for subscriber in CallSocketHandler.call_event_subscribers:
                    subscriber.write_message(message)
                    CallSocketHandler.agents = {
                        agent: exp_time for agent, exp_time in CallSocketHandler.agents.items() if time.time() - exp_time < 60
                    }
                    output = {
                        "EventType": "CALL",
                        "Event": "dashboard",
                        "AgentList": list(CallSocketHandler.agents.keys())
                    }
                    LOGGER.info(json.dumps(output, ensure_ascii=False))
                    subscriber.write_message(json.dumps(output, ensure_ascii=False))

    @classmethod
    def process_stt(cls, handler, message, data):
        agent = data['agent']
        event = data['event']
        if agent not in cls.sessions:
            cls.sessions[agent] = set()
        if event == 'subscribe':
            LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
            if handler not in cls.sessions[agent]:
                cls.sessions[agent].add(handler)
                handler.agent = agent
            else:
                LOGGER.info('Host ({0}) already subscribed agent - {1}'.format(
                    str(handler.request.remote_ip), str(agent)))
            if agent in cls.agents:
                results = util.select_real_stt_results(DB, agent)
                output = {
                    "EventType": "STT",
                    "Event": "open",
                    "Agent": agent,
                    "SttResult": list()
                }
                for item in results:
                    output['SttResult'].append(
                        {
                            "TL_CSMR_NO": item['TL_CSMR_NO'],
                            "CSMR_NM": item['CSMR_NM'],
                            "ChannelNo": item['SPK_DIV_CD'],
                            "Start": item['STMT_ST_TM'],
                            "End": item['STMT_ED_TM'],
                            "Text": item['STMT']
                        }
                    )
                for client in cls.sessions[agent]:
                    try:
                        client.write_message(json.dumps(output, ensure_ascii=False))
                    except Exception:
                        LOGGER.error("Error sending message", exc_info=True)
                    LOGGER.info('send agent list: %s' % json.dumps(output, ensure_ascii=False))
        elif event in ('start', 'stop', 'result', ):
            channelno = data['channelno']
            for client in cls.sessions[agent]:
                try:
                    if event == 'start' and channelno == '1':
                        continue
                    client.write_message(message)
                except Exception:
                    LOGGER.error("Error sending message", exc_info=True)
                LOGGER.info('send stt result: %s' % message)
            if event == 'stop':
                output = {
                    "EventType": "QA",
                    "Event": "stop",
                    "Agent": agent
                }
                for client in cls.qa_sessions[agent]:
                    try:
                        client.write_message(json.dumps(output, ensure_ascii=False))
                    except Exception:
                        LOGGER.error("Error sending message", exc_info=True)
                LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
                del cls.sessions[agent]
                if agent in cls.agents:
                    del cls.agents[agent]
                if agent in cls.qa_sessions:
                    del cls.qa_sessions[agent]
                if agent in cls.qa_agents:
                    del cls.qa_agents[agent]
            else:
                if event == 'start':
                    LOGGER.info("GET MESSAGE - %s" % message.replace(' ', ''))
                cls.agents[agent] = time.time()


#######
# def #
#######
def main():
    """
    This program that Websocket Server
    """
    tornado.options.parse_command_line()
    app = Application()
    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_ctx.load_verify_locations(config.WsSvrConfig.chain_file)
    ssl_ctx.load_cert_chain(
        certfile=config.WsSvrConfig.cert_file,
        keyfile=config.WsSvrConfig.key_file,
        password=config.WsSvrConfig.pd
    )
    http_server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_ctx)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
