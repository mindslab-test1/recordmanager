#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-19, modification: 0000-00-00"

###########
# imports #
###########
import sys
import time
import signal
import socket
import datetime
import traceback
import multiprocessing
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class QADaemon(object):
    def __init__(self):
        self.conf = config.QADaemonConfig
        # Set logger
        self.logger = logger.get_timed_rotating_logger(
            logger_name=self.conf.logger_name,
            log_dir_path=self.conf.log_dir_path,
            log_file_name=self.conf.log_file_name,
            backup_count=self.conf.backup_count,
            log_level=self.conf.log_level
        )
        self.db = self.connect_db('ORACLE', config.OracleConfig)

    def set_sig_handler(self):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        if frame:
            pass
        if sig == signal.SIGHUP:
            return
        if sig == signal.SIGTERM or sig == signal.SIGINT:
            if self.db:
                self.db.conn.commit()
            self.logger.info('stopped by interrupt')
            sys.exit(0)

    def connect_db(self, db_type, db_conf):
        db = ''
        flag = True
        while flag:
            try:
                self.logger.info('Try connecting to {0} DB ...'.format(db_type))
                if db_type.upper() == 'ORACLE':
                    db = db_connection.Oracle(db_conf, failover=True, service_name=True)
                    flag = False
                elif db_type.upper() == 'MSSQL':
                    db = db_connection.MSSQL(db_conf)
                    flag = False
                elif db_type.upper() == 'MYSQL':
                    db = db_connection.MYSQL(db_conf)
                    flag = False
                else:
                    raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
                self.logger.info('Success connect to {0} DB'.format(db_type))
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(60)
        return db

    def make_job_list(self):
        result = util.select_qa_target(self.db)
        if not result:
            return list()
        return result

    def run(self):
        try:
            self.logger.info('[START] QA daemon process started')
            self.set_sig_handler()
            pid_list = list()
            while True:
                # TA engine reset time (23:50 ~ 23:59)
                now_time = datetime.datetime.now().time()
                open_time = datetime.time(23, 50, 0)
                closed_time = datetime.time(23, 59, 59)
                if open_time < now_time < closed_time:
                    self.logger.info('TA engine reset time (23:50 ~ 00:00)')
                    time.sleep(30)
                    continue
                # Make job list
                try:
                    result_list = self.make_job_list()
                except Exception:
                    self.logger.error(traceback.format_exc())
                    time.sleep(10)
                    continue
                # Check child process
                for pid in pid_list[:]:
                    if not pid.is_alive():
                        pid_list.remove(pid)
                # Execute QA process
                for job in result_list[:self.conf.prc_max_limit]:
                    try:
                        if len(pid_list) >= self.conf.prc_max_limit:
                            self.logger.info('Processing Count is MAX ...')
                            time.sleep(10)
                            break
                        """
                        job(
                            NSPL_NO        계약번호
                            TMS_INFO       회차정보
                            PROD_CD        상품코드
                            PROD_NM        상품명
                            CONTRACT_DT    청약일자
                            QA_REQ_TM      QA요청일자
                            PROC_PRI       처리우선순위
                            TMS_FILE_CNT   회차별녹취파일수
                            PROD_CAT       보종구분
                            PLN_CD         플랜코드
                        )
                        """
                        p = multiprocessing.Process(
                            target=do_task,
                            args=(
                                job,
                            )
                        )
                        p.daemon = None
                        pid_list.append(p)
                        p.start()
                        # Update QA status
                        util.update_qa_status(
                            db=self.db,
                            host_nm=socket.gethostname(),
                            prog_stat_cd='21', # QA대기
                            prog_stat_dtl_cd=None,
                            updated_tm=datetime.datetime.fromtimestamp(time.time()),
                            nspl_no=job['NSPL_NO'],
                            tms_info=job['TMS_INFO']
                        )
                        log_str_list = list()
                        log_str_list.append("Execute [PROC_PRI(처리우선순위)= {0},".format(job['PROC_PRI']))
                        log_str_list.append("PROD_CAT(보종구분)= {0},".format(job['PROD_CAT']))
                        log_str_list.append("PLN_CD(플랜코드)= {0},".format(job['PLN_CD']))
                        log_str_list.append("QA_REQ_TM(QA요청일자)= {0},".format(job['QA_REQ_TM']))
                        log_str_list.append("CONTRACT_DT(청약일자)= {0},".format(job['CONTRACT_DT']))
                        log_str_list.append("NSPL_NO(계약번호)= {0},".format(job['NSPL_NO']))
                        log_str_list.append("TMS_INFO(회차정보)= {0},".format(job['TMS_INFO']))
                        log_str_list.append("TMS_FILE_CNT(회차별녹취파일수)= {0}]".format(job['TMS_FILE_CNT']))
                        self.logger.info(' '.join(log_str_list))
                        time.sleep(self.conf.prc_interval)
                    except Exception:
                        self.logger.error(traceback.format_exc())
                        log_str_list = list()
                        log_str_list.append("Execute [PROC_PRI(처리우선순위)= {0},".format(job['PROC_PRI']))
                        log_str_list.append("PROD_CAT(보종구분)= {0},".format(job['PROD_CAT']))
                        log_str_list.append("PLN_CD(플랜코드)= {0},".format(job['PLN_CD']))
                        log_str_list.append("QA_REQ_TM(QA요청일자)= {0},".format(job['QA_REQ_TM']))
                        log_str_list.append("CONTRACT_DT(청약일자)= {0},".format(job['CONTRACT_DT']))
                        log_str_list.append("NSPL_NO(계약번호)= {0},".format(job['NSPL_NO']))
                        log_str_list.append("TMS_INFO(회차정보)= {0},".format(job['TMS_INFO']))
                        log_str_list.append("TMS_FILE_CNT(회차별녹취파일수)= {0}]".format(job['TMS_FILE_CNT']))
                        self.logger.info(' '.join(log_str_list))
                        continue
                self.db.conn.commit()
                time.sleep(5)
        except KeyboardInterrupt:
            self.logger.info('stopped by interrupt')
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            if self.db:
                self.db.disconnect()
            self.logger.info('[E N D] QA daemon process stopped')


#######
# def #
#######
def do_task(job):
    """
    Process execute qa_process.py
    @param      job:        Job
    """
    import qa_process
    reload(qa_process)
    qa_process.main(job)


def main():
    """
    This is a program that QA Daemon process
    """
    try:
        qa_daemon = QADaemon()
        qa_daemon.run()
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    main()
