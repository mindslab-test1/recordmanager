#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-14, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import json
import socket
import requests
import traceback
import subprocess
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def sub_process(cmd):
    """
    Execute subprocess
    @param      cmd:        Command
    """
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    return response_out, response_err


def extract_speech_time(seconds):
    """
    Extract speech time
    :param      seconds:    Seconds(/s)
    :return:                HH:MM:SS.FF
    """
    time_list = str(timedelta(seconds=float(seconds))).split(':')
    hh = ('0' + time_list[0])[-2:]
    mm = time_list[1]
    ss = '%.2f' % float(time_list[2])
    ss = ss.split('.')[0]
    ss = ss if len(ss) == 2 else '0' + ss
    return ':'.join([hh, mm, ss])


def rows_to_dict_list(db):
    """
    Make dict Result
    @param      db:     DB object
    @return:            Dictionary Result
    """
    columns = [i[0] for i in db.cursor.description]
    return [dict(zip(columns, row)) for row in db.cursor]


def connect_db(db_type, db_conf):
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            break
        except Exception:
            time.sleep(1)
    if not db:
        raise Exception("Can't connect db")
    return db

########
# main #
########
def main():
    output_file = open('output', 'w')
    db = connect_db('ORACLE', config.OracleConfig)
    query = """
        SELECT
            TL_CSMR_NO
        FROM
            STTOWN.QA_CAL_INFO_TB
        WHERE
            REC_ST_DT >= '2021-03-01'
            AND PROG_STAT_CD = '13'
            AND REC_COMP_CD = '100'
            AND REC_DU_TM > 100
        GROUP BY
            TL_CSMR_NO
    """
    db.cursor.execute(query, )
    results = rows_to_dict_list(db)
    cnt = 0
    rec_cnt = 1
    tmp_rec_nm = ''
    for item in results:
        cnt += 1
        query = """
            SELECT
                T2.TL_CSMR_NO,
                T2.REC_KEY,
                T2.STT_REC_LOC_PATH,
                T2.STT_REC_FILE_NM,
                T1.STMT_ST_TM,
                T1.STMT_ED_TM,
                PLS_DECRYPT_B64(T1.STMT) AS STMT
            FROM 
                STTOWN.QA_STT_RST_TB T1,
                (
                    SELECT
                        TL_CSMR_NO,
                        REC_KEY,
                        STT_REC_LOC_PATH,
                        STT_REC_FILE_NM,
                        REC_DU_TM,
                        REC_ST_DT,
                        REC_ST_TM
                    FROM
                        STTOWN.QA_CAL_INFO_TB
                    WHERE
                        REC_ST_DT >= '2021-03-01'
                        AND PROG_STAT_CD = '13'
                        AND REC_COMP_CD = '100'
                        AND REC_DU_TM > 100
                        AND TL_CSMR_NO = TO_CHAR(:1)
                ) T2
            WHERE 
                T1.REC_KEY = T2.REC_KEY
                AND T1.SPK_DIV_CD = 'C'
            ORDER BY
                T2.TL_CSMR_NO,
                T2.REC_ST_DT,
                T2.REC_ST_TM,
                T1.REC_KEY,
                TO_NUMBER(T1.STMT_NO)
        """
        bind = (
            item['TL_CSMR_NO'],
        )
        if cnt == 4:
            break
        db.cursor.execute(query, bind)
        stt_results = rows_to_dict_list(db)
        for result in stt_results:
            query = """
                SELECT
                    STMT_ST_TM
                FROM 
                    STTOWN.QA_STT_RST_TB
                WHERE 
                    REC_KEY = TO_CHAR(:1)
                    AND STMT_NO = '1'
            """
            bind = (
                result['REC_KEY'],
            )
            db.cursor.execute(query, bind)
            fir_st_tm = db.cursor.fetchone()
            dial_st_hh, dial_st_mm, dial_st_ss = fir_st_tm[0].split(':')
            dial = (int(dial_st_hh) * 60 * 60) + (int(dial_st_mm) * 60) + int(dial_st_ss)
            st_hh, st_mm, st_ss = result['STMT_ST_TM'].split(':')
            ed_hh, ed_mm, ed_ss = result['STMT_ED_TM'].split(':')
            start = (int(st_hh) * 60 * 60) + (int(st_mm) * 60) + int(st_ss)
            end = (int(ed_hh) * 60 * 60) + (int(ed_mm) * 60) + int(ed_ss)
            start = start - dial
            end = end - dial
            stmt_st_tm = extract_speech_time(start)
            stmt_ed_tm = extract_speech_time(end)
            if 3 <= end - start <= 9:
                if len(result['STMT']) > 15:
                    print >> output_file, "{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(
                        result['TL_CSMR_NO'],
                        result['STT_REC_LOC_PATH'],
                        result['STT_REC_FILE_NM'],
                        stmt_st_tm,
                        stmt_ed_tm,
                        result['STMT']
                    )
                    if not tmp_rec_nm:
                        tmp_rec_nm = result['STT_REC_FILE_NM']
                    if tmp_rec_nm != result['STT_REC_FILE_NM']:
                        tmp_rec_nm = result['STT_REC_FILE_NM']
                        rec_cnt = 1
                    src_file_path = os.path.join(result['STT_REC_LOC_PATH'], result['STT_REC_FILE_NM'])
                    if not os.path.exists(src_file_path):
                        print("Can't find record file[{0}]".format(src_file_path))
                        continue
                    wav_file_path = os.path.join(result['STT_REC_LOC_PATH'], result['STT_REC_FILE_NM'].replace('.mp3', '.wav'))
                    cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 2 -b:a 128k {1}'.format(src_file_path, wav_file_path)
                    std_out, std_err = sub_process(cmd)
                    rx_file_name = result['STT_REC_FILE_NM'].replace('.mp3', '.wav')[:-4] + '_rx.wav'
                    tx_file_name = result['STT_REC_FILE_NM'].replace('.mp3', '.wav')[:-4] + '_tx.wav'
                    rx_file_path = os.path.join('/DATA_SHARE/maum/temp', rx_file_name)
                    tx_file_path = os.path.join('/DATA_SHARE/maum/temp', tx_file_name)
                    if not os.path.exists(rx_file_path) or not os.path.exists(tx_file_path):
                        cmd = 'ffmpeg -i {0} -filter_complex "[0:0]pan=1c|c0=c0[left];[0:0]pan=1c|c0=c1[right]" '.format(
                            wav_file_path)
                        # 흥국생명 실시간STT에서 MP3 파일의 left, right 를 반대로 생성
                        cmd += '-map "[left]" {0} -map "[right]" {1}'.format(tx_file_path, rx_file_path)
                        std_out, std_err = sub_process(cmd)
                    remote_file_path = os.path.join('/DATA_SHARE/maum/temp', "{0}_{1}_{2}".format(
                        result['TL_CSMR_NO'], rec_cnt, result['STT_REC_FILE_NM'].replace('.mp3', '.wav')))
                    rec_cnt += 1
                    cmd = "ffmpeg -y -hide_banner -i {0} -ss {1} -to {2} {3}".format(
                        rx_file_path, start, end, remote_file_path)
                    std_out, std_err = sub_process(cmd)
                    print("\t  --> Success Trim record file")
                    os.remove(wav_file_path)
    output_file.close()

if __name__ == '__main__':
    main()
