#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-14, modification: 0000-00-00"

###########
# imports #
###########
import sys
import time
import json
import socket
import requests
import traceback
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def remapping_process(log, db):
    """
    Real STT REC_FILE_NM mapping process
    @param      log:            Logger Object
    @param      db:             DB object
    """
    log.info('[START] Record remapping process started')
    target_date = datetime.strftime(datetime.now() - timedelta(days=3), '%Y-%m-%d')
    job_list = util.select_remapping_target(db, target_date)
    log.info("Record mapping target count= {0}".format(len(job_list)))
    # Execute remapping process
    for job in job_list:
        """
        job(
            REC_KEY             녹취KEY
            STT_REC_ID          STT녹취ID(SIP callid)
            REC_ST_DT           녹시작일자(YYYY-MM-DD)
        )
        """
        data = {
            "cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
            "data": [{"recdate": job['REC_ST_DT'], "callid": job['STT_REC_ID']}]
        }
        log.info('-' * 100)
        log_str = "--> [TRYING] REC_KEY= {0}, REC_ST_DT= {1}, STT_REC_ID(SIP callid)= {2}".format(
            job['REC_KEY'], job['REC_ST_DT'], job['STT_REC_ID'])
        log.info(log_str)
        result = requests.post(config.RecReMapConfig.host, json=data)
        json_result = json.loads(result.text)
        log.info("--> {0}".format(json_result))
        item = json_result['data'][0]
        rec_file_nm = item['recfilenm'].strip()
        if rec_file_nm:
            util.update_rec_mapping(db, job['REC_KEY'], job['REC_ST_DT'], rec_file_nm, socket.gethostname())
            log.info("--> [ DONE ] Update REC_FILE_NM= {0}".format(rec_file_nm))
        else:
            log_str = "--> [ FAIL ] REC_KEY= {0}, REC_ST_DT= {1}, STT_REC_ID(SIP callid)= {2}".format(
                job['REC_KEY'], job['REC_ST_DT'], job['STT_REC_ID'])
            log.info(log_str)
        time.sleep(0.1)


########
# main #
########
def main():
    """
    This is a program that Record mapping Daemon process
    """
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.RecReMapConfig.logger_name,
        log_dir_path=config.RecReMapConfig.log_dir_path,
        log_file_name=config.RecReMapConfig.log_file_name,
        backup_count=config.RecReMapConfig.backup_count,
        log_level=config.RecReMapConfig.log_level
    )
    # Connect DB
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    # Execute remapping
    try:
        remapping_process(log, db)
    except Exception:
        log.error(traceback.format_exc())
        db.conn.commit()
    finally:
        try:
            db.disconnect()
        except Exception:
            pass
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))


if __name__ == '__main__':
    main()
