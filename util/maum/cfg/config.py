class ColQAConfig(object):
    logger_name = 'COL_QA_API'
    log_dir_path = '/LOG/maum/api'
    log_file_name = 'col_qa_api.log'
    backup_count = 8
    log_level = 'info'
    svr_list = {
        'hlldsttap': '18.18.222.44',
    }
    port = 20020


class RecConvConfig(object):
    logger_name = 'REC_CONV_API'
    log_dir_path = '/LOG/maum/api'
    log_file_name = 'rec_conv_api.log'
    backup_count = 8
    log_level = 'info'
    lis_rec_dir_path = '/DATA_SHARE/maum/lis_rec'
    real_rec_dir_path = '/DATA_SHARE/maum/rec'


class RecMapConfig(object):
    logger_name = 'REC_MAP_DAEMON'
    log_dir_path = '/LOG/maum'
    log_file_name = 'rec_mapping_daemon.log'
    backup_count = 8
    log_level = 'info'
    pro_interval = 1
    host = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0003_TEST'


class RecReMapConfig(object):
    logger_name = 'REC_REMAP'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'rec_remapping.log'
    backup_count = 8
    log_level = 'info'
    host = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0003_TEST'


class OracleConfig(object):
    host = '10.18.132.39'
    host_list = ['10.18.132.39']
    user = 'sttapp'
    pd = 'sttown#138'
    port = 3521
    sid = 'HLSTTD'
    service_name = 'HLSTTD'
    reconnect_interval = 10


class HMDAPIConfig(object):
    logger_name = 'common_HMD_API'
    log_dir_path = '/LOG/maum/api'
    log_file_name = 'common_hmd_api.log'
    backup_count = 8
    log_level = 'info'


class SttDaemonConfig(object):
    logger_name = 'STT_DAEMON'
    log_dir_path = '/LOG/maum'
    log_file_name = 'stt_daemon_process.log'
    backup_count = 8
    log_level = 'info'
    top_priority_prc = 5
    prc_max_limit = 20
    prc_interval = 0.1
    biz_opn_hr = 8
    biz_opn_mn = 0
    biz_cls_hr = 19
    biz_cls_mn = 0


class STTConfig(object):
    logger_name = 'STT'
    log_dir_path = '/LOG/maum/stt'
    log_level = 'info'
    cnn_client1_remote = '127.0.0.1:15001'
    cnn_client2_remote = '127.0.0.1:15001'
    nlp_engine = 'nlp3'
    hmd_line_gap = 3
    get_rec_dir_path = '/DATA/maum/rec'
    encrypt = False
    replace = True
    masking = False
    arabic = True
    lis_tmp_dir_path = '/DATA/maum/lis_rec'
    lis_rec_svr_list = ['10.18.131.39']
    lis_rec_dir_path = '/DATA_SHARE/maum/lis_rec'
    tran_rec_svr_list = [
        'http://10.18.131.39:8786/dec_tran_rec',
    ]


class UploadSttDaemonConfig(object):
    logger_name = 'UPLOAD_STT_DAEMON'
    log_dir_path = '/LOG/maum'
    log_file_name = 'upload_stt_daemon_process.log'
    backup_count = 8
    log_level = 'info'
    prc_max_limit = 5
    prc_interval = 0.1
    biz_opn_hr = 8
    biz_opn_mn = 0
    biz_cls_hr = 19
    biz_cls_mn = 0


class UploadSTTConfig(object):
    logger_name = 'STT'
    log_dir_path = '/LOG/maum/upload_stt'
    log_level = 'info'
    cnn_client1_remote = '127.0.0.1:15001'
    cnn_client2_remote = '127.0.0.1:15001'
    get_rec_dir_path = '/DATA/maum/upload_rec'
    encrypt = False
    replace = True
    masking = False
    arabic = True
    tran_rec_svr_list = [
        'http://10.18.131.39:8786/upload_tran_rec',
    ]


class QADaemonConfig(object):
    logger_name = 'QA_DAEMON'
    log_dir_path = '/LOG/maum'
    log_file_name = 'qa_daemon_process.log'
    backup_count = 8
    log_level = 'info'
    prc_max_limit = 10
    prc_interval = 0.1
    biz_op_h = 8
    biz_op_m = 0
    biz_cl_h = 19
    biz_cl_m = 0


class QAConfig(object):
    logger_name = 'QA'
    log_dir_path = '/LOG/maum/ta/qa'
    log_level = 'info'
    nlp_engine = 'nlp3'
    m_cust_an_line = 2
    s_cust_an_line = 2
    hmd_line_gap = 3
    tlife_host = 'http://10.60.101.32:20020/stt'


class JPDaemonConfig(object):
    logger_name = 'JP_DAEMON'
    log_dir_path = '/LOG/maum'
    log_file_name = 'jp_daemon_process.log'
    backup_count = 8
    log_level = 'info'
    prc_max_limit = 10
    prc_interval = 0.1
    biz_op_h = 8
    biz_op_m = 0
    biz_cl_h = 19
    biz_cl_m = 0


class JPConfig(object):
    logger_name = 'JP'
    log_dir_path = '/LOG/maum/ta/jp'
    log_level = 'info'
    nlp_engine = 'nlp3'
    m_cust_an_line = 2
    s_cust_an_line = 2
    hmd_line_gap = 3
    tlife_host = 'http://10.60.101.32:20020/stt'


class CTSConfig(object):
    rstt_url = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0003'
    stt_url = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0004'


class PetraConfig(object):
    conf_file_path = '/APP/maum/cfg/petra_cipher_api.conf'
    enc_cold_id = 1641600 # Two-way : 1641600, One-way : 1641700


class OrgUserConfig(object):
    logger_name = 'BATCH'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'upload_org_user_info.log'
    backup_count = 8
    log_level = 'info'
    org_file_path = '/DATA/BATCH/TM/orgInfo'
    user_file_path = '/DATA/BATCH/TM/userInfo'
    pwd_chang_host = 'https://stt.heungkuklife.co.kr:8443/api/pwdChange'


class BatchConfig(object):
    logger_name = 'BatchScheduler'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'batch_scheduler.log'
    backup_count = 8
    log_level = 'info'
    target = [
#        '* * * * * echo test',
#        {
#            'second': '*/5',        # (int|str) - second (0-59)
#            'minute': '*',          # (int|str) - minute (0-59)
#            'hour': '*',            # (int|str) - hour (0-23)
#            'day': '*',             # (int|str) - day of the (1-31)
#            'month': '*',           # (int|str) - month (1-12)
#            'day_of_week': '*',     # (int|str) - number or name of weekday (0-6 or mon,tue,wed,thu,fri,sat,sun)
#            'command': '' # (str) - command
#        },
#        '0 8 * * * python /APP/maum/upload_org_user_info.py',
#        '0 * * * * python /APP/maum/rec_remapping.py',
#        '0 1 * * * python /APP/maum/delete_file.py',
#        '55 23 * * * python /APP/maum/restart_engine.py',
        '0 2 * * * python /APP/maum/ela_insert.py',
    ]


class AESConfig(object):
    pd = '/APP/maum/cfg/.aes'
    ps_path = '/APP/maum/cfg/.heungkuk'


class WsSvrConfig(object):
    logger_name = 'WSSVR'
    log_dir_path = '/LOG/maum'
    log_file_name = 'call_wsserver.log'
    backup_count = 8
    log_level = 'info'
    port = 13254
    chain_file = '/APP/maum/cfg/RapidsslDigiCert-Newchain.pem'
    cert_file = '/APP/maum/cfg/cert.pem'
    key_file = '/APP/maum/cfg/key.pem'
    pd = 'nwnac73j'
    zmq_host = 'tcp://127.0.0.1:5800'


class PostWsSvrConfig(object):
    logger_name = 'PSTWS'
    log_dir_path = '/LOG/maum'
    log_file_name = 'call_postprocess.log'
    backup_count = 8
    log_level = 'info'
    port = 13255
    ws_addr_list = {
        'hlldsttap': 'wss://sttdev.heungkuklife.co.kr:13254/callsocket',
    }
    chain_file = '/APP/maum/cfg/RapidsslDigiCert-Newchain.pem'
    cert_file = '/APP/maum/cfg/cert.pem'
    key_file = '/APP/maum/cfg/key.pem'
    pd = 'nwnac73j'
    nlp_engine = 'nlp3'
    lis_rec_dir_path = '/DATA_SHARE/maum/lis_rec'


class EventReceiverConfig(object):
    logger_name = 'EVENT'
    log_dir_path = '/srv/maum/logs'
    log_file_name = 'call_event_receiver.log'
    backup_count = 8
    log_level = 'debug'
    ws_addr_list = {
        #'hlldsttap': 'wss://sttdev.heungkuklife.co.kr:13254/callsocket',
    }
    post_ws_addr_list = {
        #'hlldsttap': 'wss://sttdev.heungkuklife.co.kr:13255/postsocket',
    }
    zmq_host = 'tcp://0.0.0.0:5800'


class RecTransDaemonConfig(object):
    logger_name = 'REC_TRAN'
    log_dir_path = '/LOG/maum'
    log_file_name = 'rec_trans.log'
    backup_count = 8
    pro_interval = 10
    log_level = 'info'
    masking = False
    src_dir_path = '/SW/maum/rec'
    rec_svr_list = ['10.18.131.39',]
    rec_dir_path = '/DATA_SHARE/maum/rec'
    lis_rec_dir_path = '/DATA_SHARE/maum/lis_rec'


class DELConfig(object):
    logger_name = 'DELETE'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'delete_file.log'
    backup_count = 8
    log_level = 'info'
    target_directory_list = [
        {'dir_path': '/LOG/maum/stt', 'mtn_period': 365},
        {'dir_path': '/LOG/maum/ta/jp', 'mtn_period': 365},
        {'dir_path': '/LOG/maum/ta/qa', 'mtn_period': 365},
        {'dir_path': '/DATA/maum/rec', 'mtn_period': 365},
        {'dir_path': '/DATA/maum/lis_rec', 'mtn_period': 50},
        {'dir_path': '/DATA_SHARE/maum/rec', 'mtn_period': 365},
        {'dir_path': '/DATA_SHARE/maum/lis_rec', 'mtn_period': 50},
    ]


class RestartConfig(object):
    logger_name = 'RESTART'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'restart_engine.log'
    backup_count = 8
    log_level = 'info'


class ElaConfig(object):
    logger_name = 'ELASTIC'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'elastic_search.log'
    backup_count = 8
    log_level = 'info'
    port = 9200
    mtn_period = 555
    host_list = {
        'hlldsttap': ['hlldsttap', ],
    }
    nlp_engine = 'nlp3'


class QABatchConfig(object):
    logger_name = 'QABatch'
    log_dir_path = '/LOG/maum/batch'
    log_file_name = 'qa_batch.log'
    backup_count = 8
    log_level = 'info'
    target_file_path = '/DATA/BATCH/TM/test'


class SMSConfig(object):
    host = 'http://eaidev.heungkuklife.co.kr:8800/weom/servlet/api.HungkukUMSDispatcher'
