#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-11-12, modification: 0000-00-00"

###########
# imports #
###########
import sys
import subprocess

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

missing_list = list()
required_list = [
    'import grpc',
    'import numpy',
    'import socket',
    'import requests',
    'import pymssql',
    'import pymysql',
    'import cx_Oracle',
    'from Crypto import Random',
    'from Crypto.Cipher import AES',
    'from elasticsearch import helpers',
    'from elasticsearch import Elasticsearch',
    'from flask_cors import CORS',
    'from flask import Flask, request',
    'from flask_restful import Api',
    'from flask_restful import reqparse',
    'from flask_restful import Resource',
    'from apscheduler.schedulers.background import BackgroundScheduler'
]
for item in required_list:
    cmd = "python -c '{0}'".format(item)
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    if response_err.strip():
        missing_list.append(item)
if missing_list:
    print("Can't import list")
    for item in missing_list:
        print('  --> {0}'.format(item))
else:
    print("Import success")
