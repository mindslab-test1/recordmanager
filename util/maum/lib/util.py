#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-03-24, modification: 0000-00-00"

###########
# imports #
###########
import time
import cx_Oracle
import traceback
from datetime import datetime


#######
# def #
#######
def rows_to_dict_list(db):
    """
    Make dict Result
    @param      db:     DB object
    @return:            Dictionary Result
    """
    columns = [i[0] for i in db.cursor.description]
    return [dict(zip(columns, row)) for row in db.cursor]


# col_qa_api.py
def select_org_info(db, brof_cd):
    """
    Select orgInfo from QA_BASE_BROF_TB(지점정보테이블)
    @param      db:                     DB object
    @param      brof_cd:                BROF_CD(지점코드)
    """
    try:
        query = """
            SELECT
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                CHN_CD,
                CHN_NM
            FROM
                STTOWN.QA_BASE_BROF_TB
            WHERE
                BROF_CD = TO_CHAR(:1)
        """
        bind = (
            brof_cd,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return dict()
        if not result:
            return dict()
        return result[0]
    except Exception:
        raise Exception(traceback.format_exc())


def select_user_info(db, user_id):
    """
    Select userInfo from QA_USR_MGT_TB(사용자관리테이블)
    @param      db:                     DB object
    @param      user_id:                USER_ID(사용자아이디)
    """
    try:
        query = """
            SELECT
                DET_CD,
                ADMIN_TLNO
            FROM
                STTOWN.QA_USR_MGT_TB
            WHERE
                USER_ID = TO_CHAR(:1)
        """
        bind = (
            user_id,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return dict()
        if not result:
            return dict()
        return result[0]
    except Exception:
        raise Exception(traceback.format_exc())


def insert_qa_cal_info(db, bind):
    """
    Insert QA target QA_CAL_INFO_TB(콜정보테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CAL_INFO_TB(
                REC_KEY,
                REC_ID,
                REC_FILE_NM,
                REC_BIZ_CD,
                PROG_STAT_CD,
                REC_ST_DT,
                REC_FILE_EXT,
                STEREO_SPLIT_YN,
                PROC_PRI,
                TL_CSMR_NO,
                CSMR_NM,
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                BROF_CD,
                BROF_NM,
                DET_CD,
                CHN_CD,
                CHN_NM,
                EMP_NO,
                EMP_NM,
                MANAGER_HP,
                CSMR_TLNO,
                TL_CD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            SELECT
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9, :10, 
                :11, :12, :13, :14, :15, 
                :16, :17, :18, :19, :20,
                :21, :22, :23, :24, :25, 
                :26, :27, :28, :29
            FROM DUAL T1
            WHERE NOT EXISTS(
                SELECT
                    0
                FROM
                    STTOWN.QA_CAL_INFO_TB
                WHERE
                    REC_ID = TO_CHAR(:30)
                    AND REC_FILE_NM = TO_CHAR(:31)
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def insert_jp_cal_info(db, bind):
    """
    Insert QA target QA_CAL_INFO_TB(콜정보테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CAL_INFO_TB(
                REC_KEY,
                REC_ID,
                REC_FILE_NM,
                REC_BIZ_CD,
                PROG_STAT_CD,
                REC_ST_DT,
                REC_FILE_EXT,
                STEREO_SPLIT_YN,
                PROC_PRI,
                TL_CSMR_NO,
                CSMR_NM,
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                BROF_CD,
                BROF_NM,
                DET_CD,
                CHN_CD,
                CHN_NM,
                EMP_NO,
                EMP_NM,
                MANAGER_HP,
                CSMR_TLNO,
                TL_CD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            SELECT
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9, :10, 
                :11, :12, :13, :14, :15, 
                :16, :17, :18, :19, :20,
                :21, :22, :23, :24, :25, 
                :26, :27, :28, :29
            FROM DUAL T1
            WHERE NOT EXISTS(
                SELECT
                    0
                FROM
                    STTOWN.QA_CAL_INFO_TB
                WHERE
                    REC_ID = TO_CHAR(:30)
                    AND REC_FILE_NM = TO_CHAR(:31)
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_qa_cal_rel_rec_existed(db, nspl_no, rec_id, rec_file_nm):
    """
    Check QA target existed from QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      rec_id:                 REC_ID(녹취ID)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_CTR_CAL_REL_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND REC_ID = TO_CHAR(:2)
                AND REC_FILE_NM = TO_CHAR(:3)
        """
        bind = (
            nspl_no,
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_cal_rel_existed(db, nspl_no, tms_info):
    """
    Check QA target existed from QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_CTR_CAL_REL_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_CHAR(:2)
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_cal_rel(db, nspl_no, tms_info):
    """
    Delete QA from QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CTR_CAL_REL_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_CHAR(:2)
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_cal_rel(db, bind_list):
    """
    Insert QA target QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CTR_CAL_REL_TB(
                NSPL_NO,
                TMS_INFO,
                REC_ID,
                REC_FILE_NM,
                REC_BIZ_CD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES(
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_brof_nm(db, brof_cd):
    """
    Check QA target existed from QA_BASE_BROF_TB(지점정보테이블)
    @param      db:                     DB object
    @param      brof_cd:                BROF_CD(지점코드)
    """
    try:
        query = """
            SELECT
                BROF_NM
            FROM
                STTOWN.QA_BASE_BROF_TB
            WHERE
                BROF_CD = TO_CHAR(:1)
        """
        bind = (
            brof_cd,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = db.cursor.fetchone()
        if result is bool:
            return None
        if not result:
            return None
        return result[0]
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_target_existed(db, nspl_no, tms_info):
    """
    Check QA target existed from QA_CTR_LIST_TB(계약리스트테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_CTR_LIST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_CHAR(:2)
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_target(db, nspl_no, tms_info):
    """
    Delete QA from QA_CTR_LIST_TB(계약리스트테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CTR_LIST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_CHAR(:2)
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_target(db, bind):
    """
    Insert QA target QA_CTR_LIST_TB(계약리스트테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CTR_LIST_TB(
                NSPL_NO,
                TMS_INFO,
                TMS_FILE_CNT,
                PROD_CD,
                PROD_NM,
                IP_CSMR_NO,
                TL_CSMR_NO,
                CSMR_NM,
                PNM,
                BROF_CD,
                BROF_NM,
                EMP_NO,
                EMP_NM,
                CONTRACT_DT,
                PROC_PRI,
                QA_REQ_TM,
                PROG_STAT_CD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES(
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9, :10,
                :11, :12, :13, :14, :15, 
                :16, :17, :18, :19, :20,
                :21
            )
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_jp_target_existed(db, tl_csmr_no, rec_id, rec_file_nm):
    """
    Check JoinPlan QA target existed from QA_JOIN_PLN_LIST_TB(가입설계리스트)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(고객관리번호(T))
    @param      rec_id:                 REC_ID(녹취ID)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            SELECT
                TL_CSMR_NO
            FROM
                STTOWN.QA_JOIN_PLN_LIST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_ID = TO_CHAR(:2)
                AND REC_FILE_NM = TO_CHAR(:3)
        """
        bind = (
            tl_csmr_no,
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_jp_target(db, tl_csmr_no, rec_id, rec_file_nm):
    """
    Delete JoinPlan from  QA_JOIN_PLN_LIST_TB(가입설계리스트)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(고객관리번호(T))
    @param      rec_id:                 REC_ID(녹취ID)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_JOIN_PLN_LIST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_ID = TO_CHAR(:2)
                AND REC_FILE_NM = TO_CHAR(:3)
        """
        bind = (
            tl_csmr_no,
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_jp_target(db, bind):
    """
    Insert JoinPlan target QA_JOIN_PLN_LIST_TB(가입설계리스트)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_JOIN_PLN_LIST_TB(
                TL_CSMR_NO,
                REC_ID,
                REC_FILE_NM,
                REC_ST_DT,
                PROD_CD,
                PROD_NM,
                CSMR_NM,
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                BROF_CD,
                BROF_NM,
                DET_CD,
                CHN_CD,
                CHN_NM,
                EMP_NO,
                EMP_NM,
                REC_BIZ_CD,
                SAVE_DTTM,
                SAVE_TIME,
                PROG_STAT_CD,
                JP_REQ_TM,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES(
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9, :10,
                :11, :12, :13, :14, :15,
                :16, :17, :18, :19, :20,
                :21, :22, :23, :24, :25, 
                :26, :27
            )
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_emp_cal_info(db, emp_no, srch_st_dt, srch_ed_dt):
    """
    Select Employ record monitoring
    @param      db:                     DB object
    @param      emp_no:                 EMP_NO
    @param      srch_st_dt:             Search start date
    @param      srch_ed_dt:             Search end date
    """
    try:
        query = """
            SELECT 
                T11.REC_CNT
                , TO_CHAR(TRUNC(T11.REC_DU_TM/3600), 'FM9900') || ':' || 
                  TO_CHAR(TRUNC(MOD(T11.REC_DU_TM, 3600)/60), 'FM00') || ':' || 
                  TO_CHAR(MOD(T11.REC_DU_TM,60),'FM00') AS REC_DU_TM
            FROM
                (
                    SELECT 
                        COUNT(*) AS REC_CNT
                        , SUM(REC_DU_TM) AS REC_DU_TM 
                    FROM 
                        STTOWN.QA_CAL_INFO_TB 
                    WHERE 
                        REC_ST_DT BETWEEN :1 AND :2  
                        AND EMP_NO = :3
                ) T11
        """
        bind = (
            srch_st_dt,
            srch_ed_dt,
            emp_no
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_emp_fwd_info(db, emp_no, srch_st_dt, srch_ed_dt):
    """
    Select Employ record monitoring
    @param      db:                     DB object
    @param      emp_no:                 EMP_NO
    @param      srch_st_dt:             Search start date
    @param      srch_ed_dt:             Search end date
    """
    try:
        query = """
            SELECT 
                T12.FD_RST_CNT
                , T14.SCRP
                , T13.DTCT_DTC_CNT
            FROM
                (
                    SELECT 
                        COUNT(*) AS FD_RST_CNT 
                    FROM 
                        STTOWN.QA_CAL_DTCT_RST_TB T1, 
                        (
                            SELECT 
                                REC_KEY 
                                , REC_ST_DT 
                                , REC_DU_TM 
                            FROM 
                                QA_CAL_INFO_TB 
                            WHERE 
                                REC_ST_DT BETWEEN :1 AND :2  
                                AND EMP_NO = :3
                        ) T2 
                    WHERE 
                        T1.REC_KEY = T2.REC_KEY 
                        AND T1.CUSL_CD = 'FD0002'
                ) T12,
                (
                    SELECT
                        T1.DTCT_DTC_NO
                        , COUNT(*) AS DTCT_DTC_CNT
                    FROM
                        STTOWN.QA_CAL_DTCT_RST_TB T1,
                        (
                            SELECT 
                                REC_KEY
                                , REC_ST_DT
                                , REC_DU_TM
                            FROM 
                                STTOWN.QA_CAL_INFO_TB
                            WHERE 
                                REC_ST_DT BETWEEN :4  AND :5 
                                AND EMP_NO = :6
                        ) T2
                    WHERE
                        T1.REC_KEY = T2.REC_KEY
                        AND T1.CUSL_CD = 'FD0002'
                    GROUP BY 
                        T1.DTCT_DTC_NO
                    ORDER BY 
                        T1.DTCT_DTC_NO
                ) T13
            LEFT OUTER JOIN 
                STTOWN.QA_STMT_TB T14
            ON 
                T13.DTCT_DTC_NO = T14.DTCT_DTC_NO
        """
        bind = (
            srch_st_dt,
            srch_ed_dt,
            emp_no,
            srch_st_dt,
            srch_ed_dt,
            emp_no
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


# rec_mapping_daemon.py
def select_rec_mapping_target(db, target_date):
    """
    Select Record mapping target from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                 DB object
    @param      target_date:        Target date(YYYY-MM-DD)
    @return:                        STT target list
    """
    try:
        query = """
            SELECT
                REC_KEY,
                STT_REC_ID,
                REC_ST_DT
            FROM
                STTOWN.QA_CAL_INFO_TB
            WHERE 
                (CHN_CD = '01' OR CHN_CD = '05')
                AND REC_ST_DT >= TO_CHAR(:1)
                AND PROG_STAT_CD IN ('13', '14')
                AND REC_FILE_NM IS NULL
                AND REC_RTY_TRAN_YN IS NULL
                AND UPDATED_TM <= SYSDATE - (INTERVAL '1' MINUTE)
                AND ROWNUM <= 100
            ORDER BY
                UPDATED_TM
            FOR UPDATE SKIP LOCKED
        """
        bind = (
            target_date,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_remapping_target(db, target_date):
    """
    Select Retry Record mapping target from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                 DB object
    @param      target_date:        Target date(YYYY-MM-DD)
    @return:                        STT target list
    """
    try:
        query = """
            SELECT
                REC_KEY,
                STT_REC_ID,
                REC_ST_DT
            FROM
                STTOWN.QA_CAL_INFO_TB
            WHERE 
                (CHN_CD = '01' OR CHN_CD = '05')
                AND REC_ST_DT >= TO_CHAR(:1)
                AND PROG_STAT_CD = '13'
                AND REC_FILE_NM IS NULL
                AND REC_RTY_TRAN_YN = 'Y'
                AND UPDATED_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                AND ROWNUM <= 100
            ORDER BY
                UPDATED_TM
            FOR UPDATE SKIP LOCKED
        """
        bind = (
            target_date,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_rec_retry_yn(db, rec_key, rec_st_dt):
    """
    Update REC_RTY_TRAN_YN from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:             DB object
    @param      rec_key:        REC_KEY(녹취KEY)
    @param      rec_st_dt:      REC_ST_DT(녹취시작일자)
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                REC_RTY_TRAN_YN = 'Y'
            WHERE
                REC_KEY = TO_CHAR(:1)
                AND REC_ST_DT = TO_CHAR(:2)
        """
        bind = (
            rec_key,
            rec_st_dt
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def update_rec_mapping(db, rec_key, rec_st_dt, rec_file_nm, host_nm):
    """
    Update Record mapping data QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                 DB object
    @param      rec_key:            REC_KEY(녹취KEY)
    @param      rec_st_dt:          REC_ST_DT(녹취시작일자)
    @param      rec_file_nm:        REC_FILE_NM(녹취파일명)
    @param      host_nm:            Hostname
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                REC_FILE_NM = :1,
                UPDATOR_ID = :2,
                UPDATED_TM = :3
            WHERE
                REC_KEY = TO_CHAR(:4)
                AND REC_ST_DT = TO_CHAR(:5)
        """
        bind = (
            rec_file_nm,
            host_nm,
            datetime.fromtimestamp(time.time()),
            rec_key,
            rec_st_dt
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# rec_conv_api.py
def update_conv_rec_info(db, bind):
    """
    Update Record mapping data QA_CAL_INFO_TB(콜정보테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                LIS_REC_FILE_NM = :1,
                LIS_REC_LOC_PATH = :2,
                UPDATOR_ID = :3,
                UPDATED_TM = :4
            WHERE
                REC_KEY = TO_CHAR(:5)
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# Common HMD
def select_common_fwd_hmd(db):
    """
    Select 공통금지어 hmd from QA_PROD_TB(상품테이블), QA_CHK_ITM_TB(평가항목테이블), QA_PROD_CHK_ITM_TB(상품별평가항목테이블),
    QA_STMT_TB(문장테이블), QA_CUSL_SEC_INFO_TB(상담구간정보테이블), QA_CUSL_KWD_TB(상담키워드테이블), QA_DTC_DTL_TB(탐지사전상세테이블)
    @param        db:     DB object
    @return:              Results
    """
    try:
        query = """
            SELECT
                T6.CHK_ITM_CD,
                T6.CHK_APP_ORD,
                T6.CHK_CTG_CD,
                T6.CUSL_CD,
                T6.CUSL_APP_ORD,
                T6.KWD_SEQ,
                T7.DTCT_DTC_NO,
                T7.DTCT_DTC_GRP_NO,
                T7.DTCT_DTC_GRP_IN_NO,
                T7.DTCT_DTC_ED_NO,
                T7.DTCT_DTC_CON,
                T8.GUD_STMT,
                T8.SCRP,
                T8.SNS_YN
            FROM
                (
                    SELECT
                        T4.CHK_ITM_CD,
                        T4.CHK_APP_ORD,
                        T4.CHK_CTG_CD,
                        T4.CUSL_CD,
                        T4.CUSL_APP_ORD,
                        T5.KWD_SEQ,
                        T5.DTCT_DTC_NO
                    FROM
                        (
                        SELECT
                            T2.CHK_ITM_CD,
                            T2.CHK_APP_ORD,
                            T2.CHK_CTG_CD,
                            T3.CUSL_CD,
                            T3.CUSL_APP_ORD
                        FROM
                            (
                                SELECT
                                    T1.PROD_CAT,
                                    T1.PROD_CD,
                                    T1.PLN_CD,
                                    T1.CHK_ITM_CD,
                                    T1.CHK_APP_ORD,
                                    T2.CHK_CTG_CD
                                FROM
                                    STTOWN.QA_PROD_CHK_ITM_TB T1,
                                    STTOWN.QA_CHK_ITM_TB T2
                                WHERE
                                    T1.PROD_CAT = 'L' 
                                    AND T1.PROD_CD = 'ZZZREC' 
                                    AND T1.PLN_CD = '0'
                                    AND T1.CHK_APP_ORD =(
                                                        SELECT 
                                                            CHK_APP_ORD 
                                                        FROM 
                                                            STTOWN.QA_PROD_TB 
                                                        WHERE 
                                                            PROD_CAT = 'L' 
                                                            AND PROD_CD = 'ZZZREC' 
                                                            AND USED_YN = 'Y'
                                                            AND TO_DATE(SALE_ST_DT, 'YYYYMMDD') <= SYSDATE
                                                            AND TO_DATE(SALE_ED_DT, 'YYYYMMDD') >= SYSDATE
                                                    )
                                    AND T1.PROD_CAT = T2.PROD_CAT
                                    AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                                    AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                                    AND T2.USED_YN = 'Y'
                            ) T2,
                            STTOWN.QA_CUSL_SEC_INFO_TB T3
                        WHERE
                            T2.PROD_CAT = T3.PROD_CAT
                            AND T2.PROD_CD = T3.PROD_CD
                            AND T2.PLN_CD = T3.PLN_CD
                            AND T2.CHK_ITM_CD = T3.CHK_ITM_CD
                        ) T4,
                        STTOWN.QA_CUSL_KWD_TB T5
                    WHERE
                        T4.CUSL_CD = T5.CUSL_CD
                        AND T4.CUSL_APP_ORD = T5.CUSL_APP_ORD
                ) T6,
                STTOWN.QA_DTC_DTL_TB T7,
                STTOWN.QA_STMT_TB T8
            WHERE
                T6.DTCT_DTC_NO = T7.DTCT_DTC_NO
                AND T6.DTCT_DTC_NO = T8.DTCT_DTC_NO
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_common_script_hmd(db):
    """
    Select 공통스크립트 hmd from QA_PROD_TB(상품테이블), QA_CHK_ITM_TB(평가항목테이블), QA_PROD_CHK_ITM_TB(상품별평가항목테이블),
    QA_STMT_TB(문장테이블), QA_SCRP_SEC_INFO_TB(스크립트구간정보테이블), QA_SCRP_TB(스크립트테이블), QA_DTC_DTL_TB(탐지사전상세테이블)
    @param        db:     DB object
    @return:              Results
    """
    try:
        query = """
            SELECT
                T6.CHK_ITM_CD,
                T6.CHK_APP_ORD,
                T6.CHK_CTG_CD,
                T6.SCRP_LCTG_CD,
                T6.SCRP_MCTG_CD,
                T9.SCRP_MCTG_NM,
                T6.SCRP_SCTG_CD,
                T9.SCRP_SCTG_NM,
                T6.SCRP_APP_ORD,
                T6.ST_SCRP_YN,
                T6.ESTY_SCRP_YN,
                T6.STMT_SEQ,
                T7.DTCT_DTC_NO,
                T7.DTCT_DTC_GRP_NO,
                T7.DTCT_DTC_GRP_IN_NO,
                T7.DTCT_DTC_ED_NO,
                T7.DTCT_DTC_CON,
                T8.ANS_CD,
                T8.CUST_ANS_YN,
                T8.GUD_STMT
            FROM
                (
                    SELECT
                        T4.CHK_ITM_CD,
                        T4.CHK_APP_ORD,
                        T4.CHK_CTG_CD,
                        T4.SCRP_LCTG_CD,
                        T4.SCRP_MCTG_CD,
                        T4.SCRP_SCTG_CD,
                        T4.SCRP_APP_ORD,
                        T5.STMT_SEQ,
                        T5.DTCT_DTC_NO,
                        T5.ST_SCRP_YN,
                        T5.ESTY_SCRP_YN
                    FROM
                        (
                        SELECT
                            T3.CHK_ITM_CD,
                            T3.CHK_APP_ORD,
                            T3.CHK_CTG_CD,
                            T4.SCRP_LCTG_CD,
                            T4.SCRP_MCTG_CD,
                            T4.SCRP_SCTG_CD,
                            T4.SCRP_APP_ORD
                        FROM
                            (
                                SELECT
                                    T1.PROD_CAT,
                                    T1.PROD_CD,
                                    T1.PLN_CD,
                                    T1.CHK_ITM_CD,
                                    T1.CHK_APP_ORD,
                                    T2.CHK_CTG_CD
                                FROM
                                    STTOWN.QA_PROD_CHK_ITM_TB T1,
                                    STTOWN.QA_CHK_ITM_TB T2
                                WHERE
                                    T1.PROD_CAT = 'L'
                                    AND T1.PROD_CD = 'ZZZSCR'
                                    AND T1.PLN_CD = '0'
                                    AND T1.CHK_APP_ORD =(
                                                        SELECT 
                                                            CHK_APP_ORD 
                                                        FROM 
                                                            STTOWN.QA_PROD_TB 
                                                        WHERE 
                                                            PROD_CAT = 'L' 
                                                            AND PROD_CD = 'ZZZSCR' 
                                                            AND USED_YN = 'Y' 
                                                            AND TO_DATE(SALE_ST_DT, 'YYYYMMDD') <= SYSDATE
                                                            AND TO_DATE(SALE_ED_DT, 'YYYYMMDD') >= SYSDATE
                                                    )
                                    AND T1.PROD_CAT = T2.PROD_CAT
                                    AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                                    AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                                    AND T2.USED_YN = 'Y'
                            ) T3,
                            STTOWN.QA_SCRP_SEC_INFO_TB T4
                        WHERE
                            T3.PROD_CAT = T4.PROD_CAT
                            AND T3.PROD_CD = T4.PROD_CD
                            AND T3.PLN_CD = T4.PLN_CD
                            AND T3.CHK_ITM_CD = T4.CHK_ITM_CD
                        ) T4,
                        STTOWN.QA_SCRP_TB T5
                    WHERE
                        T4.SCRP_LCTG_CD = T5.SCRP_LCTG_CD
                        AND T4.SCRP_MCTG_CD = T5.SCRP_MCTG_CD
                        AND T4.SCRP_SCTG_CD = T5.SCRP_SCTG_CD
                        AND T4.SCRP_APP_ORD = T5.SCRP_APP_ORD
                        AND T5.ST_SCRP_YN = 'Y'
                ) T6,
                STTOWN.QA_DTC_DTL_TB T7,
                STTOWN.QA_STMT_TB T8,
                STTOWN.QA_SCRP_SEC_CD_TB T9
            WHERE
                T6.DTCT_DTC_NO = T7.DTCT_DTC_NO
                AND T6.DTCT_DTC_NO = T8.DTCT_DTC_NO
                AND T7.DTCT_DTC_ED_NO = '1'
                AND T6.SCRP_LCTG_CD = T9.SCRP_LCTG_CD
                AND T6.SCRP_MCTG_CD = T9.SCRP_MCTG_CD
                AND T6.SCRP_SCTG_CD = T9.SCRP_SCTG_CD
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_chk_itm_cd_list(db):
    """
    Select 공통스크립트 대상 항목 from QA_PROD_TB(상품테이블), QA_PROD_CHK_ITM_CD(상품별평가항목테이블),
    QA_CHK_ITM_TB(평가항목테이블), QA_SCRP_SEC_INFO_TB(스크립트구간정보테이블), QA_SCRP_TB(스크립트테이블)
    @param      db:         DB object
    @return:                Results
    """
    try:
        query = """
            SELECT
                T4.CHK_ITM_CD,
                T4.CHK_APP_ORD,
                T4.SCRP_MCTG_CD,
                T6.SCRP_MCTG_NM,
                T4.SCRP_SCTG_CD,
                T6.SCRP_SCTG_NM
            FROM
                (
                SELECT
                    T3.CHK_ITM_CD,
                    T3.CHK_APP_ORD,
                    T3.CHK_CTG_CD,
                    T4.SCRP_LCTG_CD,
                    T4.SCRP_MCTG_CD,
                    T4.SCRP_SCTG_CD,
                    T4.SCRP_APP_ORD
                FROM
                    (
                        SELECT
                            T1.PROD_CAT,
                            T1.PROD_CD,
                            T1.PLN_CD,
                            T1.CHK_ITM_CD,
                            T1.CHK_APP_ORD,
                            T2.CHK_CTG_CD
                        FROM
                            STTOWN.QA_PROD_CHK_ITM_TB T1,
                            STTOWN.QA_CHK_ITM_TB T2
                        WHERE
                            T1.PROD_CAT = 'L'
                            AND T1.PROD_CD = 'ZZZSCR'
                            AND T1.PLN_CD = '0'
                            AND T1.CHK_APP_ORD =(
                                                SELECT 
                                                    CHK_APP_ORD 
                                                FROM 
                                                    STTOWN.QA_PROD_TB 
                                                WHERE 
                                                    PROD_CAT = 'L' 
                                                    AND PROD_CD = 'ZZZSCR' 
                                                    AND USED_YN = 'Y' 
                                                    AND TO_DATE(SALE_ST_DT, 'YYYYMMDD') <= SYSDATE
                                                    AND TO_DATE(SALE_ED_DT, 'YYYYMMDD') >= SYSDATE
                                            )
                            AND T1.PROD_CAT = T2.PROD_CAT
                            AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                            AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                            AND T2.USED_YN = 'Y'
                    ) T3,
                    STTOWN.QA_SCRP_SEC_INFO_TB T4
                WHERE
                    T3.PROD_CAT = T4.PROD_CAT
                    AND T3.PROD_CD = T4.PROD_CD
                    AND T3.PLN_CD = T4.PLN_CD
                    AND T3.CHK_ITM_CD = T4.CHK_ITM_CD
                ) T4,
                STTOWN.QA_SCRP_TB T5,
                STTOWN.QA_SCRP_SEC_CD_TB T6
            WHERE
                T4.SCRP_LCTG_CD = T5.SCRP_LCTG_CD
                AND T4.SCRP_MCTG_CD = T5.SCRP_MCTG_CD
                AND T4.SCRP_SCTG_CD = T5.SCRP_SCTG_CD
                AND T4.SCRP_APP_ORD = T5.SCRP_APP_ORD
                AND T5.ST_SCRP_YN = 'Y'
                AND T4.SCRP_LCTG_CD = T6.SCRP_LCTG_CD
                AND T4.SCRP_MCTG_CD = T6.SCRP_MCTG_CD
                AND T4.SCRP_SCTG_CD = T6.SCRP_SCTG_CD
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_real_rst_tb(db, rec_key):
    """
    Check QA Real RST existed from QA_REAL_RST_TB(실시간 평가 결과 테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                REC_KEY
            FROM
                STTOWN.QA_REAL_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_real_rst_tb(db, rec_key):
    """
    Delete QA Real RST from QA_REAL_RST_TB(실시간 평가 결과 테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_REAL_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_real_rst_tb(db, bind_list):
    """
    Insert Script TA Real result QA_REAL_RST_TB(실시간 평가 결과 테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_REAL_RST_TB
            (
                REC_KEY,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                SCRP_MCTG_CD,
                SCRP_MCTG_NM,
                SCRP_SCTG_CD,
                SCRP_SCTG_NM,
                PSN_JUG_CAT,
                DTCT_RST,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
                :1, :2, :3, :4, :5, 
                :6, :7, :8, :9, :10,
                :11, :12, :13, :14
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_common_script_start_hmd(db):
    """
    Select 공통스크립트 hmd from QA_PROD_TB(상품테이블), QA_CHK_ITM_TB(평가항목테이블), QA_PROD_CHK_ITM_TB(상품별평가항목테이블),
    QA_STMT_TB(문장테이블), QA_SCRP_SEC_INFO_TB(스크립트구간정보테이블), QA_SCRP_TB(스크립트테이블), QA_DTC_DTL_TB(탐지사전상세테이블)
    @param        db:     DB object
    @return:              Results
    """
    try:
        query = """
            SELECT
                T6.CHK_ITM_CD,
                T6.CHK_APP_ORD,
                T6.CHK_CTG_CD,
                T6.SCRP_LCTG_CD,
                T6.SCRP_MCTG_CD,
                T9.SCRP_MCTG_NM,
                T6.SCRP_SCTG_CD,
                T9.SCRP_SCTG_NM,
                T6.SCRP_APP_ORD,
                T6.ST_SCRP_YN,
                T6.ESTY_SCRP_YN,
                T6.STMT_SEQ,
                T7.DTCT_DTC_NO,
                T7.DTCT_DTC_GRP_NO,
                T7.DTCT_DTC_GRP_IN_NO,
                T7.DTCT_DTC_ED_NO,
                T7.DTCT_DTC_CON,
                T8.ANS_CD,
                T8.CUST_ANS_YN,
                T8.GUD_STMT
            FROM
                (
                    SELECT
                        T4.CHK_ITM_CD,
                        T4.CHK_APP_ORD,
                        T4.CHK_CTG_CD,
                        T4.SCRP_LCTG_CD,
                        T4.SCRP_MCTG_CD,
                        T4.SCRP_SCTG_CD,
                        T4.SCRP_APP_ORD,
                        T5.STMT_SEQ,
                        T5.DTCT_DTC_NO,
                        T5.ST_SCRP_YN,
                        T5.ESTY_SCRP_YN
                    FROM
                        (
                        SELECT
                            T3.CHK_ITM_CD,
                            T3.CHK_APP_ORD,
                            T3.CHK_CTG_CD,
                            T4.SCRP_LCTG_CD,
                            T4.SCRP_MCTG_CD,
                            T4.SCRP_SCTG_CD,
                            T4.SCRP_APP_ORD
                        FROM
                            (
                                SELECT
                                    T1.PROD_CAT,
                                    T1.PROD_CD,
                                    T1.PLN_CD,
                                    T1.CHK_ITM_CD,
                                    T1.CHK_APP_ORD,
                                    T2.CHK_CTG_CD
                                FROM
                                    STTOWN.QA_PROD_CHK_ITM_TB T1,
                                    STTOWN.QA_CHK_ITM_TB T2
                                WHERE
                                    T1.PROD_CAT = 'L'
                                    AND T1.PROD_CD = 'ZZZSTR'
                                    AND T1.PLN_CD = '0'
                                    AND T1.CHK_APP_ORD =(
                                                        SELECT 
                                                            CHK_APP_ORD 
                                                        FROM 
                                                            STTOWN.QA_PROD_TB 
                                                        WHERE 
                                                            PROD_CAT = 'L' 
                                                            AND PROD_CD = 'ZZZSTR' 
                                                            AND USED_YN = 'Y' 
                                                            AND TO_DATE(SALE_ST_DT, 'YYYYMMDD') <= SYSDATE
                                                            AND TO_DATE(SALE_ED_DT, 'YYYYMMDD') >= SYSDATE
                                                    )
                                    AND T1.PROD_CAT = T2.PROD_CAT
                                    AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                                    AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                                    AND T2.USED_YN = 'Y'
                            ) T3,
                            STTOWN.QA_SCRP_SEC_INFO_TB T4
                        WHERE
                            T3.PROD_CAT = T4.PROD_CAT
                            AND T3.PROD_CD = T4.PROD_CD
                            AND T3.PLN_CD = T4.PLN_CD
                            AND T3.CHK_ITM_CD = T4.CHK_ITM_CD
                        ) T4,
                        STTOWN.QA_SCRP_TB T5
                    WHERE
                        T4.SCRP_LCTG_CD = T5.SCRP_LCTG_CD
                        AND T4.SCRP_MCTG_CD = T5.SCRP_MCTG_CD
                        AND T4.SCRP_SCTG_CD = T5.SCRP_SCTG_CD
                        AND T4.SCRP_APP_ORD = T5.SCRP_APP_ORD
                        AND T5.ST_SCRP_YN = 'Y'
                ) T6,
                STTOWN.QA_DTC_DTL_TB T7,
                STTOWN.QA_STMT_TB T8,
                STTOWN.QA_SCRP_SEC_CD_TB T9
            WHERE
                T6.DTCT_DTC_NO = T7.DTCT_DTC_NO
                AND T6.DTCT_DTC_NO = T8.DTCT_DTC_NO
                AND T7.DTCT_DTC_ED_NO = '1'
                AND T6.SCRP_LCTG_CD = T9.SCRP_LCTG_CD
                AND T6.SCRP_MCTG_CD = T9.SCRP_MCTG_CD
                AND T6.SCRP_SCTG_CD = T9.SCRP_SCTG_CD
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


# stt_daemon.py
def select_stt_target(db):
    """
    Select STT target list from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:             DB object
    @return:                    STT target list
    """
    try:
        query = """
            SELECT
                PROC_PRI,
                REC_KEY,
                REC_ID,
                REC_FILE_NM,
                STT_REC_FILE_NM,
                REC_COMP_CD,
                CHN_CLA_CD,
                REC_ST_DT,
                REC_ST_TM,
                REC_ED_TM,
                REC_DU_TM,
                BROF_CD,
                EMP_NO,
                STEREO_SPLIT_YN
            FROM
                STTOWN.QA_CAL_INFO_TB
            WHERE
                PROG_STAT_CD = '10'
                AND PROC_PRI = (SELECT MIN(PROC_PRI) FROM STTOWN.QA_CAL_INFO_TB WHERE PROG_STAT_CD = '10')
                AND CREATED_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                AND UPDATED_TM <= SYSDATE - (INTERVAL '1' MINUTE)
                AND ROWNUM <= 10
            ORDER BY
                UPDATED_TM
            FOR UPDATE SKIP LOCKED
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            query = """
                SELECT
                    PROC_PRI,   
                    REC_KEY,
                    REC_ID,
                    REC_FILE_NM,
                    CHN_CLA_CD,
                    REC_ST_DT,
                    REC_DU_TM,
                    BROF_CD,
                    STEREO_SPLIT_YN
                FROM
                    STTOWN.QA_CAL_INFO_TB
                WHERE
                    PROG_STAT_CD = '10'
                    AND PROC_PRI IS NULL
                    AND CREATED_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                    AND UPDATED_TM <= SYSDATE - (INTERVAL '1' MINUTE)
                    AND ROWNUM <= 10
                ORDER BY
                    CREATED_TM
                FOR UPDATE SKIP LOCKED
            """
            db.check_alive()
            db.cursor.execute(query, )
            result = rows_to_dict_list(db)
        if not result:
            query = """
                SELECT
                    PROC_PRI,   
                    REC_KEY,
                    REC_ID,
                    REC_FILE_NM,
                    CHN_CLA_CD,
                    REC_ST_DT,
                    REC_DU_TM,
                    BROF_CD,
                    STEREO_SPLIT_YN
                FROM
                    STTOWN.QA_CAL_INFO_TB
                WHERE
                    PROG_STAT_CD = '10'
                    AND PROC_PRI IS NULL
                    AND CREATED_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                    AND UPDATED_TM <= SYSDATE - (INTERVAL '1' MINUTE)
                    AND ROWNUM <= 10
                ORDER BY
                    CREATED_TM
                FOR UPDATE SKIP LOCKED
            """
            db.check_alive()
            db.cursor.execute(query, )
            result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_stt_status(**kwargs):
    """
    Update STT status from QA_CAL_INFO_TB(콜정보테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                STT_PROC_SVR = :1,
                PROG_STAT_CD = :2,
                PROG_STAT_DTL_CD = :3,
                UPDATOR_ID = :4,
                UPDATED_TM = :5
            WHERE
                REC_KEY = TO_CHAR(:6)
                AND REC_ID = TO_CHAR(:7)
                AND REC_ST_DT = TO_CHAR(:8)
        """
        bind = (
            kwargs.get('host_nm'),
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('rec_key'),
            kwargs.get('rec_id'),
            kwargs.get('rec_st_dt')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        # db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# stt_process.py
def update_stt_status_commit(**kwargs):
    """
    Update STT status from QA_CAL_INFO_TB(콜정보테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                STT_PROC_SVR = :1,
                PROG_STAT_CD = :2,
                PROG_STAT_DTL_CD = :3,
                UPDATOR_ID = :4,
                UPDATED_TM = :5
            WHERE
                REC_KEY = TO_CHAR(:6)
                AND REC_ID = TO_CHAR(:7)
                AND REC_ST_DT = TO_CHAR(:8)
        """
        bind = (
            kwargs.get('host_nm'),
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('rec_key'),
            kwargs.get('rec_id'),
            kwargs.get('rec_st_dt')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_stt_results_existed(db, rec_key):
    """
    Check STT results existed from QA_STT_RST_TB(STT결과테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                REC_KEY
            FROM
                STTOWN.QA_STT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
                AND STMT_NO = '1'
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_stt_results(db, rec_key):
    """
    Delete STT results from QA_STT_RST_TB(STT결과테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_STT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_stt_result(db, bind_list):
    """
    Insert STT result QA_STT_RST_TB(STT결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_STT_RST_TB
            (
                REC_KEY,
                STMT_NO,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                STMT,
                SILENCE,
                STT_YM,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5, 
              :6, :7, :8, :9, :10,
              :11, :12
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def update_stt_status_and_info(**kwargs):
    """
    Update STT status and information QA_CAL_INFO_TB(콜정보테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                PROG_STAT_CD = :1,
                REC_COMP_CD = :2,
                REC_BIZ_TYPE = :3,
                REC_ST_TM = :4,
                REC_ED_TM = :5,
                CHN_CLA_CD = :6,
                REC_DU_TM= :7,
                STT_PROC_SP = :8,
                STT_ST_TM = :9,
                STT_COM_TM = :10,
                C_VOIC_SP = :11,
                A_VOIC_SP = :12,
                MAX_SILENCE = :13,
                REC_FILE_EXT = :14,
                LIS_REC_FILE_NM = :15,
                LIS_REC_LOC_PATH = :16,
                UPDATOR_ID = :17,
                UPDATED_TM = :18
            WHERE
                REC_KEY = TO_CHAR(:19)
                AND REC_ID = TO_CHAR(:20)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('rec_comp_cd'),
            kwargs.get('rec_biz_type'),
            kwargs.get('rec_st_tm'),
            kwargs.get('rec_ed_tm'),
            kwargs.get('chn_cla_cd'),
            kwargs.get('record_duration'),
            kwargs.get('stt_proc_sp'),
            kwargs.get('stt_st_tm'),
            kwargs.get('stt_com_tm'),
            kwargs.get('c_voic_sp'),
            kwargs.get('a_voic_sp'),
            kwargs.get('max_silence'),
            kwargs.get('rec_file_ext'),
            kwargs.get('lis_rec_file_nm'),
            kwargs.get('lis_rec_loc_path'),
            kwargs.get('updator_id'),
            kwargs.get('updated_tm'),
            kwargs.get('rec_key'),
            kwargs.get('rec_id')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_script_ta_results_existed(db, rec_key):
    """
    Check Script TA results existed from QA_REAL_DTCT_RST_TB(실시간 탐지 결과 테이블)
    @param      db:             DB object
    @param      rec_key:        REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                REC_KEY
            FROM
                STTOWN.QA_REAL_DTCT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_script_ta_results(db, rec_key):
    """
    Delete Script TA results from QA_REAL_DTCT_RST_TB(실시간 탐지 결과 테이블)
    @param      db:             DB object
    @param      rec_key:        REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_REAL_DTCT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_script_ta_result(db, bind, plsql=False):
    """
    Insert Script TA result QA_REAL_DTCT_RST_TB(실시간 탐지 결과 테이블)
    @param      db:         DB object
    @param      bind:       Bind
    @param      plsql:      PLSQL flag
    """
    try:
        bind_str = 'pls_encrypt_b64(:14)' if plsql else ':14'
        query = """
            INSERT INTO STTOWN.QA_REAL_DTCT_RST_TB
            (
                REC_KEY,
                STMT_NO,
                ALL_SEQ,
                SPK_DIV_CD,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                SCRP_LCTG_CD,
                SCRP_MCTG_CD,
                SCRP_SCTG_CD,
                SCRP_APP_ORD,
                STMT_ST_TM,
                STMT_ED_TM,
                DTCT_STMT,
                DTCT_DTC_NO,
                DTCT_KWD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
                :1, :2, (SELECT NVL(MAX(TO_NUMBER(ALL_SEQ)), 0) + 1 FROM STTOWN.QA_REAL_DTCT_RST_TB WHERE REC_KEY = :3),
                :4, :5, :6, :7, :8, 
                :9, :10, :11, :12, :13,
                {0}, :15, :16, :17,
                :18, :19, :20
            )
        """.format(bind_str)
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_ta_results_existed(db, rec_key):
    """
    Check STT results existed from QA_CAL_DTCT_RST_TB(녹취 탐지 결과 테이블(상담관리))
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                REC_KEY
            FROM
                STTOWN.QA_CAL_DTCT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_ta_results(db, rec_key):
    """
    Delete STT results from QA_CAL_DTCT_RST_TB(녹취 탐지 결과 테이블(상담관리))
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CAL_DTCT_RST_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_ta_result(db, bind_list, plsql=False):
    """
    Insert TA result QA_CAL_DTCT_RST_TB(녹취 탐지 결과 테이블(상담관리))
    @param      db:             DB object
    @param      bind_list:      Bind list
    @param      plsql:          PLSQL flag
    """
    try:
        bind_str = 'pls_encrypt_b64(:12)' if plsql else ':12'
        query = """
            INSERT INTO STTOWN.QA_CAL_DTCT_RST_TB
            (
                REC_KEY,
                STMT_NO,
                ALL_SEQ,
                SPK_DIV_CD,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                CUSL_CD,
                CUSL_APP_ORD,
                STMT_ST_TM,
                STMT_ED_TM,
                DTCT_STMT,
                DTCT_DTC_NO,
                DTCT_KWD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5, 
              :6, :7, :8, :9, :10, 
              :11, {0}, :13, :14, :15, 
              :16, :17, :18
            )
        """.format(bind_str)
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_subs_kwd(db):
    """
    Select target word from QA_SUBS_KWD_TB(치환키워드테이블)
    @param      db:         DB
    @return:
    """
    try:
        query = """
            SELECT
                TAR_WD,
                CHA_WD
            FROM
                STTOWN.QA_SUBS_KWD_TB
            WHERE
                USE_YN = 'Y'
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


# qa_daemon.py
def select_qa_target(db):
    """
    Select QA target list from QA_CTR_LIST_TB(계약리스트테이블)
    @param      db:         DB object
    @return:                QA target list
    """
    try:
        query = """
            SELECT
                T1.NSPL_NO,
                T1.TMS_INFO,
                T1.PROD_CD,
                T1.PROD_NM,
                T1.CONTRACT_DT,
                T1.QA_REQ_TM,
                T1.PROC_PRI,
                T1.TMS_FILE_CNT,
                T2.PROD_CAT,
                T2.PLN_CD
            FROM
                STTOWN.QA_CTR_LIST_TB T1,
                STTOWN.QA_PROD_TB T2
            WHERE
                T1.TMS_FILE_CNT <= (
                                    SELECT
                                        COUNT(*)
                                    FROM
                                        STTOWN.QA_CTR_CAL_REL_TB T3,
                                        STTOWN.QA_CAL_INFO_TB T4
                                    WHERE
                                        T1.NSPL_NO = T3.NSPL_NO
                                        AND T1.TMS_INFO = T3.TMS_INFO
                                        AND T3.REC_ID = T4.REC_ID
                                        AND T3.REC_FILE_NM = T4.REC_FILE_NM
                                        AND T4.PROG_STAT_CD IN ('13', '18', '19')
                                    )
                AND T1.PROG_STAT_CD = '20'
                AND T2.PROD_CD = 'ZZZSCR'
                AND T2.PLN_CD = '0'
                AND T1.QA_REQ_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                AND ROWNUM <= 10
            ORDER BY
                T1.QA_REQ_TM
            FOR UPDATE SKIP LOCKED
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_qa_status(**kwargs):
    """
    Update QA status QA_CTR_LIST_TB(계약리스트테이블)
    :param      kwargs:         Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CTR_LIST_TB
            SET
                PROG_STAT_CD = :1,
                PROG_STAT_DTL_CD = :2,
                UPDATOR_ID = :3,
                UPDATED_TM = :4
            WHERE
                NSPL_NO = TO_CHAR(:5)
                AND TMS_INFO = TO_NUMBER(:6)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('nspl_no'),
            kwargs.get('tms_info')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
#       Do not commit here
#        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# qa_process.py
def update_qa_status_commit(**kwargs):
    """
    Update QA status QA_CTR_LIST_TB(계약리스트테이블)
    :param      kwargs:         Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CTR_LIST_TB
            SET
                PROG_STAT_CD = :1,
                PROG_STAT_DTL_CD = :2,
                UPDATOR_ID = :3,
                UPDATED_TM = :4
            WHERE
                NSPL_NO = TO_CHAR(:5)
                AND TMS_INFO = TO_NUMBER(:6)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('nspl_no'),
            kwargs.get('tms_info')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_stt_prog_stat_cd_info(db, nspl_no, tms_info):
    """
    Select STT PROG_STAT_CD(진행상태코드) form QA_CAL_INFO_TB(콜정보테이블), QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:                 DB object
    @param      nspl_no:            NSPL_NO(계약번호)
    @param      tms_info:           TMS_INFO(회차정보)
    @return:                        STT status
    """
    try:
        query = """
            SELECT
                T1.TMS_INFO,
                T2.REC_KEY,
                T1.REC_ID,
                T1.REC_FILE_NM,
                T1.REC_BIZ_CD,
                T2.TL_CD,
                T2.REC_COMP_CD,
                T2.PROG_STAT_CD,
                T3.CD_NM,
                T2.STT_ST_TM,
                T2.STT_COM_TM,
                T2.REC_DU_TM
            FROM
                STTOWN.QA_CTR_CAL_REL_TB T1,
                STTOWN.QA_CAL_INFO_TB T2
            LEFT JOIN
                STTOWN.QA_CODE_DTL_TB T3
            ON
                T2.PROG_STAT_CD = T3.CD
                AND T3.CD_GP = 'PROG_STAT_CD'
            WHERE
                T1.NSPL_NO = TO_CHAR(:1)
                AND T1.TMS_INFO <= TO_NUMBER(:2)
                AND T1.REC_ID = T2.REC_ID
                AND T1.REC_FILE_NM = T2.REC_FILE_NM
            ORDER BY
                T2.REC_ST_DT,
                T2.REC_ST_TM
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_ctr_cal_rel_info(db, bind_list):
    """
    Update REC_KEY(녹취KEY) form QA_CTR_CAL_REL_TB(계약콜관계테이블)
    @param      db:              DB object
    @param      bind_list:       Bind list
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CTR_CAL_REL_TB
            SET
                REC_KEY = :1
            WHERE
                NSPL_NO = TO_CHAR(:2)
                AND TMS_INFO = TO_NUMBER(:3)
                AND REC_ID = TO_CHAR(:4)
                AND REC_FILE_NM = TO_CHAR(:5)
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_stt_results(db, nspl_no, tms_info):
    """
    Select STT results form QA_CTR_CAL_REL_TB(계약콜관계테이블), QA_CAL_INFO_TB(콜정보테이블), QA_STT_RST_TB(STT결과테이블)
    @param      db:                 DB object
    @param      nspl_no:            NSPL_NO(계약번호)
    @param      tms_info:           TMS_INFO(회차정보)
    @return:                        STT results
    """
    try:
        query = """
            SELECT
                T2.TMS_INFO,
                T1.REC_KEY,
                T2.REC_ID,
                T2.REC_FILE_NM,
                T2.REC_BIZ_CD,
                T2.REC_DU_TM,
                T1.STMT_NO,
                T1.SPK_DIV_CD,
                T1.STMT_ST_TM,
                T1.STMT_ED_TM,
                T1.STMT
            FROM 
                STTOWN.QA_STT_RST_TB T1,
                (
                    SELECT
                        T3.NSPL_NO,
                        T3.TMS_INFO,
                        T3.REC_ID,
                        T3.REC_FILE_NM,
                        T3.REC_BIZ_CD,	
                        T4.REC_KEY,
                        T4.REC_DU_TM,
                        T4.REC_ST_DT,
                        T4.REC_ST_TM
                    FROM
                        STTOWN.QA_CTR_CAL_REL_TB T3,
                        STTOWN.QA_CAL_INFO_TB T4
                    WHERE
                        T3.NSPL_NO = TO_CHAR(:1)
                        AND T3.TMS_INFO <= TO_NUMBER(:2)
                        AND T3.REC_ID = T4.REC_ID
                        AND T3.REC_FILE_NM = T4.REC_FILE_NM
                ) T2
            WHERE 
                T1.REC_KEY = T2.REC_KEY
            ORDER BY
                T2.REC_ST_DT,
                T2.REC_ST_TM,
                T1.REC_KEY,
                TO_NUMBER(T1.STMT_NO)
        """
        bind = (
            nspl_no,
            tms_info
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_chk_app_ord(db, prod_cat, prod_cd, contract_dt):
    """
    Select QA CHK_APP_ORD(항목적용차수) from QA_PROD_TB(상품테이블)
    @param        db:                   DB object
    @param        prod_cat:             보종구분
    @param        prod_cd:              상품코드
    @param        contract_dt:          청약일자
    @return:                            CHK_APP_ORD(항목적용차수)
    """
    try:
        query = """
            SELECT 
                CHK_APP_ORD 
            FROM 
                STTOWN.QA_PROD_TB 
            WHERE 
                PROD_CAT = TO_CHAR(:1)
                AND PROD_CD = TO_CHAR(:2) 
                AND SALE_ST_DT <= TO_CHAR(:3)
                AND SALE_ED_DT >= TO_CHAR(:4)
                AND USED_YN = 'Y'
                AND ROWNUM = 1
        """
        bind = (
            prod_cat,
            prod_cd,
            contract_dt,
            contract_dt,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return str(result[0]['CHK_APP_ORD'])
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_script_hmd(db, prod_cat, prod_cd, pln_cd, chk_app_ord):
    """
    Select QA script hmd from QA_CHK_ITM_TB(평가항목테이블), QA_PROD_CHK_ITM_TB(상품별평가항목테이블), QA_STMT_TB(문장테이블),
    QA_SCRP_SEC_INFO_TB(스크립트구간정보테이블), QA_SCRP_TB(스크립트테이블), QA_DTC_DTL_TB(탐지사전상세테이블)
    @param        db:                   DB object
    @param        prod_cat:             보종구분
    @param        prod_cd:              상품코드
    @param        pln_cd:               플랜코드
    @param        chk_app_ord:          항목적용차수
    @return:                            Results
    """
    try:
        query = """
            SELECT
                T6.CHK_ITM_CD,
                T6.CHK_APP_ORD,
                T6.CHK_CTG_CD,
                T6.SCRP_LCTG_CD,
                T6.SCRP_MCTG_CD,
                T6.SCRP_SCTG_CD,
                T6.SCRP_APP_ORD,
                T6.ST_SCRP_YN,
                T6.ESTY_SCRP_YN,
                T6.STMT_SEQ,
                T7.DTCT_DTC_NO,
                T7.DTCT_DTC_GRP_NO,
                T7.DTCT_DTC_GRP_IN_NO,
                T7.DTCT_DTC_ED_NO,
                T7.DTCT_DTC_CON,
                T8.GUD_STMT,
                T8.SCRP,
                T8.ANS_CD,
                T8.CUST_ANS_YN
            FROM
                (
                    SELECT
                        T4.CHK_ITM_CD,
                        T4.CHK_APP_ORD,
                        T4.CHK_CTG_CD,
                        T4.SCRP_LCTG_CD,
                        T4.SCRP_MCTG_CD,
                        T4.SCRP_SCTG_CD,
                        T4.SCRP_APP_ORD,
                        T5.STMT_SEQ,
                        T5.DTCT_DTC_NO,
                        T5.ST_SCRP_YN,
                        T5.ESTY_SCRP_YN
                    FROM
                        (
                        SELECT
                            T3.CHK_ITM_CD,
                            T3.CHK_APP_ORD,
                            T3.CHK_CTG_CD,
                            T4.SCRP_LCTG_CD,
                            T4.SCRP_MCTG_CD,
                            T4.SCRP_SCTG_CD,
                            T4.SCRP_APP_ORD
                        FROM
                            (
                                SELECT
                                    T1.PROD_CAT,
                                    T1.PROD_CD,
                                    T1.PLN_CD,
                                    T1.CHK_ITM_CD,
                                    T1.CHK_APP_ORD,
                                    T2.CHK_CTG_CD
                                FROM
                                    STTOWN.QA_PROD_CHK_ITM_TB T1,
                                    STTOWN.QA_CHK_ITM_TB T2
                                WHERE
                                    T1.PROD_CAT = TO_CHAR(:1)
                                    AND T1.PROD_CD = TO_CHAR(:2)
                                    AND T1.PLN_CD = TO_CHAR(:3)
                                    AND T1.CHK_APP_ORD = TO_NUMBER(:4)
                                    AND T1.PROD_CAT = T2.PROD_CAT
                                    AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                                    AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                                    AND T2.USED_YN = 'Y'
                            ) T3,
                            STTOWN.QA_SCRP_SEC_INFO_TB T4
                        WHERE
                            T3.PROD_CAT = T4.PROD_CAT
                            AND T3.PROD_CD = T4.PROD_CD
                            AND T3.PLN_CD = T4.PLN_CD
                            AND T3.CHK_ITM_CD = T4.CHK_ITM_CD
                        ) T4,
                        STTOWN.QA_SCRP_TB T5
                    WHERE
                        T4.SCRP_LCTG_CD = T5.SCRP_LCTG_CD
                        AND T4.SCRP_MCTG_CD = T5.SCRP_MCTG_CD
                        AND T4.SCRP_SCTG_CD = T5.SCRP_SCTG_CD
                        AND T4.SCRP_APP_ORD = T5.SCRP_APP_ORD
                ) T6,
                STTOWN.QA_DTC_DTL_TB T7,
                STTOWN.QA_STMT_TB T8
            WHERE
                T6.DTCT_DTC_NO = T7.DTCT_DTC_NO
                AND T6.DTCT_DTC_NO = T8.DTCT_DTC_NO
        """
        bind = (
            prod_cat,
            prod_cd,
            pln_cd,
            chk_app_ord
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_cusl_hmd(db, prod_cat, prod_cd, pln_cd, chk_app_ord):
    """
    Select QA cusl hmd from hmd from QA_CHK_ITM_TB(평가항목테이블), QA_PROD_CHK_ITM_TB(상품별평가항목테이블),
    QA_STMT_TB(문장테이블), QA_CUSL_SEC_INFO_TB(상담구간정보테이블),
    QA_CUSL_KWD_TB(상담키워드테이블), QA_DTC_DTL_TB(탐지사전상세테이블)
    @param        db:                   DB object
    @param        prod_cat:             보종구분
    @param        prod_cd:              상품코드
    @param        pln_cd:               플랜코드
    @param        chk_app_ord:          항목적용차수
    @return:                            Results
    """
    try:
        query = """
            SELECT
                T6.CHK_ITM_CD,
                T6.CHK_APP_ORD,
                T6.CHK_CTG_CD,
                T6.CUSL_CD,
                T6.CUSL_APP_ORD,
                T6.KWD_SEQ,
                T7.DTCT_DTC_NO,
                T7.DTCT_DTC_GRP_NO,
                T7.DTCT_DTC_GRP_IN_NO,
                T7.DTCT_DTC_ED_NO,
                T7.DTCT_DTC_CON,
                T8.GUD_STMT,
                T8.SCRP,
                T8.SNS_YN
            FROM
                (
                    SELECT
                        T4.CHK_ITM_CD,
                        T4.CHK_APP_ORD,
                        T4.CHK_CTG_CD,
                        T4.CUSL_CD,
                        T4.CUSL_APP_ORD,
                        T5.KWD_SEQ,
                        T5.DTCT_DTC_NO
                    FROM
                        (
                        SELECT
                            T2.CHK_ITM_CD,
                            T2.CHK_APP_ORD,
                            T2.CHK_CTG_CD,
                            T3.CUSL_CD,
                            T3.CUSL_APP_ORD
                        FROM
                            (
                                SELECT
                                    T1.PROD_CAT,
                                    T1.PROD_CD,
                                    T1.PLN_CD,
                                    T1.CHK_ITM_CD,
                                    T1.CHK_APP_ORD,
                                    T2.CHK_CTG_CD
                                FROM
                                    STTOWN.QA_PROD_CHK_ITM_TB T1,
                                    STTOWN.QA_CHK_ITM_TB T2
                                WHERE
                                    T1.PROD_CAT = TO_CHAR(:1)
                                    AND T1.PROD_CD = TO_CHAR(:2)
                                    AND T1.PLN_CD = TO_CHAR(:3)
                                    AND T1.CHK_APP_ORD = TO_NUMBER(:4)
                                    AND T1.PROD_CAT = T2.PROD_CAT
                                    AND T1.CHK_APP_ORD = T2.CHK_APP_ORD
                                    AND T1.CHK_ITM_CD = T2.CHK_ITM_CD
                                    AND T2.USED_YN = 'Y'
                            ) T2,
                            STTOWN.QA_CUSL_SEC_INFO_TB T3
                        WHERE
                            T2.PROD_CAT = T3.PROD_CAT
                            AND T2.PROD_CD = T3.PROD_CD
                            AND T2.PLN_CD = T3.PLN_CD
                            AND T2.CHK_ITM_CD = T3.CHK_ITM_CD
                        ) T4,
                        STTOWN.QA_CUSL_KWD_TB T5
                    WHERE
                        T4.CUSL_CD = T5.CUSL_CD
                        AND T4.CUSL_APP_ORD = T5.CUSL_APP_ORD
                ) T6,
                STTOWN.QA_DTC_DTL_TB T7,
                STTOWN.QA_STMT_TB T8
            WHERE
                T6.DTCT_DTC_NO = T7.DTCT_DTC_NO
                AND T6.DTCT_DTC_NO = T8.DTCT_DTC_NO
        """
        bind = (
            prod_cat,
            prod_cd,
            pln_cd,
            chk_app_ord
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_ans_cont(db, ans_cd):
    """
    Select Answer content form QA_ANS_DTL_TB(답변상세테이블)
    @param      db:                 DB object
    @param      ans_cd:             ANS_CD(답변코드)
    @return:                        Answer content list
    """
    try:
        query = """
            SELECT
                ANS_CONT
            FROM
                STTOWN.QA_ANS_DTL_TB
            WHERE
                ANS_CD = :1
        """
        bind = (
            ans_cd,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_qa_ctr_scrp_results_existed(db, nspl_no, tms_info):
    """
    Check QA results existed from QA_CTR_SCRP_TB(계약 스크립트 테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_CTR_SCRP_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_ctr_scrp_results(db, nspl_no, tms_info):
    """
    Delete QA results from QA_CTR_SCRP_TB(계약 스크립트 테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CTR_SCRP_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_ctr_scrp_results(db, bind_list):
    """
    Insert QA results QA_CTR_SCRP_TB(계약 스크립트 테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CTR_SCRP_TB
            (
                NSPL_NO,
                TMS_INFO,
                CHK_CTG_CD,
                ALL_SEQ,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                DTCT_DTC_NO,
                GUD_STMT,
                SCRP,
                SEQ,
                ESTY_SCRP_YN,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_qa_scrp_dtct_results_existed(db, nspl_no, tms_info):
    """
    Check QA results existed from QA_SCRP_DTCT_RST_TB(표준스크립트탐지결과테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_SCRP_DTCT_RST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_scrp_dtct_results(db, nspl_no, tms_info):
    """
    Delete QA results from QA_SCRP_DTCT_RST_TB(표준스크립트탐지결과테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_SCRP_DTCT_RST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)  
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_scrp_dtct_results(db, bind_list):
    """
    Insert QA results QA_SCRP_DTCT_RST_TB(표준스크립트탐지결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_SCRP_DTCT_RST_TB
            (
                NSPL_NO,
                TMS_INFO,
                CHK_CTG_CD,
                REC_KEY,
                STMT_NO,
                ALL_SEQ,
                SPK_DIV_CD,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                SCRP_LCTG_CD,
                SCRP_MCTG_CD,
                SCRP_SCTG_CD,
                SCRP_APP_ORD,
                CUSL_CD,
                CUSL_APP_ORD,
                STMT_ST_TM,
                STMT_ED_TM,
                DTCT_STMT,
                ANS_YN,
                ANS_NOR_YN,
                ANS_ST_TM,
                ANS_ED_TM,
                ANS_DTCT_STMT,
                DTCT_DTC_NO,
                DTCT_KWD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16, :17, :18, :19, :20,
              :21, :22, :23, :24, :25,
              :26, :27, :28, :29, :30
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_qa_chk_rst_results_existed(db, nspl_no, tms_info):
    """
    Check QA results existed from QA_CHK_RST_TB(평가항목결과테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            SELECT
                NSPL_NO
            FROM
                STTOWN.QA_CHK_RST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_qa_chk_rst_results(db, nspl_no, tms_info):
    """
    Delete QA results from QA_CHK_RST_TB(평가항목결과테이블)
    @param      db:                     DB object
    @param      nspl_no:                NSPL_NO(계약번호)
    @param      tms_info:               TMS_INFO(회차정보)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CHK_RST_TB
            WHERE
                NSPL_NO = TO_CHAR(:1)
                AND TMS_INFO = TO_NUMBER(:2)
        """
        bind = (
            nspl_no,
            tms_info,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_qa_chk_rst_results(db, bind_list):
    """
    Insert QA results QA_CHK_RST_TB(평가항목결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_CHK_RST_TB
            (
                NSPL_NO,
                TMS_INFO,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                CHK_CTG_CD,
                REC_KEY,
                PSN_JUG_CAT,
                JUG_RST,
                SCR_RST,
                ANS_RST,
                DTCT_RST,
                SEC_ST_TM,
                SEC_ED_TM,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16, :17, :18
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def update_qa_status_and_ta_info(**kwargs):
    """
    Update QA status and information QA_CTR_LIST_TB(계약리스트테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_CTR_LIST_TB
            SET
                PROG_STAT_CD = :1,
                PROG_STAT_DTL_CD = :2,
                STT_ED_TM = :3,
                TA_ST_TM = :4,
                TA_ED_TM = :5,
                TA_PROC_SVR = :6,
                CREC_VOIC_SP = :7,
                CREC_DU_TM = :8,
                SREC_VOIC_SP = :9,
                SREC_DU_TM = :10,
                UPDATOR_ID = :11,
                UPDATED_TM = :12
            WHERE
                NSPL_NO = TO_CHAR(:13)
                AND TMS_INFO = TO_NUMBER(:14)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('stt_ed_dtm'),
            kwargs.get('ta_st_dtm'),
            kwargs.get('ta_ed_dtm'),
            kwargs.get('host_nm'),
            kwargs.get('crec_voic_sp'),
            kwargs.get('crec_du_tm'),
            kwargs.get('srec_voic_sp'),
            kwargs.get('srec_du_tm'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('nspl_no'),
            kwargs.get('tms_info'),
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# jp_daemon.py
def select_jp_target(db):
    """
    Select JoinPlan TA target list from QA_JOIN_PLN_LIST_TB(가입설계리스트테이블)
    @param      db:         DB object
    @return:                QA target list
    """
    try:
        query = """
            SELECT
                T1.TL_CSMR_NO,
                T1.PROD_CD,
                T1.PROD_NM,
                T1.REC_KEY,
                T1.REC_ID,
                T1.REC_FILE_NM,
                T1.REC_ST_DT,
                T1.JP_REQ_TM,
                T1.BROF_CD,
                T1.EMP_NO,
                T1.SAVE_DTTM,
                T1.SAVE_TIME,
                T3.PROD_CAT,
                T3.PLN_CD
            FROM
                (
                    SELECT 
                        TL_CSMR_NO,
                        PROD_CD,
                        PROD_NM,
                        REC_KEY,
                        REC_ID,
                        REC_FILE_NM,
                        REC_ST_DT,
                        JP_REQ_TM,
                        BROF_CD,
                        EMP_NO,
                        SAVE_DTTM,
                        SAVE_TIME
                    FROM
                        STTOWN.QA_JOIN_PLN_LIST_TB
                    WHERE
                        PROG_STAT_CD = '30'
                        AND JP_REQ_TM <= SYSDATE - (INTERVAL '10' MINUTE)
                ) T1,
                STTOWN.QA_CAL_INFO_TB T2,
                STTOWN.QA_PROD_TB T3
            WHERE
                T1.REC_ID = T2.REC_ID
                AND T1.REC_FILE_NM = T2.REC_FILE_NM 
                AND T2.PROG_STAT_CD IN ('13', '18', '19')
                AND T3.PROD_CD = 'ZZZAGR'
                AND T3.PLN_CD = '0'
                AND ROWNUM <= 10
            ORDER BY
                T1.JP_REQ_TM
            FOR UPDATE SKIP LOCKED
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_jp_status(**kwargs):
    """
    Update JoinPlan TA status QA_JOIN_PLN_LIST_TB(가입설계리스트테이블)
    :param      kwargs:         Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_JOIN_PLN_LIST_TB
            SET
                PROG_STAT_CD = :1,
                PROG_STAT_DTL_CD = :2,
                UPDATOR_ID = :3,
                UPDATED_TM = :4
            WHERE
                TL_CSMR_NO = TO_CHAR(:5)
                AND REC_ID = TO_CHAR(:6)
                AND REC_FILE_NM = TO_CHAR(:7)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('tl_csmr_no'),
            kwargs.get('rec_id'),
            kwargs.get('rec_file_nm')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
#       Do not commit here
#        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# jp_process.py
def update_jp_status_commit(**kwargs):
    """
    Update JoinPlan TA status QA_JOIN_PLN_LIST_TB(가입설계리스트테이블)
    :param      kwargs:         Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_JOIN_PLN_LIST_TB
            SET
                PROG_STAT_CD = :1,
                PROG_STAT_DTL_CD = :2,
                UPDATOR_ID = :3,
                UPDATED_TM = :4
            WHERE
                TL_CSMR_NO = TO_CHAR(:5)
                AND REC_ID = TO_CHAR(:6)
                AND REC_FILE_NM = TO_CHAR(:7)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('tl_csmr_no'),
            kwargs.get('rec_id'),
            kwargs.get('rec_file_nm')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_stt_info(db, rec_id, rec_file_nm):
    """
    Select STT information from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                 DB object
    @param      rec_id:             REC_ID(녹취ID)
    @param      rec_file_nm:        REC_FILE_NM(녹취파일명)
    @return:                        STT information
    """
    try:
        query = """
            SELECT
                T1.REC_KEY,
                T1.REC_ID,
                T1.REC_FILE_NM,
                T1.REC_COMP_CD,
                T1.REC_BIZ_CD,
                T1.TL_CD,
                T1.PROG_STAT_CD,
                T2.CD_NM,
                T1.STT_ST_TM,
                T1.STT_COM_TM,
                T1.REC_DU_TM
            FROM
                STTOWN.QA_CAL_INFO_TB T1
            LEFT JOIN
                STTOWN.QA_CODE_DTL_TB T2
            ON
                T1.PROG_STAT_CD = T2.CD
                AND T2.CD_GP = 'PROG_STAT_CD'
            WHERE
                REC_ID = TO_CHAR(:1)
                AND REC_FILE_NM = TO_CHAR(:2)
        """
        bind = (
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_jp_stt_results(db, rec_key):
    """
    Select STT results form QA_STT_RST_TB(STT결과테이블)
    @param      db:             DB object
    @param      rec_key:        REC_KEY(녹취KEY)
    @return:                    STT results
    """
    try:
        query = """
            SELECT
                STMT_NO,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                STMT
            FROM 
                STTOWN.QA_STT_RST_TB
            WHERE 
                REC_KEY = TO_CHAR(:1)
            ORDER BY
                TO_NUMBER(STMT_NO)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_jp_status_and_ta_info(**kwargs):
    """
    Update JoinPlan TA status and information QA_JOIN_PLN_LIST_TB(가입설계리스트테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_JOIN_PLN_LIST_TB
            SET
                REC_KEY = :1,
                PROG_STAT_CD = :2,
                PROG_STAT_DTL_CD = :3,
                STT_ED_TM = :4,
                TA_ST_TM = :5,
                TA_ED_TM = :6,
                TA_PROC_SVR = :7,
                UPDATOR_ID = :8,
                UPDATED_TM = :9
            WHERE
                TL_CSMR_NO = TO_CHAR(:10)
                AND REC_ID = TO_CHAR(:11)
                AND REC_FILE_NM = TO_CHAR(:12)
        """
        bind = (
            kwargs.get('rec_key'),
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('stt_ed_dtm'),
            kwargs.get('ta_st_dtm'),
            kwargs.get('ta_ed_dtm'),
            kwargs.get('host_nm'),
            kwargs.get('host_nm'),
            kwargs.get('updated_tm'),
            kwargs.get('tl_csmr_no'),
            kwargs.get('rec_id'),
            kwargs.get('rec_file_nm')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_jp_scrp_results_existed(db, tl_csmr_no, rec_key):
    """
    Check QA results existed from QA_JOIN_PLN_SCRP_TB(가입설계스크립트테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                TL_CSMR_NO
            FROM
                STTOWN.QA_JOIN_PLN_SCRP_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_jp_scrp_results(db, tl_csmr_no, rec_key):
    """
    Delete QA results from QA_JOIN_PLN_SCRP_TB(가입설계스크립트테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_JOIN_PLN_SCRP_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_jp_scrp_results(db, bind_list):
    """
    Insert JoinPlan results QA_JOIN_PLN_SCRP_TB(가입설계스크립트테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_JOIN_PLN_SCRP_TB
            (
                TL_CSMR_NO,
                REC_KEY,
                CHK_CTG_CD,
                ALL_SEQ,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                DTCT_DTC_NO,
                GUD_STMT,
                SCRP,
                SEQ,
                ESTY_SCRP_YN,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_jp_dtct_results_existed(db, tl_csmr_no, rec_key):
    """
    Check JoinPlan results existed from QA_JOIN_PLN_DTCT_RST_TB(가입설계탐지결과테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                TL_CSMR_NO
            FROM
                STTOWN.QA_JOIN_PLN_DTCT_RST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_jp_dtct_results(db, tl_csmr_no, rec_key):
    """
    Delete JoinPlan results from QA_JOIN_PLN_DTCT_RST_TB(가입설계탐지결과테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_JOIN_PLN_DTCT_RST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_jp_dtct_results(db, bind_list):
    """
    Insert JoinPlan results QA_JOIN_PLN_DTCT_RST_TB(가입설계탐지결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_JOIN_PLN_DTCT_RST_TB
            (
                TL_CSMR_NO,
                CHK_CTG_CD,
                REC_KEY,
                STMT_NO,
                ALL_SEQ,
                SPK_DIV_CD,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                SCRP_LCTG_CD,
                SCRP_MCTG_CD,
                SCRP_SCTG_CD,
                SCRP_APP_ORD,
                CUSL_CD,
                CUSL_APP_ORD,
                STMT_ST_TM,
                STMT_ED_TM,
                DTCT_STMT,
                ANS_YN,
                ANS_NOR_YN,
                ANS_ST_TM,
                ANS_ED_TM,
                ANS_DTCT_STMT,
                DTCT_DTC_NO,
                DTCT_KWD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16, :17, :18, :19, :20,
              :21, :22, :23, :24, :25,
              :26, :27, :28, :29
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_jp_rst_results_existed(db, tl_csmr_no, rec_key):
    """
    Check JoinPlan results existed from QA_JOIN_PLN_RST_TB(가입설계평가결과테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            SELECT
                TL_CSMR_NO
            FROM
                STTOWN.QA_JOIN_PLN_RST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_jp_rst_results(db, tl_csmr_no, rec_key):
    """
    Delete JoinPlan results from QA_JOIN_PLN_RST_TB(가입설계평가결과테이블)
    @param      db:                     DB object
    @param      tl_csmr_no:             TL_CSMR_NO(계약번호)
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_JOIN_PLN_RST_TB
            WHERE
                TL_CSMR_NO = TO_CHAR(:1)
                AND REC_KEY = TO_CHAR(:2)
        """
        bind = (
            tl_csmr_no,
            rec_key
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_jp_rst_results(db, bind_list):
    """
    Insert JoinPlan results QA_JOIN_PLN_RST_TB(가입설계평가결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_JOIN_PLN_RST_TB
            (
                TL_CSMR_NO,
                PROD_CAT,
                CHK_APP_ORD,
                CHK_ITM_CD,
                CHK_CTG_CD,
                REC_KEY,
                PSN_JUG_CAT,
                JUG_RST,
                SCR_RST,
                ANS_RST,
                DTCT_RST,
                SEC_ST_TM,
                SEC_ED_TM,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5,
              :6, :7, :8, :9, :10,
              :11, :12, :13, :14, :15,
              :16, :17
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def merge_into_base_brof_tb(**kwargs):
    """
    Merge into QA_BASE_BROF_TB(지점정보테이블)
    :param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            MERGE INTO STTOWN.QA_BASE_BROF_TB
                USING DUAL
                ON(
                    BROF_CD = TO_CHAR(:1)
                )
                WHEN MATCHED THEN
                    UPDATE SET
                        BROF_NM = :2,
                        SAES_CD = :3,
                        SAES_NM = :4,
                        HQT_CD = :5,
                        HQT_NM = :6,
                        CHN_CD = :7,
                        CHN_NM = :8,
                        USE_YN = :9,
                        UPDATOR_ID = :10,
                        UPDATED_TM = :11
                WHEN NOT MATCHED THEN
                    INSERT
                    (
                        BROF_CD,
                        BROF_NM,
                        SAES_CD,
                        SAES_NM,
                        HQT_CD,
                        HQT_NM,
                        CHN_CD,
                        CHN_NM,
                        USE_YN,
                        UPDATOR_ID,
                        UPDATED_TM,
                        CREATOR_ID,
                        CREATED_TM
                    )
                    VALUES
                    (
                        :12, :13, :14, :15, :16, 
                        :17, :18, :19, :20, :21,
                        :22, :23, :24
                    )
        """
        bind = (
            kwargs.get('brof_cd'),
            kwargs.get('brof_nm'),
            kwargs.get('saes_cd'),
            kwargs.get('saes_nm'),
            kwargs.get('hqt_cd'),
            kwargs.get('hqt_nm'),
            kwargs.get('chn_cd'),
            kwargs.get('chn_nm'),
            kwargs.get('use_yn'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time())
        )
        bind = bind * 2 + (kwargs.get('host_nm'), datetime.fromtimestamp(time.time()))
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except cx_Oracle.DatabaseError as e:
        error, = e.args
        if error.code == 1:
            update_base_brof_tb(
                db=kwargs.get('db'),
                brof_cd=kwargs.get('brof_cd'),
                brof_nm=kwargs.get('brof_nm'),
                saes_cd=kwargs.get('saes_cd'),
                saes_nm=kwargs.get('saes_nm'),
                hqt_cd=kwargs.get('hqt_cd'),
                hqt_nm=kwargs.get('hqt_nm'),
                chn_cd=kwargs.get('chn_cd'),
                chn_nm=kwargs.get('chn_nm'),
                use_yn=kwargs.get('use_yn'),
                host_nm=kwargs.get('host_nm')
            )
        else:
            exc_info = traceback.format_exc()
            db.conn.rollback()
            raise Exception(exc_info)
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def update_base_brof_tb(**kwargs):
    """
    Update QA_BASE_BROF_TB(지점정보테이블)
    :param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_BASE_BROF_TB
            SET
                BROF_NM = :1,
                SAES_CD = :2,
                SAES_NM = :3,
                HQT_CD = :4,
                HQT_NM = :5,
                CHN_CD = :6,
                CHN_NM = :7,
                USE_YN = :8,
                UPDATOR_ID = :9,
                UPDATED_TM = :10
            WHERE
                BROF_CD = TO_CHAR(:11)
        """
        bind = (
            kwargs.get('brof_nm'),
            kwargs.get('saes_cd'),
            kwargs.get('saes_nm'),
            kwargs.get('hqt_cd'),
            kwargs.get('hqt_nm'),
            kwargs.get('chn_cd'),
            kwargs.get('chn_nm'),
            kwargs.get('use_yn'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('brof_cd'),
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def delete_base_brof_tb(db):
    """
    Delete QA_BASE_BROF_TB(지점정보테이블)
    @param      db:         DB object
    """
    try:
        query = """
            DELETE 
                STTOWN.QA_BASE_BROF_TB
        """
        db.check_alive()
        db.cursor.execute(query, )
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def insert_base_brof_tb(db, bind):
    """
    Insert into QA_BASE_BROF_TB(지점정보테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_BASE_BROF_TB
            (
                BROF_CD,
                BROF_NM,
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                CHN_CD,
                CHN_NM,
                USE_YN,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
                :1, :2, :3, :4, :5, 
                :6, :7, :8, :9, :10, 
                :11, :12, :13
            )
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def merge_into_usr_mgt_tb(**kwargs):
    """
    Merge into QA_USR_MGT_TB(사용자관리테이블)
    :param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            MERGE INTO STTOWN.QA_USR_MGT_TB
                USING DUAL
                ON(
                    USER_ID = TO_CHAR(:1)
                )
                WHEN MATCHED THEN
                    UPDATE SET
                        EMP_NO = :2,
                        USER_NM = :3,
                        USER_PW = :4,
                        BROF_CD = :5,
                        DET_CD = :6,
                        ENABLED = :7,
                        AUTHORITY = :8,
                        AUT_CD = :9,
                        ADMIN_TLNO = :10, 
                        EXT_NO = :11,
                        UPDATOR_ID = :12,
                        UPDATED_TM = :13
                WHEN NOT MATCHED THEN
                    INSERT
                    (
                        USER_ID,
                        EMP_NO,
                        USER_NM,
                        USER_PW,
                        BROF_CD,
                        DET_CD,
                        ENABLED,
                        AUTHORITY,
                        AUT_CD,
                        ADMIN_TLNO,
                        EXT_NO,
                        UPDATOR_ID,
                        UPDATED_TM,
                        CREATOR_ID,
                        CREATED_TM
                    )
                    VALUES
                    (
                        :14, :15, :16, :17, :18, 
                        :19, :20, :21, :22, :23, 
                        :24, :25, :26, :27, :28
                    )
        """
        bind = (
            kwargs.get('user_id'),
            kwargs.get('emp_no'),
            kwargs.get('user_nm'),
            kwargs.get('user_pw'),
            kwargs.get('brof_cd'),
            kwargs.get('det_cd'),
            kwargs.get('enabled'),
            kwargs.get('authority'),
            kwargs.get('aut_cd'),
            kwargs.get('admin_tlno'),
            kwargs.get('ext_no'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time())
        )
        bind = bind * 2 + (kwargs.get('host_nm'), datetime.fromtimestamp(time.time()))
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except cx_Oracle.DatabaseError as e:
        error, = e.args
        if error.code == 1:
            update_usr_mgt_tb(
                db=kwargs.get('db'),
                user_id=kwargs.get('user_id'),
                emp_no=kwargs.get('emp_no'),
                user_nm=kwargs.get('user_nm'),
                user_pw=kwargs.get('user_pw'),
                brof_cd=kwargs.get('brof_cd'),
                det_cd=kwargs.get('det_cd'),
                enabled=kwargs.get('enabled'),
                authority=kwargs.get('authority'),
                aut_cd=kwargs.get('aut_cd'),
                admin_tlno=kwargs.get('admin_tlno'),
                ext_no=kwargs.get('ext_no'),
                host_nm=kwargs.get('host_nm'),
            )
        else:
            exc_info = traceback.format_exc()
            db.conn.rollback()
            raise Exception(exc_info)
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def update_usr_mgt_tb(**kwargs):
    """
    Update QA_USR_MGT_TB(사용자관리테이블)
    :param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_USR_MGT_TB
            SET
                EMP_NO = :1,
                USER_NM = :2,
                USER_PW = :3,
                BROF_CD = :4,
                DET_CD = :5,
                ENABLED = :6,
                AUTHORITY = :7,
                AUT_CD = :8,
                EXT_NO = :9,
                UPDATOR_ID = :10,
                UPDATED_TM = :11
            WHERE
                USER_ID = TO_CHAR(:12)
        """
        bind = (
            kwargs.get('emp_no'),
            kwargs.get('user_nm'),
            kwargs.get('user_pw'),
            kwargs.get('brof_cd'),
            kwargs.get('det_cd'),
            kwargs.get('enabled'),
            kwargs.get('authority'),
            kwargs.get('aut_cd'),
            kwargs.get('ext_no'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('user_id')
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def delete_usr_mgt_tb(db):
    """
    Delete QA_USR_MGT_TB(사용자관리테이블)
    @param      db:         DB object
    """
    try:
        query = """
            DELETE 
                STTOWN.QA_USR_MGT_TB
            WHERE 
                AUT_CD NOT IN ('ROLE_ADMIN', 'ROLE_ADMIN1')
        """
        db.check_alive()
        db.cursor.execute(query, )
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def insert_usr_mgt_tb(db, bind):
    """
    Insert into QA_USR_MGT_TB(사용자관리테이블)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_USR_MGT_TB
            (
                USER_ID,
                EMP_NO,
                USER_NM,
                USER_PW,
                BROF_CD,
                DET_CD,
                ENABLED,
                AUTHORITY,
                AUT_CD,
                ADMIN_TLNO,
                EXT_NO,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
                :1, :2, :3, :4, :5, 
                :6, :7, :8, :9, :10, 
                :11, :12, :13, :14, :15
            )
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def insert_real_cal_info(db, bind, plsql=False):
    """
    Insert STT QA_CAL_INFO_TB(콜정보테이블)
    @param      db:         DB object
    @param      bind:       Bind
    @param      plsql:      PLSQL flag
    """
    try:
        bind_str = 'pls_encrypt_b64(:31)' if plsql else ':31'
        query = """
            INSERT INTO STTOWN.QA_CAL_INFO_TB(
                REC_KEY,
                REC_ID,
                STT_REC_ID,
                STT_REC_FILE_NM,
                STT_REC_LOC_PATH,
                REC_COMP_CD,
                REC_BIZ_TYPE,
                PROG_STAT_CD,
                REC_ST_DT,
                REC_ST_TM,
                REC_FILE_EXT,
                STEREO_SPLIT_YN,
                CHN_CLA_CD,
                STT_PROC_SVR,
                STT_ST_TM,
                PROC_PRI,
                TL_CSMR_NO,
                CSMR_NM,
                SAES_CD,
                SAES_NM,
                HQT_CD,
                HQT_NM,
                BROF_CD,
                BROF_NM,
                DET_CD,
                CHN_CD,
                CHN_NM,
                EMP_NO,
                EMP_NM,
                MANAGER_HP,
                CSMR_TLNO,
                TL_CD,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            SELECT
                :1, :2, :3, :4, :5,
                :6, :7, :8, :9, :10, 
                :11, :12, :13, :14, :15, 
                :16, :17, :18, :19, :20,
                :21, :22, :23, :24, :25, 
                :26, :27, :28, :29, :30,
                {0}, :32, :33, :34, :35,
                :36
            FROM DUAL T1
            WHERE NOT EXISTS(
                SELECT
                    0
                FROM
                    STTOWN.QA_CAL_INFO_TB
                WHERE
                    REC_KEY = :37
            )
        """.format(bind_str)
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def insert_real_stt_rst(db, bind, plsql=False):
    """
    Insert STT result QA_STT_RST_TB(STT결과테이블)
    @param      db:        DB object
    @param      bind:      Bind
    @param      plsql:     PLSQL flag
    """
    try:
        bind_str = 'pls_encrypt_b64(:6)' if plsql else ':6'
        query = """
            INSERT INTO STTOWN.QA_STT_RST_TB
            (
                REC_KEY,
                STMT_NO,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                STMT,
                STT_YM,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1,
              (SELECT NVL(MAX(TO_NUMBER(STMT_NO)), 0) + 1 FROM STTOWN.QA_STT_RST_TB WHERE REC_KEY = :2),
              :3, :4, :5, {0}, 
              :7, :8, :9, :10, :11
            )
        """.format(bind_str)
        success_flag = False
        exc_info = str()
        for _ in range(3):
            try:
                db.check_alive()
                db.cursor.execute(query, bind)
                db.conn.commit()
                success_flag = True
                break
            except Exception:
                exc_info = traceback.format_exc()
        if not success_flag:
            raise Exception(exc_info)
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_stt_st_tm(db, rec_key):
    """
    Select STT_ST_TM from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY
    """
    try:
        query = """
            SELECT
                REC_ST_DT,
                REC_ST_TM
            FROM
                STTOWN.QA_CAL_INFO_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return dict()
        if not result:
            return dict()
        return result[0]
    except Exception:
        raise Exception(traceback.format_exc())


def update_real_cal_info(db, bind):
    """
    Update STT status from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:        DB object
    @param      bind:      Bind
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                PROG_STAT_CD = :1,
                REC_ED_TM = :2,
                REC_DU_TM = :3,
                STT_COM_TM = :4,
                LIS_REC_FILE_NM = :5,
                LIS_REC_LOC_PATH = :6,
                C_VOIC_SP = :7,
                A_VOIC_SP = :8,
                MAX_SILENCE = :9,
                UPDATOR_ID = :10,
                UPDATED_TM = :11
            WHERE
                REC_KEY = TO_CHAR(:12)
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_real_stt_results(db, ext_no):
    """
    Select STT results form QA_CAL_INFO_TB(콜정보테이블), QA_STT_RST_TB(STT결과테이블)
    @param      db:                 DB object
    @param      ext_no:             EXT_NO(내선번호)
    @return:                        STT results
    """
    try:
        query = """
            SELECT
                T2.TL_CSMR_NO,
                T2.CSMR_NM,
                T1.REC_KEY,
                T1.SPK_DIV_CD,
                T1.STMT_ST_TM,
                T1.STMT_ED_TM,
                pls_decrypt_b64(T1.STMT) AS STMT
            FROM 
                STTOWN.QA_STT_RST_TB T1,
                (
                    SELECT 
                        MAX(T3.TL_CSMR_NO) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS TL_CSMR_NO,
                        MAX(T3.CSMR_NM) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS CSMR_NM,
                        MAX(T3.REC_KEY) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS REC_KEY,
                        MAX(T3.REC_ST_TM) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS REC_ST_TM 
                    FROM 
                        STTOWN.QA_CAL_INFO_TB T3, 
                        STTOWN.QA_USR_MGT_TB T4
                    WHERE 
                        T4.EXT_NO = :1
                        AND T3.REC_COMP_CD = '100'
                        AND T3.REC_ST_TM IS NOT NULL
                        AND T3.EMP_NO = T4.USER_ID
                ) T2
            WHERE
                T1.REC_KEY = T2.REC_KEY
            ORDER BY
                TO_NUMBER(T1.STMT_NO)
        """
        bind = (
            ext_no,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_real_stt_results_by_rec_key(db, rec_key):
    """
    Select STT results form QA_STT_RST_TB(STT결과테이블)
    @param      db:                 DB object
    @param      rec_key:            REC_KEY
    @return:                        STT results
    """
    try:
        query = """
            SELECT
                REC_KEY,
                STT_YM,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                pls_decrypt_b64(STMT) AS STMT
            FROM 
                STTOWN.QA_STT_RST_TB
            WHERE
                REC_KEY = :1
            ORDER BY
                STMT_ST_TM
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        # result = rows_to_dict_list(db)
        result = db.cursor.fetchall()
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def insert_real_stt_rst_list(db, bind_list, plsql=False):
    """
    Insert STT result QA_STT_RST_TB(STT결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    @param      plsql:          PLSQL flag
    """
    try:
        bind_str = 'pls_encrypt_b64(:6)' if plsql else ':6'
        query = """
            INSERT INTO STTOWN.QA_STT_RST_TB
            (
                REC_KEY,
                STMT_NO,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                STMT,
                STT_YM,
                SILENCE,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5, 
              {0}, :7, :8, :9, :10, 
              :11, :12
            )
        """.format(bind_str)
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def select_qa_cal_existed(db, rec_id, rec_file_nm):
    """
    Check QA target existed from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                     DB object
    @param      rec_id:                 REC_ID(녹취ID)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            SELECT
                PROG_STAT_CD
            FROM
                STTOWN.QA_CAL_INFO_TB
            WHERE
                REC_ID = TO_CHAR(:1)
                AND REC_FILE_NM = TO_CHAR(:2)
        """
        bind = (
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_stt_in_progress(db, host_nm):
    """
    Update STT in progress from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                 DB object
    @param      host_nm:            Hostname
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                PROG_STAT_CD = '10'
            WHERE
                PROG_STAT_CD = '12'
                AND REC_COMP_CD != '100'
                AND UPDATOR_ID = :1
        """
        bind = (
            host_nm,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def update_qa_in_progress(db, host_nm):
    """
    Update TA QA in progress from QA_CTR_LIST_TB(계약리스트테이블)
    @param      db:                 DB object
    @param      host_nm:            Hostname
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CTR_LIST_TB
            SET
                PROG_STAT_CD = '20'
            WHERE
                PROG_STAT_CD = '22'
                AND UPDATOR_ID = :1
        """
        bind = (
            host_nm,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def update_jp_in_progress(db, host_nm):
    """
    Update TA JoinPlan in progress from QA_JOIN_PLN_LIST_TB(가입설계리스트)
    @param      db:                 DB object
    @param      host_nm:            Hostname
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_JOIN_PLN_LIST_TB
            SET
                PROG_STAT_CD = '30'
            WHERE
                PROG_STAT_CD = '32'
                AND UPDATOR_ID = :1
        """
        bind = (
            host_nm,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_real_qa_results(db, ext_no):
    """
    Select STT results from QA_REAL_RST_TB(실시간 평가 결과 테이블)
    @param      db:             DB object
    @param      ext_no:         EXT_NO(내선번호)
    @return:                    QA results
    """
    try:
        query = """
            SELECT
                T2.TL_CSMR_NO,
                T2.CSMR_NM,
                T1.REC_KEY,
                T1.PROD_CAT,
                T1.CHK_APP_ORD,
                T1.CHK_ITM_CD,
                T1.SCRP_MCTG_NM,
                T1.SCRP_SCTG_NM,
                T1.PSN_JUG_CAT,
                T1.DTCT_RST
            FROM 
                STTOWN.QA_REAL_RST_TB T1,
                (
                    SELECT 
                        MAX(T3.TL_CSMR_NO) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS TL_CSMR_NO,
                        MAX(T3.CSMR_NM) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS CSMR_NM,
                        MAX(T3.REC_KEY) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS REC_KEY,
                        MAX(T3.REC_ST_TM) KEEP (DENSE_RANK FIRST ORDER BY T3.REC_ST_TM DESC) AS REC_ST_TM 
                    FROM 
                        STTOWN.QA_CAL_INFO_TB T3, 
                        STTOWN.QA_USR_MGT_TB T4
                    WHERE
                        T4.EXT_NO = :1
                        AND T3.REC_COMP_CD = '100'
                        AND T3.REC_ST_TM IS NOT NULL
                        AND T3.EMP_NO = T4.USER_ID
                ) T2,
                STTOWN.QA_CHK_ITM_TB T5 
            WHERE
                T1.REC_KEY = T2.REC_KEY
                AND T1.PROD_CAT = T5.PROD_CAT
                AND T1.CHK_APP_ORD = T5.CHK_APP_ORD
                AND T1.CHK_ITM_CD = T5.CHK_ITM_CD
            ORDER BY
                T5.CHK_NO
        """
        bind = (
            ext_no,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_qa_real_rst_tb(db, bind):
    """
    Update Detect Result from QA_REAL_RST_TB(가입설계리스트)
    @param      db:         DB object
    @param      bind:       Bind
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_REAL_RST_TB
            SET
                DTCT_RST = :1,
                UPDATOR_ID = :2,
                UPDATED_TM = :3
            WHERE
                REC_KEY = :4
                AND PROD_CAT = :5
                AND CHK_APP_ORD = :6
                AND CHK_ITM_CD = :7
                AND PSN_JUG_CAT = :8
        """
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def update_real_stt(db, rec_key, updated_tm):
    """
    Update STT PROG_STAT_CD from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:             DB object
    @param      rec_key:        REC_KEY(녹취KEY)
    @param      updated_tm:     UPDATED_TM
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                PROG_STAT_CD = '10',
                UPDATED_TM = :1
            WHERE
                REC_KEY = TO_CHAR(:2)
        """
        bind = (
            updated_tm,
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def delete_qa_cal_info(db, rec_key):
    """
    Delete STT record from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:                     DB object
    @param      rec_key:                REC_KEY(녹취KEY)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_CAL_INFO_TB
            WHERE
                REC_KEY = TO_CHAR(:1)
        """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def update_qa_cal_info(db, rec_id, rec_file_nm, rec_biz_cd, tl_cd):
    """
    Update STT REC_BIZ_CD(), TL_CD() from QA_CAL_INFO_TB(콜정보테이블)
    @param      db:             DB object
    @param      rec_id:         REC_ID(녹취ID)
    @param      rec_file_nm:    REC_FILE_NM(녹취파일명)
    @param      rec_biz_cd:     REC_BIZ_CD(녹취업무코드)
    @param      tl_cd:          TL_CD(전화구분)
    """
    try:
        query = """
            UPDATE
                STTOWN.QA_CAL_INFO_TB
            SET
                REC_BIZ_CD = :1,
                TL_CD = :2
            WHERE
                REC_ID = TO_CHAR(:3)
                AND REC_FILE_NM = TO_CHAR(:4)
        """
        bind = (
            rec_biz_cd,
            tl_cd,
            rec_id,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# upload_stt_daemon.py
def select_upload_stt_target(db):
    """
    Select STT target list from QA_UPLD_CAL_INFO_TB(STT변환테이블)
    @param      db:             DB object
    @return:                    STT target list
    """
    try:
        query = """
            SELECT
                T1.EMP_NO,
                T1.ALL_SEQ,
                T1.REC_FILE_NM,
                T1.REC_LOC_PATH,
                T1.REQ_TM
            FROM
                STTOWN.QA_UPLD_CAL_INFO_TB T1
            WHERE
                T1.PROG_STAT_CD = '40'
                AND (SELECT  COUNT(*) FROM STTOWN.QA_UPLD_CAL_INFO_TB WHERE T1.EMP_NO = EMP_NO AND PROG_STAT_CD = '42') < 1
                AND T1.ALL_SEQ = (SELECT MIN(ALL_SEQ) FROM STTOWN.QA_UPLD_CAL_INFO_TB WHERE PROG_STAT_CD = '40' AND T1.EMP_NO = EMP_NO)
                AND ROWNUM <= 5
            ORDER BY
                T1.REQ_TM
            FOR UPDATE SKIP LOCKED
        """
        db.check_alive()
        db.cursor.execute(query, )
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def update_upload_stt_status(**kwargs):
    """
    Update STT status from QA_UPLD_CAL_INFO_TB(STT변환테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_UPLD_CAL_INFO_TB
            SET
                STT_PROC_SVR = :1,
                PROG_STAT_CD = :2,
                PROG_STAT_DTL_CD = :3,
                UPDATOR_ID = :4,
                UPDATED_TM = :5
            WHERE
                EMP_NO = TO_CHAR(:6)
                AND ALL_SEQ = TO_CHAR(:7)
                AND REC_FILE_NM = TO_CHAR(:8)
        """
        bind = (
            kwargs.get('host_nm'),
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('emp_no'),
            kwargs.get('all_seq'),
            kwargs.get('rec_file_nm'),
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        # db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


# upload_stt_process.py
def update_upload_stt_status_commit(**kwargs):
    """
    Update STT status from QA_UPLD_CAL_INFO_TB(STT변환테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_UPLD_CAL_INFO_TB
            SET
                STT_PROC_SVR = :1,
                PROG_STAT_CD = :2,
                PROG_STAT_DTL_CD = :3,
                UPDATOR_ID = :4,
                UPDATED_TM = :5
            WHERE
                EMP_NO = TO_CHAR(:6)
                AND ALL_SEQ = TO_CHAR(:7)
                AND REC_FILE_NM = TO_CHAR(:8)
        """
        bind = (
            kwargs.get('host_nm'),
            kwargs.get('prog_stat_cd'),
            kwargs.get('prog_stat_dtl_cd'),
            kwargs.get('host_nm'),
            datetime.fromtimestamp(time.time()),
            kwargs.get('emp_no'),
            kwargs.get('all_seq'),
            kwargs.get('rec_file_nm'),
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_upload_stt_results_existed(db, emp_no, all_seq, rec_file_nm):
    """
    Check STT results existed from QA_UPLD_STT_RST_TB(STT변환결과테이블)
    @param      db:                     DB object
    @param      emp_no:                 EMP_NO(사원번호)
    @param      all_seq:                ALL_SEQ(전체순번)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            SELECT
                EMP_NO
            FROM
                STTOWN.QA_UPLD_STT_RST_TB
            WHERE
                EMP_NO = TO_CHAR(:6)
                AND ALL_SEQ = TO_CHAR(:7)
                AND REC_FILE_NM = TO_CHAR(:8)
        """
        bind = (
            emp_no,
            all_seq,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return False
        if not result:
            return False
        return True
    except Exception:
        raise Exception(traceback.format_exc())


def delete_upload_stt_results(db, emp_no, all_seq, rec_file_nm):
    """
    Delete STT results from QA_UPLD_STT_RST_TB(STT변환결과테이블)
    @param      db:                     DB object
    @param      emp_no:                 EMP_NO(사원번호)
    @param      all_seq:                ALL_SEQ(전체순번)
    @param      rec_file_nm:            REC_FILE_NM(녹취파일명)
    """
    try:
        query = """
            DELETE
                STTOWN.QA_UPLD_STT_RST_TB
            WHERE
                EMP_NO = TO_CHAR(:6)
                AND ALL_SEQ = TO_CHAR(:7)
                AND REC_FILE_NM = TO_CHAR(:8)
        """
        bind = (
            emp_no,
            all_seq,
            rec_file_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def insert_upload_stt_result(db, bind_list):
    """
    Insert STT result QA_UPLD_STT_RST_TB(STT변환결과테이블)
    @param      db:             DB object
    @param      bind_list:      Bind list
    """
    try:
        query = """
            INSERT INTO STTOWN.QA_UPLD_STT_RST_TB
            (
                EMP_NO,
                ALL_SEQ,
                REC_FILE_NM,
                STMT_NO,
                SPK_DIV_CD,
                STMT_ST_TM,
                STMT_ED_TM,
                STMT,
                SILENCE,
                UPDATOR_ID,
                UPDATED_TM,
                CREATOR_ID,
                CREATED_TM
            )
            VALUES
            (
              :1, :2, :3, :4, :5, 
              :6, :7, :8, :9, :10,
              :11, :12, :13
            )
        """
        db.check_alive()
        db.cursor.executemany(query, bind_list)
        db.conn.commit()
    except Exception:
        exc_info = traceback.format_exc()
        db.conn.rollback()
        raise Exception(exc_info)


def update_upload_stt_status_and_info(**kwargs):
    """
    Update STT status and information QA_UPLD_CAL_INFO_TB(STT변환테이블)
    @param      kwargs:     Arguments
    """
    db = kwargs.get('db')
    try:
        query = """
            UPDATE
                STTOWN.QA_UPLD_CAL_INFO_TB
            SET
                PROG_STAT_CD = :1,
                CHN_CLA_CD = :2,
                REC_DU_TM= :3,
                STT_PROC_SP = :4,
                STT_ST_TM = :5,
                STT_COM_TM = :6,
                C_VOIC_SP = :7,
                A_VOIC_SP = :8,
                MAX_SILENCE = :9,
                UPDATOR_ID = :10,
                UPDATED_TM = :11
            WHERE
                EMP_NO = TO_CHAR(:12)
                AND ALL_SEQ = TO_CHAR(:13)
                AND REC_FILE_NM = TO_CHAR(:14)
        """
        bind = (
            kwargs.get('prog_stat_cd'),
            kwargs.get('chn_cla_cd'),
            kwargs.get('record_duration'),
            kwargs.get('stt_proc_sp'),
            kwargs.get('stt_st_tm'),
            kwargs.get('stt_com_tm'),
            kwargs.get('c_voic_sp'),
            kwargs.get('a_voic_sp'),
            kwargs.get('max_silence'),
            kwargs.get('updator_id'),
            kwargs.get('updated_tm'),
            kwargs.get('emp_no'),
            kwargs.get('all_seq'),
            kwargs.get('rec_file_nm'),
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        db.conn.commit()
    except Exception:
        db.conn.rollback()
        raise Exception(traceback.format_exc())


def select_ela_stt_target(db, st_dt, ed_dt, host_nm):
    """
    Select STT result from QA_CTR_LIST_TB(계약 리스트 테이블),QA_CTR_CAL_REL_TB(계약 콜 관계 테이블),
                            QA_CAL_INFO_TB(콜정보테이블),QA_STT_RST_TB(STT 결과 테이블)
    @param      db:             DB object
    @param      st_dt:          Search start date
    @param      ed_dt:          Search end date
    @param      host_nm:        Host name
    @return:
    """
    try:
        query = """
            SELECT
                T2.REC_KEY,     -- 녹취키
                T3.SAES_CD,     -- 본부명
                T3.SAES_NM,     -- 본부코드
                T3.CHN_CD,      -- 채널코드
                T3.CHN_NM,      -- 채널명
                T1.BROF_CD,     -- 지점코드
                T1.BROF_NM,     -- 지점명
                T1.EMP_NM,      -- 사원명
                T1.EMP_NO,      -- 사원번호
                T1.IP_CSMR_NO,  -- 고객관리번호(I)
                T1.NSPL_NO,     -- 계약번호
                T1.TMS_INFO,    -- 회차정보
                T1.CSMR_NM,     -- 고객성명(계약자)
                T1.PNM,         -- 피보험자명
                T1.PROD_CD,     -- 상품코드
                T1.PROD_NM,     -- 상품명
                T1.CONTRACT_DT, -- 청약일자
                T3.REC_BIZ_CD,  -- 녹취종류
                T3.REC_ST_DT,   -- 녹취일시
                T2.REC_FILE_NM, -- 녹취파일명
                T4.SPK_DIV_CD,  -- 화자구분코드
                T4.STMT_NO,     -- 문장번호
                T4.STMT         -- 원문장
            FROM
                STTOWN.QA_CTR_LIST_TB T1,
                STTOWN.QA_CTR_CAL_REL_TB T2,
                STTOWN.QA_CAL_INFO_TB T3,
                STTOWN.QA_STT_RST_TB T4
            WHERE
                T1.UPDATED_TM BETWEEN :1 AND :2
                AND T1.TA_PROC_SVR = :3
                AND T1.PROG_STAT_CD IN ('23', '29') -- 점검완료 기준
                AND T1.NSPL_NO = T2.NSPL_NO
                AND T1.TMS_INFO = T2.TMS_INFO
                AND T2.REC_KEY = T3.REC_KEY
                AND T3.REC_KEY = T4.REC_KEY
            ORDER BY
                T1.NSPL_NO, T1.TMS_INFO, T2.REC_FILE_NM, ABS(T4.STMT_NO)
        """
        bind = (
            st_dt,
            ed_dt,
            host_nm
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_sms_info(db, saes_emno):
    """
    Select Information for SMS from QA_USR_MGT_TB(사용자 관리 테이블),QA_SMS_YN_TB(실SNS 발송여부 테이블)
    @param      db:             DB object
    @param      saes_emno:      SAES EMNO 사원번호
    @return:
    """
    try:
        query = """
                SELECT
                    T2.USER_NM,     -- 사원번호
                    T2.ADMIN_TLNO   -- 관리자 번호
                FROM
                    STTOWN.QA_SMS_YN_TB T1,
                    STTOWN.QA_USR_MGT_TB T2
                WHERE
                    T1.BROF_CD = T2.BROF_CD
                    AND T1.DET_CD = T2.DET_CD
                    AND T1.SNS_YN = 'Y'
                    AND T2.EMP_NO = :1
            """
        bind = (
            saes_emno,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = rows_to_dict_list(db)
        if result is bool:
            return list()
        if not result:
            return list()
        return result
    except Exception:
        raise Exception(traceback.format_exc())


def select_stt_fir_line(db, rec_key):
    """
    Select STT first line from QA_STT_RST_TB(STT결과테이블)
    @param      db:           DB object
    @param      rec_key:      REC_KEY(녹취KEY)
    @return:
    """
    try:
        query = """
            SELECT
                STMT_ST_TM
            FROM 
                STTOWN.QA_STT_RST_TB
            WHERE 
                REC_KEY = TO_CHAR(:1)
		        AND ROWNUM = 1
            ORDER BY
                TO_NUMBER(STMT_NO)
            """
        bind = (
            rec_key,
        )
        db.check_alive()
        db.cursor.execute(query, bind)
        result = db.cursor.fetchone()
        if result is bool:
            return None
        if not result:
            return None
        return result[0]
    except Exception:
        raise Exception(traceback.format_exc())
