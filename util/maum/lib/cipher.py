#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-05-11, modification: 0000-00-00"

###########
# imports #
###########
import sys
import base64
import hashlib
import argparse
import subprocess
from Crypto import Random
from Crypto.Cipher import AES

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class AESCipher(object):
    def __init__(self, conf):
        self.bs = 16
        self.conf = conf
        self.key = self.openssl_dec()
        self.salt = 'anySaltYouCanUse0f0n'
        # self.private_key = self.get_private_key(self.key, self.salt)
        self.private_key = self.key

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[0:-ord(s[-1:])]

    @staticmethod
    def get_private_key(secret_key, salt):
        return hashlib.pbkdf2_hmac('SHA256', secret_key.encode(), salt.encode(), 65536, 32)

    def encrypt(self, message):
        message = self._pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.private_key, AES.MODE_CBC, iv)
        cipher_bytes = base64.b64encode(iv + cipher.encrypt(message))
        return bytes.decode(cipher_bytes)

    def decrypt(self, encoded):
        cipher_text = base64.b64decode(encoded)
        iv = cipher_text[:AES.block_size]
        cipher = AES.new(self.private_key, AES.MODE_CBC, iv)
        plain_bytes = self._unpad(cipher.decrypt(cipher_text[self.bs:]))
        return bytes.decode(plain_bytes)

    def decrypt_hk(self, encoded):
        cipher_text = base64.b64decode(encoded)
        iv = self.private_key[:AES.block_size]
        cipher = AES.new(self.private_key, AES.MODE_CBC, iv)
        decrypted_text = self._unpad(cipher.decrypt(cipher_text))
        return decrypted_text.decode('utf-8')

    def is_encrypt(self, message):
        try:
            self.decrypt(message)
            return True
        except Exception:
            return False

    @staticmethod
    def sub_process(cmd):
        sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        response_out, response_err = sub_pro.communicate()
        return response_out, response_err

    def openssl_dec(self):
        cmd = "openssl enc -seed -d -a -in {0} -pass file:{1}".format(self.conf.pd, self.conf.ps_path)
        std_out, std_err = self.sub_process(cmd)
        return std_out.strip()


if __name__ == '__main__':
    class AESConfig(object):
        pd = '/APP/maum/cfg/.aes'
        ps_path = '/APP/maum/cfg/.heungkuk'
    temp = AESCipher(AESConfig)
    test_str = '안녕하세요 상담원 홍길동입니다.'
    enc_str = temp.encrypt(test_str)
    print(enc_str)
    dec_str = temp.decrypt(enc_str)
    print(dec_str)
    sample_str = temp.decrypt_hk('4GIyO5S6Bgd7Clqbysi7WQ3D2YcB9StsBhTCeyHN0/Q=')
    print(sample_str)
