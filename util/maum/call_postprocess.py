#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-18, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import ssl
import json
import time
import socket
import requests
import websocket
import traceback
import collections
import tornado.web
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.websocket
import tornado.httpserver
from datetime import datetime, timedelta
from tornado.options import define, options
from cfg import config
from lib import logger, util, py3_db_connection, py3_nlp, py3_hmd, py3_change_arabic


#############
# constants #
#############
SCRIPT_TARGET_DICT = dict()
CONF = config.PostWsSvrConfig
HOST_NM = socket.gethostname()
HMD_CLIENT = py3_hmd.HmdClient()
NLP_CLIENT = py3_nlp.NlpClient(CONF.nlp_engine)
define("port", default=int(CONF.port), help="run on the given port", type=int)
WS = websocket.create_connection(CONF.ws_addr_list[HOST_NM], setdefaulttimeout=3)
DB = py3_db_connection.Oracle(config.OracleConfig, failover=True, service_name=True)
LOGGER = logger.get_timed_rotating_logger(
    logger_name=CONF.logger_name,
    log_dir_path=CONF.log_dir_path,
    log_file_name=CONF.log_file_name,
    backup_count=CONF.backup_count,
    log_level=CONF.log_level
)


#########
# class #
#########
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/postsocket", CallSocketHandler)]
        settings = dict(debug=True)
        super(Application, self).__init__(handlers, **settings)


class CallSocketHandler(tornado.websocket.WebSocketHandler):
    # Key: agent, value: socketHandler
    waiters = set()
    sessions = dict()
    def check_origin(self, origin):
        return True

    def open(self):
        self.agent = None
        LOGGER.info("Client connected")
        CallSocketHandler.waiters.add(self)

    def on_close(self):
        LOGGER.info("on_close")
        if self.agent and self.agent in CallSocketHandler.sessions:
            CallSocketHandler.sessions[self.agent].remove(self)
            LOGGER.info("unsubscribe {0}".format(self.agent))
        CallSocketHandler.waiters.remove(self)

    def on_message(self, message):
        try:
            data = json.loads(message)
            data = dict((k.lower(), v) for k, v in data.items())
            self.event_process(data)
        except Exception:
            LOGGER.error(traceback.format_exc())
        return

    @staticmethod
    def send_ws(message):
        WS.send(message)

    @staticmethod
    def petra_enc(target_sent):
        try:
            req_host = "http://{0}:{1}/encrypt_string".format(socket.gethostbyname(HOST_NM), 8788)
            payload = {'string': target_sent}
            req_result = requests.post(req_host, data=payload)
            response_out = json.loads(req_result.text)
            if response_out['code'] == 0:
                return response_out['rst']
            else:
                return False
        except Exception:
            LOGGER.error(traceback.format_exc())
            return False

    @staticmethod
    def petra_dec(target_sent):
        try:
            req_host = "http://{0}:{1}/decrypt_string".format(socket.gethostbyname(HOST_NM), 8788)
            payload = {'string': target_sent}
            req_result = requests.post(req_host, data=payload)
            response_out = json.loads(req_result.text)
            if response_out['code'] == 0:
                return response_out['rst']
            else:
                return False
        except Exception:
            LOGGER.error(traceback.format_exc())
            return False

    @staticmethod
    def extract_speech_speed(start_time, end_time, sent):
        sntc_len = len(sent.replace(' ', ''))
        during_time = (end_time / 8000.0) - (start_time / 8000.0)
        if during_time > 0:
            speech_speed = round(float(sntc_len) / during_time, 2)
        else:
            speech_speed = 0
        if speech_speed > 999:
            speech_speed = 0.0
        return speech_speed

    @staticmethod
    def extract_speech_time(seconds):
        time_list = str(timedelta(seconds=float(seconds))).split(':')
        hh = ('0' + time_list[0])[-2:]
        mm = time_list[1]
        ss = '%.2f' % float(time_list[2])
        ss = ss.split('.')[0]
        ss = ss if len(ss) == 2 else '0' + ss
        return ':'.join([hh, mm, ss])

    @staticmethod
    def make_target_line(end_idx, line_gap, dtc_line_list):
        if '1' not in dtc_line_list:
            return list()
        dtc_list = list()
        start_line_list = dtc_line_list['1']
        for line_num in start_line_list:
            temp_dict = dict()
            temp_dict['1'] = [line_num]
            for idx in range(1, end_idx):
                temp_list = list()
                if str(idx) not in temp_dict:
                    continue
                for num in temp_dict[str(idx)]:
                    for cnt in range(0, line_gap + 1):
                        next_line_num = int(num) + cnt
                        if str(idx + 1) not in dtc_line_list:
                            return list()
                        if next_line_num in dtc_line_list[str(idx + 1)]:
                            if next_line_num not in temp_list:
                                temp_list.append(next_line_num)
                            temp_dict[str(idx + 1)] = temp_list
            if len(temp_dict) != end_idx:
                continue
            dtc_list.append(temp_dict)
        output_list = list()
        for item_dict in dtc_list:
            for line_list in item_dict.values():
                for line_number in line_list:
                    if line_number not in output_list:
                        output_list.append(line_number)
        return output_list

    @staticmethod
    def sending_sms(sorted_dtct_kwd_list, extra):
        LOGGER.info('\t\t   -- Sending SMS')
        host = config.SMSConfig.host
        dt = datetime.now().strftime("%Y%m%d%H%M%S%f")
        header_boundary = '{0:->37}'.format(dt)
        body_start_boundary = '{0:->39}'.format(dt)
        body_end_boundary = '{0:-<41}'.format(body_start_boundary)
        headers = {
            'Accept-Language': 'ko',
            'ContentType': 'multipart/form-data',
            'Content-Type': 'multipart/form-data;boundary={0}'.format(header_boundary)
        }
        adrs_nm = u'HKSTT'
        # EMP_NO로 조회
        results = util.select_sms_info(DB, extra['body']['SAES_ENMO'].strip())
        if not results:
            return
        result = str()
        for result_dict in results:
            recr_nm = 'STT시스템'
            recr_brnu = ''
            dec_flag = CallSocketHandler.petra_dec(result_dict['ADMIN_TLNO'])
            dec_admin_tlno = dec_flag if dec_flag else result_dict['ADMIN_TLNO']
            recr_tlno = dec_admin_tlno if dec_admin_tlno else str()
            msg = str()
            msg += '[STT 안내]\n'
            msg += '사원: {0}\n'.format(result_dict['USER_NM'].strip())
            msg += '고객: {0}\n'.format(extra['body']['CSMR_NM'].strip())
            msg += '금지어: {0} 외 {1}건\n'.format(sorted_dtct_kwd_list[0][0], len(sorted_dtct_kwd_list))
            msg = msg.replace("!@#$", ", ")
            format_setting = collections.OrderedDict({
                # header (채널공통헤더)
                'MSGLENGTH': {'length': 8, 'value': 0},
                'DATE': {'length': 8, 'value': datetime.now().strftime("%Y%m%d")},
                'BOCID': {'length': 10, 'value': str()},
                'RESCODE': {'length': 1, 'value': str()},
                'CSMSGSEQ': {'length': 10, 'value': str()},
                'SOURCE': {'length': 8, 'value': 'HKSTT'},
                'TARGET': {'length': 8, 'value': 'HKUMS'},
                'SOURCEMSGSEQ': {'length': 10, 'value': str()},
                'SOURCEREQTIME': {'length': 15, 'value': datetime.now().strftime("%Y%m%d%H%M%S")},
                'TARGETRESTIME': {'length': 15, 'value': datetime.now().strftime("%Y%m%d%H%M%S")},
                'TARGETPGM': {'length': 8, 'value': 'HKUMS'},
                'COMMITFLAG': {'length': 1, 'value': '1'},
                'TRANFLAG': {'length': 1, 'value': 'T'},
                'CSERRORCODE': {'length': 4, 'value': str()},
                'TESTORREAL': {'length': 1, 'value': 'T'},
                'USERID': {'length': 13, 'value': 'HKSTT'},
                'USERIP': {'length': 15, 'value': socket.gethostbyname(socket.gethostname())},
                'VERSION': {'length': 3, 'value': str()},
                'NOTIFY': {'length': 1, 'value': '1'},
                'EXTERNAL': {'length': 1, 'value': 'N'},
                'FILLER': {'length': 9, 'value': str()},
                # body
                'Authkey': {'length': 36, 'value': '135694A9-C357-A15E-B50T-1TAFS2F45F5A'},
                'UMS_GB': {'length': 1, 'value': '2'},
                'CHANNEL_GB': {'length': 2, 'value': 'TM'},
                'CHANNEL_KEY_NO': {'length': 20, 'value': str()},
                'UMS_INFO_CODE': {'length': 3, 'value': '310'},
                'EMPN': {'length': 15, 'value': str()},
                'UMS_GUID_ID': {'length': 14, 'value': str()},
                'UMS_GUID_ID_GB': {'length': 2, 'value': str()},
                'UMS_REF_NO_GB': {'length': 2, 'value': str()},
                'UMS_REF_NO': {'length': 20, 'value': str()},
                'UMS_ISUE_YMD': {'length': 8, 'value': datetime.now().strftime("%Y%m%d")},
                'UMS_GBK_KND': {'length': 4, 'value': '1310'},
                'ADRS_NM_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(adrs_nm.encode('euc-kr')))},
                'ADRM_NM': {'length': len(adrs_nm), 'value': adrs_nm},
                'ADRS_TLNO': {'length': 15, 'value': '15882288'},
                'RECR_NM_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_nm.encode('euc-kr')))},
                'RECR_NM': {'length': len(recr_nm), 'value': recr_nm},
                'RECR_BRNU_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_brnu))},
                'RECR_BRNU': {'length': len(recr_brnu), 'value': recr_brnu},
                'RECR_TLNO_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_tlno))},
                'RECR_TLNO': {'length': len(recr_tlno), 'value': recr_tlno},
                'MSG_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(msg.encode('euc-kr')))},
                'MSG': {'length': len(msg.encode('euc-kr')), 'value': msg},
            })
            total_msg = str()
            for key, value_dict in format_setting.items():
                if key == 'MSG':
                    total_msg += value_dict['value']
                    continue
                total_msg = '{0}{1:{2}<{3}}'.format(total_msg, value_dict['value'], ' ', value_dict['length'])
            msglength_length = format_setting['MSGLENGTH']['length']
            format_setting['MSGLENGTH']['value'] = '{0:0>{1}}'.format(len(total_msg), msglength_length)
            total_msg = str()
            for key, value_dict in format_setting.items():
                if key == 'MSG':
                    total_msg += value_dict['value']
                    continue
                total_msg = '{0}{1:{2}<{3}}'.format(total_msg, value_dict['value'], ' ', value_dict['length'])
            body_list = list()
            body_list.append(body_start_boundary)
            body_list.append('Content-Disposition: form-data; name="data"')
            body_list.append('Content-Type: text/plain; charset=EUC-KR')
            body_list.append('Content-Transfer-Encoding: 8bit\n')
            body_list.append(total_msg)
            body_list.append(body_end_boundary)
            body = '\n'.join(body_list)
            body = body.encode('euc-kr')
            result = requests.post(host, data=body, headers=headers)
        LOGGER.info('Sending Success')
        return result

    def insert_ta_output(self, rec_key, output_list, extra):
        LOGGER.info('\t  -- 4) Insert HMD output ...')
        all_seq = 1
        bind_list = list()
        plsql_bind_list = list()
        if not output_list:
            return
        del_flag = util.select_ta_results_existed(DB, rec_key)
        if del_flag:
            util.delete_ta_results(DB, rec_key)
        dtct_kwd_dict = dict()
        for item in output_list:
            if item['SCRP'] not in dtct_kwd_dict:
                dtct_kwd_dict[item['SCRP']] = int()
            dtct_kwd_dict[item['SCRP']] += 1
            enc_flag = self.petra_enc(item['DTCT_STMT'])
            enc_dtct_stmt = enc_flag if enc_flag else item['DTCT_STMT']
            bind = (
                item['REC_KEY'],
                item['STMT_NO'],
                all_seq,
                item['SPK_DIV_CD'],
                'L',
                item['CHK_APP_ORD'],
                item['CHK_ITM_CD'],
                item['CUSL_CD'],
                item['CUSL_APP_ORD'],
                item['STMT_ST_TM'],
                item['STMT_ED_TM'],
                enc_dtct_stmt,
                item['DTCT_DTC_NO'],
                item['DTCT_KWD'],
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                HOST_NM,
                datetime.fromtimestamp(time.time())
            )
            if enc_flag:
                bind_list.append(bind)
            else:
                plsql_bind_list.append(bind)
            all_seq += 1
        if bind_list:
            util.insert_ta_result(DB, bind_list)
        if plsql_bind_list:
            util.insert_ta_result(DB, plsql_bind_list, True)
        try:
            sorted_dtct_kwd_list = sorted(dtct_kwd_dict.items(), key=(lambda x:x[1]), reverse=True)
            self.sending_sms(sorted_dtct_kwd_list, extra)
        except Exception:
            LOGGER.error('SMS sending Error')

    @staticmethod
    def extract_ta_output(hmd_output_list, cate_check_dict):
        LOGGER.info('\t  -- 3) Extract HMD output ...')
        # 탐지구간정보 추출
        output_list = list()
        for item, dtc_cat_result in hmd_output_list:
            """
            item = {
                'REC_KEY'           : 녹취KEY
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'nlp_sent'          : 형태소 분석 문장 
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
            """
            if dtc_cat_result:
                for dtc_cate, value_list in dtc_cat_result.items():
                    dtc_cate_list = dtc_cate.split('!@#$')
                    """
                    dtc_cate_list = [
                        'CHK_ITM_CD',           평가항목코드
                        'CHK_APP_ORD',          항목적용차수
                        'CUSL_CD',              상담코드
                        'CUSL_APP_ORD',         상담적용차수
                        'DTCT_DTC_NO',          탐지사전번호
                        'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                        'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                        'DTCT_DTC_ED_NO',       탐지사전끝번호
                        'SNS_YN',               SMS전송여부
                        'SCRP',                 스크립트
                    ]
                    """
                    chk_itm_cd = dtc_cate_list[0]
                    chk_app_ord = dtc_cate_list[1]
                    cusl_cd = dtc_cate_list[2]
                    cusl_app_ord = dtc_cate_list[3]
                    dtct_dtc_no = dtc_cate_list[4]
                    dtct_dtc_grp_no = dtc_cate_list[5]
                    scrp = dtc_cate_list[9]
                    check_key = (
                        chk_itm_cd,
                        chk_app_ord,
                        cusl_cd,
                        cusl_app_ord,
                        dtct_dtc_no,
                        dtct_dtc_grp_no
                    )
                    # 탐지 된 키워드
                    dtct_kwd_list = list()
                    for value in value_list:
                        dtct_kwd = value[0]
                        if dtct_kwd not in dtct_kwd_list:
                            dtct_kwd_list.append(dtct_kwd)
                    tmp_dtct_kwd = ','.join(dtct_kwd_list)
                    if len(tmp_dtct_kwd.encode('utf-8')) > 300:
                        output_dtct_kwd = dtct_kwd_list[0]
                    else:
                        output_dtct_kwd = tmp_dtct_kwd
                    if check_key in cate_check_dict:
                        if item['STMT_NO'] in cate_check_dict[check_key]:
                            tmp_dict = dict()
                            tmp_dict['REC_KEY'] = item['REC_KEY']
                            tmp_dict['STMT_NO'] = item['STMT_NO']
                            tmp_dict['SPK_DIV_CD'] = item['SPK_DIV_CD']
                            tmp_dict['STMT_ST_TM'] = item['STMT_ST_TM']
                            tmp_dict['STMT_ED_TM'] = item['STMT_ED_TM']
                            tmp_dict['DTCT_STMT'] = item['STMT']
                            tmp_dict['CHK_ITM_CD'] = chk_itm_cd
                            tmp_dict['CHK_APP_ORD'] = chk_app_ord
                            tmp_dict['CUSL_CD'] = cusl_cd
                            tmp_dict['CUSL_APP_ORD'] = cusl_app_ord
                            tmp_dict['DTCT_DTC_NO'] = dtct_dtc_no
                            tmp_dict['DTCT_KWD'] = output_dtct_kwd
                            tmp_dict['SCRP'] = scrp
                            output_list.append(tmp_dict)
                return output_list

    def execute_hmd(self, rec_key, nlp_output_list, extra):
        LOGGER.info('\t  -- 2) HMD analyzing ...')
        hmd_output_list = list()
        hmd_model = HMD_CLIENT.load_hmd_model("!@#$", 'common_fwd')
        for item in nlp_output_list:
            """
            item = {
                'REC_KEY'           : 녹취KEY
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'SILENCE'           : 묵음시간
                'nlp_sent'          : 형태소 분석 문장 
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
            """
            try:
                if item['SPK_DIV_CD'].upper() == 'C':
                    continue
                dtc_cat_result = HMD_CLIENT.execute_hmd(item['STMT'], item['nlp_sent'], hmd_model)
            except Exception:
                LOGGER.error(traceback.format_exc())
                LOGGER.error("Can't analyze HMD Sentence -> {0}".format(item['STMT']))
                continue
            hmd_output_list.append((item, dtc_cat_result))
        dtc_cate_dict = dict()
        cate_idx_dict = dict()
        for item, dtc_cat_result in hmd_output_list:
            """
            item = {
                'REC_KEY'           : 녹취KEY
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'nlp_sent'          : 형태소 분석 문장 
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
            """
            if dtc_cat_result:
                for dtc_cate, value_list in dtc_cat_result.items():
                    dtc_cate_list = dtc_cate.split('!@#$')
                    """
                    dtc_cate_list = [
                        'CHK_ITM_CD',           평가항목코드
                        'CHK_APP_ORD',          항목적용차수
                        'CUSL_CD',              상담코드
                        'CUSL_APP_ORD',         상담적용차수
                        'DTCT_DTC_NO',          탐지사전번호
                        'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                        'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                        'DTCT_DTC_ED_NO',       탐지사전끝번호
                        'SNS_YN',               SMS전송여부
                        'SCRP',                 스크립트
                    ]
                    """
                    chk_itm_cd = dtc_cate_list[0]
                    chk_app_ord = dtc_cate_list[1]
                    cusl_cd = dtc_cate_list[2]
                    cusl_app_ord = dtc_cate_list[3]
                    dtct_dtc_no = dtc_cate_list[4]
                    dtct_dtc_grp_no = dtc_cate_list[5]
                    dtct_dtc_grp_in_no = dtc_cate_list[6]
                    dtct_dtc_grp_ed_no = dtc_cate_list[7]
                    cate_key = (
                        chk_itm_cd,
                        chk_app_ord,
                        cusl_cd,
                        cusl_app_ord,
                        dtct_dtc_no,
                        dtct_dtc_grp_no
                    )
                    if cate_key not in cate_idx_dict:
                        cate_idx_dict[cate_key] = (dtct_dtc_grp_ed_no, [dtct_dtc_grp_in_no])
                    else:
                        cate_idx_dict[cate_key][1].append(dtct_dtc_grp_in_no)
                    if cate_key in dtc_cate_dict:
                        if dtct_dtc_grp_in_no not in dtc_cate_dict[cate_key]:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no] = [item['STMT_NO']]
                        else:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no].append(item['STMT_NO'])
                    else:
                        dtc_cate_dict[cate_key] = {dtct_dtc_grp_in_no: [item['STMT_NO']]}
        cate_check_dict = dict()
        for cate_key, value in cate_idx_dict.items():
            flag = True
            end_idx = int(value[0])
            idx_list = value[1]
            for cnt in range(1, end_idx + 1):
                if str(cnt) in idx_list:
                    continue
                else:
                    flag = False
            if flag:
                output_list = self.make_target_line(end_idx, config.STTConfig.hmd_line_gap, dtc_cate_dict[cate_key])
                cate_check_dict[cate_key] = output_list
        output_list = self.extract_ta_output(hmd_output_list, cate_check_dict)
        self.insert_ta_output(rec_key, output_list, extra)

    @staticmethod
    def execute_nlp(stt_result_list):
        LOGGER.info('\t  -- 1) NLP analyzing ...')
        tmp_line_no = 1
        nlp_output_list = list()
        for item in stt_result_list:
            """
                item = {
                    'REC_KEY'           : 녹취KEY
                    'STMT_NO'           : 문장번호
                    'SPK_DIV_CD'        : 화자구분코드
                    'STMT_ST_TM'        : 문장시작시간
                    'STMT_ED_TM'        : 문장종료시간
                    'STMT'              : 원문
                    'SILENCE'           : 묵음시간
                }
            """
            try:
                result = NLP_CLIENT.analyze(item['STMT'])
                sent, nlp_sent, morph_sent = result
                item['nlp_sent'] = nlp_sent
                item['tmp_line_no'] = tmp_line_no
            except Exception:
                LOGGER.error("Can't analyze NLP Sentence --> {0}".format(item['STMT']))
                LOGGER.error(traceback.format_exc())
                continue
            nlp_output_list.append(item)
            tmp_line_no += 1
        return nlp_output_list

    def extract_script_ta_output(self, data, dtc_cat_result):
        # 탐지구간정보 추출
        output_list = list()
        if dtc_cat_result:
            for dtc_cate, value_list in dtc_cat_result.items():
                dtc_cate_list = dtc_cate.split('!@#$')
                """
                dtc_cate_list = [
                    'CHK_ITM_CD',           평가항목코드
                    'CHK_APP_ORD',          항목적용차수
                    'SCRP_LCTG_CD',         스크립트 대분류코드
                    'SCRP_MCTG_CD',         스크립트 중분류코드
                    'SCRP_MCTG_NM',         스크립트 중분류명
                    'SCRP_SCTG_CD',         스크립트 소분류코드
                    'SCRP_SCTG_NM',         스크립트 소분류명
                    'SCRP_APP_ORD',         스크립트 적용차수
                    'ST_SCRP_YN',           시작문장여부
                    'ESTY_SCRP_YN',         필수문장여부
                    'DTCT_DTC_NO',          탐지사전번호
                    'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                    'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                    'DTCT_DTC_ED_NO',       탐지사전끝번호
                    'CUST_ANS_YN'           고객답변여부
                ]
                """
                chk_itm_cd = dtc_cate_list[0]
                chk_app_ord = dtc_cate_list[1]
                scrp_lctg_cd = dtc_cate_list[2]
                scrp_mctg_cd = dtc_cate_list[3]
                scrp_mctg_nm = dtc_cate_list[4]
                scrp_sctg_cd = dtc_cate_list[5]
                scrp_sctg_nm = dtc_cate_list[6]
                scrp_app_ord = dtc_cate_list[7]
                dtct_dtc_no = dtc_cate_list[10]
                # 탐지 된 키워드
                dtct_kwd_list = list()
                for value in value_list:
                    dtct_kwd = value[0]
                    if dtct_kwd not in dtct_kwd_list:
                        dtct_kwd_list.append(dtct_kwd)
                tmp_dtct_kwd = ','.join(dtct_kwd_list)
                if len(tmp_dtct_kwd.encode('utf-8')) > 300:
                    output_dtct_kwd = dtct_kwd_list[0]
                else:
                    output_dtct_kwd = tmp_dtct_kwd
                tmp_dict = dict()
                tmp_dict['REC_KEY'] = data['call_id']
                tmp_dict['STMT_NO'] = '0'
                tmp_dict['SPK_DIV_CD'] = 'A' if data['channelno'] == '0' else 'C'
                tmp_dict['CHK_APP_ORD'] = chk_app_ord
                tmp_dict['CHK_ITM_CD'] = chk_itm_cd
                tmp_dict['SCRP_LCTG_CD'] = scrp_lctg_cd
                tmp_dict['SCRP_MCTG_CD'] = scrp_mctg_cd
                tmp_dict['SCRP_MCTG_NM'] = scrp_mctg_nm
                tmp_dict['SCRP_SCTG_CD'] = scrp_sctg_cd
                tmp_dict['SCRP_SCTG_NM'] = scrp_sctg_nm
                tmp_dict['SCRP_APP_ORD'] = scrp_app_ord
                tmp_dict['STMT_ST_TM'] = self.extract_speech_time(data['start'])
                tmp_dict['STMT_ED_TM'] = self.extract_speech_time(data['end'])
                tmp_dict['DTCT_STMT'] = data['text'].strip()
                tmp_dict['DTCT_DTC_NO'] = dtct_dtc_no
                tmp_dict['DTCT_KWD'] = output_dtct_kwd
                output_list.append(tmp_dict)
        return output_list

    def insert_script_ta_output(self, data, output_list):
        LOGGER.info('\t  -- 3) Insert Script HMD output ...')
        if not output_list:
            return
        for item in output_list:
            enc_flag = self.petra_enc(item['DTCT_STMT'])
            enc_dtct_stmt = enc_flag if enc_flag else item['DTCT_STMT']
            bind = (
                item['REC_KEY'],
                item['STMT_NO'],
                item['REC_KEY'],
                item['SPK_DIV_CD'],
                'L',
                item['CHK_APP_ORD'],
                item['CHK_ITM_CD'],
                item['SCRP_LCTG_CD'],
                item['SCRP_MCTG_CD'],
                item['SCRP_SCTG_CD'],
                item['SCRP_APP_ORD'],
                item['STMT_ST_TM'],
                item['STMT_ED_TM'],
                enc_dtct_stmt,
                item['DTCT_DTC_NO'],
                item['DTCT_KWD'],
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                HOST_NM,
                datetime.fromtimestamp(time.time())
            )
            if enc_flag:
                util.insert_script_ta_result(DB, bind)
            else:
                util.insert_script_ta_result(DB, bind, True)
            bind = (
                '01',
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                item['REC_KEY'],
                'L',
                item['CHK_APP_ORD'],
                item['CHK_ITM_CD'],
                'STT'
            )
            util.update_qa_real_rst_tb(DB, bind)
            # Send websocket
            qa_data = {
                'EventType': "QA",
                'Event': 'result',
                'Agent': data['agent'],
                'CALL_ID': data['call_id'],
                'CHK_APP_ORD': item['CHK_APP_ORD'],
                'CHK_ITM_CD': item['CHK_ITM_CD'],
                'PSN_JUG_CAT': 'STT',
                'DTCT_RST': '01',
                'SCRP_MCTG_NM': item['SCRP_MCTG_NM'],
                'SCRP_SCTG_NM': item['SCRP_SCTG_NM']
            }
            qa_message = json.dumps(qa_data, ensure_ascii=False)
            self.send_ws(qa_message)

    def execute_script_hmd(self, data, nlp_output, model_name):
        if 'start' not in model_name:
            LOGGER.info('\t  -- 1) Script HMD analyzing ...')
        hmd_model = HMD_CLIENT.load_hmd_model("!@#$", model_name)
        dtc_cat_result = dict()
        try:
            if data['channelno'] == '0':
                dtc_cat_result = HMD_CLIENT.execute_hmd(data['text'].strip(), nlp_output, hmd_model)
        except Exception:
            LOGGER.error(traceback.format_exc())
            LOGGER.error("Can't anlyze HMD Sentence -> {0}".format(data['text'].strip()))
        if 'start' not in model_name:
            LOGGER.info('\t  -- 2) Extract Script HMD output ...')
        output_list = self.extract_script_ta_output(data, dtc_cat_result)
        if 'start' in model_name:
            return len(output_list) > 0
        self.insert_script_ta_output(data, output_list)

    @staticmethod
    def execute_script_nlp(data):
        nlp_sent = str()
        try:
            result = NLP_CLIENT.analyze(data['text'].strip())
            sent, nlp_sent, morph_sent = result
        except Exception:
            LOGGER.error("Can't analyze NLP Sentence --> {0}".format(data['text']))
            LOGGER.error(traceback.format_exc())
        return nlp_sent

    def event_process(self, data):
        event = data['event']
        event_type = data['eventtype']
        if event_type == 'STT':
            if event == 'start':
                pass
            elif event == 'result':
                enc_flag = self.petra_enc(data['text'].strip())
                enc_stmt = enc_flag if enc_flag else data['text'].strip()
                bind = (
                    data['call_id'],
                    data['call_id'],
                    'A' if data['channelno'] == '0' else 'C',
                    self.extract_speech_time(data['start']),
                    self.extract_speech_time(data['end']),
                    enc_stmt,
                    datetime.strftime(datetime.now(), "%Y%m"),
                    HOST_NM,
                    datetime.fromtimestamp(time.time()),
                    HOST_NM,
                    datetime.fromtimestamp(time.time())
                )
                if enc_flag:
                    util.insert_real_stt_rst(DB, bind)
                else:
                    util.insert_real_stt_rst(DB, bind, True)
                try:
                    # STT 결과 기준 실시간 QA 처리
                    nlp_output = self.execute_script_nlp(data)
                    # Script 대상인지 확인
                    if data['call_id'] not in SCRIPT_TARGET_DICT:
                        if self.execute_script_hmd(data, nlp_output, 'common_script_start'):
                            LOGGER.info('[QA START    ] REC_KEY= {0}'.format(data['call_id']))
                            LOGGER.info('  --> Execute Script HMD [REC_KEY= {0}]'.format(data['call_id']))
                            SCRIPT_TARGET_DICT[data['call_id']] = True
                            # 45개 QA_REAL_RST_TB 생성
                            chk_itm_cd_list = util.select_chk_itm_cd_list(DB)
                            bind_list = list()
                            for chk_itm_cd_dict in chk_itm_cd_list:
                                bind = (
                                    data['call_id'],
                                    'L',
                                    chk_itm_cd_dict['CHK_APP_ORD'],
                                    chk_itm_cd_dict['CHK_ITM_CD'],
                                    chk_itm_cd_dict['SCRP_MCTG_CD'],
                                    chk_itm_cd_dict['SCRP_MCTG_NM'],
                                    chk_itm_cd_dict['SCRP_SCTG_CD'],
                                    chk_itm_cd_dict['SCRP_SCTG_NM'],
                                    'STT',
                                    '03',
                                    HOST_NM,
                                    datetime.fromtimestamp(time.time()),
                                    HOST_NM,
                                    datetime.fromtimestamp(time.time())
                                )
                                bind_list.append(bind)
                            del_flag = util.select_qa_real_rst_tb(DB, data['call_id'])
                            if del_flag:
                                util.delete_qa_real_rst_tb(DB, data['call_id'])
                            util.insert_qa_real_rst_tb(DB, bind_list)
                            # Send websocket
                            qa_data = {
                                'EventType': "QA",
                                'Event': 'start',
                                'Agent': data['agent']
                            }
                            qa_message = json.dumps(qa_data, ensure_ascii=False)
                            self.send_ws(qa_message)
                    if data['call_id'] in SCRIPT_TARGET_DICT:
                        LOGGER.info('[QA PROCESS  ] REC_KEY= {0}'.format(data['call_id']))
                        self.execute_script_hmd(data, nlp_output, 'common_script')
                except Exception:
                    LOGGER.error(traceback.format_exc())
            elif event == 'stop':
                LOGGER.info('[STT STOP    ] REC_KEY= {0}'.format(data['call_id']))
            else:
                LOGGER.error('unknown event type: {0}'.format(event))
        elif event_type == 'CALL':
            status = data['status']
            if status == "CALLING":
                LOGGER.info('[CALLING     ] REC_KEY= {0}'.format(data['call_id']))
                extra = json.loads(data['extra'].replace("'", "\""))
                json_body = extra['body']
                brof_cd = json_body.get('BROF_CD').strip()
                saes_enmo = json_body.get('SAES_ENMO').strip()
                saes_emno = json_body.get('SAES_EMNM').strip()
                csmr_tlno = json_body.get('HOME_TLNO').strip()
                tl_csmr_no = json_body.get('TL_CSMR_NO').strip()
                csmr_nm = json_body.get('CSMR_NM').strip()
                tl_cd = json_body.get('TL_CD').strip()
                if brof_cd:
                    brof_nm = util.select_brof_nm(DB, brof_cd)
                    org_info = util.select_org_info(DB, brof_cd)
                    user_info = util.select_user_info(DB, saes_enmo)
                    enc_flag = self.petra_enc(csmr_tlno)
                    enc_csmr_tlno = enc_flag if enc_flag else csmr_tlno
                    bind = (
                        data['call_id'],
                        brof_cd,
                        data['call_id'],
                        "{0}.mp3".format(data['call_id']),
                        '/DATA_SHARE/maum/rec/{0}'.format(datetime.now().strftime("%Y/%m%d")),
                        '100',  # 실시간STT
                        'O',  # Outbound
                        '14',  # 실시간STT처리중
                        datetime.now().strftime("%Y-%m-%d"),
                        datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'mp3',
                        'N',
                        'S',
                        HOST_NM,
                        datetime.fromtimestamp(time.time()),
                        1,
                        tl_csmr_no,
                        csmr_nm,
                        org_info.get('SAES_CD'),
                        org_info.get('SAES_NM'),
                        org_info.get('HQT_CD'),
                        org_info.get('HQT_NM'),
                        brof_cd,
                        brof_nm,
                        user_info.get('DET_CD'),
                        org_info.get('CHN_CD'),
                        org_info.get('CHN_NM'),
                        saes_enmo,
                        saes_emno,
                        user_info.get('ADMIN_TLNO'),
                        enc_csmr_tlno,
                        tl_cd,
                        HOST_NM,
                        datetime.fromtimestamp(time.time()),
                        HOST_NM,
                        datetime.fromtimestamp(time.time()),
                        data['call_id']
                    )
                    LOGGER.info('  --> Delete QA_STT_RST_TB  [REC_KEY= {0}]'.format(data['call_id']))
                    util.delete_stt_results(DB, data['call_id'])
                    LOGGER.info('  --> Insert QA_CAL_INFO_TB [REC_KEY= {0}]'.format(data['call_id']))
                    if enc_flag:
                        util.insert_real_cal_info(DB, bind)
                    else:
                        util.insert_real_cal_info(DB, bind, True)
                else:
                    LOGGER.error('Check BROF_CD(지점코드) : {0}'.format(brof_cd))
            elif status == "CONNECTED":
                LOGGER.info('[CONNECTED   ] REC_KEY= {0}'.format(data['call_id']))
                pass
            elif status == "DISCONNECTED":
                LOGGER.info('[DISCONNECTED] REC_KEY= {0}'.format(data['call_id']))
                extra = json.loads(data['extra'].replace("'", "\""))
                if data['call_id'] in SCRIPT_TARGET_DICT:
                    del SCRIPT_TARGET_DICT[data['call_id']]
                # Update QA_CAL_INFO_TB
                st_tm_result = util.select_stt_st_tm(DB, data['call_id'])
                rec_st_dt = st_tm_result.get('REC_ST_DT')
                rec_st_dt = rec_st_dt.replace('/', '').replace('-', '').strip() if rec_st_dt else None
                rec_st_tm = st_tm_result.get('REC_ST_TM')
                rec_ed_tm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                if rec_st_tm:
                    rec_du_tm = (datetime.now() - datetime.strptime(rec_st_tm, "%Y-%m-%d %H:%M:%S")).total_seconds()
                    rec_du_tm = int(rec_du_tm)
                else:
                    rec_du_tm = None
                stt_com_tm = datetime.fromtimestamp(time.time())
                # Delete and Insert QA_STT_RST_TB
                results = util.select_real_stt_results_by_rec_key(DB, data['call_id'])
                line_num = 1
                bind_list = list()
                plsql_bind_list = list()
                stt_result_list = list()
                max_silence_time = 0.0
                temp_end_seconds = 0.0
                speech_speed_info = dict()
                rec_stt_ym = rec_st_dt[:6] if rec_st_dt else datetime.now().strftime("%Y%m")
                change = False
                if config.STTConfig.arabic:
                    LOGGER.info('  --> STT Arabic ...')
                    change = py3_change_arabic.ChangeArabic()
                for item in results:
                    rec_key = item[0]
                    spk_div_cd = item[2]
                    stmt_st_tm = item[3]
                    stmt_ed_tm = item[4]
                    stmt = item[5] if item[5] else str()
                    speaker = spk_div_cd
                    st_hh, st_mm, st_ss = stmt_st_tm.split(':')
                    ed_hh, ed_mm, ed_ss = stmt_ed_tm.split(':')
                    start = ((int(st_hh) * 60 * 60) + (int(st_mm) * 60) + int(st_ss)) * 8000
                    end = ((int(ed_hh) * 60 * 60) + (int(ed_mm) * 60) + int(ed_ss)) * 8000
                    stt_txt = stmt.strip()
                    spch_sped = self.extract_speech_speed(start, end, stt_txt)
                    silence_time = (start / 8000.0) - temp_end_seconds
                    silence_time = round(silence_time, 2)
                    if silence_time < 0:
                        silence_time = 0.0
                    if max_silence_time < silence_time:
                        max_silence_time = silence_time
                    if speaker in speech_speed_info:
                        speech_speed_info[speaker][0] += 1
                        speech_speed_info[speaker][1] += spch_sped
                    else:
                        speech_speed_info[speaker] = [1, spch_sped]
                    if change:
                        stmt = change.change_arabic_text(stmt)
                    enc_flag = self.petra_enc(stmt)
                    enc_stmt = enc_flag if enc_flag else stmt
                    bind = (
                        rec_key,
                        line_num,
                        speaker,
                        stmt_st_tm,
                        stmt_ed_tm,
                        enc_stmt,
                        rec_stt_ym,
                        silence_time,
                        HOST_NM,
                        datetime.fromtimestamp(time.time()),
                        HOST_NM,
                        datetime.fromtimestamp(time.time())
                    )
                    if enc_flag:
                        bind_list.append(bind)
                    else:
                        plsql_bind_list.append(bind)
                    stt_dict = {
                        'REC_KEY': data['call_id'],
                        'STMT_NO': line_num,
                        'SPK_DIV_CD': speaker,
                        'STMT_ST_TM': stmt_st_tm,
                        'STMT_ED_TM': stmt_ed_tm,
                        'STMT': stmt,
                        'SILENCE': silence_time
                    }
                    stt_result_list.append(stt_dict)
                    line_num += 1
                    temp_end_seconds = end / 8000.0
                LOGGER.info('  --> Delete QA_STT_RST_TB  [REC_KEY= {0}]'.format(data['call_id']))
                util.delete_stt_results(DB, data['call_id'])
                LOGGER.info('  --> Insert QA_STT_RST_TB  [REC_KEY= {0}]'.format(data['call_id']))
                if bind_list:
                    util.insert_real_stt_rst_list(DB, bind_list)
                if plsql_bind_list:
                    util.insert_real_stt_rst_list(DB, plsql_bind_list, True)
                spch_sped_rx = 0
                spch_sped_tx = 0
                for speaker, value in speech_speed_info.items():
                    if speaker == 'A':
                        spch_sped_tx = round(value[1] / value[0], 2)
                    elif speaker == 'C':
                        spch_sped_rx = round(value[1] / value[0], 2)
                    else:
                        spch_sped_tx = round(value[1] / value[0], 2)
                        spch_sped_rx = round(value[1] / value[0], 2)
                #if rec_st_dt:
                #    lis_rec_loc_path = os.path.join(
                #        CONF.lis_rec_dir_path, "{0}/{1}".format(rec_st_dt[:4], rec_st_dt[4:]))
                #    lis_rec_file_nm = "{0}.wav".format(data['call_id'])
                #else:
                #    lis_rec_loc_path = None
                #    lis_rec_file_nm = None
                lis_rec_loc_path = None
                lis_rec_file_nm = None
                bind = (
                    '13',  # STT처리완료
                    rec_ed_tm,
                    rec_du_tm,
                    stt_com_tm,
                    lis_rec_file_nm,
                    lis_rec_loc_path,
                    spch_sped_rx,
                    spch_sped_tx,
                    max_silence_time,
                    HOST_NM,
                    datetime.fromtimestamp(time.time()),
                    data['call_id']
                )
                LOGGER.info('  --> Update QA_CAL_INFO_TB [REC_KEY= {0}]'.format(data['call_id']))
                util.update_real_cal_info(DB, bind)
                LOGGER.info('  --> Execute Forbidden HMD [REC_KEY= {0}]'.format(data['call_id']))
                nlp_output_list = self.execute_nlp(stt_result_list)
                self.execute_hmd(data['call_id'], nlp_output_list, extra)
            else:
                LOGGER.error('unknown status type: {0}'.format(status))
        else:
            LOGGER.error('unknown event type: {0}'.format(event_type))


#######
# def #
#######
def main():
    """
    This program that Websocket Server
    """
    tornado.options.parse_command_line()
    app = Application()
    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_ctx.load_verify_locations(CONF.chain_file)
    ssl_ctx.load_cert_chain(
        certfile=CONF.cert_file,
        keyfile=CONF.key_file,
        password=CONF.pd
    )
    http_server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_ctx)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()

