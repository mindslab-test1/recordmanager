#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-03-24, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import socket
import traceback
import subprocess
from datetime import datetime
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def sub_process(cmd):
    """
    Execute subprocess
    @param      cmd:        Command
    """
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    return response_out, response_err


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


########
# main #
########
def main():
    """
    This is a program that restart engine
    """
    st = datetime.fromtimestamp(time.time())
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.RestartConfig.logger_name,
        log_dir_path=config.RestartConfig.log_dir_path,
        log_file_name=config.RestartConfig.log_file_name,
        backup_count=config.RestartConfig.backup_count,
        log_level=config.RestartConfig.log_level
    )
    log.info("-" * 100)
    log.info("[START] Restart engine")
    host_nm = socket.gethostname()
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(
            PRO_START_TIME, elapsed_time(PRO_START_TIME)))
        sys.exit(1)
    try:
        log.info('1. Stop Service and Engine ... ')
        for engine in ['service:', 'engine:']:
            cmd = 'svctl stop {0}'.format(engine)
            log.info('\t--> Command= {0}'.format(cmd))
            std_out, std_err = sub_process(cmd)
            if len(std_out.strip()) > 0:
                log.info('\t--> Standard out:')
                log.info('\n' + std_out.strip())
            if len(std_err.strip()) > 0:
                log.info('\t--> Standard err:')
                log.info('\n' + std_err.strip())
            time.sleep(1)
        log.info('2. Reset STT/TA(QA, JP) processing ...')
        try:
            log.info('\t--> Update STT in progress ..')
            util.update_stt_in_progress(db, host_nm)
            log.info('\t--> Update QA in progress ..')
            util.update_qa_in_progress(db, host_nm)
            log.info('\t--> Update JoinPlan in progress ..')
            util.update_jp_in_progress(db, host_nm)
        except Exception:
            exc_info = traceback.format_exc()
            log.error(exc_info)
        log.info('3. Start Engine and Service ... ')
        for engine in ['engine:', 'service:']:
            cmd = 'svctl start {0}'.format(engine)
            log.info('\t--> Command = {0}'.format(cmd))
            std_out, std_err = sub_process(cmd)
            if len(std_out.strip()) > 0:
                log.info('\t--> Standard out:')
                log.info('\n' + std_out.strip())
            if len(std_err.strip()) > 0:
                log.info('\t--> Standard err:')
                log.info('\n' + std_err.strip())
            log.info('\t--> Waiting 60 seconds ...')
            time.sleep(60)
        log.info('4. Checking Service and Engine status ... ')
        cmd = 'svctl status'
        log.info('\t--> Command = {0}'.format(cmd))
        std_out, std_err = sub_process(cmd)
        if len(std_out.strip()) > 0:
            log.info('\t--> Standard out:')
            log.info('\n' + std_out.strip())
        if len(std_err.strip()) > 0:
            log.info('\t--> Standard err:')
            log.info('\n' + std_err.strip())
    except Exception:
        exc_info = traceback.format_exc()
        log.error(exc_info)
    finally:
        log.info("[E N D] Start time = {0}, The time required = {1}".format(st, elapsed_time(st)))
        for handler in log.handlers:
            handler.close()
            log.removeHandler(handler)


if __name__ == '__main__':
    main()
