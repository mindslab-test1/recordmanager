#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-30, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import zmq
import time
import json
import copy
import socket
import requests
import traceback
from datetime import datetime, timedelta
from flask import Flask, request
from flask_cors import CORS
from flask_restful import reqparse, Api, Resource
from cfg import config
from lib import logger, util, db_connection, libpcpython

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
CONTEXT = zmq.Context()
SOCK = CONTEXT.socket(zmq.PUSH)
SOCK.connect("tcp://0.0.0.0:5550")

#########
# class #
#########
class ColQAapi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.db = kwargs.get('db')
        self.petra_sid = kwargs.get('petra_sid')
        self.host_nm = socket.gethostname()
        self.json_data = request.get_json(silent=True, cache=False, force=True)

    def post(self):
        return main(self.log, self.db, self.host_nm, self.json_data, self.petra_sid)


#######
# def #
#######
def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:            Logger Object
    """
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    petra_sid = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))
    return petra_sid


def pro_stt00006(log, db, json_data):
    """
    Process STT00006 상담사 콜모니터링 정보 전달
    @param     log:            Logger Object
    @param     db:             DB object
    @param     json_data:      Json data
    @return:                   Json output
    """
    log.info("[STT00006 콜모니터링 --> SAES_ENMO(상담사ID)= {0}, SRCH_ST_DT= {1}, SRCH_ED_DT= {2}]".format(
        json_data['body']['SAES_ENMO'].strip(),
        json_data['body']['SRCH_ST_DT'].strip(),
        json_data['body']['SRCH_ED_DT'].strip()
    ))
    try:
        emp_no = json_data['body']['SAES_ENMO'].strip()
        st_dt = json_data['body']['SRCH_ST_DT'].replace('/', '').replace('-', '').strip()
        ed_dt = json_data['body']['SRCH_ED_DT'].replace('/', '').replace('-', '').strip()
        srch_st_dt = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        srch_ed_dt = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        if st_dt and ed_dt:
            srch_st_dt = "{0}-{1}-{2}".format(st_dt[:4], st_dt[4:6], st_dt[6:8])
            srch_ed_dt = "{0}-{1}-{2}".format(ed_dt[:4], ed_dt[4:6], ed_dt[6:8])
        rst_info_list = list()
        cal_result = util.select_emp_cal_info(db, emp_no, srch_st_dt, srch_ed_dt)
        fwd_result = util.select_emp_fwd_info(db, emp_no, srch_st_dt, srch_ed_dt)
        rec_cnt = '0'
        rec_du_tm = '00:00:00'
        if cal_result:
            for item in cal_result:
                rec_cnt = str(item['REC_CNT'])
                rec_du_tm = item['REC_DU_TM']
            if rec_cnt == '0':
                rec_cnt = '0'
                rec_du_tm = '00:00:00'
        fd_rst_cnt = '0'
        if fwd_result:
            for item in fwd_result:
                fd_rst_cnt = item['FD_RST_CNT']
                rst_dict = {"SCRP": item['SCRP'], "DTCT_DTC_CNT": item["DTCT_DTC_CNT"]}
                rst_info_list.append(rst_dict)
        while len(rst_info_list[:]) < 100:
            rst_info_list.append({"SCRP": "", "DTCT_DTC_CNT": ""})
        rst_result = {
            "SRCH_ST_DT": srch_st_dt.replace('-', ''),
            "SRCH_ED_DT": srch_ed_dt.replace('-', ''),
            "REC_CNT": rec_cnt,
            "REC_DU_TM": rec_du_tm,
            "FD_RST_CNT": fd_rst_cnt,
            "FD_RST_INFO": rst_info_list
        }
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        log.error("[FAIL][STT00006] SAES_ENMO(상담사ID)= {0}, SRCH_ST_DT= {1}, SRCH_ED_DT= {2}".format(
            json_data['body'].get('SAES_ENMO'),
            json_data['body'].get('SRCH_ST_DT'),
            json_data['body'].get('SRCH_ED_DT')
        ))
        rst_info_list = list()
        while len(rst_info_list[:]) < 100:
            rst_info_list.append({"SCRP": "", "DTCT_DTC_CNT": ""})
        rst_result = {
            "SRCH_ST_DT": (datetime.now() - timedelta(days=1)).strftime("%Y%m%d"),
            "SRCH_ED_DT": (datetime.now() - timedelta(days=1)).strftime("%Y%m%d"),
            "REC_CNT": '0',
            "REC_DU_TM": '00:00:00',
            "FD_RST_CNT": '0',
            "FD_RST_INFO": rst_info_list
        }
    json_data['body'].update(rst_result)
    return json_data


def pro_stt00003(log, db, host_nm, json_data):
    """
    Process STT00003 청약정보전달
    @param     log:            Logger Object
    @param     db:             DB object
    @param     host_nm:        Host name
    @param     json_data:      Json data
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    log.info("[STT00003 청약 --> NSPL_NO(계약번호)= {0}, TMS_INFO(회차정보)= {1}]".format(
        json_data['body']['NSPL_NO'].strip(),
        json_data['body']['TMS_INFO'].strip()
    ))
    flag = False
    err_msg = ""
    tms_file_cnt = 0
    rec_overlap_check = dict()
    json_body = json_data['body']
    for item in json_body['REC_INFO']:
        # 1: 'C', 2: 'R', 3: 'S', 4: 'E'
        rec_file_nm = item['REC_FILE_NM'].strip()
        if not rec_file_nm:
            continue
        rec_biz_cd = item['REC_BIZ_CD'].strip()
        if rec_file_nm in rec_overlap_check:
            if rec_overlap_check[rec_file_nm][0] == 'C':
                continue
            elif rec_overlap_check[rec_file_nm][0] == 'R':
                if rec_biz_cd in ('C', ):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
            elif rec_overlap_check[rec_file_nm][0] == 'S':
                if rec_biz_cd in ('C', 'R'):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
            else:
                if rec_biz_cd in ('C', 'R', 'S'):
                    rec_overlap_check[rec_file_nm][0] = rec_biz_cd
        else:
            rec_overlap_check[rec_file_nm] = [rec_biz_cd, True]
    for _ in range(0, 3):
        try:
            nspl_no = json_body['NSPL_NO'].strip()
            tms_info = json_body['TMS_INFO'].strip()
            prod_cd = json_body['PROD_CD'].strip()
            prod_nm = json_body['PROD_NM'].strip()
            ip_csmr_no = json_body['IP_CSMR_NO'].strip()
            tl_csmr_no = json_body['TL_CSMR_NO'].strip()
            csmr_nm = json_body['CSMR_NM'].strip()
            pnm = json_body['PNM'].strip()
            brof_cd = json_body['BROF_CD'].strip()
            emp_no = json_body['SAES_ENMO'].strip()
            emp_nm = json_body['SAES_EMNM'].strip()
            contract_dt = json_body['CONTRACT_DT'].strip()
            rec_info = json_body['REC_INFO']
            g_home_tno = json_body['G_HOME_TNO'].strip()
            g_offc_tno = json_body['G_OFFC_TNO'].strip()
            g_move_tno = json_body['G_MOVE_TNO'].strip()
            p_home_tno = json_body['P_HOME_TNO'].strip()
            p_offc_tno = json_body['P_OFFC_TNO'].strip()
            p_move_tno = json_body['P_MOVE_TNO'].strip()
            brof_nm = util.select_brof_nm(db, brof_cd)
            log.info("  --> G_HOME_TNO= {0}".format(g_home_tno))
            log.info("  --> G_OFFC_TNO= {0}".format(g_offc_tno))
            log.info("  --> G_MOVE_TNO= {0}".format(g_move_tno))
            log.info("  --> P_HOME_TNO= {0}".format(p_home_tno))
            log.info("  --> P_OFFC_TNO= {0}".format(p_offc_tno))
            log.info("  --> P_MOVE_TNO= {0}".format(p_move_tno))
            if not brof_nm:
                brof_nm = None
            org_info = util.select_org_info(db, brof_cd)
            user_info = util.select_user_info(db, emp_no)
            cal_info_list = list()
            cal_rel_bind_list = list()
            for item in rec_info:
                rec_file_nm = item['REC_FILE_NM'].strip()
                if not rec_file_nm:
                    continue
                csmr_tlno = item['CSMR_TLNO'].strip() if item['CSMR_TLNO'].strip() else ''
                log.info("  --> CSMR_TLNO= {0}".format(csmr_tlno))
                if csmr_tlno == g_home_tno:
                    tl_cd = '11'
                elif csmr_tlno == g_offc_tno:
                    tl_cd = '12'
                elif csmr_tlno == g_move_tno:
                    tl_cd = '13'
                elif csmr_tlno == p_home_tno:
                    tl_cd = '21'
                elif csmr_tlno == p_offc_tno:
                    tl_cd = '22'
                elif csmr_tlno == p_move_tno:
                    tl_cd = '23'
                else:
                    tl_cd = item['TL_CD'].strip() if item['TL_CD'].strip() else ''
                log.info("  --> REC_FILE_NM= {0}, TL_CD= {1}".format(rec_file_nm, tl_cd))
                rec_biz_cd =  item['REC_BIZ_CD'].strip()
                rec_st_dt = item['REC_ST_DT'].replace('/', '').replace('-', '').strip()
                # Update
                util.update_qa_cal_info(db, brof_cd, rec_file_nm, rec_biz_cd, tl_cd)
                # QA_CTR_CAL_REL_TB(계약콜관계테이블)
                exists_yn = util.select_qa_cal_rel_existed(db, nspl_no, tms_info)
                if exists_yn:
                    util.delete_qa_cal_rel(db, nspl_no, tms_info)
                # NSPL_NO(계약번호) 기준 다른 회차에 동일한 녹취 파일 존재 확인
                rec_exists_yn = util.select_qa_cal_rel_rec_existed(db, nspl_no, brof_cd, rec_file_nm)
                if rec_exists_yn:
                    continue
                # 같은 회차에 중복된 녹취 파일 제거
                if rec_overlap_check[rec_file_nm][0] != rec_biz_cd:
                    continue
                if not rec_overlap_check[rec_file_nm][1]:
                    continue
                rec_overlap_check[rec_file_nm][1] = False
                tms_file_cnt += 1
                cal_rel_bind = (
                    nspl_no,
                    tms_info,
                    brof_cd,
                    rec_file_nm,
                    rec_biz_cd,
                    host_nm,
                    datetime.fromtimestamp(time.time()),
                    host_nm,
                    datetime.fromtimestamp(time.time())
                )
                cal_rel_bind_list.append(cal_rel_bind)
                results = util.select_qa_cal_existed(db, brof_cd, rec_file_nm)
                if results:
                    #util.update_qa_cal_info(db, brof_cd, rec_file_nm, rec_biz_cd, tl_cd)
                    for result in results:
                        if result['PROG_STAT_CD'] == '14':
                            cal_info_bind = (
                                "{0}!@#${1}".format(brof_cd, rec_file_nm),
                                brof_cd,
                                rec_file_nm,
                                rec_biz_cd,
                                '10',
                                "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                                os.path.splitext(rec_file_nm)[1][1:],
                                'N',
                                1,
                                tl_csmr_no,
                                csmr_nm,
                                org_info.get('SAES_CD'),
                                org_info.get('SAES_NM'),
                                org_info.get('HQT_CD'),
                                org_info.get('HQT_NM'),
                                brof_cd,
                                brof_nm,
                                user_info.get('DET_CD'),
                                org_info.get('CHN_CD'),
                                org_info.get('CHN_NM'),
                                emp_no,
                                emp_nm,
                                user_info.get('ADMIN_TLNO'),
                                csmr_tlno,
                                tl_cd,
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                brof_cd,
                                rec_file_nm
                            )
                            cal_info_list.append(cal_info_bind)
                            break
                else:
                    cal_info_bind = (
                        "{0}!@#${1}".format(brof_cd, rec_file_nm),
                        brof_cd,
                        rec_file_nm,
                        rec_biz_cd,
                        '10',
                        "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                        os.path.splitext(rec_file_nm)[1][1:],
                        'N',
                        1,
                        tl_csmr_no,
                        csmr_nm,
                        org_info.get('SAES_CD'),
                        org_info.get('SAES_NM'),
                        org_info.get('HQT_CD'),
                        org_info.get('HQT_NM'),
                        brof_cd,
                        brof_nm,
                        user_info.get('DET_CD'),
                        org_info.get('CHN_CD'),
                        org_info.get('CHN_NM'),
                        emp_no,
                        emp_nm,
                        user_info.get('ADMIN_TLNO'),
                        csmr_tlno,
                        tl_cd,
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        brof_cd,
                        rec_file_nm
                    )
                    cal_info_list.append(cal_info_bind)
            # QA_CAL_INFO_TB(콜정보테이블)
            if cal_info_list:
                util.insert_qa_cal_info(db, cal_info_list)
            # QA_CTR_CAL_REL_TB(계약콜관계테이블)
            if cal_rel_bind_list:
                util.insert_qa_cal_rel(db, cal_rel_bind_list)
            # QA_CTR_LIST_TB(계약리스트테이블)
            ctr_list_bind = (
                nspl_no,
                tms_info,
                tms_file_cnt,
                prod_cd,
                prod_nm,
                ip_csmr_no,
                tl_csmr_no,
                csmr_nm,
                pnm,
                brof_cd,
                brof_nm,
                emp_no,
                emp_nm,
                contract_dt,
                1,
                datetime.fromtimestamp(time.time()),
                '20',
                host_nm,
                datetime.fromtimestamp(time.time()),
                host_nm,
                datetime.fromtimestamp(time.time())
            )
            exists_yn = util.select_qa_target_existed(db, nspl_no, tms_info)
            if exists_yn:
                util.delete_qa_target(db, nspl_no, tms_info)
            util.insert_qa_target(db, ctr_list_bind)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            time.sleep(0.1)
    if not flag:
        output = {"CODE": -1, "MSG": err_msg}
        json_data['body'].update(output)
        log.error("[FAIL][STT00003] NSPL_NO(계약번호)= {0}, TMS_INFO(회차정보)= {1}".format(
            json_body.get('NSPL_NO'), json_body.get('TMS_INFO')))
        return json_data
    output = {"CODE": 0, "MSG": "Success, NSPL_NO(계약번호)={0}, TMS_INFO(회차정보)={1}".format(
        json_body.get('NSPL_NO'), json_body.get('TMS_INFO'))}
    json_data['body'].update(output)
    return json_data


def pro_stt00002(log, db, host_nm, json_data, petra_sid):
    """
    Process STT00002 가입설계동의녹취파일정보전달
    @param     log:            Logger Object
    @param     db:             DB object
    @param     host_nm:        Host name
    @param     json_data:      Json data
    @param     petra_sid:      Petra SID
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    log.info("[STT00002 가입설계동의 --> TL_CSMR_NO(고객관리번호(T))= {0}, BROF_CD(지점코드)= {1}]".format(
        json_data['body']['TL_CSMR_NO'].strip(),
        json_data['body']['BROF_CD'].strip(),
    ))
    flag = False
    err_msg = ""
    json_body = json_data['body']
    for _ in range(0, 3):
        try:
            tl_csmr_no = json_body['TL_CSMR_NO'].strip()
            rec_biz_cd = json_body['REC_BIZ_CD'].strip()
            rec_st_dt = json_body['REC_ST_DT'].replace('/', '').replace('-', '').strip()
            csmr_nm = json_body['CSMR_NM'].strip()
            brof_cd = json_body['BROF_CD'].strip()
            brof_nm = util.select_brof_nm(db, brof_cd)
            if not brof_nm:
                brof_nm = None
            emp_no = json_body['SAES_ENMO'].strip()
            emp_nm = json_body['SAES_EMNM'].strip()
            save_dttm = json_body['SAVE_DTTM'].strip()
            save_time = json_body['SAVE_TIME'].strip()
            rec_info = json_body['REC_INFO']
            org_info = util.select_org_info(db, brof_cd)
            # 본사코드 10040 일 경우만 처리(TM)
            if org_info.get('SAES_CD') != '100040':
                flag = True
                break
            user_info = util.select_user_info(db, emp_no)
            cal_info_bind_list = list()
            for item in rec_info:
                rec_file_nm = item['REC_FILE_NM'].strip()
                if not rec_file_nm:
                    continue
                csmr_tlno = item['CSMR_TLNO'].strip() if item['CSMR_TLNO'].strip() else ''
                tl_cd = item['TL_CD'].strip() if item['TL_CD'].strip() else ''
                # QA_CTR_CAL_REL_TB(계약콜관계테이블)
                exists_yn = util.select_jp_target_existed(db, tl_csmr_no, brof_cd, rec_file_nm)
                if exists_yn:
                    util.delete_jp_target(db, tl_csmr_no, brof_cd, rec_file_nm)
                jp_ctr_list_bind = (
                    tl_csmr_no,
                    brof_cd,
                    rec_file_nm,
                    "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                    'ZZZAGR',
                    '가입설계스크립트',
                    csmr_nm,
                    org_info.get('SAES_CD'),
                    org_info.get('SAES_NM'),
                    org_info.get('HQT_CD'),
                    org_info.get('HQT_NM'),
                    brof_cd,
                    brof_nm,
                    user_info.get('DET_CD'),
                    org_info.get('CHN_CD'),
                    org_info.get('CHN_NM'),
                    emp_no,
                    emp_nm,
                    rec_biz_cd,
                    save_dttm,
                    save_time,
                    '30',
                    datetime.fromtimestamp(time.time()),
                    host_nm,
                    datetime.fromtimestamp(time.time()),
                    host_nm,
                    datetime.fromtimestamp(time.time())
                )
                util.insert_jp_target(db, jp_ctr_list_bind)
                results = util.select_qa_cal_existed(db, brof_cd, rec_file_nm)
                if results:
                    util.update_qa_cal_info(db, brof_cd, rec_file_nm, rec_biz_cd, tl_cd)
                    for result in results:
                        if result['PROG_STAT_CD'] == '14':
                            if csmr_tlno:
                                csmr_tlno = str(csmr_tlno)
                                enc_csmr_tlno = libpcpython.PcAPI_enc_id(
                                    petra_sid, config.PetraConfig.enc_cold_id, csmr_tlno, len(csmr_tlno))
                            else:
                                enc_csmr_tlno = ''
                            cal_info_bind = (
                                "{0}!@#${1}!@#${2}".format(time.time(), tl_csmr_no, rec_file_nm),
                                brof_cd,
                                rec_file_nm,
                                rec_biz_cd,
                                '10',
                                "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                                os.path.splitext(rec_file_nm)[1][1:],
                                'N',
                                1,
                                tl_csmr_no,
                                csmr_nm,
                                org_info.get('SAES_CD'),
                                org_info.get('SAES_NM'),
                                org_info.get('HQT_CD'),
                                org_info.get('HQT_NM'),
                                brof_cd,
                                brof_nm,
                                user_info.get('DET_CD'),
                                org_info.get('CHN_CD'),
                                org_info.get('CHN_NM'),
                                emp_no,
                                emp_nm,
                                user_info.get('ADMIN_TLNO'),
                                enc_csmr_tlno,
                                tl_cd,
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                host_nm,
                                datetime.fromtimestamp(time.time()),
                                brof_cd,
                                rec_file_nm
                            )
                            cal_info_bind_list.append(cal_info_bind)
                            break
                else:
                    if csmr_tlno:
                        csmr_tlno = str(csmr_tlno)
                        enc_csmr_tlno = libpcpython.PcAPI_enc_id(
                            petra_sid, config.PetraConfig.enc_cold_id, csmr_tlno, len(csmr_tlno))
                    else:
                        enc_csmr_tlno = ''
                    cal_info_bind = (
                        "{0}!@#${1}!@#${2}".format(time.time(), tl_csmr_no, rec_file_nm),
                        brof_cd,
                        rec_file_nm,
                        rec_biz_cd,
                        '10',
                        "{0}-{1}-{2}".format(rec_st_dt[:4], rec_st_dt[4:6], rec_st_dt[6:8]),
                        os.path.splitext(rec_file_nm)[1][1:],
                        'N',
                        1,
                        tl_csmr_no,
                        csmr_nm,
                        org_info.get('SAES_CD'),
                        org_info.get('SAES_NM'),
                        org_info.get('HQT_CD'),
                        org_info.get('HQT_NM'),
                        brof_cd,
                        brof_nm,
                        user_info.get('DET_CD'),
                        org_info.get('CHN_CD'),
                        org_info.get('CHN_NM'),
                        emp_no,
                        emp_nm,
                        user_info.get('ADMIN_TLNO'),
                        enc_csmr_tlno,
                        tl_cd,
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        host_nm,
                        datetime.fromtimestamp(time.time()),
                        brof_cd,
                        rec_file_nm
                    )
                    cal_info_bind_list.append(cal_info_bind)
            # QA_CAL_INFO_TB(콜정보테이블)
            if cal_info_bind_list:
                util.insert_jp_cal_info(db, cal_info_bind_list)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            time.sleep(0.1)
    if not flag:
        output = {"CODE": -1, "MSG": err_msg}
        json_data['body'].update(output)
        log.error("[FAIL][STT00002] TL_CSMR_NO(고객번호(T))= {0}, BROF_CD(지점코드)= {1}".format(
            json_body.get('TL_CSMR_NO'), json_data['body'].get('BROF_CD')))
        return json_data
    output = {"CODE": 0, "MSG": "Success, TL_CSMR_NO(고객번호(T))={0}, BROF_CD(지점코드)= {1}".format(
        json_body.get('TL_CSMR_NO'), json_data['body'].get('BROF_CD'))}
    json_data['body'].update(output)
    return json_data


def pro_stt00001(log, json_data):
    """
    Process STT00001 UM녹취시작/종료정보전달(실시간STT)
    @param     log:            Logger Object
    @param     json_data:      Json data
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    call_status = json_data['body']['CAL_STAUS'].strip()
    if call_status == '01':
        call_status_str = "{0}(시작)".format(call_status)
    elif call_status == '02':
        call_status_str = "{0}(종료)".format(call_status)
    else:
        call_status_str = "{0}(후처리)".format(call_status)
    try:
        log.info("[STT00001 녹취시작/종료 --> TL_CSMR_NO(고객관리번호(T))= {0}, BROF_CD(지점코드)= {1}, "
                 "INLN_TLNO(내선번호)= {2}, CAL_STAUS(통화타입)= {3}]".format(
            json_data['body']['TL_CSMR_NO'].strip(),
            json_data['body']['BROF_CD'].strip(),
            json_data['body']['INLN_TLNO'].strip(),
            call_status_str
        ))
        if call_status == '01':
            if not json_data['body']['HOME_TLNO'].startswith('1588'):
                SOCK.send_pyobj(json_data)
        output = {"CODE": 0, "MSG": "Success"}
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        output = {"CODE": -1, "MSG": err_msg}
        log.error("[FAIL][STT00001] 녹취시작/종료 --> TL_CSMR_NO(고객관리번호(T))= {0}, BROF_CD(지점코드)= {1},"
                  " INLN_TLNO(내선번호)= {2}, CAL_STAUS(통화타입)= {3}".format(
            json_data['body'].get('TL_CSMR_NO'),
            json_data['body'].get('BROF_CD'),
            json_data['body'].get('INLN_TLNO'),
            call_status_str
        ))
    json_data['body'].update(output)
    return json_data


def main(log, db, host_nm, json_data, petra_sid):
    """
    This is a program that Collector QA API
    @param     host_nm:        Host name
    @param     log:            Logger Object
    @param     db:             DB object
    @param     json_data:      Json data
    @param     petra_sid:      Petra SID
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    try:
        js_out_file_nm = "GetJson.log.{0}".format(datetime.now().strftime("%Y-%m-%d"))
        js_out_dir_path = os.path.join(config.ColQAConfig.log_dir_path, 'get_json')
        if not os.path.exists(js_out_dir_path):
            os.makedirs(js_out_dir_path)
        js_out_file = open(os.path.join(js_out_dir_path, js_out_file_nm), 'a+')
        print >> js_out_file, str(json_data).replace(' ', '')
        js_out_file.close()
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        pass
    try:
        pro_id = json_data['header']['HDR_DEST_PROG'].strip()
        if pro_id == 'STT00001':
            output_json = pro_stt00001(log, json_data)
            flag = False
            for _ in range(0, 3):
                try:
                    temp_data = copy.deepcopy(json_data)
                    temp_data['header']['HDR_DEST_PROG'] = 'STT1COPY'
                    host = 'http://{0}:{1}/col_qa'.format(
                        config.ColQAConfig.svr_list[socket.gethostname()], config.ColQAConfig.port)
                    requests.post(host, json=temp_data, timeout=1)
                    flag = True
                    break
                except requests.ReadTimeout:
                    break
                except Exception:
                    err_msg = traceback.format_exc()
                    log.error(err_msg)
                    time.sleep(0.1)
            if not flag:
                call_status = json_data['body'].get('CAL_STAUS')
                if call_status == '01':
                    call_status_str = "{0}(시작)".format(call_status)
                elif call_status == '02':
                    call_status_str = "{0}(종료)".format(call_status)
                else:
                    call_status_str = "{0}(후처리)".format(call_status)
                log.error("[FAIL TRNAS][STT00001] 녹취시작/종료 --> TL_CSMR_NO(고객관리번호(T))= {0}, BROF_CD(지점코드)= {1},"
                         " INLN_TLNO(내선번호)= {2}, CAL_STAUS(통화타입)= {3}".format(
                    json_data['body'].get('TL_CSMR_NO'),
                    json_data['body'].get('BROF_CD'),
                    json_data['body'].get('INLN_TLNO'),
                    call_status_str
                ))
            return output_json
        elif pro_id == 'STT1COPY':
            pro_stt00001(log, json_data)
            return 'Success'
        elif pro_id == 'STT00002':
            output_json = pro_stt00002(log, db, host_nm, json_data, petra_sid)
            return output_json
        elif pro_id == 'STT00003':
            output_json = pro_stt00003(log, db, host_nm, json_data)
            return output_json
        elif pro_id == 'STT00006':
            output_json = pro_stt00006(log, db, json_data)
            return output_json
        else:
            output = {"CODE": -1, "MSG": "Unknown process"}
            json_data['body'].update(output)
            return json_data
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        output = {"CODE": -1, "MSG": err_msg}
        json_data['body'].update(output)
        return json_data

# Logger 생성
log_obj = logger.get_timed_rotating_logger(
    logger_name=config.ColQAConfig.logger_name,
    log_dir_path=config.ColQAConfig.log_dir_path,
    log_file_name=config.ColQAConfig.log_file_name,
    backup_count=config.ColQAConfig.backup_count,
    log_level=config.ColQAConfig.log_level
)
log_obj.info('[START] Collector QA API process start')
# Connect DB
db_obj = connect_db(log_obj, 'ORACLE', config.OracleConfig)
petra_sid_obj = load_petra_api(log_obj)
# Flask 인스턴스 생성
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)
api = Api(app)
parser = reqparse.RequestParser()
api.add_resource(ColQAapi, '/col_qa', resource_class_kwargs={'log': log_obj, 'db': db_obj, 'petra_sid': petra_sid_obj})

