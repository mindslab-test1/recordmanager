#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-25, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import grpc
import json
import time
import socket
import requests
import argparse
import traceback
from datetime import datetime
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
DELETE_FILE_LIST = list()
PRO_START_TIME = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    Elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def del_file(log):
    """
    Delete file
    @param      log:    Logger object
    """
    for file_path in DELETE_FILE_LIST:
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't delete {0}".format(file_path))
            continue


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def change_pwd(pwd):
    """
    Change password
    @param      pwd:            Password
    @return:                    Changed Password
    """
    payload = {
        'sPwd': pwd
    }
    result = requests.post(config.OrgUserConfig.pwd_chang_host, data=payload)
    result_dict = json.loads(result.text)
    return result_dict['tpwd']


def insert_user_info(log, db, user_file_path):
    """
    Delete and Insert into orgInfo
    @param      log:                Logger Object
    @param      db:                 DB object
    @param      user_file_path:     userInfo File path
    """
    log.info("  2) Delete and insert into userInfo")
    line_cnt = 0
    upload_cnt = 0
    log.info("    2-1) Delete QA_USR_MGT_TB(사용자관리테이블)")
    util.delete_usr_mgt_tb(db)
    log.info("    2-2) Insert QA_USR_MGT_TB(사용자관리테이블)")
    user_file_list = list()
    admin_tlno_dict = dict()
    user_file = open(user_file_path)
    for line in user_file:
        user_file_list.append(line)
        try:
            line_list = line.strip().split('|')
            if len(line_list) < 9:
                continue
            if len(line_list) == 9:
                user_id, user_pw, user_nm, aut_cd, aut_nm, brof_cd, det_cd, retire_date, user_tlno = line_list
            elif len(line_list) == 10:
                user_id, user_pw, user_nm, aut_cd, aut_nm, brof_cd, det_cd, retire_date, user_tlno, ext_no = line_list
            else:
                continue
            if aut_cd.strip() == '31':
                admin_tlno_dict[(det_cd, det_cd)] = user_tlno
        except Exception:
            log.error(traceback.format_exc())
    user_file.close()
    for item in user_file_list:
        try:
            line_cnt += 1
            line_list = item.strip().split('|')
            if len(line_list) < 9:
                log.error("\t--> Wrong format line [{0}]".format(item.strip()))
                continue
            upload_cnt += 1
            if len(line_list) == 9:
                user_id, user_pw, user_nm, aut_cd, aut_nm, brof_cd, det_cd, retire_date, user_tlno = line_list
                ext_no = None
            elif len(line_list) == 10:
                user_id, user_pw, user_nm, aut_cd, aut_nm, brof_cd, det_cd, retire_date, user_tlno, ext_no = line_list
            else:
                log.error("\t--> Wrong format line [{0}]".format(item.strip()))
                continue
            changed_pw = change_pwd(user_pw)
            enabled = 1
            try:
                re_dt = datetime.strptime(retire_date, '%Y%m%d')
                enabled = 1 if datetime.now() < re_dt else 0
            except Exception:
                pass
            if (det_cd, det_cd) in admin_tlno_dict:
                admin_tlno = admin_tlno_dict[(det_cd, det_cd)]
            else:
                admin_tlno = None
            bind = (
                user_id,
                user_id,
                user_nm,
                changed_pw,
                brof_cd,
                det_cd,
                enabled,
                1,
                aut_cd,
                admin_tlno,
                ext_no,
                socket.gethostname(),
                datetime.fromtimestamp(time.time()),
                socket.gethostname(),
                datetime.fromtimestamp(time.time())
            )
            log.info("\t  --> USER_ID= {0}, USER_NM= {1}".format(user_id, user_nm))
            util.insert_usr_mgt_tb(db, bind)
        except Exception:
            log.error(traceback.format_exc())
    log.info("  --> Done upload userInfo target count= {0}, upload count= {1}".format(line_cnt, upload_cnt))


def insert_org_info(log, db, org_file_path):
    """
    Delete and Insert into orgInfo
    @param      log:                Logger Object
    @param      db:                 DB object
    @param      org_file_path:      orgInfo File path
    """
    log.info("  1) Delete and insert into orgInfo")
    org_file = open(org_file_path)
    line_cnt = 0
    upload_cnt = 0
    log.info("    1-1) Delete QA_BASE_BROF_TB(지점정보테이블)")
    util.delete_base_brof_tb(db)
    log.info("    1-2) Insert QA_BASE_BROF_TB(지점정보테이블)")
    for line in org_file:
        try:
            line_cnt += 1
            line_list = line.strip().split('|')
            if len(line_list) != 10:
                log.error("\t--> Wrong format line [{0}]".format(line.strip()))
                continue
            upload_cnt += 1
            saes_cd, chn_cd, chn_nm, hqt_cd, hqt_nm, brof_cd, brof_nm, closed_yn, updated_tm, updator_id = line_list
            use_yn = 'N' if closed_yn in ('y', 'Y') else 'Y'
            if saes_cd == '100040':
                saes_nm = 'TM'
            elif saes_cd == '100030':
                saes_nm = 'GA'
            elif saes_cd == '100030':
                saes_nm = 'FC'
            else:
                saes_nm = ''
            if saes_cd == '100030' and brof_cd.startswith('008'):
                use_yn = 'N'
            bind = (
                brof_cd,
                brof_nm,
                saes_cd,
                saes_nm,
                hqt_cd,
                hqt_nm,
                chn_cd,
                chn_nm,
                use_yn,
                socket.gethostname(),
                datetime.fromtimestamp(time.time()),
                socket.gethostname(),
                datetime.fromtimestamp(time.time())
            )
            log.info("\t  --> BROF_CD= {0}, BROF_NM= {1}".format(brof_cd, brof_nm))
            util.insert_base_brof_tb(db, bind)
        except Exception:
            log.error(traceback.format_exc())
    org_file.close()
    log.info("  --> Done upload orgInfo target count= {0}, upload count= {1}".format(line_cnt, upload_cnt))


def main(args):
    """
    This is a program that upload orgInfo and usrInfo
    @param      args:     Arguments
    """
    log = logger.get_timed_rotating_logger(
        logger_name=config.OrgUserConfig.logger_name,
        log_dir_path=config.OrgUserConfig.log_dir_path,
        log_file_name=config.OrgUserConfig.log_file_name,
        backup_count=config.OrgUserConfig.backup_count,
        log_level=config.OrgUserConfig.log_level
    )
    log.info("[START] Upload orgInfo and usrInfo ..")
    # Connect DB
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(
            PRO_START_TIME, elapsed_time(PRO_START_TIME)))
        sys.exit(1)
    try:
        if args.input_file:
            if args.file_type.lower() == 'org':
                insert_org_info(log, db, args.input_file)
            elif args.file_type.lower() == 'user':
                insert_user_info(log, db, args.input_file)
            else:
                log.error("Wrong file type Input file type [org or user]")
        else:
            org_file_path = config.OrgUserConfig.org_file_path
            user_file_path = config.OrgUserConfig.user_file_path
            DELETE_FILE_LIST.append(org_file_path)
            DELETE_FILE_LIST.append(user_file_path)
            if os.path.exists(org_file_path):
                insert_org_info(log, db, org_file_path)
            else:
                log.info("  --> orfInfo file is not existed")
            if os.path.exists(org_file_path):
                insert_user_info(log, db, user_file_path)
            else:
                log.info("  --> userInfo file is not existed")
            #del_file(log)
    except Exception:
        log.error(traceback.format_exc())
    finally:
        try:
            db.disconnect()
        except Exception:
            pass
        log.info("[E N D] Start time = {0}, The time required = {1}".format(
            PRO_START_TIME, elapsed_time(PRO_START_TIME)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', nargs='?', action='store', dest='input_file', required=False, default=False,
                        help="Input target file path")
    parser.add_argument('-t', nargs='?', action='store', dest='file_type', required=False,
                        help="Input file type [org or user]")
    arguments = parser.parse_args()
    main(arguments)

