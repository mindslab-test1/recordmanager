#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-18, modification: 0000-00-00"

###########
# imports #
###########
import socket
from elasticsearch import Elasticsearch

host_name = socket.gethostname()
host_ip = socket.gethostbyname(host_name)
es = Elasticsearch(host=host_ip, port=9200)
document = {
    "settings": {
        "number_of_shards": 5,      # 3
        "number_of_replicas": 1     # 1
    },
    "mappings": {
        "properties": {
            "SAES_CD": {
                "type": "keyword"
            },
            "SAES_NM": {
                "type": "keyword"
            },
            "CHN_CD": {
                "type": "keyword"
            },
            "CHN_NM": {
                "type": "keyword"
            },
            "BROF_CD": {
                "type": "keyword"
            },
            "BROF_NM": {
                "type": "keyword"
            },
            "EMP_NM": {
                "type": "keyword"
            },
            "EMP_NO": {
                "type": "keyword"
            },
            "IP_CSMR_NO": {
                "type": "keyword"
            },
            "NSPL_NO": {
                "type": "keyword"
            },
            "TMS_INFO": {
                "type": "keyword"
            },
            "CSMR_NM": {
                "type": "keyword"
            },
            "PNM": {
                "type": "keyword"
            },
            "PROD_CD": {
                "type": "keyword"
            },
            "PROD_NM": {
                "type": "keyword"
            },
            "CONTRACT_DT": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
            },
            "REC_BIZ_CD": {
                "type": "keyword"
            },
            "REC_BIZ_NM": {
                "type": "keyword"
            },
            "REC_ST_DT": {
                "type": "keyword"
            },
            "REC_FILE_NM": {
                "type": "keyword"
            },
            "DATE_CREATED": {
                "type": "date"
            },
            "STT_RESULTS": {
                "type": "nested",
                "properties": {
                    "SPK_DIV_CD": {
                        "type": "keyword"
                    },
                    "STMT": {
                        "type": "text"
                    },
                    "STMT_NO": {
                        "type": "integer"
                    },
                    "STMT_NLP": {
                        "type": "text"
                    }
                }
            }
        }
    }
}
if es.indices.exists(index='stt'):
    print("Already existed index")
    es.indices.delete(index='stt', ignore=[400, 404])
    #es.indices.create(index='stt', body=document)
else:
    es.indices.create(index='stt', body=document)
    # Max result window update
    es.indices.put_settings(index='stt', body={"index": {"max_result_window": 50000}})
    # Max inner result window update
    es.indices.put_settings(index='stt', body={"index": {"max_inner_result_window": 50000}})
    # Refresh
    es.indices.refresh(index='stt')