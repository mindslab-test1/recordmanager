#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-18, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import socket
import argparse
import traceback
import collections
import elasticsearch
from datetime import date, datetime, timedelta
from elasticsearch import Elasticsearch, helpers
from cfg import config
from lib import logger, util, db_connection, nlp, libpcpython


###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#############
# constants #
#############
PETRA_SID = str()
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:        Logger Object
    """
    global PETRA_SID
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    PETRA_SID = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))


def execute_ela_deleting(log, es_client, index):
    """
    Execute Elasticsearch deleting
    @param      log:                Logger object
    @param      es_client:          Elasticsearch Client
    @param      index:              Index name
    """
    target_datetime = datetime.now() - timedelta(days=config.ElaConfig.mtn_period)
    del_target_date = str(target_datetime.date())
    log.info('\t--> Delete index data DATE_CREATED before {0}'.format(del_target_date))
    doc = {
        'query': {
            'range': {
                "DATE_CREATED": {
                    'lt': del_target_date
                }
            }
        }
    }
    es_client.delete_by_query(index=index, body=doc)
    log.info('\t--> Success deleted index data')


def execute_ela_indexing(log, es_client, index, prepro_results):
    """
    Execute Elasticsearch indexing
    @param      log:                Logger object
    @param      es_client:          Elasticsearch Client
    @param      index:              Index name
    @param      prepro_results:     Preprocessing results
    """
    tmp_cnt = 1
    err_cnt = 0
    file_cnt = 0
    docs_list = list()
    total_cnt = len(prepro_results.keys())
    for id, doc in prepro_results.items():
        try:
            file_cnt += 1
            if file_cnt == 1000:
                elasticsearch.helpers.bulk(es_client, docs_list)
                prg_prc = (file_cnt * tmp_cnt / float(total_cnt) * 100.0)
                prg_cnt = file_cnt * tmp_cnt
                log.info("\t\t--> Done {0}% .. [{1} / {2}]".format(prg_prc, prg_cnt, total_cnt))
                tmp_cnt += 1
                file_cnt = 0
                docs_list = list()
            docs_list.append({'_op_type': 'index', '_index': index, '_id': id, '_source': doc})
            #es_client.index(index=index, id=id, body=doc)
        except Exception:
            err_cnt += 1
            log.error(traceback.format_exc())
            log.error("Error file= {0}".format(id))
    if docs_list:
        elasticsearch.helpers.bulk(es_client, docs_list)
    log_str = "\t\t--> Done 100% .."
    log_str += " [Total: {0}, Success: {1}, Fail: {2}]".format(total_cnt, total_cnt - err_cnt, err_cnt)
    log.info(log_str)


def execute_preprocessing(log, nlp_client, stt_results):
    """
    Execute preprocessing stt results
    @param      log:                Logger object
    @param      nlp_client:         NLP Client
    @param      stt_results:        STT results
    @return                         Preprocessing results
    """
    prepro_results = collections.OrderedDict()
    now_date = datetime.now()
    for item in stt_results:
        try:
            stmt = item['STMT'].strip()
            dec_stmt = libpcpython.PcAPI_dec_id(PETRA_SID, config.PetraConfig.enc_cold_id, stmt, len(stmt))
            sent, nlp_sent, morph_sent = nlp_client.analyze(dec_stmt)
            morph_sent_list = morph_sent.split()
            stmt_nlp = ''
            for morph_sent in morph_sent_list:
                morph_list = morph_sent.split('/')
                if len(morph_list) == 2:
                    word = morph_list[0]
                    morph = morph_list[1]
                else:
                    word = '/'
                    morph = morph_list[1]
                if morph in ['NNG'] and len(word) >= 6:
                    stmt_nlp += ' ' + word
            line_info = {
                "SPK_DIV_CD": item['SPK_DIV_CD'].strip(),
                "STMT": dec_stmt,
                "STMT_NO": item['STMT_NO'].strip(),
                "STMT_NLP": stmt_nlp,
            }
            key = "{0}_{1}_{2}".format(item['NSPL_NO'].strip(), item['TMS_INFO'], item['REC_KEY'])
            if key in prepro_results:
                prepro_results[key]['STT_RESULTS'].append(line_info)
            else:
                modified_st_dt = datetime.strftime(now_date, '%Y-%m-%d %H:%M:%S')
                if item['REC_BIZ_CD'] == 'C':
                    rec_biz_nm = '청약'
                elif item['REC_BIZ_CD'] == 'R':
                    rec_biz_nm = '보완'
                elif item['REC_BIZ_CD'] == 'E':
                    rec_biz_nm = '일반'
                elif item['REC_BIZ_CD'] == 'S':
                    rec_biz_nm = '상담'
                else:
                    rec_biz_nm = str()
                prepro_results[key] = {
                    "HQT_CD": item['HQT_CD'],
                    "HQT_NM": item['HQT_NM'],
                    "CHN_CD": item['CHN_CD'],
                    "CHN_NM": item['CHN_NM'],
                    "BROF_CD": item['BROF_CD'],
                    "BROF_NM": item['BROF_NM'],
                    "EMP_NM": item["EMP_NM"],
                    "EMP_NO": item["EMP_NO"],
                    "IP_CSMR_NO": item["IP_CSMR_NO"],
                    "NSPL_NO": item['NSPL_NO'],
                    "TMS_INFO": item['TMS_INFO'],
                    "CSMR_NM": item['CSMR_NM'],
                    "PNM": item['PNM'],
                    "PROD_CD": item['PROD_CD'],
                    "PROD_NM": item['PROD_NM'],
                    "CONTRACT_DT": item['CONTRACT_DT'],
                    "REC_BIZ_CD": item['REC_BIZ_CD'],
                    "REC_BIZ_NM": rec_biz_nm,
                    "REC_ST_DT": item['REC_ST_DT'],
                    "REC_FILE_NM": item['REC_FILE_NM'],
                    "DATE_CREATED": now_date,
                    "STT_RESULTS": [line_info]
                }
        except Exception:
            log.error(traceback.format_exc())
            continue
    log.info("\t\t--> Total target record file count: {0}".format(len(prepro_results.keys())))
    return prepro_results


def select_stt_results(log, mssql_db, host_nm, st_dt):
    """
    Select STT results
    @param      log:            Logger object
    @param      mssql_db:       MSSQL DB object
    @param      host_nm:        Host name
    @param      st_dt:          Search start date
    @return:                    STT results
    """
    ed_dt = st_dt + timedelta(days=1)
    log.info('\t\t--> Search between {0} and {1}, host name= {2}'.format(st_dt, ed_dt, host_nm))
    stt_results = util.select_ela_stt_target(mssql_db, st_dt, ed_dt, host_nm)
    if stt_results:
        log.info('\t\t--> Success selected STT results [line count: {0}]'.format(len(stt_results)))
    else:
        log.info("\t\t--> No STT results data")
    return stt_results


def main(args):
    """
    This program that
    @param      args:    Arguments
    """
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.ElaConfig.logger_name,
        log_dir_path=config.ElaConfig.log_dir_path,
        log_file_name=config.ElaConfig.log_file_name,
        backup_count=config.ElaConfig.backup_count,
        log_level=config.ElaConfig.log_level
    )
    log.info("[START] Execute STT results indexing ..")
    if args.wait > 0:
        log.info("Waiting ... ({0}/s)".format(args.wait))
        time.sleep(args.wait)
    # Connect DB and Load Petra cipher API
    try:
        db_obj = connect_db(log, 'ORACLE', config.OracleConfig)
        load_petra_api(log)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    try:
        log.info('[Target date= {0}, Search term= {1}, maintain period]'.format(
            args.target_date, args.search_term, config.ElaConfig.mtn_period))
        log.info('1. Load NLP client')
        nlp_client = nlp.NlpClient(config.ElaConfig.nlp_engine)
        log.info('\t--> Success load NLP client')
        log.info('2. Load ElasticSearch client')
        host_nm = socket.gethostname()
        host_ip = socket.gethostbyname(host_nm)
        target_host_nm_list = config.ElaConfig.host_list[host_nm] if host_nm in config.ElaConfig.host_list else list()
        if target_host_nm_list:
            log.info('\t--> Target host list= {0}'.format(target_host_nm_list))
            log.info('\t--> Elasticsearch host= {0}, port= {1}'.format(host_ip, config.ElaConfig.port))
            es_client = Elasticsearch(host=host_ip, port=config.ElaConfig.port)
            log.info('\t--> Success load Elasticsearch client')
            for target_host_nm in target_host_nm_list:
                log.info('-' * 100)
                log.info('3. Execute Host[{0}]'.format(target_host_nm))
                for cnt in range(0, int(args.search_term)):
                    log.info('\t' + '*' * 96 + '\t')
                    st_dt = datetime.strptime(args.target_date, '%Y%m%d') + timedelta(days=cnt)
                    log.info('\t3-{0}) Target date {1})'.format(cnt + 1, st_dt.date()))
                    log.info('\t  1) Select STT results')
                    stt_results = select_stt_results(log, db_obj, target_host_nm, st_dt)
                    if stt_results:
                        log.info('\t  2) Execute preprocessing stt results')
                        prepro_results = execute_preprocessing(log, nlp_client, stt_results)
                        log.info('\t  3) Execute Elasticsearch indexing')
                        execute_ela_indexing(log, es_client, args.index, prepro_results)
                log.info('\t' + '*' * 96 + '\t')
            log.info('-' * 100)
            log.info('4. Delete old index data')
            execute_ela_deleting(log, es_client, args.index)
        else:
            log.info("\t--> Can't find target host list")
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', action='store', dest='target_date', required=False, type=str,
                        default=(date.today() - timedelta(days=1)).strftime('%Y%m%d'),
                        help='Select target date [default= System Yesterday]')
    parser.add_argument('-s', action='store', dest='search_term', required=False, type=int,
                        default=1,
                        help='Search term day [default= 1 day]')
    parser.add_argument('-i', action='store', dest='index', required=False, type=str,
                        default='stt',
                        help='Index name [default= stt]')
    parser.add_argument('-w', action='store', dest='wait', required=False, type=int,
                        default=0,
                        help='Waiting time to run [default= 0]')
    arguments = parser.parse_args()
    main(arguments)
