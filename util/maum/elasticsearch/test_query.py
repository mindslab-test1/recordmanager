#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-06-05, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import pprint
import socket
import traceback
from datetime import datetime
from elasticsearch import Elasticsearch, helpers
import elasticsearch


###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


########
# main #
########
def main():
    """
    This program that
    """
    host_nm = socket.gethostname()
    host_ip = socket.gethostbyname(host_nm)
    es = Elasticsearch(host=host_ip, port=9200)
    id = '0117515000003_3_002917!@#$20210318094956-01053401907-1006031897.wav'
    doc = {
        '_source': [
            'BIZ_DIV_CD',
            'BO_ID',
            'BO_NAME',
            'BRANCH_ID',
            'BRANCH_NAME',
            'HQ_ID',
            'HQ_NAME',
            'PRV_COT_POL_NO',
            'RECORD_FILE_NAME',
            'RECORD_START_DATE',
            'REC_ID'
        ],
        'query': {
            'nested': {
                "path": "STT_RESULTS",
                "query": {
                    "match": {
                        "STT_RESULTS.STMT": "상담원"
                    }
                },
                "inner_hits": {"size": 10000}
            }
        }
    }
    #
    doc = {
            '_source': [
                'REC_FILE_NM',
                'REC_ST_DT',
                'CONTRACT_DT',
                'REC_BIZ_NM'
            ],
            "query":{
                "bool": {
                    "filter": [
                        {"range":{"CONTRACT_DT":{"gte":"2021-01-01","lte":"2021-03-23"}}},
                        {"nested":{
                                "path": "STT_RESULTS",
                                "query":{
                                    "bool":{
                                        "filter":
                                            [
                                                {"query_string": {"query": "STT_RESULTS.STMT:(은행 AND 이용)", "default_operator": "and"}}
                                            ]
                                    }
                                },
                                "inner_hits": {"size": 10000}
                            }
                        },
                    ]
                }
            },
            "sort": [{"CONTRACT_DT": {"order": "desc"}}],
            "size": 3
    }
    # Select example
    #results = es.search(index='stt', body={'query': {'match_all': {}}}, size=1)
    #pprint.pprint(results)
    body = {
        '_source': [
           'STMT',
           'STMT_NLP',
           'REC_BIZ_NM'
        ],
        'query': {
            'nested': {
                'path': 'STT_RESULTS',
                'query': {
                    'match': {
                       'STMT_NLP': '어머니'
                    }
                }
            }
        }
    }
    results = es.search(index='stt', body=doc, size=10000)
#    results = es.search(index='stt', body={'query': {"nested": {"path": "STT_RESULTS", "query": {"bool": {"must": [{'match': {'STMT_NLP':'어머니'}}]}}}}}, size=10000)
    pprint.pprint(results)
    # results = es.search(index='test', body={'query': {'match': {'STMT_NLP': '안녕', 'STMT': '안녕'}}}, size=10000)
    # print results
    # results = es.search(index='test', body={'query': {'bool': {'must': [{'STMT_NLP': '안녕'}, {'STMT': '안녕'}]}}})
    # print results
    #results = es.search(index='test', body={'query': {'bool': {'must': [ {"match": {"STMT": "안녕"}}, {"match": {"STMT_NLP": '안녕'}}]}}})
    #pprint.pprint(results)
    # results = es.get(index='test', id=id, ignore=404)
    # Insert example
    # --> Single
    now_date = datetime.now()
    doc = {
        "HQT_CD": '000404',
        "HQT_NM": 'CM',
        "CHN_CD": '04',
        "CHN_NM": 'OB',
        "BROF_CD": '002917',
        "BROF_NM": '미래인슈',
        "EMP_NM": '최명선',
        "EMP_NO": '1006031897',
        "IP_CSMR_NO": '011751500',
        "NSPL_NO": '0117515000003',
        "TMS_INFO": '3',
        "CSMR_NM": '김동석',
        "PNM": '김동석',
        "PROD_CD": '20462',
        "PROD_NM": '(무)흥국생명 내사랑내곁에 치매간병보험(해지환급금 미지급형V3)',
        "CONTRACT_DT": '20210316',
        "REC_BIZ_CD": 'R',
        "REC_BIZ_NM": '보완',
        "REC_ST_DT": '2021-03-18',
        "REC_FILE_NM": '20210318094956-01053401907-1006031897.wav',
        "DATE_CREATED": now_date,
        "STT_RESULTS": [
            {
                "SPK_DIV_CD": 'M',
                "STMT": '네 안녕하세요 고객님',
                "STMT_NO": '1',
                "STMT_NLP": '안녕 고객',
            }
        ]
    }
    #es.index(index='stt', id=id, body=doc)
    # --> Bulk
    #docs_list = list()
    #docs_list.append({'_op_type': 'index', '_index': 'stt', '_id': id, '_source': doc})
    #elasticsearch.helpers.bulk(es, docs_list)
    # Update example
    # --> Single
    """
    doc = {
        "doc": {
            "BIZ_DIV_CD": '계약',
            "PRV_COT_POL_NO": 'SAMPLE',
            "TRANSFER_NUMBER": 'SAMPLE'
        },
        "doc_as_upsert": True
    }
    es.update(index='test', id=id, body=doc)
    """
    # --> Bulk
    """
    docs_list.append({'_op_type': 'update', '_index': 'test', '_id': id, '_source': doc})
    elasticsearch.helpers.bulk(es, docs_list)
    """
    # Print example
    #for result in results['hits']['hits']:
    #    print(result['_source']['STMT_NLP'], '-->', result['_source']['STMT'])


if __name__ == '__main__':
    main()
