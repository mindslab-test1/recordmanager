#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-02, modification: 0000-00-00"

###########
# imports #
###########
import zmq
import json
import time
import socket
#import websocket
import traceback
from cfg import config
from lib import logger


#########
# class #
#########
class CallEventReceiver(object):
    def __init__(self):
        self.ws = None
        self.post_ws = None
        self.conf = config.EventReceiverConfig
        # Set logger
        self.logger = logger.get_timed_rotating_logger(
            logger_name=self.conf.logger_name,
            log_dir_path=self.conf.log_dir_path,
            log_file_name=self.conf.log_file_name,
            backup_count=self.conf.backup_count,
            log_level=self.conf.log_level
        )
        self.host_nm = socket.gethostname()

    def connect_ws(self):
        flag = True
        while flag:
            try:
                # Websocket Server에 메시지를 보내기 위한 websocket client
                self.ws = websocket.create_connection(
                    config.EventReceiverConfig.ws_addr_list[self.host_nm], setdefaulttimeout=3
                )
                flag = False
            except socket.error as e:
                self.logger.error(e)
                time.sleep(10)
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(10)

    def connect_post_ws(self):
        flag = True
        while flag:
            try:
                # Websocket Server에 메시지를 보내기 위한 websocket client
                self.post_ws = websocket.create_connection(
                    config.EventReceiverConfig.post_ws_addr_list[self.host_nm], setdefaulttimeout=3
                )
                flag = False
            except socket.error as e:
                self.logger.error(e)
                time.sleep(10)
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(10)

    def send_ws(self, message):
        if self.ws is None:
            self.connect_ws()
        if self.ws:
            self.ws.send(message)

    def send_post_ws(self, message):
        if self.post_ws is None:
            self.connect_post_ws()
        if self.post_ws:
            self.post_ws.send(message)

    def run(self):
        #self.connect_ws()
        #self.connect_post_ws()
        done = True
        context = zmq.Context()
        endpoint = config.EventReceiverConfig.zmq_host
        collector = context.socket(zmq.PULL)
        collector.bind(endpoint)
        poller = zmq.Poller()
        #self.logger.info('poller: {}, {}'.format(endpoint, poller))
        poller.register(collector, zmq.POLLIN)
        while done:
            try:
                socks = dict(poller.poll())
                #self.logger.info('socks: {}'.format(socks))
                if collector in socks and socks[collector] == zmq.POLLIN:
                    message = collector.recv_string()
                    self.logger.info('message: {}'.format(message))
                    #self.send_ws(message)
                    #self.send_post_ws(message)
                    data = json.loads(message)
                    data = dict((k.lower(), v) for k, v in data.items())
                    event_type = data['eventtype']
                    event = data['event']
                    if event_type == "CALL" and event != "subscribe":
                        if not data['agent']:
                            continue
                    if event_type == 'CALL':
                        if data['status'] == "CALLING":
                            self.logger.info('[CALLING     ] REC_KEY= {0}'.format(data['call_id']))
                        elif data['status'] == "CONNECTED":
                            self.logger.info('[CONNECTED   ] REC_KEY= {0}'.format(data['call_id']))
                        elif data['status'] == "DISCONNECTED":
                            self.logger.info('[DISCONNECTED] REC_KEY= {0}'.format(data['call_id']))
                        else:
                            self.logger.error('unknown status type: {}'.format(data['status']))
            except KeyboardInterrupt:
                self.logger.info('program stopped')
                done = False
            except Exception:
                self.logger.error(traceback.format_exc())
                done = False
            time.sleep(0.1)


#######
# def #
#######
def main():
    """
    This program that Websocket Server
    """
    try:
        er = CallEventReceiver()
        er.run()
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    main()
