#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-15, modification: 0000-00-00"

###########
# imports #
###########
import sys
import time
import signal
import socket
import datetime
import traceback
import multiprocessing
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class SttDaemon(object):
    def __init__(self):
        self.conf = config.SttDaemonConfig
        # Set logger
        self.logger = logger.get_timed_rotating_logger(
            logger_name=self.conf.logger_name,
            log_dir_path=self.conf.log_dir_path,
            log_file_name=self.conf.log_file_name,
            backup_count=self.conf.backup_count,
            log_level=self.conf.log_level
        )
        self.db = self.connect_db('ORACLE', config.OracleConfig)

    def set_sig_handler(self):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        if frame:
            pass
        if sig == signal.SIGHUP:
            return
        if sig == signal.SIGTERM or sig == signal.SIGINT:
            if self.db:
                self.db.conn.rollback()
                self.db.disconnect()
            self.logger.info('stopped by interrupt')
            self.logger.info('[E N D] STT daemon Process stopped')
            sys.exit(0)

    def connect_db(self, db_type, db_conf):
        db = ''
        flag = True
        while flag:
            try:
                self.logger.info('Try connecting to {0} DB ...'.format(db_type))
                if db_type.upper() == 'ORACLE':
                    db = db_connection.Oracle(db_conf, failover=True, service_name=True)
                    flag = False
                elif db_type.upper() == 'MSSQL':
                    db = db_connection.MSSQL(db_conf)
                    flag = False
                elif db_type.upper() == 'MYSQL':
                    db = db_connection.MYSQL(db_conf)
                    flag = False
                else:
                    raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
                self.logger.info('Success connect to {0} DB'.format(db_type))
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(60)
        return db

    def make_job_list(self):
        result = util.select_stt_target(self.db)
        if not result:
            return list()
        return result

    def run(self):
        try:
            self.logger.info('[START] STT daemon process started')
            pid_list = list()
            while True:
                # STT engine reset time (23:50 ~ 23:59)
                now_time = datetime.datetime.now().time()
                open_time = datetime.time(23, 50, 0)
                closed_time = datetime.time(23, 59, 59)
                if open_time < now_time < closed_time:
                    self.logger.info('STT engine reset time (23:50 ~ 00:00)')
                    time.sleep(30)
                    continue
                # Make job list
                try:
                    prc_max_cnt = self.conf.prc_max_limit
                    if self.conf.prc_max_limit < self.conf.top_priority_prc:
                        self.conf.top_priority_prc = self.conf.prc_max_limit
                    result_list = self.make_job_list()
                except Exception:
                    self.logger.error(traceback.format_exc())
                    time.sleep(10)
                    continue
                # Check child process
                for pid in pid_list[:]:
                    if not pid.is_alive():
                        pid_list.remove(pid)
                # Execute STT process
                for job in result_list[:self.conf.prc_max_limit]:
                    try:
                        # 우선 순위가 0순위가 아닐 경우 가용 프로세스 수를 제한 (우선순위 프로세스 수를 제외)
                        if job['PROC_PRI'] != 0:
                            prc_max_cnt = self.conf.prc_max_limit - self.conf.top_priority_prc
                        # 운영 요일이 주밀일 경우 우선 순위에 따른 가용 프로세스 수를 제한하지 않음
                        if 5 <= datetime.datetime.now().weekday() <= 6:
                            prc_max_cnt = self.conf.prc_max_limit
                        # 운영 시간이 아닐 경우 가용 프로세스 수를 제한하지 않음
                        now_time = datetime.datetime.now().time()
                        open_time = datetime.time(self.conf.biz_opn_hr, self.conf.biz_opn_mn, 0)
                        closed_time = datetime.time(self.conf.biz_cls_hr, self.conf.biz_cls_mn, 0)
                        if not open_time < now_time < closed_time:
                            prc_max_cnt = self.conf.prc_max_limit
                        if len(pid_list) >= prc_max_cnt:
                            self.logger.info('Processing Count is MAX ...')
                            time.sleep(10)
                            break
                        """
                        job(
                            PROC_PRI,          처리우선순위
                            REC_KEY,           녹취KEY
                            REC_ID,            녹취ID
                            REC_FILE_NM,       녹취파일명
                            STT_REC_FILE_NM,   실시간STT녹취파일명
                            REC_COMP_CD,       녹취업체구분코드
                            CHN_CLA_CD,        채널구분코드
                            REC_ST_DT,         녹취시작일자
                            REC_ST_TM,         녹취시작시간
                            REC_ED_TM,         녹취종료시간
                            REC_DU_TM,         녹취통화시간
                            BROF_CD,           지점코드
                            EMP_NO,            사원번호
                            STEREO_SPLIT_YN    스테레오분할여부
                        )
                        """
                        p = multiprocessing.Process(
                            target=do_task,
                            args=(
                                job,
                            )
                        )
                        p.daemon = None
                        pid_list.append(p)
                        p.start()
                        # Update STT status
                        util.update_stt_status(
                            db=self.db,
                            host_nm=socket.gethostname(),
                            prog_stat_cd='11', # STT대기
                            prog_stat_dtl_cd=None,
                            rec_key=job['REC_KEY'],
                            rec_id=job['REC_ID'],
                            rec_file_nm=job['REC_FILE_NM'],
                            rec_st_dt=job['REC_ST_DT']
                        )
                        log_str_list = list()
                        log_str_list.append("Execute [PROC_PRI(처리우선순위)= {0},".format(job['PROC_PRI']))
                        log_str_list.append("REC_ST_DT(녹취시작일자)= {0}".format(job['REC_ST_DT']))
                        log_str_list.append("REC_KEY(녹취KEY)= {0},".format(job['REC_KEY']))
                        log_str_list.append("REC_ID(녹취ID)= {0},".format(job['REC_ID']))
                        log_str_list.append("REC_FILE_NM(녹취파일명)= {0}]".format(job['REC_FILE_NM']))
                        self.logger.info(' '.join(log_str_list))
                        time.sleep(self.conf.prc_interval)
                    except Exception:
                        self.logger.error(traceback.format_exc())
                        log_str_list = list()
                        log_str_list.append("Execute [PROC_PRI(처리우선순위)= {0},".format(job['PROC_PRI']))
                        log_str_list.append("REC_ST_DT(녹취시작일자)= {0}".format(job['REC_ST_DT']))
                        log_str_list.append("REC_KEY(녹취KEY)= {0},".format(job['REC_KEY']))
                        log_str_list.append("REC_ID(녹취ID)= {0},".format(job['REC_ID']))
                        log_str_list.append("REC_FILE_NM(녹취파일명)= {0}]".format(job['REC_FILE_NM']))
                        self.logger.info(' '.join(log_str_list))
                        continue
                self.db.conn.commit()
                time.sleep(5)
        except KeyboardInterrupt:
            self.logger.info('stopped by interrupt')
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            if self.db:
                self.db.conn.commit()
                self.db.disconnect()
            self.logger.info('[E N D] STT daemon process stopped')


#######
# def #
#######
def do_task(job):
    """
    Process execute stt_process.py
    :param      job:        Job
    """
    import stt_process
    reload(stt_process)
    stt_process.main(job)


def main():
    """
    This is a program that STT Daemon process
    """
    try:
        stt_daemon = SttDaemon()
        stt_daemon.run()
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    main()
