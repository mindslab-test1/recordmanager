#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-05-12, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import grpc
import json
import argparse
from google.protobuf import json_format
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
from common.config import Config
from maum.common import lang_pb2
from maum.brain.nlp import nlp_pb2, nlp_pb2_grpc
from maum.brain.hmd import hmd_pb2, hmd_pb2_grpc

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class NlpClient(object):
    def __init__(self, engine):
        self.conf = Config()
        self.conf.init('brain-ta.conf')
        if engine.lower() == 'nlp1':
            self.remote = 'localhost:{0}'.format(self.conf.get('brain-ta.nlp.1.kor.port'))
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)
        elif engine.lower() == 'nlp2':
            self.remote = 'localhost:{0}'.format(self.conf.get('brain-ta.nlp.2.kor.port'))
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)
        elif engine.lower() == 'nlp3':
            self.remote = 'localhost:{0}'.format(self.conf.get('brain-ta.nlp.3.kor.port'))
            channel = grpc.insecure_channel(self.remote)
            self.stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)
        else:
            LOGGER.error('Not existed Engine')
            raise Exception('Not existed Engine')

    def analyze(self, target_text):
        in_text = nlp_pb2.InputText()
        try:
            in_text.text = target_text.replace('*', ' ')
        except Exception:
            in_text.text = unicode(target_text.replace('*', ' '), 'euc-kr').encode('utf-8')
            target_text = unicode(target_text, 'euc-kr').encode('utf-8')
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.use_space = False
        in_text.level = 1
        in_text.keyword_frequency_level = 0
        ret = self.stub.Analyze(in_text)
        nlp_sent = ''
        morph_sent = ''
        for idx in range(len(ret.sentences)):
            nlp_word_list = list()
            morph_word_list = list()
#            text = ret.sentences[idx].text
            analysis = ret.sentences[idx].morps
            for ana_idx in range(len(analysis)):
                morphs_word = analysis[ana_idx].lemma.encode('utf-8')
                morphs_type = analysis[ana_idx].type.split('[')[0].strip()
                if morphs_type in ['VV', 'VA', 'VX', 'VCP', 'VCN']:
                    nlp_word_list.append('{0}다'.format(morphs_word))
                    morph_word_list.append('{0}다/{1}'.format(morphs_word, morphs_type))
                else:
                    nlp_word_list.append('{0}'.format(morphs_word))
                    morph_word_list.append('{0}/{1}'.format(morphs_word, morphs_type))
            nlp_sent += ' '.join(nlp_word_list) + ' '
            morph_sent += ' '.join(morph_word_list) + ' '
        result = (target_text, nlp_sent.strip(), morph_sent.strip())
        return result


class HmdClient(object):
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-ta.conf')
        remote = 'localhost:{0}'.format(self.conf.get('brain-ta.hmd.front.port'))
        channel = grpc.insecure_channel(remote)
        self.stub = hmd_pb2_grpc.HmdClassifierStub(channel)

    def set_model(self, model_name, hmd_model_list):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = model_name
        rules_list = list()
        for item in hmd_model_list:
            hmd_client = hmd_pb2.HmdRule()
            hmd_client.rule = item[1]
            hmd_client.categories.extend(item[0])
            rules_list.append(hmd_client)
        model.rules.extend(rules_list)
        self.stub.set_model(model)

    def get_class_by_text(self, model_name, text, nlp_vs='nlp3'):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = model_name
        in_doc.lang = lang_pb2.kor
        in_doc.nlp_vs = nlp_vs
        ret = self.stub.get_class_by_text(in_doc)
        json_ret = json_format.MessageToJson(ret, True)
        return json_ret

    def load_model(self, model_name):
        model_path = '{0}/{1}__0.hmdmodel'.format(self.conf.get('brain-ta.hmd.model.dir'), model_name)
        in_file = open(model_path, 'rb')
        hm = hmd_pb2.HmdModel()
        hm.ParseFromString(in_file.read())
        in_file.close()
        return hm

    def del_hmd_model(self, model_name):
        model_path = '{0}/{1}__0.hmdmodel'.format(self.conf.get('brain-ta.hmd.model.dir'), model_name)
        if os.path.exists(model_path):
            os.remove(model_path)


#######
# def #
#######
def main(args):
    """
    This is a program that execute HMD
    @param      args:     Arguments
    """
    hmd_file = open(args.hmd_file)
    # Make HMD model
    hmd_model_list = list()
    hmd_line_num = 0
    for line in hmd_file:
        hmd_line_num += 1
        line = line.strip()
        line_list = line.split('\t')
        if len(line_list) != 2:
            print("[ERROR] Line number {0} isn't two column".format(hmd_line_num))
            continue
        cate, rule = line_list
        hmd_model_list.append(([cate, ], rule))
    hmd_client = HmdClient()
    hmd_client.set_model(args.hmd_file, hmd_model_list)
    hmd_file.close()
    # Execute HMD
    overlap_dict = dict()
    target_file = open(args.input_file)
    output_file = open(args.input_file + '_output', 'w')
    nlp_client = NlpClient(args.nlp_vs)
    for line in target_file:
        target_sent = line.strip()
        if len(target_sent) < 1:
            continue
        target_text, nlp_sent, morph_sent = nlp_client.analyze(target_sent)
        result = hmd_client.get_class_by_text(args.hmd_file, target_text, args.nlp_vs)
        temp = json.loads(result)
        for item in temp['cls']:
            overlap_key = "{0}\t{1}\t{2}\t{3}".format(
                item['category'], item['sentence'], item['searchKey'], item['pattern'])
            if overlap_key in overlap_dict:
                continue
            overlap_dict[overlap_key] = 1
            print("{0}\t{1}\t{2}\t{3}\t{4}".format(
                item['category'], item['sentence'], item['nlpSentence'], item['searchKey'], item['pattern']))
            print >> output_file, "{0}\t{1}\t{2}\t{3}\t{4}".format(
                item['category'], item['sentence'], item['nlpSentence'], item['searchKey'], item['pattern'])
        if not temp['cls']:
            print("None\t{0}\t{1}\tNone\tNone".format(target_text, nlp_sent))
            print >> output_file, "None\t{0}\t{1}\tNone\tNone".format(target_sent, nlp_sent)
    hmd_client.del_hmd_model(args.hmd_file)
    target_file.close()
    output_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', nargs='?', action='store', dest='input_file', required=True,
                        help="Input target text file")
    parser.add_argument('-t', nargs='?', action='store', dest='hmd_file', required=True,
                        help="Input HMD file")
    parser.add_argument('-e', nargs='?', action='store', dest='nlp_vs', default='nlp3',
                        help="Choose NLP engine version [Default= nlp3]\n[ex) nlp1 / nlp2 / nlp3 ]")
    arguments = parser.parse_args()
    main(arguments)
