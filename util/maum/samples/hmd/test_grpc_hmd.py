#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-24, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import grpc
import json
from google.protobuf import json_format
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
from common.config import Config
from maum.common import lang_pb2
from maum.brain.hmd import hmd_pb2, hmd_pb2_grpc

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class HmdClient(object):
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-ta.conf')
        remote = 'localhost:{0}'.format(self.conf.get('brain-ta.hmd.front.port'))
        channel = grpc.insecure_channel(remote)
        self.stub = hmd_pb2_grpc.HmdClassifierStub(channel)

    def set_model(self, model_name, hmd_model_list):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = model_name
        rules_list = list()
        for item in hmd_model_list:
            hmd_client = hmd_pb2.HmdRule()
            hmd_client.rule = item[1]
            hmd_client.categories.extend(item[0])
            rules_list.append(hmd_client)
        model.rules.extend(rules_list)
        self.stub.set_model(model)

    def get_class_by_text(self, model_name, text, nlp_vs='nlp3'):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = model_name
        in_doc.lang = lang_pb2.kor
        in_doc.nlp_vs = nlp_vs
        ret = self.stub.get_class_by_text(in_doc)
        json_ret = json_format.MessageToJson(ret, True)
        return json_ret

    def load_model(self, model_name):
        model_path = '{0}/{1}__0.hmdmodel'.format(self.conf.get('brain-ta.hmd.model.dir'), model_name)
        in_file = open(model_path, 'rb')
        hm = hmd_pb2.HmdModel()
        hm.ParseFromString(in_file.read())
        in_file.close()
        return hm

    def del_hmd_model(self, model_name):
        model_path = '{0}/{1}__0.hmdmodel'.format(self.conf.get('brain-ta.hmd.model.dir'), model_name)
        if os.path.exists(model_path):
            os.remove(model_path)

    def hmd_model_path(self, model_name):
        model_path = '{0}/{1}__0.hmdmodel'.format(self.conf.get('brain-ta.hmd.model.dir'), model_name)
        return model_path


def test_hmd():
    hmd_client = HmdClient()
    hmd_model_list = [(['AA', 'BB'], '(안녕)')]
    hmd_client.set_model('hmd_test', hmd_model_list)
    # hmd_client.load_model('hmd_test')
    # hmd_client.set_matrix('hmd_test', hmd_model_list)
    result = hmd_client.get_class_by_text("hmd_test", "안녕하세요. HMD 분석을 테스트합니다.", 'nlp3')
    temp = json.loads(result)
    for item in temp['cls']:
        print('-' * 100)
        print(u"category : {0}".format(item['category']))
        print(u"searchKey : {0}".format(item['searchKey']))
        print(u"sentSeq : {0}".format(item['sentSeq']))
        print(u"sentence : {0}".format(item['sentence']))
        print(u"pattern : {0}".format(item['pattern']))
        print(u"nlpSentence : {0}".format(item['nlpSentence']))
    print('-' * 100)
    hmd_client.del_hmd_model('hmd_test')


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    test_hmd()
