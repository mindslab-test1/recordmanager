#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'https://10.18.131.39:8443/api/pwdChange'
    host = 'https://sttd.heungkuklife.co.kr:8443/api/pwdChange'
    payload = {
        'sPwd': 'pP5KGbfLq3nAGqrmQQ3haNsBsYB9t3bb59vizmjq0WMA='
    }
    result = requests.post(host, data=payload)
    print(result)
    if result:
        result_dict = json.loads(result.text)
        for key, value in result_dict.items():
            print("{0}: {1}".format(key, value))
