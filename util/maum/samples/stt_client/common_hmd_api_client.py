#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://{0}:{1}/common_fwd_load'.format(socket.gethostbyname(socket.gethostname()), '8788')
    print("*  Check API [{0}]  *".format(host))
    result = requests.post(host, )
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/common_script_load'.format(socket.gethostbyname(socket.gethostname()), '8788')
    print("*  Check API [{0}]  *".format(host))
    result = requests.post(host, )
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/common_script_start_load'.format(socket.gethostbyname(socket.gethostname()), '8788')
    print("*  Check API [{0}]  *".format(host))
    result = requests.post(host, )
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/encrypt_string'.format(socket.gethostbyname(socket.gethostname()), '8788')
    print("*  Check API [{0}]  *".format(host))
    payload = {'string': '테스트 문장입니다'}
    result = requests.post(host, data=payload)
    print(json.loads(result.text))
    enc_string = json.loads(result.text)['rst']
    print(enc_string)
    ##
    host = 'http://{0}:{1}/decrypt_string'.format(socket.gethostbyname(socket.gethostname()), '8788')
    print("*  Check API [{0}]  *".format(host))
    payload = {'string': enc_string}
    result = requests.post(host, data=payload)
    print(json.loads(result.text))
    dec_string = json.loads(result.text)['rst']
    print(dec_string)