#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://{0}:{1}/rec_conv'.format(socket.gethostbyname(socket.gethostname()), 8786)
    payload = {
        'rec_st_dt': '2020-11-26',
        'brof_cd': '002311',
        'rec_file_nm': '20201126183801-0232104783-01095062901.wav'
    }
    result = requests.post(host, json=payload)
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/dec_tran_rec'.format(socket.gethostbyname(socket.gethostname()), 8786)
    payload = {
        'rec_st_dt': '2021-03-11',
        'srv_host': '10.10.106.168', # '18.18.2.81'
        'stt_rec_file_nm': '24679390293202154639@192.168.30.168.mp3'
    }
    result = requests.post(host, json=payload)
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/upload_tran_rec'.format(socket.gethostbyname(socket.gethostname()), 8786)
    payload = {
        'rec_st_dt': '2020-11-26',
        'brof_cd': '002311',
        'rec_file_nm': '20201126183801-0232104783-01095062901.wav'
    }
    result = requests.post(host, json=payload)
    print(json.loads(result.text))
    ##
    host = 'http://{0}:{1}/trim_rec'.format(socket.gethostbyname(socket.gethostname()), 8786)
    payload = {
        'rec_st_dt': '2020-11-26',
        'brof_cd': '002311',
        'rec_file_nm': '20201126183801-0232104783-01095062901.wav'
    }
    result = requests.post(host, json=payload)
    print(json.loads(result.text))
