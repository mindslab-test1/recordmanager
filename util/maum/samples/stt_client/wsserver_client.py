#!/usr/bin/python
# -*- coding:utf-8 -*-


import websocket
import ssl

def on_open(ws):
    print('on_open')
    ws.send('hi')

websocket.enableTrace(True)
#ws = websocket.WebSocketApp("wss://sttdev.heungkuklife.co.kr:13254/callsocket")
ws = websocket.WebSocketApp("wss://10.18.131.39:13254/callsocket")
ws.on_open = on_open
#ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
ws.run_forever()