#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import json
import socket
import requests
import paramiko
import cipher

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    class AESConfig(object):
        pd = '/APP/maum/cfg/.aes'
        ps_path = '/APP/maum/cfg/.heungkuk'
    host = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0004'
    aes_cipher = cipher.AESCipher(AESConfig)
    rec_st_dt = '2021-03-11'
    brof_cd = '002311'
    rec_file_nm = '20210311104742-027799949-01056460578.wav'
    data = {
            "cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
            "data": [{'recdate': rec_st_dt, 'branchcd': brof_cd, 'recfilenm': rec_file_nm}]
    }
    print("\t  --> Execute Get Record information from CallTechSolution(통합녹취서버)")
    result = requests.post(host, json=data)
    json_result = json.loads(result.text)
    if len(json_result['data']) < 1:
        log.error("\t  --> Can't get Record information from CallTechSolution(통합녹취서버)")
    print("\t  --> Success, Get Record information from CallTechSolution(통합녹취서버)")
    print("\t  --> Response = {0}".format(json_result))
    item = json_result['data'][0]
    print('  1-2) SFTP CallTechSolution(통합녹취서버) download Record file')
    print("\t  --> Execute connecting CallTechSolution(통합녹취서버)")
    transport = paramiko.Transport(item['ftpipvoice'], item['ftppot'])
    print(aes_cipher.decrypt_hk(item['ftppwd']))
    transport.connect(username=item['ftpacc'], password=aes_cipher.decrypt_hk(item['ftppwd']))
    sftp = paramiko.SFTPClient.from_transport(transport)
    print("\t  --> Success, connect CallTechSolution(통합녹취서버)")
    source_file_path = os.path.join(item['recfilepath'].replace('\\', '/'), rec_file_nm)
    local_path = '{0}'.format(rec_file_nm)
    print("\t  --> Execute Download {0} --> {1}".format(source_file_path, local_path))
    sftp.get(source_file_path, local_path)
    print("\t  --> Success, Download Record file")
    sftp.close()
    transport.close()
