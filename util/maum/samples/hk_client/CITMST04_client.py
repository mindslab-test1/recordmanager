#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests
from datetime import datetime

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://10.60.101.32:20020/stt'
    data = {
        "header":{
            "HDR_PACKET_SIZE":"00001530",
            "HDR_TRX_DATE":"",
            "HDR_TRX_CODE":"",
            "HDR_RESULT_CODE":"",
            "HDR_CHANNEL_SEQ_ID":"",
            "HDR_REQ_SYS_ID":"HKSTT",
            "HDR_DEST_SYS_ID":"HKTM",
            "HDR_REQ_SYS_SEQ_ID":"",
            "HDR_REQ_SYS_SEND_DATE":datetime.strftime(datetime.now(), "%Y%m%d%H%M%s")[:14],
            "HDR_DEST_SYS_RECV_DATE":"",
            "HDR_DEST_PROG":"CITMST04",
            "HDR_TRA_FLAG":"1",
            "HDR_OPERATION_FLAG":"",
            "HDR_CHANNEL_ERR_CODE":"",
            "HDR_DATA_FLAG":"T",
            "HDR_USER_ID":"",
            "HDR_CLIENT_IP":"10.18.131.39",
            "HDR_VERSION":"",
            "HDR_REPLY_FLAG":"",
            "HDR_EXTERNAL_FLAG":"N",
            "HDR_FILLER":""
        },
        'body': [
            {
                'NSPL_NO': 'TEST20210120',
                'DTCT_RST_CNT': '11',
                'TOTAL_DTCT_CNT': '25',
                'VOIC_SP': '0.0',
                'C_DU_TM': '00:00:00',
                'FD_RST_CNT': '3',
                'QA_RST_INFO': [
                    {'SCRP_SCTG_CD': 'S0208', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0221', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0217', 'ANS_RST': 'N', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': 'S0218', 'ANS_RST': '', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0101', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': 'S0219', 'ANS_RST': '', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0230', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0205', 'ANS_RST': '', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0207', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0236', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0202', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0239', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0214', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0122', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0210', 'ANS_RST': '', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0211', 'ANS_RST': 'N', 'JUG_RST': '02'},
                    {'SCRP_SCTG_CD': 'S0220', 'ANS_RST': 'N', 'JUG_RST': '02'},
                    {'SCRP_SCTG_CD': 'S0213', 'ANS_RST': '', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0237', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0225', 'ANS_RST': '', 'JUG_RST': '02'},
                    {'SCRP_SCTG_CD': 'S0203', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0238', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0228', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': 'S0229', 'ANS_RST': 'N', 'JUG_RST': '01'},
                    {'SCRP_SCTG_CD': 'S0212', 'ANS_RST': '', 'JUG_RST': '03'},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''},
                    {'SCRP_SCTG_CD': '', 'ANS_RST': '', 'JUG_RST': ''}
                ],
            }
        ]
    }
    result = requests.post(host, json=data)
    print(json.loads(result.text))
