#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'https://rec.heungkuklife.co.kr/unitrec/interface/IF_S0003_TEST'
    data = {"cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
            "data": [{'recdate': "2021-03-25", "callid": "592335017233202142255@192.168.30.168"}]
            }
    result = requests.post(host, json=data)
    print(json.loads(result.text))
