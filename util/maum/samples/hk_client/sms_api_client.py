#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests
from datetime import datetime

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://eaidev.heungkuklife.co.kr:8800/weom/servlet/api.HungkukUMSDispatcher'
    dt = datetime.now().strftime("%Y%m%d%H%M%S%f")
    header_boundary = '{0}{1}'.format('-' * 17, dt) # 37
    body_start_boundary = '{0}{1}'.format('-' * 19, dt) # 39
    body_end_boundary = '{0}{1}{2}'.format('-' * 19, dt, '-' * 2) # 41
    headers = {
        'Accept-Language': 'ko',
        'ContentType': 'multipart/form-data',
        'Content-Type': 'multipart/form-data;boundary={0}'.format(header_boundary)
    }
    msg = '0000040920201215SMS                  HKSTT   HKUMS   000000000020201215133317                SMS     1T    T0009102121   255.255.255.1  V011N         135694A9-C357-A15E-B50T-1TAFS2F45F5A2BC00091021212011160335751999999999      20201215133317SC019070555             20201215STUC8    흥국생명15882286       6    이정일13   111111111111111   0108701423354   가/SC/압구정동/조영근/02-542-4741/변액/월/10/999/100천'
    msg = msg.encode('euc-kr')
    body_list = list()
    body_list.append(body_start_boundary)
    body_list.append('Content-Disposition: form-data; name="data"')
    body_list.append('Content-Type: text/plain; charset=EUC-KR')
    body_list.append('Content-Transfer-Encoding: 8bit\n')
    body_list.append(msg)
    body_list.append(body_end_boundary)
    body = '\n'.join(body_list)
    result = requests.post(host, data=body, headers=headers)
    print(result)
    print(result.text)
