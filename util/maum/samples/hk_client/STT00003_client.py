#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://{0}:{1}/col_qa'.format(socket.gethostbyname(socket.gethostname()), 20020)
    print("\n*  Check API [{0}]  *".format(host))
    payload = {
		u'body':{
			u'NSPL_NO':u'8360436200002',
			u'TMS_INFO': u'1',
			u'PROD_CD': u'20263',
			u'PROD_NM': u'(\ubb34)\ud765\uad6d\uc0dd\uba85\ud2b9\uc815\uac10\uc5fc\ubcd1\uc815\uae30\ubcf4\ud5d8(\uac31\uc2e0\ud615,\ucd5c\ucd08\uacc4\uc57d)',
			u'IP_CSMR_NO': u'836043620',
			u'TL_CSMR_NO': u'12008702',
			u'CSMR_NM': u'/fv4mwWgVkSoQbGt3jHhwgAA==AAkA',
			u'PNM': u'\ub9f9\uc131\uc900',
			u'BROF_CD': u'00290',
			u'SAES_EMNM':u'\uae40\ubcf4\ub78c',
			u'SAES_ENMO':u'0026100942',
			u'CONTRACT_DT': u'20210126',
			u'REC_INFO':[
				{u'REC_BIZ_CD':u'C',u'REC_FILE_NM':u'20210126144551-027799948-01040227787.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '11'},
				{u'REC_BIZ_CD':u'C',u'REC_FILE_NM':u'20210126145048-027799948-01040227787.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '11'},
				{u'REC_BIZ_CD':u'C',u'REC_FILE_NM':u'20210125110332-027799948-01020233911.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '11'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210126145549-027799948-01040227787.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '12'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210126134029-027799948-01020233911.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '12'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210126104639-027799948-01020233911.wav', 'REC_ST_DT': '20210126', 'CSMR_TLNO': '', 'TL_CD': '13'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210125110120-027799948-01020233911.wav', 'REC_ST_DT': '20210125', 'CSMR_TLNO': '', 'TL_CD': '12'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210125095121-027799948-01020233911.wav', 'REC_ST_DT': '20210125', 'CSMR_TLNO': '', 'TL_CD': '13'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210125164035-027799948-01040227787.wav', 'REC_ST_DT': '20210125', 'CSMR_TLNO': '', 'TL_CD': '12'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210125165626-027799948-01040227787.wav', 'REC_ST_DT': '20210125', 'CSMR_TLNO': '', 'TL_CD': '13'},
				{u'REC_BIZ_CD':u'S',u'REC_FILE_NM':u'20210125160815-027799948-01040227787.wav', 'REC_ST_DT': '20210125', 'CSMR_TLNO': '', 'TL_CD': '12'},
				{u'REC_BIZ_CD':u'',u'REC_FILE_NM':u'', 'REC_ST_DT': '', 'CSMR_TLNO': '', 'TL_CD': ''}
			],
			u'CODE':u'',
			u'MSG':u'',
		},
		u'header':{
			u'HDR_EXTERNAL_FLAG':u'',
			u'HDR_REQ_SYS_SEQ_ID':u'0000000000',
			u'HDR_REPLY_FLAG':u'1',
			u'HDR_USER_ID':u'0026100942',
			u'HDR_TRX_DATE':u'20210126',
			u'HDR_DEST_PROG':u'STT00003',
			u'HDR_DEST_SYS_RECV_DATE':u'',
			u'HDR_VERSION':u'V01',
			u'HDR_REQ_SYS_SEND_DATE':u'202101261414953',
			u'HDR_DATA_FLAG':u'R',
			u'HDR_CHANNEL_SEQ_ID':u'2615420824',
			u'HDR_DEST_SYS_ID':u'HKSTT',
			u'HDR_PACKET_SIZE':u'00037676',
			u'HDR_RESULT_CODE':u'',
			u'HDR_CLIENT_IP':u'10.70.11.27',
			u'HDR_TRA_FLAG':u'1',
			u'HDR_FILLER':u'',
			u'HDR_CHANNEL_ERR_CODE':u'',
			u'HDR_OPERATION_FLAG':u'',
			u'HDR_REQ_SYS_ID':u'HKTM',
			u'HDR_TRX_CODE':u'0000000000'
		}
	}
    result = requests.post(host, json=payload)
    if result:
        result_dict = json.loads(result.text)
        for key, value in result_dict.items():
            print("{0}: {1}".format(key, value))
    else:
        print('Error', result)
