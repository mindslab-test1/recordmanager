#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    host = 'http://{0}:{1}/col_qa'.format(socket.gethostbyname(socket.gethostname()), 20020)
    print("\n*  Check API [{0}]  *".format(host))
    payload = {
		u'body': {
			u'TL_CSMR_NO': u'12008702',
			u'BROF_CD': u'002903',
			u'INLN_TLNO': u'11319 ',
			u'HOME_TLNO': u'01040227787 ',
			u'CSMR_NM': u'\ub9f9\uc131\uc900',
			u'SAES_ENMO': u'1006233204',
			u'SAES_EMNM': u'\ubc15\ud601',
			u'CAL_STATUS': u'01',
  		    u'TL_CD': u'11',
			u'CODE': u'',
			u'MSG': u''
		},
		u'header': {
			u'HDR_EXTERNAL_FLAG': u' ',
			u'HDR_REQ_SYS_SEQ_ID': u'0000000000',
			u'HDR_REPLY_FLAG': u'1',
			u'HDR_USER_ID': u'1006233204 ',
			u'HDR_TRX_DATE': u'20210125',
			u'HDR_DEST_PROG': u'STT00001',
			u'HDR_DEST_SYS_RECV_DATE': u' ',
			u'HDR_VERSION': u'V01',
			u'HDR_REQ_SYS_SEND_DATE': u'202101251311852',
			u'HDR_DATA_FLAG': u'R',
			u'HDR_CHANNEL_SEQ_ID': u'2516570864',
			u'HDR_DEST_SYS_ID': u'HKSTT',
			u'HDR_PACKET_SIZE': u'00005227',
			u'HDR_RESULT_CODE': u' ',
			u'HDR_CLIENT_IP': u'10.70.11.27',
			u'HDR_TRA_FLAG': u'1',
			u'HDR_FILLER': u' ',
			u'HDR_CHANNEL_ERR_CODE': u'',
			u'HDR_OPERATION_FLAG': u' ',
			u'HDR_REQ_SYS_ID': u'HKTM',
			u'HDR_TRX_CODE': u'0000000000'
		}
	}
    result = requests.post(host, json=payload)
    if result:
        result_dict = json.loads(result.text)
        for key, value in result_dict.items():
            print("{0}: {1}".format(key, value))
    else:
        print('Error', result)
