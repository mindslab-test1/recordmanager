#!/usr/bin/python3
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests
import traceback
import collections
from datetime import datetime
sys.path.append('/APP/maum')
from cfg import config
from lib import util, py3_db_connection

#############
# constants #
#############
DB = py3_db_connection.Oracle(config.OracleConfig, failover=True, service_name=True)
HOST_NM = socket.gethostname()


#######
# def #
#######
def petra_dec(target_sent):
    try:
        req_host = "http://{0}:{1}/decrypt_string".format(socket.gethostbyname(HOST_NM), 8788)
        payload = {'string': target_sent}
        req_result = requests.post(req_host, data=payload)
        response_out = json.loads(req_result.text)
        if response_out['code'] == 0:
            return response_out['rst']
        else:
            return False
    except Exception:
        print(traceback.format_exc())
        return False


def sending_sms(sorted_dtct_kwd_list, extra):
    print('\t\t   -- Sending SMS')
    host = config.SMSConfig.host
    dt = datetime.now().strftime("%Y%m%d%H%M%S%f")
    header_boundary = '{0:->37}'.format(dt)
    body_start_boundary = '{0:->39}'.format(dt)
    body_end_boundary = '{0:-<41}'.format(body_start_boundary)
    headers = {
        'Accept-Language': 'ko',
        'ContentType': 'multipart/form-data',
        'Content-Type': 'multipart/form-data;boundary={0}'.format(header_boundary)
    }
    adrs_nm = u'흥국생명'
    # EMP_NO로 조회
    results = util.select_sms_info(DB, extra['body']['SAES_ENMO'].strip())
    if not results:
        return 'Not exist data'
    for result_dict in results:
        recr_nm = '홍길동'
        recr_brnu = ''
        dec_flag = petra_dec(result_dict['ADMIN_TLNO'])
        dec_admin_tlno = dec_flag if dec_flag else result_dict['ADMIN_TLNO']
        recr_tlno = dec_admin_tlno if dec_admin_tlno else str()
        recr_tlno = '01092444745'
        msg = str()
        msg += '[STT 안내]\n'
        msg += '사원: {0}\n'.format(result_dict['USER_NM'].strip())
        msg += '고객: {0}\n'.format(extra['body']['CSMR_NM'].strip())
        msg += '금지어: {0} 외 {1}건'.format(sorted_dtct_kwd_list[0][0], len(sorted_dtct_kwd_list))
        msg = msg.replace("!@#$", ", ")
        format_setting = collections.OrderedDict({
            # header (채널공통헤더)
            'MSGLENGTH': {'length': 8, 'value': 0},
            'DATE': {'length': 8, 'value': datetime.now().strftime("%Y%m%d")},
            'BOCID': {'length': 10, 'value': str()},
            'RESCODE': {'length': 1, 'value': str()},
            'CSMSGSEQ': {'length': 10, 'value': str()},
            'SOURCE': {'length': 8, 'value': 'HKSTT'},
            'TARGET': {'length': 8, 'value': 'HKUMS'},
            'SOURCEMSGSEQ': {'length': 10, 'value': str()},
            'SOURCEREQTIME': {'length': 15, 'value': datetime.now().strftime("%Y%m%d%H%M%S")},
            'TARGETRESTIME': {'length': 15, 'value': datetime.now().strftime("%Y%m%d%H%M%S")},
            'TARGETPGM': {'length': 8, 'value': 'HKUMS'},
            'COMMITFLAG': {'length': 1, 'value': '1'},
            'TRANFLAG': {'length': 1, 'value': 'T'},
            'CSERRORCODE': {'length': 4, 'value': str()},
            'TESTORREAL': {'length': 1, 'value': 'T'},
            'USERID': {'length': 13, 'value': 'HKSTT'},
            'USERIP': {'length': 15, 'value': socket.gethostbyname(socket.gethostname())},
            'VERSION': {'length': 3, 'value': str()},
            'NOTIFY': {'length': 1, 'value': '1'},
            'EXTERNAL': {'length': 1, 'value': 'N'},
            'FILLER': {'length': 9, 'value': str()},
            # body
            'Authkey': {'length': 36, 'value': '135694A9-C357-A15E-B50T-1TAFS2F45F5A'},
            'UMS_GB': {'length': 1, 'value': '2'},
            'CHANNEL_GB': {'length': 2, 'value': 'TM'},
            'CHANNEL_KEY_NO': {'length': 20, 'value': str()},
            'UMS_INFO_CODE': {'length': 3, 'value': '310'},
            'EMPN': {'length': 15, 'value': str()},
            'UMS_GUID_ID': {'length': 14, 'value': str()},
            'UMS_GUID_ID_GB': {'length': 2, 'value': str()},
            'UMS_REF_NO_GB': {'length': 2, 'value': str()},
            'UMS_REF_NO': {'length': 20, 'value': str()},
            'UMS_ISUE_YMD': {'length': 8, 'value': datetime.now().strftime("%Y%m%d")},
            'UMS_GBK_KND': {'length': 4, 'value': '1310'},
            'ADRS_NM_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(adrs_nm.encode('euc-kr')))},
            'ADRM_NM': {'length': len(adrs_nm), 'value': adrs_nm},
            'ADRS_TLNO': {'length': 15, 'value': '15882288'},
            'RECR_NM_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_nm.encode('euc-kr')))},
            'RECR_NM': {'length': len(recr_nm), 'value': recr_nm},
            'RECR_BRNU_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_brnu))},
            'RECR_BRNU': {'length': len(recr_brnu), 'value': recr_brnu},
            'RECR_TLNO_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(recr_tlno))},
            'RECR_TLNO': {'length': len(recr_tlno), 'value': recr_tlno},
            # 'RECR_TLNO': {'length': 11, 'value': '01092444745'},
            'MSG_LEN': {'length': 5, 'value': '{0:0>5}'.format(len(msg.encode('euc-kr')))},
            'MSG': {'length': len(msg.encode('euc-kr')), 'value': msg},
        })
        total_msg = str()
        for key, value_dict in format_setting.items():
            print('{0}: {1}'.format(key, value_dict))
            if key == 'MSG':
                total_msg += value_dict['value']
                continue
            total_msg = '{0}{1:{2}<{3}}'.format(total_msg, value_dict['value'], ' ', value_dict['length'])
        format_setting['MSGLENGTH']['value'] = '{0:0>{1}}'.format(len(total_msg), format_setting['MSGLENGTH']['length'])
        total_msg = str()
        for key, value_dict in format_setting.items():
            if key == 'MSG':
                total_msg += value_dict['value']
                continue
            total_msg = '{0}{1:{2}<{3}}'.format(total_msg, value_dict['value'], ' ', value_dict['length'])
        # total_msg = total_msg.encode('euc-kr')
        body_list = list()
        body_list.append(body_start_boundary)
        body_list.append('Content-Disposition: form-data; name="data"')
        body_list.append('Content-Type: text/plain; charset=EUC-KR')
        body_list.append('Content-Transfer-Encoding: 8bit\n')
        body_list.append(total_msg)
        body_list.append(body_end_boundary)
        body = '\n'.join(body_list)
        body = body.encode('euc-kr')
        result = requests.post(host, data=body, headers=headers)
        print(body)
        return result


if __name__ == '__main__':
    ##
    
    kwd_list = [('금칙어', 3), ('금지 문장', 1)]
    extra = {
        'body': {
            'SAES_ENMO': '1006101233',
            'CSMR_NM': '홍수연',
        }
    }
    result = sending_sms(kwd_list, extra)
    print(result)
    print(result.text)
