#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests
from datetime import datetime

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://10.60.101.32:20020/stt'
    data = {
        "header":{
            "HDR_PACKET_SIZE":"00000309",
            "HDR_TRX_DATE":"",
            "HDR_TRX_CODE":"",
            "HDR_RESULT_CODE":"",
            "HDR_CHANNEL_SEQ_ID":"",
            "HDR_REQ_SYS_ID":"HKSTT",
            "HDR_DEST_SYS_ID":"HKTM",
            "HDR_REQ_SYS_SEQ_ID":"",
            "HDR_REQ_SYS_SEND_DATE": datetime.strftime(datetime.now(), "%Y%m%d%H%M%s")[:14],
            "HDR_DEST_SYS_RECV_DATE":"",
            "HDR_DEST_PROG":"CITMST05",
            "HDR_TRA_FLAG":"1",
            "HDR_OPERATION_FLAG":"",
            "HDR_CHANNEL_ERR_CODE":"",
            "HDR_DATA_FLAG":"T",
            "HDR_USER_ID":"",
            "HDR_CLIENT_IP":"10.18.131.39",
            "HDR_VERSION":"",
            "HDR_REPLY_FLAG":"",
            "HDR_EXTERNAL_FLAG":"N",
            "HDR_FILLER":""
        },
        "body":[
            {
                'SAVE_DTTM': '20210125',
                'TL_CSMR_NO': 'TEST123456789',
                'BROF_CD': '002305',
                'ANS_RST': 'N',
                'JUG_RST': '02',
                'SAVE_TIME': '123456',
                'SAES_ENMO': '1006089173'
            }
        ]
    }
    result = requests.post(host, json=data)
    print(json.loads(result.text))
