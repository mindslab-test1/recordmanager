#!/usr/bin/python
# -*- coding:utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-08, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
import socket
import requests

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

if __name__ == '__main__':
    ##
    host = 'http://{0}:{1}/col_qa'.format(socket.gethostbyname(socket.gethostname()), 20020)
    print("\n*  Check API [{0}]  *".format(host))
    payload = {
		u'body':{
			u'SAES_ENMO':u'1006202788',
			u'SRCH_ST_DT': u'20210101',
			u'SRCH_ED_DT': u'20210223',
			u'REC_CNT': '',
			u'REC_DU_TM': '',
			u'FD_RST_CNT': '',
			u'FD_RST_INFO':[
				{
					'SCRP': '', 'DTCT_DTC_CNT': ''
				}

			]
		},
		u'header':{
			u'HDR_EXTERNAL_FLAG':u'',
			u'HDR_REQ_SYS_SEQ_ID':u'0000000000',
			u'HDR_REPLY_FLAG':u'1',
			u'HDR_USER_ID':u'0026100942',
			u'HDR_TRX_DATE':u'20210126',
			u'HDR_DEST_PROG':u'STT00006',
			u'HDR_DEST_SYS_RECV_DATE':u'',
			u'HDR_VERSION':u'V01',
			u'HDR_REQ_SYS_SEND_DATE':u'202101261414953',
			u'HDR_DATA_FLAG':u'R',
			u'HDR_CHANNEL_SEQ_ID':u'2615420824',
			u'HDR_DEST_SYS_ID':u'HKSTT',
			u'HDR_PACKET_SIZE':u'00037676',
			u'HDR_RESULT_CODE':u'',
			u'HDR_CLIENT_IP':u'10.70.11.27',
			u'HDR_TRA_FLAG':u'1',
			u'HDR_FILLER':u'',
			u'HDR_CHANNEL_ERR_CODE':u'',
			u'HDR_OPERATION_FLAG':u'',
			u'HDR_REQ_SYS_ID':u'HKTM',
			u'HDR_TRX_CODE':u'0000000000'
		}
	}
    result = requests.post(host, json=payload)
    if result:
        result_dict = json.loads(result.text)
        for key, value in result_dict.items():
            print("{0}: {1}".format(key, value))
    else:
        print('Error', result)
