#!/usr/bin/python
# -*- coding:utf-8 -*-

import libpcpython

# initialize config
rtn = libpcpython.PcAPI_initialize('petra_cipher_api.conf', '')
print("initialize return is [{0}]".format(rtn))
# get session id
sid = libpcpython.PcAPI_getSession('')
print("getSession sid is [{0}]".format(sid))
# plain text define
plainText = '테스트 문장입니다. Sinsiway Petra Cipher'
plainTextLength = len(plainText)
print("plain text [{0}] length [{1}]".format(plainText, plainTextLength))
# encrypt data
# 양방향 :  1641600   ,  단방향 : 1641700
encryptString = libpcpython.PcAPI_enc_id(sid, 1641600, plainText, plainTextLength)
encryptStringLength = len(encryptString)
print("use enc_id => encrypt string [{1}] length [{0}]".format(encryptString, encryptStringLength))
# decrypt data
decryptString = libpcpython.PcAPI_dec_id(sid, 1641600, encryptString, encryptStringLength)
decryptStringLength = len(decryptString)
print("use enc_id => decrypt string [{0}] length [{1}]".format(decryptString, decryptStringLength))
