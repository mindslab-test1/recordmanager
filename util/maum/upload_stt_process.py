#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-03-22, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import json
import signal
import socket
import random
import requests
import traceback
import subprocess
from operator import itemgetter
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection, cnn, masking, cipher, flashtext, change_arabic, libpcpython

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
PETRA_SID = ''
PROG_STAT_CD = '43'  # '43': STT변환완료
PROG_STAT_DTL_CD = None
DELETE_FILE_LIST = list()
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def sub_process(cmd):
    """
    Execute subprocess
    @param      cmd:        Command
    """
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    return response_out, response_err


def del_file(log):
    """
    Delete file
    @param  log:    Logger object
    """
    for file_path in DELETE_FILE_LIST:
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't delete {0}".format(file_path))
            continue


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def extract_speech_time(seconds):
    """
    Extract speech time
    :param      seconds:    Seconds(/s)
    :return:                HH:MM:SS.FF
    """
    time_list = str(timedelta(seconds=float(seconds))).split(':')
    hh = ('0' + time_list[0])[-2:]
    mm = time_list[1]
    ss = '%.2f' % float(time_list[2])
    ss = ss.split('.')[0]
    ss = ss if len(ss) == 2 else '0' + ss
    return ':'.join([hh, mm, ss])


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:            Logger Object
    """
    global PETRA_SID
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    PETRA_SID = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))


def update_stt_status_and_info(log, db, job, max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_info):
    """
    Update STT status and information to QA_CAL_INFO_TB(콜정보테이블)
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        EMP_NO            사원번호
                                        ALL_SEQ           전체순번
                                        REC_FILE_NM       녹취파일명
                                        REC_LOC_PATH      녹취파일위치
                                        REQ_TM            요청시간
                                        REC_DU_TM         녹취통화시간
                                        CHN_CLA_CD        채널구분코드
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                    }
    @param      max_silence_time:   Max Silence time
    @param      spch_sped_rx:       Speech speed rx
    @param      spch_sped_tx:       Speech speed tx
    @param      stt_result_info:    STT result information
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        log.info('  6-1) Update STT status and information')
        util.update_upload_stt_status_and_info(
            db=db,
            prog_stat_cd='43',
            chn_cla_cd=job['CHN_CLA_CD'],
            record_duration=job['REC_DU_TM'],
            stt_proc_sp=stt_result_info['stt_proc_sp'],
            stt_st_tm=stt_result_info['stt_st_tm'],
            stt_com_tm=stt_result_info['stt_com_tm'],
            c_voic_sp=spch_sped_rx,
            a_voic_sp=spch_sped_tx,
            max_silence=max_silence_time,
            updator_id=socket.gethostname(),
            updated_tm=datetime.fromtimestamp(time.time()),
            emp_no=job['EMP_NO'],
            all_seq=job['ALL_SEQ'],
            rec_file_nm=job['REC_FILE_NM']
        )
    except Exception:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR06'
        raise Exception(traceback.format_exc())
    log.info('\t  --> Done updated STT status and information')


def insert_stt_result(log, db, job, stt_result_info):
    """
    Insert STT result
    @param      log:                    Logger object
    @param      db:                     DB object
    @param      job:                    job{
                                            EMP_NO            사원번호
                                            ALL_SEQ           전체순번
                                            REC_FILE_NM       녹취파일명
                                            REC_LOC_PATH      녹취파일위치
                                            REQ_TM            요청시간
                                            REC_DU_TM         녹취통화시간
                                            CHN_CLA_CD        채널구분코드
                                            REC_ENC_TP        녹취파일인코딩
                                            CONV_LOC_PATH     녹취변완위치경로
                                            CONV_FILE_NM      녹취변환파일명
                                        }
    @param      stt_result_info:        STT result information
    @return:                            Max Silence Time, Speech speed, STT result
    """
    """
    stt_result_info = {
        'stt_st_dtm': stt_st,
        'stt_ed_dtm': stt_ed,
        'stt_pcs_spd': analyzing_speed,
        'stt_result': stt_result_list #[(speaker, seg.start, seg.end, spch_sped, masked_str), ...]
    }
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        line_num = 1
        bind_list = list()
        max_silence_time = 0.0
        stt_result_list = list()
        temp_end_seconds = 0.0
        speech_speed_info = dict()
        emp_no = job['EMP_NO']
        all_seq = job['ALL_SEQ']
        rec_file_nm = job['REC_FILE_NM']
        log.info('  5-1) Make upload data')
        for item in stt_result_info['stt_result']:
            speaker, start, end, spch_sped, sent = item
            silence_time = (start / 8000.0) - temp_end_seconds
            silence_time = round(silence_time, 2)
            if silence_time < 0:
                silence_time = 0.0
            if max_silence_time < silence_time:
                max_silence_time = silence_time
            sntc_sttm = extract_speech_time(start / 8000.0)
            sntc_endtm = extract_speech_time(end / 8000.0)
            if speaker in speech_speed_info:
                speech_speed_info[speaker][0] += 1
                speech_speed_info[speaker][1] += spch_sped
            else:
                speech_speed_info[speaker] = [1, spch_sped]
            enc_sent = libpcpython.PcAPI_enc_id(PETRA_SID, config.PetraConfig.enc_cold_id, sent, len(sent))
            bind = (
                emp_no,
                all_seq,
                rec_file_nm,
                str(line_num),
                speaker,
                sntc_sttm,
                sntc_endtm,
                enc_sent,
                silence_time,
                socket.gethostname(),
                datetime.fromtimestamp(time.time()),
                socket.gethostname(),
                datetime.fromtimestamp(time.time())
            )
            bind_list.append(bind)
            stt_item = {
                'EMP_NO': emp_no,
                'ALL_SEQ' : all_seq,
                'REC_FILE_NM': rec_file_nm,
                'STMT_NO': str(line_num),
                'SPK_DIV_CD': speaker,
                'STMT_ST_TM': sntc_sttm,
                'STMT_ED_TM': sntc_endtm,
                'STMT': sent,
                'SILENCE': silence_time
            }
            stt_result_list.append(stt_item)
            line_num += 1
            temp_end_seconds = end / 8000.0
        log.info('\t  --> Check STT results')
        del_flag = util.select_upload_stt_results_existed(db, emp_no, all_seq, rec_file_nm)
        if del_flag:
            log.info('\t  --> Already existed. Delete STT results')
            util.delete_upload_stt_results(db, emp_no, all_seq, rec_file_nm)
            log.info('\t  --> Done delete STT results')
        log.info('\t  --> Insert STT results')
        util.insert_upload_stt_result(db, bind_list)
        log.info('\t  --> Done insert STT results')
        spch_sped_rx = 0
        spch_sped_tx = 0
        for speaker, value in speech_speed_info.items():
            if speaker == 'A':
                spch_sped_tx = round(value[1] / value[0], 2)
            elif speaker == 'C':
                spch_sped_rx = round(value[1] / value[0], 2)
            else:
                spch_sped_tx = round(value[1] / value[0], 2)
                spch_sped_rx = round(value[1] / value[0], 2)
        return max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_list
    except Exception:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR07'
        raise Exception(traceback.format_exc())


def load_replace_keyword(db):
    """
    Load replace keyword
    @param:         db:       DB Object
    @return:                  Replace keyword trie dictionary
    """
    keyword_processor = flashtext.KeywordProcessor(case_sensitive=True)
    target_list = util.select_subs_kwd(db)
    for target_dict in target_list:
        keyword_processor.add_keyword(
            target_dict['TAR_WD'].decode('utf-8'), target_dict['CHA_WD'].decode('utf-8'))
    return keyword_processor


def extract_speech_speed(start_time, end_time, sent):
    """
    Extract speech speed
    @param      start_time:     Start time(/ms)
    @param      end_time:       End time(/ms)
    @param      sent:           Sentence (encoding='utf-8')
    @return:                    Speech speed
    """
    sntc_len = len(sent.replace(' ', ''))
    during_time = (end_time / 8000.0) - (start_time / 8000.0)
    if during_time > 0:
        speech_speed = round(float(sntc_len) / during_time, 2)
    else:
        speech_speed = 0
    if speech_speed > 999:
        speech_speed = 0.0
    return speech_speed


def postprocessing_stt_result(log, db, stt_result_dict):
    """
    Postprocessing STT result(sorted, masking)
    @param          log:                    Logger object
    @param          db:                     DB object
    @param          stt_result_dict:        STT result dictionary
    @return:                                Modified STT result dictionary
    """
    stt_result_list = list()
    aes_cipher = cipher.AESCipher(config.AESConfig)
    sorted_stt_result_list = sorted(stt_result_dict.iteritems(), key=itemgetter(0), reverse=False)
    log.info('\t  --> Sorted ...')
    line_list = list()
    for key, item in sorted_stt_result_list:
        speaker, seg = item
        line_list.append(seg.txt.encode('utf-8').strip())
    masking_result = list()
    if config.UploadSTTConfig.masking:
        log.info('\t  --> Masking ...')
        masking_result, index_output_dict = masking.masking(line_list, 'utf-8')
    if config.UploadSTTConfig.encrypt:
        log.info('\t  --> Encrypt ...')
    log.info('\t  --> Extract speech speed')
    line_num = 0
    change = False
    keyword_processor = False
    if config.UploadSTTConfig.replace:
        log.info('\t  --> Replace ...')
        keyword_processor = load_replace_keyword(db)
    if config.UploadSTTConfig.arabic:
        log.info('\t  --> Arabic ...')
        change = change_arabic.ChangeArabic()
    for key, item in sorted_stt_result_list:
        line_num += 1
        speaker, seg = item
        stt_txt = seg.txt.encode('utf-8').strip()
        spch_sped = extract_speech_speed(seg.start, seg.end, stt_txt)
        if config.UploadSTTConfig.replace and keyword_processor:
            stt_txt = keyword_processor.replace_keywords(stt_txt.decode('utf-8')).encode('utf-8')
            stt_txt = keyword_processor.replace_keywords(stt_txt.decode('utf-8')).encode('utf-8')
        masked_str = masking_result[line_num - 1] if line_num - 1 in masking_result else stt_txt
        if config.UploadSTTConfig.arabic and change:
            masked_str = change.change_arabic_text(masked_str)
        if config.UploadSTTConfig.encrypt:
            output_str = aes_cipher.encrypt(masked_str)
        else:
            output_str = masked_str
        if config.UploadSTTConfig.masking:
            stt_result_list.append((speaker, seg.start, seg.end, spch_sped, masked_str.encode('euc-kr').strip()))
        else:
            stt_result_list.append((speaker, seg.start, seg.end, spch_sped, output_str))
    return stt_result_list


def make_stt_result_dict(speak, results, stt_result_dict):
    """
    Make STT result dictionary
    @param      speak:                  Speaker('M', 'A', 'C')
    @param      results:                STT result
    @param      stt_result_dict:        STT result dictionary
    @return:                            STT result dictionary
    """
    for seg in results:
        flag = True
        key = seg.start
        while flag:
            if key in stt_result_dict:
                key += 0.1
            else:
                stt_result_dict[key] = (speak, seg)
                flag = False
    return stt_result_dict


def stt_recognize(log, record_duration, stt_client_1, stt_client_2, pcm_file_path):
    """
    STT recognize
    @param      log:                Logger object
    @param      record_duration:    Record duration
    @param      stt_client_1:       GPU0 STT client
    @param      stt_client_2:       GPU1 STT client
    @param      pcm_file_path:      PCM file path
    @return:                        STT results
    """
    random_cnt = random.randrange(1, 3)
    results = list()
    for cnt in range(1, 4):
        if random_cnt == 1:
            results = stt_client_1.stream_recognize(pcm_file_path)
        else:
            results = stt_client_2.stream_recognize(pcm_file_path)
        if not results:
            if record_duration <= 60:
                break
            log.info("\t  --> {0} STT results is empty. {1}st Retry ...".format(os.path.basename(pcm_file_path), cnt))
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    if not results:
        log.info("\t  --> {0} STT results is empty. Please, check wav file".format(os.path.basename(pcm_file_path)))
        return list()
    return results


def execute_cnn_stt(log, db, job, pcm_info_dict):
    """
    Execute CNN STT
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        EMP_NO            사원번호
                                        ALL_SEQ           전체순번
                                        REC_FILE_NM       녹취파일명
                                        REC_LOC_PATH      녹취파일위치
                                        REQ_TM            요청시간
                                        REC_DU_TM         녹취통화시간
                                        CHN_CLA_CD        채널구분코드
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                    }
    @param      pcm_info_dict:      PCM file information
    @return:                        STT results information
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    stt_comp_flag = False
    stt_result_dict = dict()
    stt_st = datetime.fromtimestamp(time.time())
    stt_ed = ''
    analyzing_speed = ''
    for _ in range(0, 3):
        try:
            log.info('  4-1) Load CNN STT client')
            stt_client_1 = cnn.W2lClient(remote=config.UploadSTTConfig.cnn_client1_remote)
            stt_client_2 = cnn.W2lClient(remote=config.UploadSTTConfig.cnn_client2_remote)
            total_required_time = float()
            log.info('  4-2) Execute CNN STT analyzing ...')
            if job['CHN_CLA_CD'] == 'M':
                pcm_file_path = pcm_info_dict['wav']
                results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, pcm_file_path)
                stt_result_dict = make_stt_result_dict('M', results, stt_result_dict)
            else:
                rx_pcm_file_path = pcm_info_dict['rx']
                tx_pcm_file_path = pcm_info_dict['tx']
                rx_results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, rx_pcm_file_path)
                tx_results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, tx_pcm_file_path)
                stt_result_dict = make_stt_result_dict('C', rx_results, stt_result_dict)
                stt_result_dict = make_stt_result_dict('A', tx_results, stt_result_dict)
            stt_ed = datetime.fromtimestamp(time.time())
            required_time = elapsed_time(stt_st)
            total_required_time += required_time.total_seconds()
            log.info('\t  --> Done STT, The time required = {0}'.format(required_time))
            analyzing_speed = round(float(job['REC_DU_TM']) / total_required_time, 1)
            log.info('\t  --> STT analyzing speed is {0} [Duration= {1}(/s)]'.format(
                analyzing_speed, job['REC_DU_TM']))
            stt_comp_flag = True
            break
        except Exception:
            log.error(traceback.format_exc())
            log.info('  4-0) Retry CNN STT analyzing ...')
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    if not stt_comp_flag:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR05'
        raise Exception("Can't STT recognize {0}".format(job['REC_FILE_NM']))
    try:
        log.info('  4-3) Postprocessing STT result(sorted, masking)')
        stt_result_list = postprocessing_stt_result(log, db, stt_result_dict)
        stt_result_info = {
            'stt_st_tm': stt_st,
            'stt_com_tm': stt_ed,
            'stt_proc_sp': analyzing_speed,
            'stt_result': stt_result_list
        }
        return stt_result_info
    except Exception:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR06'
        log.error(traceback.format_exc())
        raise Exception("STT postprocessing error {0}".format(job['REC_FILE_NM']))


def separation_wav_file(log, target_file_path):
    """
    Separation wav file
    @param      log:                    Logger object
    @param      target_file_path:       Target file path
    @return:                            rx, tx file path
    """
    log.info('\t  --> Separation wav file')
    wav_dir_path = os.path.dirname(target_file_path)
    wav_file_name = os.path.basename(target_file_path)
    rx_file_name = wav_file_name[:-4] + '_rx.wav'
    tx_file_name = wav_file_name[:-4] + '_tx.wav'
    rx_file_path = os.path.join(wav_dir_path, rx_file_name)
    tx_file_path = os.path.join(wav_dir_path, tx_file_name)
    # IF rx.wav or tx.wav file is already existed remove file.
    try:
        if os.path.exists(rx_file_path):
            os.remove(rx_file_path)
        if os.path.exists(tx_file_path):
            os.remove(tx_file_path)
    except Exception:
        log.error("\t  --> Fail delete file -> {0}".format(traceback.format_exc()))
    cmd = 'ffmpeg -i {0} -filter_complex "[0:0]pan=1c|c0=c0[left];[0:0]pan=1c|c0=c1[right]" '.format(target_file_path)
    #cmd += '-map "[left]" {0} -map "[right]" {1}'.format(rx_file_path, tx_file_path)
    # 흥국생명 실시간STT에서 MP3 파일의 left, right 를 반대로 생성
    cmd += '-map "[left]" {0} -map "[right]" {1}'.format(tx_file_path, rx_file_path)
    std_out, std_err = sub_process(cmd)
    if len(std_err) > 0:
        log.debug(std_err)
    log.info("\t  --> Success separation wav file")
    return rx_file_name, tx_file_name


def make_pcm_file(log, job):
    """
    Make pcm file list
    @param      log:            Logger object
    @param      job:            job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                    REC_DU_TM         녹취통화시간
                                    CHN_CLA_CD        채널구분코드
                                    REC_ENC_TP        녹취파일인코딩
                                    CONV_LOC_PATH     녹취변완위치경로
                                    CONV_FILE_NM      녹취변환파일명
                                }
    @return                     PCM information {'wav': pcm_file_path, 'rx': '', 'tx': ''}
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    global DELETE_FILE_LIST
    log_str = '''  3-1) Make WAV to PCM law file'''
    log.info(log_str)
    log.info('\t  --> Convert wav to pcm')
    if not job:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR04' # 녹취파일미탐지
        raise Exception("Not existed wav file")
    conv_loc_path = job['CONV_LOC_PATH']
    conv_file_nm = job['CONV_FILE_NM']
    chn_cla_cd = job['CHN_CLA_CD']
    wav_file_path = os.path.join(conv_loc_path, conv_file_nm)
    # 녹취 파일 없음
    if not os.path.exists(wav_file_path):
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR04' # 녹취파일미탐지
        raise Exception("Not existed wav file [{0}]".format(wav_file_path))
    try:
        if job['REC_ENC_TP'] in ('PCM', 'GSM'):
            # 스테레오 파일일 경우
            if chn_cla_cd == 'S':
                rx_file_name, tx_file_name = separation_wav_file(log, wav_file_path)
                rx_file_path = os.path.join(conv_loc_path, rx_file_name)
                rx_pcm_file_path = os.path.join(conv_loc_path, rx_file_name[:-4] + '.pcm')
                tx_file_path = os.path.join(conv_loc_path, tx_file_name)
                tx_pcm_file_path = os.path.join(conv_loc_path, tx_file_name[:-4] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(rx_file_path, rx_pcm_file_path)
                log.info('\t  --> RX CONV_FILE_NM = {0}'.format(rx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(tx_file_path, tx_pcm_file_path)
                log.info('\t  --> TX CONV_FILE_NM = {0}'.format(tx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': '', 'rx': rx_pcm_file_path, 'tx': tx_pcm_file_path}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(rx_file_path)
                DELETE_FILE_LIST.append(rx_pcm_file_path)
                DELETE_FILE_LIST.append(tx_file_path)
                DELETE_FILE_LIST.append(tx_pcm_file_path)
            # 모노 파일일 경우
            else:
                pcm_file_path = os.path.join(conv_loc_path, os.path.splitext(conv_file_nm)[0] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(wav_file_path, pcm_file_path)
                log.info('\t  --> CONV_FILE_NM = {0}'.format(conv_file_nm))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': pcm_file_path, 'rx': '', 'tx': ''}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(pcm_file_path)
        elif  job['REC_ENC_TP'] in ('G723', ):
            log.info('\t  --> g723 wav file to gsm')
            # 스테레오 파일일 경우
            if chn_cla_cd == 'S':
                gsm_wav_path = os.path.join(conv_loc_path, 'gsm_' + conv_file_nm)
                cmd = "ffmpeg -y -hide_banner -i {0} -c:a gsm_ms -ar 8000 -b:a 13K {1}".format(
                    wav_file_path, gsm_wav_path)
                std_out, std_err = sub_process(cmd)
                if len(std_err) > 0:
                    log.info(std_err)
                rx_file_name, tx_file_name = separation_wav_file(log, gsm_wav_path)
                rx_file_path = os.path.join(conv_loc_path, rx_file_name)
                rx_pcm_file_path = os.path.join(conv_loc_path, rx_file_name[:-4] + '.pcm')
                tx_file_path = os.path.join(conv_loc_path, tx_file_name)
                tx_pcm_file_path = os.path.join(conv_loc_path, tx_file_name[:-4] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(rx_file_path, rx_pcm_file_path)
                log.info('\t  --> RX CONV_FILE_NM = {0}'.format(rx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(tx_file_path, tx_pcm_file_path)
                log.info('\t  --> TX CONV_FILE_NM = {0}'.format(tx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': '', 'rx': rx_pcm_file_path, 'tx': tx_pcm_file_path}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(gsm_wav_path)
                DELETE_FILE_LIST.append(rx_file_path)
                DELETE_FILE_LIST.append(rx_pcm_file_path)
                DELETE_FILE_LIST.append(tx_file_path)
                DELETE_FILE_LIST.append(tx_pcm_file_path)
            # 모노 파일일 경우
            else:
                gsm_wav_path = os.path.join(conv_loc_path, 'gsm_' + conv_file_nm)
                cmd = "ffmpeg -y -hide_banner -i {0} -c:a gsm_ms -ar 8000 -b:a 13K {1}".format(
                    wav_file_path, gsm_wav_path)
                std_out, std_err = sub_process(cmd)
                if len(std_err) > 0:
                    log.info(std_err)
                log.info('\t  --> Convert wav file to pcm')
                pcm_file_path = os.path.join(conv_loc_path, os.path.splitext(conv_file_nm)[0] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(gsm_wav_path, pcm_file_path)
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.info(std_out)
                pcm_info_dict = {'wav': pcm_file_path, 'rx': '', 'tx': ''}
                DELETE_FILE_LIST.append(gsm_wav_path)
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(pcm_file_path)
        else:
            PROG_STAT_CD = '49'
            PROG_STAT_DTL_CD = 'UPLDERR02'  # 지원하지않는코덱
            raise Exception("\t  --> Not support wav encoding. [REC_ENC_TP = {0}]".format(job['REC_ENC_TP']))
        log.info("\t  --> Success convert wav to pcm")
        return pcm_info_dict
    except Exception:
        PROG_STAT_DTL_CD = 'UPLDERR03' # PCM파일생성실패
        raise Exception(traceback.format_exc())


def extract_record_info_and_download(log, job):
    """
    Extract Real STT record information and download
    @param      log:            Logger object
    @param      job:            job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                }
    @return                     job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                    REC_DU_TM         녹취통화시간
                                    CHN_CLA_CD        채널구분코드
                                    REC_ENC_TP        녹취파일인코딩
                                    CONV_LOC_PATH     녹취변완위치경로
                                    CONV_FILE_NM      녹취변환파일명
                                }
    """
    try:
        global PROG_STAT_CD
        global DELETE_FILE_LIST
        global PROG_STAT_DTL_CD
        cnt = 1
        log.info('  2-{0}) SFTP STT AP1,2(10.10.107.81, 10.10.107.82) download Record file'.format(cnt))
        cnt += 1
        payload = {
            'srv_host': socket.gethostbyname(socket.gethostname()),
            'emp_no': job['EMP_NO'],
            'rec_file_nm': job['REC_FILE_NM'],
            'rec_loc_path': job['REC_LOC_PATH'],

        }
        log.info("\t  --> Execute Get Record information from STT AP1,2(10.10.107.81, 10.10.107.82)")
        log.info("\t  --> Execute Download ")
        local_dir_path = False
        for _ in range(0, 3):
            random_cnt = random.randrange(0, 2)
            try:
                result = requests.post(config.UploadSTTConfig.tran_rec_svr_list[random_cnt], json=payload)
                local_dir_path = json.loads(result.text)
                if not local_dir_path:
                    log.error("\t  --> Can't Download Record from STT AP1,2(10.10.107.81, 10.10.107.82)")
                    return False
            except Exception:
                result = requests.post(config.UploadSTTConfig.tran_rec_svr_list[random_cnt - 1], json=payload)
                local_dir_path = json.loads(result.text)
                if not local_dir_path:
                    log.error("\t  --> Can't Download Record from STT AP1,2(10.10.107.81, 10.10.107.82)")
                    return False
        if not local_dir_path:
            return False
        log.info("\t  --> Response = {0}".format(local_dir_path))
        local_file_path = os.path.join(local_dir_path, job['REC_FILE_NM'])
        DELETE_FILE_LIST.append(local_file_path)
        log.info("\t  --> Success, Download Record file")
        if job['REC_FILE_NM'].endswith('.mp3'):
            wav_file_nm = job['REC_FILE_NM'].replace(".mp3", ".wav")
            wav_file_path = os.path.join(local_dir_path, wav_file_nm)
            log.info('  2-{0}) Convert Record file [MP3 --> PCM WAV]'.format(cnt))
            cnt += 1
            cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 2 -b:a 128k {1}'.format(local_file_path, wav_file_path)
            std_out, std_err = sub_process(cmd)
            if len(std_out) > 0:
                log.debug(std_out)
            if len(std_err) > 0:
                log.debug(std_err)
            log.info("\t  --> Success, Convert Record file")
        else:
            wav_file_nm = job['REC_FILE_NM'].strip()
        log.info('  2-{0}) Extract Record file Encoding, Channels, Duration'.format(cnt))
        cnt += 1
        channel = 'M'
        encoding = ''
        record_duration = 0
        try:
            cmd = 'ffmpeg -i {0}'.format(os.path.join(local_dir_path, wav_file_nm))
            std_out, std_err = sub_process(cmd)
            log.info("\t  -->\n{0}".format(std_err.strip()))
            # Encoding
            encoding = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 30]
            if 'g723' in encoding:
                encoding = 'G723'
            elif 'gsm' in encoding:
                encoding = 'GSM'
            elif 'pcm' in encoding:
                encoding = 'PCM'
            else:
                encoding = encoding.replace("Audio:", "").strip().split()[0]
            log.info("\t  --> Encoding = {0}".format(encoding))
            # Channels
            info_str = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 100]
            if 'mono' in info_str:
                channel = 'M'
            elif '1 channel' in info_str:
                channel = 'M'
            elif 'stereo' in info_str:
                channel = 'S'
            elif '2 channel' in info_str:
                channel = 'S'
            else:
                channel = 'M'
            cha_str = channel + ' (스테레오)' if channel == 'S' else channel + ' (모노)'
            log.info("\t  --> Extract channels = {0}".format(cha_str))
            # Duration
            du_st_idx = std_err.find('Duration:')
            duration = std_err[du_st_idx:du_st_idx + 30].split(",")[0].replace("Duration:", "").strip()
            tmp_list = duration.split(":")
            if len(tmp_list) == 3:
                duration_int = int(tmp_list[0]) * 3600
                duration_int += int(tmp_list[1]) * 60
                duration_int += int(float(tmp_list[2]))
                record_duration = duration_int
                log.info("\t  --> Extract duration = {0}(/s)".format(record_duration))
            if record_duration >= 99999:
                if encoding == 'G723':
                    try:
                        file_size = os.path.getsize(local_file_path)
                        record_duration = int(file_size) / 800
                        log.info("\t  --> Duration is too long change to {0}(/s)".format(record_duration))
                    except Exception:
                        log.error(traceback.format_exc())
                        record_duration = 0
                else:
                    log.info("\t  --> Duration is too long change to 0(/s)")
                    record_duration = 0
        except Exception:
            log.error(traceback.format_exc())
            pass
        job.update(
            {
                "REC_DU_TM": record_duration,
                "REC_ENC_TP": encoding,
                "CHN_CLA_CD": channel,
                "CONV_LOC_PATH": local_dir_path,
                "CONV_FILE_NM": wav_file_nm,
            }
        )
        return job
    except Exception:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR00' #녹취다운오류
        raise Exception(traceback.format_exc())


def update_stt_status_to_start(log, db, job):
    """
    Update STT status to start QA_UPLD_CAL_INFO_TB(STT변환테이블)
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                }
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        log.info('  1-1) Update STT PROG_STAT_CD to 42(STT처리중)')
        util.update_upload_stt_status_commit(
            db=db,
            host_nm=socket.gethostname(),
            prog_stat_cd='42', # STT처리중
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            emp_no=job['EMP_NO'],
            all_seq=job['ALL_SEQ'],
            rec_file_nm=job['REC_FILE_NM']
        )
    except Exception:
        PROG_STAT_CD = '49'
        PROG_STAT_DTL_CD = 'UPLDERR01' # STT상태변환오류
        raise Exception(traceback.format_exc())
    log.info('\t  --> Done updated STT status')


def stt_process(log, db, job):
    """
    STT process
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                }
    """
    log.info('1. Update STT status to start QA_UPLD_CAL_INFO_TB(STT변환테이블)')
    update_stt_status_to_start(log, db, job)
    log.info('2. Extract record information and download record file')
    job = extract_record_info_and_download(log, job)
    log.info('3. Make pcm file')
    pcm_file_info = make_pcm_file(log, job)
    log.info('4. Execute STT')
    stt_result_info = execute_cnn_stt(log, db, job, pcm_file_info)
    log.info('5. Insert STT results')
    max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_list = insert_stt_result(log, db, job, stt_result_info)
    log.info('6. Update STT status to end and information to STTARECSTAT(STTA녹취상태)')
    update_stt_status_and_info(log, db, job, max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_info)


def main(job):
    """
    This program that execute STT
    @param      job:            job{
                                    EMP_NO            사원번호
                                    ALL_SEQ           전체순번
                                    REC_FILE_NM       녹취파일명
                                    REC_LOC_PATH      녹취파일위치
                                    REQ_TM            요청시간
                                }
    """
    # Ignore kill signal
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    # Set logger
    base_dir_path = "{0}".format(job['EMP_NO'].strip())
    log = logger.set_logger(
        logger_name=config.UploadSTTConfig.logger_name,
        log_dir_path=os.path.join(config.UploadSTTConfig.log_dir_path, base_dir_path),
        log_file_name='{0}.log'.format(job['REC_FILE_NM'].replace(" ", "")),
        log_level=config.UploadSTTConfig.log_level
    )
    # Execute STT
    log.info("[START] Execute STT ..")
    log_str = '''Record information
                                    EMP_NO         사원번호 = {0}
                                    ALL_SEQ        전체순번 = {1}
                                    REC_FILE_NM    녹취파일명 = {2}
                                    REC_LOC_PATH   녹취파일위치 = {3}
                                    REQ_TM         요청시간 = {4}'''.format(
        job['EMP_NO'], job['ALL_SEQ'], job['REC_FILE_NM'], job['REC_LOC_PATH'], job['REQ_TM']
    )
    log.info(log_str)
    # Connect DB and Load Petra cipher API
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
        load_petra_api(log)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    # Execute STT
    try:
        stt_process(log, db, job)
    except Exception:
        log.error(traceback.format_exc())
        log_str_list = list()
        log_str_list.append('Update QA_UPLD_CAL_INFO_TB(STT변환테이블) status to error code')
        log_str_list.append('[PROG_STAT_CD= {0}, PROG_STAT_DTL_CD= {1}]'.format(PROG_STAT_CD, PROG_STAT_DTL_CD))
        log.info(' '.join(log_str_list))
        # Update QA_UPLD_CAL_INFO_TB(STT변환테이블) status
        util.update_upload_stt_status_commit(
            db=db,
            host_nm=socket.gethostname(),
            prog_stat_cd=PROG_STAT_CD,
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            emp_no=job['EMP_NO'],
            all_seq=job['ALL_SEQ'],
            rec_file_nm=job['REC_FILE_NM']
        )
    finally:
        try:
            db.conn.commit()
            db.disconnect()
        except Exception:
            pass
        del_file(log)
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
