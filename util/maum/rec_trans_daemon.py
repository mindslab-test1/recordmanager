#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-02-26, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import signal
import paramiko
import traceback
import subprocess
from datetime import datetime
from cfg import config
from lib import logger, util, db_connection

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class RecTransDaemon(object):
    def __init__(self, log):
        self.conf = config.RecTransDaemonConfig
        self.logger = log
        self.db = self.connect_db('ORACLE', config.OracleConfig)

    def set_sig_handler(self):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        if frame:
            pass
        if sig == signal.SIGHUP:
            return
        if sig == signal.SIGTERM or sig == signal.SIGINT:
            self.logger.info('stopped by interrupt')
            sys.exit(0)

    def connect_db(self, db_type, db_conf):
        db = ''
        flag = True
        while flag:
            try:
                self.logger.info('Try connecting to {0} DB ...'.format(db_type))
                if db_type.upper() == 'ORACLE':
                    db = db_connection.Oracle(db_conf, failover=True, service_name=True)
                    flag = False
                elif db_type.upper() == 'MSSQL':
                    db = db_connection.MSSQL(db_conf)
                    flag = False
                elif db_type.upper() == 'MYSQL':
                    db = db_connection.MYSQL(db_conf)
                    flag = False
                else:
                    raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
                self.logger.info('Success connect to {0} DB'.format(db_type))
            except Exception:
                err_str = traceback.format_exc()
                self.logger.error(err_str)
                time.sleep(60)
        return db

    def del_file(self, delete_file_list):
        for file_path in delete_file_list:
            try:
                if os.path.exists(file_path):
                    os.remove(file_path)
            except Exception:
                self.logger.error(traceback.format_exc())
                self.logger.error("Can't delete {0}".format(file_path))
                continue

    @staticmethod
    def sub_process(cmd):
        sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        response_out, response_err = sub_pro.communicate()
        return response_out, response_err

    def sftp_trans(self, src_file_path, remote_dir_path, remote_file_path):
        ssh = ''
        sftp = ''
        for svr_host in self.conf.rec_svr_list:
            try:
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.load_host_keys(
                    os.path.expanduser(os.path.join('~', '.ssh', 'known_hosts')))
                ssh.connect(svr_host, username='minds', allow_agent=True, look_for_keys=True, timeout=10)
                sftp = ssh.open_sftp()
                try:
                    ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                    sftp.stat(remote_dir_path)
                except IOError:
                    ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                    pass
                sftp.put(src_file_path, remote_file_path)
                ssh.close()
                sftp.close()
                return True
            except Exception:
                self.logger.error(traceback.format_exc())
                if ssh:
                    ssh.close()
                if sftp:
                    sftp.close()
                continue
        return False

    def pcm_to_wav(self, pcm_file, wav_file):
        cmd = "ffmpeg -y -f s16le -ar 8000 -i {0} {1}".format(pcm_file, wav_file)
        std_out, std_err = self.sub_process(cmd)
        if len(std_err) > 0:
            self.logger.debug(std_err)

    def run(self):
        try:
            self.logger.info('[START] Record Trans daemon process started')
            self.set_sig_handler()
            if self.conf.src_dir_path.endswith('/'):
                src_dir_path = self.conf.src_dir_path.strip()[:-1]
            else:
                src_dir_path = self.conf.src_dir_path.strip()
            if self.conf.rec_dir_path.endswith('/'):
                rec_dir_path = self.conf.rec_dir_path.strip()[:-1]
            else:
                rec_dir_path = self.conf.rec_dir_path.strip()
            #if self.conf.lis_rec_dir_path.endswith('/'):
            #    lis_rec_dir_path = self.conf.lis_rec_dir_path.strip()[:-1]
            #else:
            #    lis_rec_dir_path = self.conf.lis_rec_dir_path.strip()
            while True:
                try:
                    now_time = datetime.now()
                    w_ob = os.walk(src_dir_path)
                    for dir_path, sub_dirs, files in w_ob:
                        for target_file in files:
                            delete_file_list = list()
                            src_file_path = os.path.join(dir_path, target_file)
                            remote_dir_path = dir_path.replace(src_dir_path, rec_dir_path)
                            remote_file_path = os.path.join(remote_dir_path, target_file)
                            if target_file.endswith('.mp3'):
                                mtime = datetime.fromtimestamp(os.path.getmtime(src_file_path))
                                if (now_time - mtime).total_seconds() < 5:
                                    continue
                                self.logger.info("  --> SFTP upload {0} file".format(target_file))
                                a_pcm_file = os.path.join(dir_path, target_file.replace('.mp3', '_0.pcm'))
                                c_pcm_file = os.path.join(dir_path, target_file.replace('.mp3', '_1.pcm'))
                                flag = self.sftp_trans(src_file_path, remote_dir_path, remote_file_path)
                                if flag:
                                    delete_file_list.append(src_file_path)
                                    delete_file_list.append(a_pcm_file)
                                    delete_file_list.append(c_pcm_file)
                                    #file_size = os.path.getsize(src_file_path)
                                    #record_duration = int(file_size) / 3000
                                    #if record_duration > 60:
                                    #    util.update_real_stt(
                                    #        self.db,
                                    #        target_file.replace('.mp3', ''),
                                    #        datetime.fromtimestamp(time.time())
                                    #    )
                                else:
                                    self.logger.ifno("  --> Fail SFTP upload {0} file".format(target_file))
                                #a_wav_file = os.path.join(dir_path, target_file.replace('.mp3', '_0.wav'))
                                #if os.path.exists(a_pcm_file):
                                #    self.pcm_to_wav(a_pcm_file, a_wav_file)
                                #c_wav_file = os.path.join(dir_path, target_file.replace('.mp3', '_1.wav'))
                                #if os.path.exists(c_pcm_file):
                                #    self.pcm_to_wav(c_pcm_file, c_wav_file)
                                #wav_file_name = target_file.replace('.mp3', '.wav')
                                #wav_file = os.path.join(dir_path, wav_file_name)
                                #if os.path.exists(a_wav_file) and os.path.exists(c_wav_file):
                                #    cmd = "sox -M -c 1 {0} -c 1 {1} {2}".format(c_wav_file, a_wav_file, wav_file)
                                #    std_out, std_err = self.sub_process(cmd)
                                #    if len(std_err) > 0:
                                #        self.logger.debug(std_err)
                                #    if os.path.exists(wav_file):
                                #        remote_dir_path = dir_path.replace(src_dir_path, lis_rec_dir_path)
                                #        remote_file_path = os.path.join(remote_dir_path, wav_file_name)
                                #        self.logger.info("  --> SFTP upload {0} file".format(wav_file_name))
                                #        flag = self.sftp_trans(wav_file, remote_dir_path, remote_file_path)
                                #        if flag:
                                #            delete_file_list.append(wav_file)
                                #            delete_file_list.append(a_wav_file)
                                #            delete_file_list.append(c_wav_file)
                                #        else:
                                #            self.logger.ifno("  --> Fail SFTP upload {0} file".format(wav_file_name))
                            else:
                                continue
                            self.del_file(delete_file_list)
                except Exception:
                    self.logger.error(traceback.format_exc())
                    pass
                time.sleep(self.conf.pro_interval)
        except KeyboardInterrupt:
            self.logger.info('stopped by interrupt')
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.logger.info('[E N D] Record Trans daemon process stopped')
            raise Exception


########
# main #
########
def main():
    """
    This is a program that Record Trans Daemon process
    """
    # Set logger
    log = logger.get_timed_rotating_logger(
        logger_name=config.RecTransDaemonConfig.logger_name,
        log_dir_path=config.RecTransDaemonConfig.log_dir_path,
        log_file_name=config.RecTransDaemonConfig.log_file_name,
        backup_count=config.RecTransDaemonConfig.backup_count,
        log_level=config.RecTransDaemonConfig.log_level
    )
    rec_trans_daemon = RecTransDaemon(log)
    while True:
        try:
            rec_trans_daemon.run()
        except Exception:
            raise Exception
        finally:
            time.sleep(1)


if __name__ == '__main__':
    main()
