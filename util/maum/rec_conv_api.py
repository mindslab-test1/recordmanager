#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-26, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import json
import socket
import paramiko
import requests
import traceback
import subprocess
from datetime import datetime
from flask import Flask, request
from flask_cors import CORS
from flask_restful import reqparse, Api, Resource
from cfg import config
from lib import logger, util, db_connection, cipher

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class ColQAapi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.db = kwargs.get('db')
        self.host_nm = socket.gethostname()
        self.json_data = request.get_json(silent=True, cache=False, force=True)

    def post(self):
        return main(self.log, self.db, self.host_nm, self.json_data)


class DecTranRECapi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.json_data = request.get_json(silent=True, cache=False, force=True)
        self.srv_host = self.json_data.get('srv_host')
        self.rec_st_dt = self.json_data.get('rec_st_dt')
        self.stt_rec_file_nm = self.json_data.get('stt_rec_file_nm')

    def post(self):
        return trans_rec(self.log, self.srv_host, self.rec_st_dt, self.stt_rec_file_nm)


class UploadTranRECapi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.json_data = request.get_json(silent=True, cache=False, force=True)
        self.srv_host = self.json_data.get('srv_host')
        self.emp_no = self.json_data.get('emp_no')
        self.rec_file_nm = self.json_data.get('rec_file_nm')
        self.rec_loc_path = self.json_data.get('rec_loc_path')

    def post(self):
        return upload_trans_rec(self.log, self.srv_host, self.emp_no, self.rec_file_nm, self.rec_loc_path)


class TrimRECapi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.json_data = request.get_json(silent=True, cache=False, force=True)
        self.rec_file_nm = self.json_data.get('rec_file_nm')
        self.rec_loc_path = self.json_data.get('rec_loc_path')
        self.stmt_st_tm = self.json_data.get('stmt_st_tm')
        self.stmt_ed_tm = self.json_data.get('stmt_ed_tm')

    def post(self):
        return trim_rec(self.log, self.rec_file_nm, self.rec_loc_path, self.stmt_st_tm, self.stmt_ed_tm)


#######
# def #
#######
def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def sub_process(cmd):
    """
    Execute subprocess
    @param      cmd:        Command
    """
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    return response_out, response_err


def trim_rec(log, rec_file_nm, rec_loc_path, stmt_st_tm, stmt_ed_tm):
    """
    Trim record file
    @param      log:                Logger Object
    @param      rec_file_nm:        RECORD_FILE_NM
    @param      rec_loc_path:       REC_LOC_PATH
    @param      stmt_st_tm:         STMT_STM_TM
    @param      stmt_ed_tm:         STMT_ED_TM
    @return:                        False or record file path
    """
    log.info('-' * 100)
    log_list = list()
    log_list.append("[START] Trim record REC_FILE_NM= {0}, REC_LOC_PATH= {1},".format(rec_file_nm, rec_loc_path))
    log_list.append("STMT_ST_TM= {0}, STMT_ED_TM= {1}".format(stmt_st_tm, stmt_ed_tm))
    log.info(' '.join(log_list))
    try:
        src_file_path = os.path.join(rec_loc_path, rec_file_nm)
        if not os.path.exists(src_file_path):
            raise Exception("Can't find record file[{0}]".format(src_file_path))
        st_hh, st_mm, st_ss = stmt_st_tm.split(':')
        ed_hh, ed_mm, ed_ss = stmt_ed_tm.split(':')
        start = ((int(st_hh) * 60 * 60) + (int(st_mm) * 60) + int(st_ss))
        end = ((int(ed_hh) * 60 * 60) + (int(ed_mm) * 60) + int(ed_ss))
        remote_file_path = os.path.join(rec_loc_path, "{0}_{1}_{2}".format(start, end, rec_file_nm))
        cmd = "ffmpeg -y -hide_banner -i {0} -ss {1} -to {2} {3}".format(src_file_path, start, end, remote_file_path)
        std_out, std_err = sub_process(cmd)
        if len(std_out) > 0:
            log.info(std_out)
        if len(std_err) > 0:
            log.info(std_err)
        log.info("\t  --> Success Trim record file")
        log_list = list()
        log_list.append("[Done] Trim record REC_FILE_NM= {0}, REC_LOC_PATH= {1},".format(rec_file_nm, rec_loc_path))
        log_list.append("STMT_ST_TM= {0}, STMT_ED_TM= {1}".format(stmt_st_tm, stmt_ed_tm))
        log.info(' '.join(log_list))
        return remote_file_path
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        log_list = list()
        log_list.append("[FAIL] Trim record REC_FILE_NM= {0}, REC_LOC_PATH= {1},".format(rec_file_nm, rec_loc_path))
        log_list.append("STMT_ST_TM= {0}, STMT_ED_TM= {1}".format(stmt_st_tm, stmt_ed_tm))
        log.info(' '.join(log_list))
        return -1


def upload_trans_rec(log, srv_host, emp_no, rec_file_nm, rec_loc_path):
    """
    Upload STT transfer STT server
    @param      log:                Logger Object
    @param      srv_host:           Target host
    @param      emp_no:             EMP_NO
    @param      rec_file_nm:        RECORD_FILE_NM
    @param      rec_loc_path:       REC_LOC_PATH
    @return:                        False or Remote record file path
    """
    log.info('-' * 100)
    log.info("[START] Upload STT requests host={0}, EMP_NO={1}, REC_FILE_NM= {2}".format(srv_host, emp_no, rec_file_nm))
    try:
        remote_dir_path = os.path.join(config.UploadSTTConfig.get_rec_dir_path, "{0}".format(emp_no))
        src_file_path = os.path.join(rec_loc_path, rec_file_nm)
        if not os.path.exists(src_file_path):
            return False
        remote_file_path = os.path.join(remote_dir_path, rec_file_nm)
        log.info("\t  --> Upload STT SFTP upload record file")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_host_keys(os.path.expanduser(os.path.join('~', '.ssh', 'known_hosts')))
        ssh.connect(srv_host, username='minds', allow_agent=True, look_for_keys=True, timeout=10)
        sftp = ssh.open_sftp()
        try:
            ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
            sftp.stat(remote_dir_path)
        except IOError:
            ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
            pass
        sftp.put(src_file_path, remote_file_path)
        ssh.close()
        sftp.close()
        log.info("\t  --> Success Upload STT SFTP upload record file")
        log.info("[DONE] Upload STT requests host={0}, EMP_NO={1}, REC_FILE_NM= {2}".format(
            srv_host, emp_no, rec_file_nm))
        return remote_dir_path
    except Exception:
        err_msg = traceback.format_exc()
        log.error(err_msg)
        log.info("[FAIL] Upload STT requests host={0}, REC_ST_DT={1}, REC_FILE_NM= {2}".format(
            srv_host, emp_no, rec_file_nm))
        return False


def trans_rec(log, srv_host, rec_st_dt, stt_rec_file_nm):
    """
    Transfer Real STT mp3 file
    @param      log:                Logger Object
    @param      srv_host:           Target host
    @param      rec_st_dt:          REC_ST_DT
    @param      stt_rec_file_nm:    Real STT record file name
    @return:                        False or Remote record file path
    """
    log.info('-' * 100)
    log.info("[START] Requests host={0}, REC_ST_DT={1}, STT_REC_FILE_NM= {2}".format(
        srv_host, rec_st_dt, stt_rec_file_nm)
    )
    flag = False
    remote_dir_path = False
    for _ in range(1, 3):
        try:
            year, month, day = rec_st_dt.split('-')
            src_dir_path = os.path.join(config.RecConvConfig.real_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
            remote_dir_path = os.path.join(config.STTConfig.get_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
            src_file_path = os.path.join(src_dir_path, stt_rec_file_nm)
            if not os.path.exists(src_file_path):
                continue
            remote_file_path = os.path.join(remote_dir_path, stt_rec_file_nm)
            log.info("\t  --> Real STT SFTP upload mp3 file")
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.load_host_keys(os.path.expanduser(os.path.join('~', '.ssh', 'known_hosts')))
            ssh.connect(srv_host, username='minds', allow_agent=True, look_for_keys=True, timeout=10)
            sftp = ssh.open_sftp()
            try:
                ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                sftp.stat(remote_dir_path)
            except IOError:
                ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                pass
            sftp.put(src_file_path, remote_file_path)
            ssh.close()
            sftp.close()
            log.info("\t  --> Success Real STT SFTP upload mp3 file")
            log.info("[DONE] Requests host={0}, REC_ST_DT={1}, STT_REC_FILE_NM= {2}".format(
                srv_host, rec_st_dt, stt_rec_file_nm)
            )
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            time.sleep(1)
            continue
    if flag:
        return remote_dir_path
    else:
        log.info("[FAIL] Requests host={0}, REC_ST_DT={1}, STT_REC_FILE_NM= {2}".format(
            srv_host, rec_st_dt, stt_rec_file_nm)
        )
        return False


def convert_real_rec(log, db, host_nm, rec_st_dt, rec_key, stt_rec_file_nm, stt_rec_loc_path):
    """
    Convert record MP3 to PCM wav
    @param      host_nm:                Host name
    @param      log:                    Logger Object
    @param      db:                     DB object
    @param      rec_st_dt:              REC_ST_DT(녹취시작일자)
    @param      rec_key:                REC_KEY(녹취KEY)
    @param      stt_rec_file_nm:        STT_REC_FILE_NM(STT녹취파일명)
    @param      stt_rec_loc_path:       STT_REC_LOC_PATH(STT녹취파일경로)
    """
    year, month, day = rec_st_dt.strip().split('-')
    local_file_path = os.path.join(stt_rec_loc_path, stt_rec_file_nm)
    lis_dir_path = os.path.join(config.RecConvConfig.lis_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
    if not os.path.exists(lis_dir_path):
        os.makedirs(lis_dir_path)
    wav_file_path = os.path.join(lis_dir_path, stt_rec_file_nm.replace('.mp3', '.wav'))
    cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 1 -b:a 128k {1}'.format(local_file_path, wav_file_path)
    std_out, std_err = sub_process(cmd)
    if len(std_out) > 0:
        log.debug(std_out)
    if len(std_err) > 0:
        log.debug(std_err)
    log.info("\t  --> Success, Convert Record file")
    bind = (
        stt_rec_file_nm.replace('.mp3', '.wav'),
        lis_dir_path,
        host_nm,
        datetime.fromtimestamp(time.time()),
        rec_key
    )
    log.info("\t  --> Update LIS_REC_FILE_NM, LIS_REC_LOC_PATH")
    util.update_conv_rec_info(db, bind)
    return wav_file_path


def convert_rec(log, db, host_nm, rec_key, rec_file_nm, rec_file_path, encoding):
    """
    Convert record GSM to PCM wav
    @param      host_nm:            Host name
    @param      log:                Logger Object
    @param      db:                 DB object
    @param      rec_key:            REC_KEY(녹취KEY)
    @param      rec_file_nm:        REC_FILE_NM(녹취파일명)
    @param      rec_file_path:      Local record file path
    @param      encoding:           Record file encoding
    """
    tmp_rec_file_path = os.path.join(os.path.dirname(rec_file_path), 'pcm_' + rec_file_nm)
    if encoding == 'G723':
        cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 1 {1}'.format(rec_file_path, tmp_rec_file_path)
    else:
        cmd = 'sox -r 8000 -c 1 {0} -r 8000 -c 1 -e signed-integer {1}'.format(rec_file_path, tmp_rec_file_path)
    std_out, std_err = sub_process(cmd)
    if len(std_out) > 0:
        log.info(std_out)
    if len(std_err) > 0:
        log.info(std_err)
    log.info("\t  --> Success, Convert Record file")
    try:
        if os.path.exists(rec_file_path):
            os.remove(rec_file_path)
        os.rename(tmp_rec_file_path, rec_file_path)
    except Exception:
        log.error(traceback.format_exc())
        log.error("Can't delete {0}".format(rec_file_path))
    bind = (
        rec_file_nm,
        os.path.dirname(rec_file_path),
        host_nm,
        datetime.fromtimestamp(time.time()),
        rec_key
    )
    log.info("\t  --> Update LIS_REC_FILE_NM, LIS_REC_LOC_PATH")
    util.update_conv_rec_info(db, bind)


def download_rec(log, rec_st_dt, brof_cd, rec_file_nm):
    """
    Download record from CallTechSolution(통합녹취서버)
    @param      log:                Logger Object
    @param      rec_st_dt:          REC_ST_DT(녹취시작일자)
    @param      brof_cd:            BROF_CD(지점코드)
    @param      rec_file_nm:        REC_FILE_NM(녹취파일명)
    @return:                        Local record file path, Record file encoding
    """
    aes_cipher = cipher.AESCipher(config.AESConfig)
    data = {
        "cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
        "data": [{'recdate': rec_st_dt, 'branchcd': brof_cd, 'recfilenm': rec_file_nm}]
    }
    log.info("\t  --> Execute Get Record information from CallTechSolution(통합녹취서버)")
    result = requests.post(config.CTSConfig.stt_url, json=data)
    json_result = json.loads(result.text)
    if len(json_result['data']) < 1:
        raise Exception("Can't get Record information from CallTechSolution(통합녹취서버)")
    log.info("\t  --> Response = {0}".format(json_result))
    item = json_result['data'][0]
    if not item['ftpipvoice']:
        raise Exception("Can't get Record information from CallTechSolution(통합녹취서버)")
    log.info("\t  --> Success, Get Record information from CallTechSolution(통합녹취서버)")
    log.info('  1-2) SFTP CallTechSolution(통합녹취서버) download Record file')
    log.info("\t  --> Execute connecting CallTechSolution(통합녹취서버)")
    transport = paramiko.Transport(item['ftpipvoice'], item['ftppot'])
    transport.connect(username=item['ftpacc'], password=aes_cipher.decrypt_hk(item['ftppwd']))
    sftp = paramiko.SFTPClient.from_transport(transport)
    log.info("\t  --> Success, connect CallTechSolution(통합녹취서버)")
    source_file_path = os.path.join(item['recfilepath'].replace('\\', '/'), rec_file_nm)
    year, month, day = rec_st_dt.strip().split('-')
    local_dir_path = os.path.join(config.RecConvConfig.lis_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
    if not os.path.exists(local_dir_path):
        os.makedirs(local_dir_path)
    local_file_path = os.path.join(local_dir_path, rec_file_nm)
    log.info("\t  --> Execute Download {0} --> {1}".format(source_file_path, local_file_path))
    sftp.get(source_file_path, local_file_path)
    log.info("\t  --> Success, Download Record file")
    sftp.close()
    transport.close()
    log.info('  1-3) Extract Record file Encoding')
    encoding = 'GSM'
    try:
        cmd = 'ffmpeg -i {0}'.format(
            os.path.join(local_dir_path, rec_file_nm.replace(" ", "").replace('(', '\(').replace(')', '\)')))
        std_out, std_err = sub_process(cmd)
        log.info("\t  -->\n{0}".format(std_err.strip()))
        # Encoding
        encoding = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 30]
        if 'g723' in encoding:
            encoding = 'G723'
        elif 'gsm' in encoding:
            encoding = 'GSM'
        elif 'pcm' in encoding:
            encoding = 'PCM'
        else:
            encoding = encoding.replace("Audio:", "").strip().split()[0]
        log.info("\t  --> Encoding = {0}".format(encoding))
    except Exception:
        log.error(traceback.format_exc())
        pass
    return local_file_path, encoding


def main(log, db, host_nm, json_data):
    """
    This is a program that download and convert record GSM to PCM
    @param     log:            Logger Object
    @param     db:             DB object
    @param     host_nm:        Host name
    @param     json_data:      Json data
    @return:                   Json output({code: 0 OR -1, msg: Message})
    """
    flag = False
    err_msg = ""
    local_file_path = ""
    log.info('-' * 100)
    log.info("[START] REC_ST_DT={0}, BROF_CD={1}, REC_FILE_NM= {2}, REC_COMP_CD= {3}".format(
        json_data.get('rec_st_dt'),
        json_data.get('brof_cd'),
        json_data.get('rec_file_nm'),
        json_data.get('rec_comp_cd'))
    )
    for _ in range(0, 3):
        try:
            rec_st_dt = json_data.get('rec_st_dt')
            rec_key = json_data.get('rec_key')
            brof_cd = json_data.get('brof_cd')
            rec_file_nm = json_data.get('rec_file_nm')
            #rec_comp_cd = json_data.get('rec_comp_cd')
            #stt_rec_file_nm = json_data.get('stt_rec_file_nm')
            #stt_rec_loc_path = json_data.get('stt_rec_loc_path')
            #stt_file_path = os.path.join(stt_rec_loc_path, stt_rec_file_nm)
            #if rec_comp_cd == '100' and os.path.exists(stt_file_path):
            #    log.info('1) Convert Real STT Record file [MP3 --> PCM WAV]')
            #    log.info("\t --> STT_REC_FILE_NM= {0}, STT_REC_LOC_PATH= {1}".format(
            #        json_data.get('stt_rec_file_nm'), json_data.get('stt_rec_loc_path'))
            #    )
            #    local_file_path = convert_real_rec(
            #        log, db, host_nm, rec_st_dt, rec_key, stt_rec_file_nm, stt_rec_loc_path)
            #    flag = True
            #else:
            log.info('1) Select Record information from CallTechSolution(통합녹취서버)')
            local_file_path, encoding = download_rec(log, rec_st_dt, brof_cd, rec_file_nm)
            log.info('2) Convert GSM to PCM wav')
            convert_rec(log, db, host_nm, rec_key, rec_file_nm, local_file_path, encoding)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            time.sleep(0.1)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg, "rec_file_path": local_file_path}
        log.info("[FAIL] REC_ST_DT={0}, BROF_CD={1}, REC_FILE_NM= {2}, REC_COMP_CD= {3}".format(
            json_data.get('rec_st_dt'),
            json_data.get('brof_cd'),
            json_data.get('rec_file_nm'),
            json_data.get('rec_comp_cd'))
        )
        return output
    output = {"code": 0, "msg": "Success", "rec_file_path": local_file_path}
    log.info("\t --> Listen file path= {0}".format(local_file_path))
    log.info("[DONE] REC_ST_DT={0}, BROF_CD={1}, REC_FILE_NM= {2}, REC_COMP_CD= {3}".format(
        json_data.get('rec_st_dt'),
        json_data.get('brof_cd'),
        json_data.get('rec_file_nm'),
        json_data.get('rec_comp_cd'))
    )
    return output


# Logger 생성
log_obj = logger.get_timed_rotating_logger(
    logger_name=config.RecConvConfig.logger_name,
    log_dir_path=config.RecConvConfig.log_dir_path,
    log_file_name=config.RecConvConfig.log_file_name,
    backup_count=config.RecConvConfig.backup_count,
    log_level=config.RecConvConfig.log_level
)
log_obj.info('[START] Record download and convert API process start')
# Connect DB
db_obj = connect_db(log_obj, 'ORACLE', config.OracleConfig)
# Flask 인스턴스 생성
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)
api = Api(app)
parser = reqparse.RequestParser()
parser.add_argument('rec_st_dt', type=str)
parser.add_argument('brof_cd', type=str)
parser.add_argument('rec_file_nm', type=str)
parser.add_argument('srv_host', type=str)
parser.add_argument('stt_rec_file_nm', type=str)
api.add_resource(ColQAapi, '/rec_conv', resource_class_kwargs={'log': log_obj, 'db': db_obj})
api.add_resource(DecTranRECapi, '/dec_tran_rec', resource_class_kwargs={'log': log_obj, 'db': db_obj})
api.add_resource(UploadTranRECapi, '/upload_tran_rec', resource_class_kwargs={'log': log_obj, 'db': db_obj})
api.add_resource(TrimRECapi, '/trim_rec', resource_class_kwargs={'log': log_obj, 'db': db_obj})
#app.run(debug=True, use_reloader=False, host='::', port=8786)
