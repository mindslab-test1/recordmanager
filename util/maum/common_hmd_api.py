#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2020-12-15, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import grpc
import json
import traceback
from flask import Flask, request
from flask_cors import CORS
from google.protobuf import json_format
from flask_restful import reqparse, Api, Resource
from cfg import config
from lib import logger, util, db_connection, hmd, libpcpython

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")


#########
# class #
#########
class CommonFwdLoadApi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.db = kwargs.get('db')
        self.post()

    def post(self):
        return load_common_fwd_hmd(self.log, self.db)


class CommonScriptLoadApi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.db = kwargs.get('db')
        self.post()

    def post(self):
        return load_common_script_hmd(self.log, self.db)


class CommonScriptStartLoadApi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.db = kwargs.get('db')
        self.post()

    def post(self):
        return load_common_script_start_hmd(self.log, self.db)


class EncryptStringApi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.petra = kwargs.get('petra')
        self.string = request.form.get('string')

    def post(self):
        return encrypt_string(self.log, self.petra, self.string)


class DecryptStringApi(Resource):
    def __init__(self, **kwargs):
        self.log = kwargs.get('log')
        self.petra = kwargs.get('petra')
        self.string = request.form.get('string')

    def post(self):
        return decrypt_string(self.log, self.petra, self.string)


#######
# def #
#######
def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def load_common_fwd_hmd(log, db):
    """
    This is a program that Reload Common Forbidden word HMD
    @param:     log:    Logger object
    @param:     db:     DB object
    @return:            Json output({code: 0 OR -1, msg: Message})
    """
    flag = False
    err_msg = ""
    for _ in range(0, 3):
        try:
            # Make HMD model
            hmd_model_list = list()
            result = util.select_common_fwd_hmd(db)
            for item in result:
                if not item['SNS_YN'] == 'Y':
                    item['SNS_YN'] = 'N'
                category = [
                    str(item['CHK_ITM_CD']).strip(),
                    str(item['CHK_APP_ORD']).strip(),
                    str(item['CUSL_CD']).strip(),
                    str(item['CUSL_APP_ORD']).strip(),
                    str(item['DTCT_DTC_NO']).strip(),
                    str(item['DTCT_DTC_GRP_NO']).strip(),
                    str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                    str(item['DTCT_DTC_ED_NO']).strip(),
                    str(item['SNS_YN']).strip(),
                    str(item['SCRP']).strip()
                ]
                if 'None' in category:
                    log.error("Category is Not NULL -> {0}".format(category))
                    continue
                hmd_model_list.append((category, item['DTCT_DTC_CON']))
            hmd_client = hmd.HmdClient()
            hmd_client.set_model('common_fwd', hmd_model_list)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg}
        log.info("Fail, Common Forbidden word HMD load")
        return output
    output = {"code": 0, "msg": "Success, Common Forbidden word HMD load"}
    log.info("Success, Common Forbidden word HMD load")
    return output


def load_common_script_hmd(log, db):
    """
    This is a program that Reload Common Script HMD
    @param:     log:    Logger object
    @param:     db:     DB object
    @return:            Json output({code: 0 OR -1, msg: Message})
    """
    flag = False
    err_msg = ""
    for _ in range(0, 3):
        try:
            # Make HMD model
            hmd_model_list = list()
            result = util.select_common_script_hmd(db)
            for item in result:
                category = [
                    str(item['CHK_ITM_CD']).strip(),
                    str(item['CHK_APP_ORD']).strip(),
                    str(item['SCRP_LCTG_CD']).strip(),
                    str(item['SCRP_MCTG_CD']).strip(),
                    str(item['SCRP_MCTG_NM']).strip(),
                    str(item['SCRP_SCTG_CD']).strip(),
                    str(item['SCRP_SCTG_NM']).strip(),
                    str(item['SCRP_APP_ORD']).strip(),
                    str(item['ST_SCRP_YN']).strip(),
                    str(item['ESTY_SCRP_YN']).strip(),
                    str(item['DTCT_DTC_NO']).strip(),
                    str(item['DTCT_DTC_GRP_NO']).strip(),
                    str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                    str(item['DTCT_DTC_ED_NO']).strip(),
                    str(item['CUST_ANS_YN']).strip()
                ]
                if 'None' in category:
                    log.error("Category is Not NULL -> {0}".format(category))
                    continue
                hmd_model_list.append((category, item['DTCT_DTC_CON']))
            hmd_client = hmd.HmdClient()
            hmd_client.set_model('common_script', hmd_model_list)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg}
        log.info("Fail, Common Script HMD load")
        return output
    output = {"code": 0, "msg": "Success, Common Script HMD load"}
    log.info("Success, Common Script HMD load")
    return output


def load_common_script_start_hmd(log, db):
    """
    This is a program that Reload Common Script Start HMD
    @param:     log:    Logger object
    @param:     db:     DB object
    @return:            Json output({code: 0 OR -1, msg: Message})
    """
    flag = False
    err_msg = ""
    for _ in range(0, 3):
        try:
            # Make HMD model
            hmd_model_list = list()
            result = util.select_common_script_start_hmd(db)
            for item in result:
                category = [
                    str(item['CHK_ITM_CD']).strip(),
                    str(item['CHK_APP_ORD']).strip(),
                    str(item['SCRP_LCTG_CD']).strip(),
                    str(item['SCRP_MCTG_CD']).strip(),
                    str(item['SCRP_MCTG_NM']).strip(),
                    str(item['SCRP_SCTG_CD']).strip(),
                    str(item['SCRP_SCTG_NM']).strip(),
                    str(item['SCRP_APP_ORD']).strip(),
                    str(item['ST_SCRP_YN']).strip(),
                    str(item['ESTY_SCRP_YN']).strip(),
                    str(item['DTCT_DTC_NO']).strip(),
                    str(item['DTCT_DTC_GRP_NO']).strip(),
                    str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                    str(item['DTCT_DTC_ED_NO']).strip(),
                    str(item['CUST_ANS_YN']).strip()
                ]
                if 'None' in category:
                    log.error("Category is Not NULL -> {0}".format(category))
                    continue
                hmd_model_list.append((category, item['DTCT_DTC_CON']))
            hmd_client = hmd.HmdClient()
            hmd_client.set_model('common_script_start', hmd_model_list)
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg}
        log.info("Fail, Common Script Start HMD load")
        return output
    output = {"code": 0, "msg": "Success, Common Script Start HMD load"}
    log.info("Success, Common Script Start HMD load")
    return output


def encrypt_string(log, petra, string):
    """
    This is a program that Encrypt String
    @param:     log:            Logger object
    @param:     petra:          Petra sid
    @return:                    Json output({code: 0 OR -1, msg: Message, rst: Result})
    """
    flag = False
    err_msg = ""
    enc_string = str()
    #log.info('-' * 100)
    for _ in range(0, 3):
        try:
            #log.info('enc_cold_id: {0}'.format(config.PetraConfig.enc_cold_id))
            #log.info('string type: {0}'.format(type(string)))
            enc_string = libpcpython.PcAPI_enc_id(
                petra, config.PetraConfig.enc_cold_id, string.encode('utf-8'), len(string.encode('utf-8')))
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg, 'rst': enc_string}
        log.error("Fail, Encrypt String: {0}".format(output))
        return output
    output = {"code": 0, "msg": "Success", 'rst': enc_string}
    # log.info("Success, Encrypt String")
    return output


def decrypt_string(log, petra, enc_string):
    """
    This is a program that Decrypt String
    @param:     log:            Logger object
    @param:     petra:          Petra sid
    @return:                    Json output({code: 0 OR -1, msg: Message, rst: Result})
    """
    flag = False
    string = ""
    err_msg = ""
    #log.info('-' * 100)
    #log.info("[START] Decrypt String (Target: {0})".format(enc_string))
    for _ in range(0, 3):
        try:
            enc_string = enc_string.encode('utf-8')
            string = libpcpython.PcAPI_dec_id(petra, config.PetraConfig.enc_cold_id, enc_string, len(enc_string))
            flag = True
            break
        except Exception:
            err_msg = traceback.format_exc()
            log.error(err_msg)
            pass
    if not flag:
        output = {"code": -1, "msg": err_msg, 'rst': string}
        log.info("Fail, Decrypt String")
        return output
    output = {"code": 0, "msg": "Success", 'rst': string}
    log.info("Success, Decrypt String")
    return output


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:            Logger Object
    """
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    petra_sid = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))
    return petra_sid


# Logger 생성
log_obj = logger.get_timed_rotating_logger(
    logger_name=config.HMDAPIConfig.logger_name,
    log_dir_path=config.HMDAPIConfig.log_dir_path,
    log_file_name=config.HMDAPIConfig.log_file_name,
    backup_count=config.HMDAPIConfig.backup_count,
    log_level=config.HMDAPIConfig.log_level
)
log_obj.info('[START] Common HMD API process start')
db_obj = connect_db(log_obj, 'ORACLE', config.OracleConfig)
petra_sid_obj = load_petra_api(log_obj)
# Flask 인스턴스 생성
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)
api = Api(app)
parser = reqparse.RequestParser()
parser.add_argument('string', type=str)
# Common Forbidden word 공통금지어
load_common_fwd_hmd(log_obj, db_obj)
api.add_resource(CommonFwdLoadApi, '/common_fwd_load', resource_class_kwargs={'log': log_obj, 'db': db_obj})
# Common QA script 공통스크립트
load_common_script_hmd(log_obj, db_obj)
api.add_resource(CommonScriptLoadApi, '/common_script_load', resource_class_kwargs={'log': log_obj, 'db': db_obj})
# Common QA script Start 공통스크립트
load_common_script_start_hmd(log_obj, db_obj)
api.add_resource(
    CommonScriptStartLoadApi, '/common_script_start_load', resource_class_kwargs={'log': log_obj, 'db': db_obj})
# Encrypt String
api.add_resource(EncryptStringApi, '/encrypt_string', resource_class_kwargs={'log': log_obj, 'petra': petra_sid_obj})
# Decrypt String
api.add_resource(DecryptStringApi, '/decrypt_string', resource_class_kwargs={'log': log_obj, 'petra': petra_sid_obj})
#app.run(debug=True, use_reloader=False, host='::', port=8788)
