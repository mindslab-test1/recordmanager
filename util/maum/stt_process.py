#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-15, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import json
import grpc
import signal
import socket
import random
import paramiko
import requests
import traceback
import subprocess
from operator import itemgetter
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection, cnn, masking, cipher, flashtext, nlp, hmd, change_arabic, libpcpython

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
PETRA_SID = ''
PROG_STAT_CD = '13'  # '13': STT변환완료
PROG_STAT_DTL_CD = None
DELETE_FILE_LIST = list()
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def sub_process(cmd):
    """
    Execute subprocess
    @param      cmd:        Command
    """
    sub_pro = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    response_out, response_err = sub_pro.communicate()
    return response_out, response_err


def del_file(log):
    """
    Delete file
    @param  log:    Logger object
    """
    for file_path in DELETE_FILE_LIST:
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't delete {0}".format(file_path))
            continue


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def extract_speech_time(seconds):
    """
    Extract speech time
    :param      seconds:    Seconds(/s)
    :return:                HH:MM:SS.FF
    """
    time_list = str(timedelta(seconds=float(seconds))).split(':')
    hh = ('0' + time_list[0])[-2:]
    mm = time_list[1]
    ss = '%.2f' % float(time_list[2])
    ss = ss.split('.')[0]
    ss = ss if len(ss) == 2 else '0' + ss
    return ':'.join([hh, mm, ss])


def make_target_line(end_idx, line_gap, dtc_line_list):
    """
    Make target line
    @param      end_idx:            End index
    @param      line_gap:           Line gap
    @param      dtc_line_list:      Start line number list
    @return:                        Output list
    """
    if '1' not in dtc_line_list:
        return list()
    dtc_list = list()
    start_line_list = dtc_line_list['1']
    for line_num in start_line_list:
        temp_dict = dict()
        temp_dict['1'] = [line_num]
        for idx in range(1, end_idx):
            temp_list = list()
            if str(idx) not in temp_dict:
                continue
            for num in temp_dict[str(idx)]:
                for cnt in range(0, line_gap + 1):
                    next_line_num = int(num) + cnt
                    if str(idx + 1) not in dtc_line_list:
                        return list()
                    if next_line_num in dtc_line_list[str(idx + 1)]:
                        if next_line_num not in temp_list:
                            temp_list.append(next_line_num)
                        temp_dict[str(idx + 1)] = temp_list
        if len(temp_dict) != end_idx:
            continue
        dtc_list.append(temp_dict)
    output_list = list()
    for item_dict in dtc_list:
        for line_list in item_dict.values():
            for line_number in line_list:
                if line_number not in output_list:
                    output_list.append(line_number)
    return output_list


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:            Logger Object
    """
    global PETRA_SID
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    PETRA_SID = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))


def insert_ta_output(log, db, rec_key, output_list):
    """
    Insert TA output
    @param      log:                Logger object
    @param      db:                 DB object
    @param      rec_key:            녹취KEY
    @param      output_list:        HMD Output list[
                                        {
                                            'REC_KEY': 녹취KEY,
                                            'STMT_NO': 문장번호,
                                            'SPK_DIV_CD': 화자구분코드,
                                            'STMT_NO': 문장번호,
                                            'STMT_ST_TM': 문장시작시간,
                                            'STMT_ED_TM': 문장종료시간,
                                            'DTCT_STMT': 탐지문장,
                                            'CHK_ITM_CD': 평가항목코드,
                                            'CHK_APP_ORD': 항목적용차수,
                                            'CUSL_CD': 상담코드,
                                            'CUSL_APP_ORD': 상담적용차수,
                                            'DTCT_DTC_NO': 탐지사전번호,
                                            'DTCT_KWD': 탐지키워드
                                        }......
                                    ]
    """
    all_seq = 1
    bind_list = list()
    if not output_list:
        log.info('\t  --> Noting TA results')
        return
    del_flag = util.select_ta_results_existed(db, rec_key)
    if del_flag:
        log.info('\t  --> Already existed. Delete TA results')
        util.delete_ta_results(db, rec_key)
        log.info('\t  --> Done delete TA results')
    for item in output_list:
        enc_dtct_stmt = libpcpython.PcAPI_enc_id(
            PETRA_SID, config.PetraConfig.enc_cold_id, item['DTCT_STMT'], len(item['DTCT_STMT']))
        bind = (
            item['REC_KEY'],
            item['STMT_NO'],
            all_seq,
            item['SPK_DIV_CD'],
            'L',
            item['CHK_APP_ORD'],
            item['CHK_ITM_CD'],
            item['CUSL_CD'],
            item['CUSL_APP_ORD'],
            item['STMT_ST_TM'],
            item['STMT_ED_TM'],
            enc_dtct_stmt,
            item['DTCT_DTC_NO'],
            item['DTCT_KWD'],
            socket.gethostname(),
            datetime.fromtimestamp(time.time()),
            socket.gethostname(),
            datetime.fromtimestamp(time.time())
        )
        bind_list.append(bind)
        all_seq += 1
    log.info('\t  --> Insert TA results')
    util.insert_ta_result(db, bind_list)
    log.info('\t  --> Done insert TA results')


def extract_ta_output(log, hmd_output_list, cate_check_dict):
    """
    Extract data from TA output
    @param      log:                    Logger object
    @param      hmd_output_list:        HMD output list
    @param      cate_check_dict:        Category Dictionary
    @return:                            HMD output list
    """
    # Modifying HMD output
    log.info("\t  --> Modifying HMD output")
    # 탐지구간정보 추출
    output_list = list()
    for item, dtc_cat_result in hmd_output_list:
        """
        item = {
            'REC_KEY'           : 녹취KEY
            'STMT_NO'           : 문장번호
            'SPK_DIV_CD'        : 화자구분코드
            'STMT_ST_TM'        : 문장시작시간
            'STMT_ED_TM'        : 문장종료시간
            'STMT'              : 원문
            'nlp_sent'          : 형태소 분석 문장 
            'tmp_line_no'       : 전체 STT 임시 문장번호
        }
        """
        if dtc_cat_result:
            for dtc_cate, value_list in dtc_cat_result.items():
                dtc_cate_list = dtc_cate.split('!@#$')
                """
                dtc_cate_list = [
                    'CHK_ITM_CD',           평가항목코드
                    'CHK_APP_ORD',          항목적용차수
                    'CUSL_CD',              상담코드
                    'CUSL_APP_ORD',         상담적용차수
                    'DTCT_DTC_NO',          탐지사전번호
                    'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                    'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                    'DTCT_DTC_ED_NO',       탐지사전끝번호
                    'SNS_YN',               SMS전송여부
                ]
                """
                chk_itm_cd = dtc_cate_list[0]
                chk_app_ord = dtc_cate_list[1]
                cusl_cd = dtc_cate_list[2]
                cusl_app_ord = dtc_cate_list[3]
                dtct_dtc_no = dtc_cate_list[4]
                dtct_dtc_grp_no = dtc_cate_list[5]
                check_key = (
                    chk_itm_cd,
                    chk_app_ord,
                    cusl_cd,
                    cusl_app_ord,
                    dtct_dtc_no,
                    dtct_dtc_grp_no
                )
                # 탐지 된 키워드
                dtct_kwd_list = list()
                for value in value_list:
                    dtct_kwd = value[0]
                    if dtct_kwd not in dtct_kwd_list:
                        dtct_kwd_list.append(dtct_kwd)
                tmp_dtct_kwd = ','.join(dtct_kwd_list)
                if len(tmp_dtct_kwd.encode('utf-8')) > 300:
                    output_dtct_kwd = dtct_kwd_list[0]
                else:
                    output_dtct_kwd = tmp_dtct_kwd
                if check_key in cate_check_dict:
                    if item['STMT_NO'] in cate_check_dict[check_key]:
                        tmp_dict = dict()
                        tmp_dict['REC_KEY'] = item['REC_KEY']
                        tmp_dict['STMT_NO'] = item['STMT_NO']
                        tmp_dict['SPK_DIV_CD'] = item['SPK_DIV_CD']
                        tmp_dict['STMT_ST_TM'] = item['STMT_ST_TM']
                        tmp_dict['STMT_ED_TM'] = item['STMT_ED_TM']
                        tmp_dict['DTCT_STMT'] = item['STMT']
                        tmp_dict['CHK_ITM_CD'] = chk_itm_cd
                        tmp_dict['CHK_APP_ORD'] = chk_app_ord
                        tmp_dict['CUSL_CD'] = cusl_cd
                        tmp_dict['CUSL_APP_ORD'] = cusl_app_ord
                        tmp_dict['DTCT_DTC_NO'] = dtct_dtc_no
                        tmp_dict['DTCT_KWD'] = output_dtct_kwd
                        output_list.append(tmp_dict)
            log.info("\t  --> Done extract HMD results")
            return output_list


def execute_nlp(log, nlp_client, stt_result_list):
    """
    Execute NLP
    @param      log:                    Logger
    @param      nlp_client:             NLP client
    @param      stt_result_list:        STT results list
    @return:                            NLP output list
    """
    try:
        log.info('\t  --> NLP analyzing ...')
        tmp_line_no = 1
        nlp_output_list = list()
        for item in stt_result_list:
            """
                item = {
                    'REC_KEY'           : 녹취KEY
                    'STMT_NO'           : 문장번호
                    'SPK_DIV_CD'        : 화자구분코드
                    'STMT_ST_TM'        : 문장시작시간
                    'STMT_ED_TM'        : 문장종료시간
                    'STMT'              : 원문
                    'SILENCE'           : 묵음시간
                }
            """
            try:
                result = nlp_client.analyze(item['STMT'])
                sent, nlp_sent, morph_sent = result
                item['nlp_sent'] = nlp_sent
                item['tmp_line_no'] = tmp_line_no
            except Exception:
                log.error("  Can't analyze NLP")
                log.error("  File name --> {0}".format(item['REC_FILE_NM']))
                log.error("  Sentence --> {0}".format(item['STMT']))
                log.error(traceback.format_exc())
                continue
            nlp_output_list.append(item)
            tmp_line_no += 1
        log.info('\t  --> Done NLP')
        return nlp_output_list
    except Exception:
        raise Exception(traceback.format_exc())


def execute_hmd(log, hmd_client, hmd_model, stt_result_list):
    """
    Execute Common Forbidden word HMD
    @param      log:                    Logger
    @param      hmd_client:             HMD Client
    @param      hmd_model:              HMD model
    @param      stt_result_list:        STT result list
    @return:                            HMD output list and Category dictionary
    """
    log.info('\t  --> Load NLP client')
    nlp_client = nlp.NlpClient(config.STTConfig.nlp_engine)
    nlp_output_list = execute_nlp(log, nlp_client, stt_result_list)
    log.info("\t  --> HMD analyzing ...")
    hmd_output_list = list()
    for item in nlp_output_list:
        """
        item = {
            'REC_KEY'           : 녹취KEY
            'STMT_NO'           : 문장번호
            'SPK_DIV_CD'        : 화자구분코드
            'STMT_ST_TM'        : 문장시작시간
            'STMT_ED_TM'        : 문장종료시간
            'STMT'              : 원문
            'SILENCE'           : 묵음시간
            'nlp_sent'          : 형태소 분석 문장 
            'tmp_line_no'       : 전체 STT 임시 문장번호
        }
        """
        try:
            if item['SPK_DIV_CD'].upper() == 'C':
                continue
            dtc_cat_result = hmd_client.execute_hmd(item['STMT'], item['nlp_sent'], hmd_model)
        except Exception:
            log.error(traceback.format_exc())
            log.error("\t  --> Can't analyze HMD")
            log.error("\t  --> Sentence -> {0}".format(item['STMT']))
            continue
        hmd_output_list.append((item, dtc_cat_result))
    log.info('\t  --> Setup category dictionary')
    dtc_cate_dict = dict()
    cate_idx_dict = dict()
    for item, dtc_cat_result in hmd_output_list:
        """
        item = {
            'REC_KEY'           : 녹취KEY
            'STMT_NO'           : 문장번호
            'SPK_DIV_CD'        : 화자구분코드
            'STMT_ST_TM'        : 문장시작시간
            'STMT_ED_TM'        : 문장종료시간
            'STMT'              : 원문
            'nlp_sent'          : 형태소 분석 문장 
            'tmp_line_no'       : 전체 STT 임시 문장번호
        }
        """
        if dtc_cat_result:
            for dtc_cate, value_list in dtc_cat_result.items():
                dtc_cate_list = dtc_cate.split('!@#$')
                """
                dtc_cate_list = [
                    'CHK_ITM_CD',           평가항목코드
                    'CHK_APP_ORD',          항목적용차수
                    'CUSL_CD',              상담코드
                    'CUSL_APP_ORD',         상담적용차수
                    'DTCT_DTC_NO',          탐지사전번호
                    'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                    'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                    'DTCT_DTC_ED_NO',       탐지사전끝번호
                    'SNS_YN',               SMS전송여부
                ]
                """
                chk_itm_cd = dtc_cate_list[0]
                chk_app_ord = dtc_cate_list[1]
                cusl_cd = dtc_cate_list[2]
                cusl_app_ord = dtc_cate_list[3]
                dtct_dtc_no = dtc_cate_list[4]
                dtct_dtc_grp_no = dtc_cate_list[5]
                dtct_dtc_grp_in_no = dtc_cate_list[6]
                dtct_dtc_grp_ed_no = dtc_cate_list[7]
                cate_key = (
                    chk_itm_cd,
                    chk_app_ord,
                    cusl_cd,
                    cusl_app_ord,
                    dtct_dtc_no,
                    dtct_dtc_grp_no
                )
                if cate_key not in cate_idx_dict:
                    cate_idx_dict[cate_key] = (dtct_dtc_grp_ed_no, [dtct_dtc_grp_in_no])
                else:
                    cate_idx_dict[cate_key][1].append(dtct_dtc_grp_in_no)
                if cate_key in dtc_cate_dict:
                    if dtct_dtc_grp_in_no not in dtc_cate_dict[cate_key]:
                        dtc_cate_dict[cate_key][dtct_dtc_grp_in_no] = [item['STMT_NO']]
                    else:
                        dtc_cate_dict[cate_key][dtct_dtc_grp_in_no].append(item['STMT_NO'])
                else:
                    dtc_cate_dict[cate_key] = {dtct_dtc_grp_in_no: [item['STMT_NO']]}
    log.info('\t  --> Check category')
    cate_check_dict = dict()
    for cate_key, value in cate_idx_dict.items():
        flag = True
        end_idx = int(value[0])
        idx_list = value[1]
        for cnt in range(1, end_idx + 1):
            if str(cnt) in idx_list:
                continue
            else:
                flag = False
        if flag:
            output_list = make_target_line(end_idx, config.STTConfig.hmd_line_gap, dtc_cate_dict[cate_key])
            cate_check_dict[cate_key] = output_list
    log.info("\t  --> Done execute HMD")
    return hmd_output_list, cate_check_dict


def make_qa_hmd_model(log, db, job, hmd_client):
    """
    Make HMD Tree dictionary
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        PROC_PRI          처리우선순위
                                        REC_KEY           녹취KEY
                                        REC_ID            녹취ID
                                        REC_FILE_NM       녹취파일명
                                        STT_REC_FILE_NM   실시간STT녹취파일명
                                        REC_COMP_CD       녹취업체구분코드
                                        CHN_CLA_CD        채널구분코드
                                        REC_ST_DT         녹취시작일자
                                        REC_ST_TM         녹취시작시간
                                        REC_ED_TM         녹취종료시간
                                        REC_DU_TM         녹취통화시간
                                        BROF_CD           지점코드
                                        EMP_NO            사원번호
                                        STEREO_SPLIT_YN   스테레오분할여부
                                        REC_BIZ_TYPE      녹취업무유형
                                        REC_TRANS_TM      녹취전송일시
                                        REC_FILE_EXT      녹취파일확장자
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                        LIS_REC_FILE_NM   청취녹취파일명
                                        LIS_REC_LOC_PATH  청취녹취파일경로
                                    }
    @param      hmd_client:         HMD Client
    @return:                        HMD model
    """
    global DELETE_FILE_LIST
    model_name = '{0}_common_fwd'.format(job['REC_KEY'])
    hmd_model = False
    for _ in range(0, 3):
        try:
            # Make HMD model
            hmd_model_list = list()
            result = util.select_common_fwd_hmd(db)
            for item in result:
                if not item['SNS_YN'] == 'Y':
                    item['SNS_YN'] = 'N'
                category = [
                    str(item['CHK_ITM_CD']).strip(),
                    str(item['CHK_APP_ORD']).strip(),
                    str(item['CUSL_CD']).strip(),
                    str(item['CUSL_APP_ORD']).strip(),
                    str(item['DTCT_DTC_NO']).strip(),
                    str(item['DTCT_DTC_GRP_NO']).strip(),
                    str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                    str(item['DTCT_DTC_ED_NO']).strip(),
                    str(item['SNS_YN']).strip()
                ]
                if 'None' in category:
                    log.error("Category is Not NULL -> {0}".format(category))
                    continue
                hmd_model_list.append((category, item['DTCT_DTC_CON']))
            log.info("\t  --> Make HMD model ..")
            hmd_client.set_model(model_name, hmd_model_list)
            log.info("\t  --> Load HMD model ..")
            hmd_model = hmd_client.load_hmd_model("!@#$", model_name)
            break
        except Exception:
            log.error(traceback.format_exc())
            log.info("\t  --> Retry make HMD model ..")
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    DELETE_FILE_LIST.append(hmd_client.hmd_model_path(model_name))
    if not hmd_model:
        raise Exception("Can't make HMD model")
    return hmd_model


def execute_common_fwd_hmd(log, db, job, stt_result_list):
    """
    Execute Common Forbidden word HMD(공통금지어 PROD_CAT="L", PROD_CD ="ZZZREC", PLN_CD="0")
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        PROC_PRI          처리우선순위
                                        REC_KEY           녹취KEY
                                        REC_ID            녹취ID
                                        REC_FILE_NM       녹취파일명
                                        STT_REC_FILE_NM   실시간STT녹취파일명
                                        REC_COMP_CD       녹취업체구분코드
                                        CHN_CLA_CD        채널구분코드
                                        REC_ST_DT         녹취시작일자
                                        REC_ST_TM         녹취시작시간
                                        REC_ED_TM         녹취종료시간
                                        REC_DU_TM         녹취통화시간
                                        BROF_CD           지점코드
                                        EMP_NO            사원번호
                                        STEREO_SPLIT_YN   스테레오분할여부
                                        REC_BIZ_TYPE      녹취업무유형
                                        REC_TRANS_TM      녹취전송일시
                                        REC_FILE_EXT      녹취파일확장자
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                        LIS_REC_FILE_NM   청취녹취파일명
                                        LIS_REC_LOC_PATH  청취녹취파일경로
                                    }
    @param      stt_result_list:    STT results list
    @return:                        HMD output list
    """
    log.info('  7-1) Make HMD model')
    log.info("\t  --> Load HMD Client ..")
    hmd_client = hmd.HmdClient()
    hmd_model = make_qa_hmd_model(log, db, job, hmd_client)
    log.info('  7-2) Execute HMD')
    hmd_output_list, cate_check_dict = execute_hmd(log, hmd_client, hmd_model, stt_result_list)
    log.info('  7-3) Extract HMD Output')
    output_list = extract_ta_output(log, hmd_output_list, cate_check_dict)
    return output_list


def update_stt_status_and_info(log, db, job, max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_info):
    """
    Update STT status and information to QA_CAL_INFO_TB(콜정보테이블)
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        PROC_PRI          처리우선순위
                                        REC_KEY           녹취KEY
                                        REC_ID            녹취ID
                                        REC_FILE_NM       녹취파일명
                                        STT_REC_FILE_NM   실시간STT녹취파일명
                                        REC_COMP_CD       녹취업체구분코드
                                        CHN_CLA_CD        채널구분코드
                                        REC_ST_DT         녹취시작일자
                                        REC_ST_TM         녹취시작시간
                                        REC_ED_TM         녹취종료시간
                                        REC_DU_TM         녹취통화시간
                                        BROF_CD           지점코드
                                        EMP_NO            사원번호
                                        STEREO_SPLIT_YN   스테레오분할여부
                                        REC_BIZ_TYPE      녹취업무유형
                                        REC_TRANS_TM      녹취전송일시
                                        REC_FILE_EXT      녹취파일확장자
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                        LIS_REC_FILE_NM   청취녹취파일명
                                        LIS_REC_LOC_PATH  청취녹취파일경로
                                    }
    @param      max_silence_time:   Max Silence time
    @param      spch_sped_rx:       Speech speed rx
    @param      spch_sped_tx:       Speech speed tx
    @param      stt_result_info:    STT result information
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        log.info('  6-1) Update STT status and information')
        util.update_stt_status_and_info(
            db=db,
            prog_stat_cd='13',
            stt_st_tm=stt_result_info['stt_st_tm'],
            stt_com_tm=stt_result_info['stt_com_tm'],
            c_voic_sp=spch_sped_rx,
            a_voic_sp=spch_sped_tx,
            max_silence=max_silence_time,
            stt_proc_sp=stt_result_info['stt_proc_sp'],
            lis_rec_file_nm=job['LIS_REC_FILE_NM'],
            lis_rec_loc_path=job['LIS_REC_LOC_PATH'],
            updator_id=socket.gethostname(),
            updated_tm=datetime.fromtimestamp(time.time()),
            rec_key=job['REC_KEY'],
            rec_comp_cd=job['REC_COMP_CD'],
            rec_biz_type=job['REC_BIZ_TYPE'],
            rec_st_tm=job['REC_ST_TM'],
            rec_ed_tm=job['REC_ED_TM'],
            chn_cla_cd=job['CHN_CLA_CD'],
            record_duration=job['REC_DU_TM'],
            rec_id=job['REC_ID'],
            rec_file_nm=job['REC_FILE_NM'],
            rec_file_ext=job['REC_FILE_EXT']
        )
    except Exception:
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR06'
        raise Exception(traceback.format_exc())
    log.info('\t  --> Done updated STT status and information')


def insert_stt_result(log, db, job, stt_result_info):
    """
    Insert STT result
    @param      log:                    Logger object
    @param      db:                     DB object
    @param      job:                    job{
                                            PROC_PRI          처리우선순위
                                            REC_KEY           녹취KEY
                                            REC_ID            녹취ID
                                            REC_FILE_NM       녹취파일명
                                            STT_REC_FILE_NM   실시간STT녹취파일명
                                            REC_COMP_CD       녹취업체구분코드
                                            CHN_CLA_CD        채널구분코드
                                            REC_ST_DT         녹취시작일자
                                            REC_ST_TM         녹취시작시간
                                            REC_ED_TM         녹취종료시간
                                            REC_DU_TM         녹취통화시간
                                            BROF_CD           지점코드
                                            EMP_NO            사원번호
                                            STEREO_SPLIT_YN   스테레오분할여부
                                            REC_BIZ_TYPE      녹취업무유형
                                            REC_TRANS_TM      녹취전송일시
                                            REC_FILE_EXT      녹취파일확장자
                                            REC_ENC_TP        녹취파일인코딩
                                            CONV_LOC_PATH     녹취변완위치경로
                                            CONV_FILE_NM      녹취변환파일명
                                            LIS_REC_FILE_NM   청취녹취파일명
                                            LIS_REC_LOC_PATH  청취녹취파일경로
                                        }
    @param      stt_result_info:        STT result information
    @return:                            Max Silence Time, Speech speed, STT result
    """
    """
    stt_result_info = {
        'stt_st_dtm': stt_st,
        'stt_ed_dtm': stt_ed,
        'stt_pcs_spd': analyzing_speed,
        'stt_result': stt_result_list #[(speaker, seg.start, seg.end, spch_sped, masked_str), ...]
    }
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        line_num = 1
        bind_list = list()
        max_silence_time = 0.0
        stt_result_list = list()
        temp_end_seconds = 0.0
        speech_speed_info = dict()
        rec_key = job['REC_KEY']
        log.info('  5-1) Make upload data')
        for item in stt_result_info['stt_result']:
            speaker, start, end, spch_sped, sent = item
            silence_time = (start / 8000.0) - temp_end_seconds
            silence_time = round(silence_time, 2)
            if silence_time < 0:
                silence_time = 0.0
            if max_silence_time < silence_time:
                max_silence_time = silence_time
            sntc_sttm = extract_speech_time(start / 8000.0)
            sntc_endtm = extract_speech_time(end / 8000.0)
            if speaker in speech_speed_info:
                speech_speed_info[speaker][0] += 1
                speech_speed_info[speaker][1] += spch_sped
            else:
                speech_speed_info[speaker] = [1, spch_sped]
            enc_sent = libpcpython.PcAPI_enc_id(PETRA_SID, config.PetraConfig.enc_cold_id, sent, len(sent))
            bind = (
                rec_key,
                str(line_num),
                speaker,
                sntc_sttm,
                sntc_endtm,
                enc_sent,
                silence_time,
                datetime.strftime(datetime.now(), "%Y%m"),
                socket.gethostname(),
                datetime.fromtimestamp(time.time()),
                socket.gethostname(),
                datetime.fromtimestamp(time.time())
            )
            bind_list.append(bind)
            stt_item = {
                'REC_KEY': rec_key,
                'STMT_NO': str(line_num),
                'SPK_DIV_CD': speaker,
                'STMT_ST_TM': sntc_sttm,
                'STMT_ED_TM': sntc_endtm,
                'STMT': sent,
                'SILENCE': silence_time
            }
            stt_result_list.append(stt_item)
            line_num += 1
            temp_end_seconds = end / 8000.0
        log.info('\t  --> Check STT results')
        del_flag = util.select_stt_results_existed(db, rec_key)
        if del_flag:
            log.info('\t  --> Already existed. Delete STT results')
            util.delete_stt_results(db, rec_key)
            log.info('\t  --> Done delete STT results')
        log.info('\t  --> Insert STT results')
        util.insert_stt_result(db, bind_list)
        log.info('\t  --> Done insert STT results')
        spch_sped_rx = 0
        spch_sped_tx = 0
        for speaker, value in speech_speed_info.items():
            if speaker == 'A':
                spch_sped_tx = round(value[1] / value[0], 2)
            elif speaker == 'C':
                spch_sped_rx = round(value[1] / value[0], 2)
            else:
                spch_sped_tx = round(value[1] / value[0], 2)
                spch_sped_rx = round(value[1] / value[0], 2)
        return max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_list
    except Exception:
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR07'
        raise Exception(traceback.format_exc())


def load_replace_keyword(db):
    """
    Load replace keyword
    @param:         db:       DB Object
    @return:                  Replace keyword trie dictionary
    """
    keyword_processor = flashtext.KeywordProcessor(case_sensitive=True)
    target_list = util.select_subs_kwd(db)
    for target_dict in target_list:
        keyword_processor.add_keyword(
            target_dict['TAR_WD'].decode('utf-8'), target_dict['CHA_WD'].decode('utf-8'))
    return keyword_processor


def extract_speech_speed(start_time, end_time, sent):
    """
    Extract speech speed
    @param      start_time:     Start time(/ms)
    @param      end_time:       End time(/ms)
    @param      sent:           Sentence (encoding='utf-8')
    @return:                    Speech speed
    """
    sntc_len = len(sent.replace(' ', ''))
    during_time = (end_time / 8000.0) - (start_time / 8000.0)
    if during_time > 0:
        speech_speed = round(float(sntc_len) / during_time, 2)
    else:
        speech_speed = 0
    if speech_speed > 999:
        speech_speed = 0.0
    return speech_speed


def postprocessing_stt_result(log, db, stt_result_dict):
    """
    Postprocessing STT result(sorted, masking)
    @param          log:                    Logger object
    @param          db:                     DB object
    @param          stt_result_dict:        STT result dictionary
    @return:                                Modified STT result dictionary
    """
    stt_result_list = list()
    aes_cipher = cipher.AESCipher(config.AESConfig)
    sorted_stt_result_list = sorted(stt_result_dict.iteritems(), key=itemgetter(0), reverse=False)
    log.info('\t  --> Sorted ...')
    line_list = list()
    for key, item in sorted_stt_result_list:
        speaker, seg = item
        line_list.append(seg.txt.encode('utf-8').strip())
    masking_result = list()
    if config.STTConfig.masking:
        log.info('\t  --> Masking ...')
        masking_result, index_output_dict = masking.masking(line_list, 'utf-8')
    if config.STTConfig.encrypt:
        log.info('\t  --> Encrypt ...')
    log.info('\t  --> Extract speech speed')
    line_num = 0
    change = False
    keyword_processor = False
    if config.STTConfig.replace:
        log.info('\t  --> Replace ...')
        keyword_processor = load_replace_keyword(db)
    if config.STTConfig.arabic:
        log.info('\t  --> Arabic ...')
        change = change_arabic.ChangeArabic()
    for key, item in sorted_stt_result_list:
        line_num += 1
        speaker, seg = item
        stt_txt = seg.txt.encode('utf-8').strip()
        spch_sped = extract_speech_speed(seg.start, seg.end, stt_txt)
        if config.STTConfig.replace and keyword_processor:
            stt_txt = keyword_processor.replace_keywords(stt_txt.decode('utf-8')).encode('utf-8')
            stt_txt = keyword_processor.replace_keywords(stt_txt.decode('utf-8')).encode('utf-8')
        masked_str = masking_result[line_num - 1] if line_num - 1 in masking_result else stt_txt
        if config.STTConfig.arabic and change:
            masked_str = change.change_arabic_text(masked_str)
        if config.STTConfig.encrypt:
            output_str = aes_cipher.encrypt(masked_str)
        else:
            output_str = masked_str
        if config.STTConfig.masking:
            stt_result_list.append((speaker, seg.start, seg.end, spch_sped, masked_str.encode('euc-kr').strip()))
        else:
            stt_result_list.append((speaker, seg.start, seg.end, spch_sped, output_str))
    return stt_result_list


def make_stt_result_dict(speak, results, stt_result_dict):
    """
    Make STT result dictionary
    @param      speak:                  Speaker('M', 'A', 'C')
    @param      results:                STT result
    @param      stt_result_dict:        STT result dictionary
    @return:                            STT result dictionary
    """
    for seg in results:
        flag = True
        key = seg.start
        while flag:
            if key in stt_result_dict:
                key += 0.1
            else:
                stt_result_dict[key] = (speak, seg)
                flag = False
    return stt_result_dict


def stt_recognize(log, record_duration, stt_client_1, stt_client_2, pcm_file_path):
    """
    STT recognize
    @param      log:                Logger object
    @param      record_duration:    Record duration
    @param      stt_client_1:       GPU0 STT client
    @param      stt_client_2:       GPU1 STT client
    @param      pcm_file_path:      PCM file path
    @return:                        STT results
    """
    random_cnt = random.randrange(1, 3)
    results = list()
    for cnt in range(1, 4):
        if random_cnt == 1:
            results = stt_client_1.stream_recognize(pcm_file_path)
        else:
            results = stt_client_2.stream_recognize(pcm_file_path)
        if not results:
            if record_duration <= 60:
                break
            log.info("\t  --> {0} STT results is empty. {1}st Retry ...".format(os.path.basename(pcm_file_path), cnt))
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    if not results:
        log.info("\t  --> {0} STT results is empty. Please, check wav file".format(os.path.basename(pcm_file_path)))
        return list()
    return results


def execute_cnn_stt(log, db, job, pcm_info_dict):
    """
    Execute CNN STT
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                job{
                                        PROC_PRI          처리우선순위
                                        REC_KEY           녹취KEY
                                        REC_ID            녹취ID
                                        REC_FILE_NM       녹취파일명
                                        STT_REC_FILE_NM   실시간STT녹취파일명
                                        REC_COMP_CD       녹취업체구분코드
                                        CHN_CLA_CD        채널구분코드
                                        REC_ST_DT         녹취시작일자
                                        REC_ST_TM         녹취시작시간
                                        REC_ED_TM         녹취종료시간
                                        REC_DU_TM         녹취통화시간
                                        BROF_CD           지점코드
                                        EMP_NO            사원번호
                                        STEREO_SPLIT_YN   스테레오분할여부
                                        REC_BIZ_TYPE      녹취업무유형
                                        REC_TRANS_TM      녹취전송일시
                                        REC_FILE_EXT      녹취파일확장자
                                        REC_ENC_TP        녹취파일인코딩
                                        CONV_LOC_PATH     녹취변완위치경로
                                        CONV_FILE_NM      녹취변환파일명
                                        LIS_REC_FILE_NM   청취녹취파일명
                                        LIS_REC_LOC_PATH  청취녹취파일경로
                                    }
    @param      pcm_info_dict:      PCM file information
    @return:                        STT results information
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    stt_comp_flag = False
    stt_result_dict = dict()
    stt_st = datetime.fromtimestamp(time.time())
    stt_ed = ''
    analyzing_speed = ''
    for _ in range(0, 3):
        try:
            log.info('  4-1) Load CNN STT client')
            stt_client_1 = cnn.W2lClient(remote=config.STTConfig.cnn_client1_remote)
            stt_client_2 = cnn.W2lClient(remote=config.STTConfig.cnn_client2_remote)
            total_required_time = float()
            log.info('  4-2) Execute CNN STT analyzing ...')
            if job['CHN_CLA_CD'] == 'M':
                pcm_file_path = pcm_info_dict['wav']
                results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, pcm_file_path)
                stt_result_dict = make_stt_result_dict('M', results, stt_result_dict)
            else:
                rx_pcm_file_path = pcm_info_dict['rx']
                tx_pcm_file_path = pcm_info_dict['tx']
                rx_results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, rx_pcm_file_path)
                tx_results = stt_recognize(log, job['REC_DU_TM'], stt_client_1, stt_client_2, tx_pcm_file_path)
                stt_result_dict = make_stt_result_dict('C', rx_results, stt_result_dict)
                stt_result_dict = make_stt_result_dict('A', tx_results, stt_result_dict)
            stt_ed = datetime.fromtimestamp(time.time())
            required_time = elapsed_time(stt_st)
            total_required_time += required_time.total_seconds()
            log.info('\t  --> Done STT, The time required = {0}'.format(required_time))
            analyzing_speed = round(float(job['REC_DU_TM']) / total_required_time, 1)
            log.info('\t  --> STT analyzing speed is {0} [Duration= {1}(/s)]'.format(
                analyzing_speed, job['REC_DU_TM']))
            stt_comp_flag = True
            break
        except Exception:
            log.error(traceback.format_exc())
            log.info('  4-0) Retry CNN STT analyzing ...')
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    if not stt_comp_flag:
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR05'
        raise Exception("Can't STT recognize {0}".format(job['REC_FILE_NM']))
    try:
        log.info('  4-3) Postprocessing STT result(sorted, masking)')
        stt_result_list = postprocessing_stt_result(log, db, stt_result_dict)
        stt_result_info = {
            'stt_st_tm': stt_st,
            'stt_com_tm': stt_ed,
            'stt_proc_sp': analyzing_speed,
            'stt_result': stt_result_list
        }
        return stt_result_info
    except Exception:
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR06'
        log.error(traceback.format_exc())
        raise Exception("STT postprocessing error {0}".format(job['REC_FILE_NM']))


def separation_wav_file(log, target_file_path):
    """
    Separation wav file
    @param      log:                    Logger object
    @param      target_file_path:       Target file path
    @return:                            rx, tx file path
    """
    log.info('\t  --> Separation wav file')
    wav_dir_path = os.path.dirname(target_file_path)
    wav_file_name = os.path.basename(target_file_path)
    rx_file_name = wav_file_name[:-4] + '_rx.wav'
    tx_file_name = wav_file_name[:-4] + '_tx.wav'
    rx_file_path = os.path.join(wav_dir_path, rx_file_name)
    tx_file_path = os.path.join(wav_dir_path, tx_file_name)
    # IF rx.wav or tx.wav file is already existed remove file.
    try:
        if os.path.exists(rx_file_path):
            os.remove(rx_file_path)
        if os.path.exists(tx_file_path):
            os.remove(tx_file_path)
    except Exception:
        log.error("\t  --> Fail delete file -> {0}".format(traceback.format_exc()))
    cmd = 'ffmpeg -i {0} -filter_complex "[0:0]pan=1c|c0=c0[left];[0:0]pan=1c|c0=c1[right]" '.format(target_file_path)
    #cmd += '-map "[left]" {0} -map "[right]" {1}'.format(rx_file_path, tx_file_path)
    # 흥국생명 실시간STT에서 MP3 파일의 left, right 를 반대로 생성
    cmd += '-map "[left]" {0} -map "[right]" {1}'.format(tx_file_path, rx_file_path)
    std_out, std_err = sub_process(cmd)
    if len(std_err) > 0:
        log.debug(std_err)
    log.info("\t  --> Success separation wav file")
    return rx_file_name, tx_file_name


def make_pcm_file(log, job):
    """
    Make pcm file list
    @param      log:            Logger object
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                    REC_BIZ_TYPE      녹취업무유형
                                    REC_TRANS_TM      녹취전송일시
                                    REC_FILE_EXT      녹취파일확장자
                                    REC_ENC_TP        녹취파일인코딩
                                    CONV_LOC_PATH     녹취변완위치경로
                                    CONV_FILE_NM      녹취변환파일명
                                    LIS_REC_FILE_NM   청취녹취파일명
                                    LIS_REC_LOC_PATH  청취녹취파일경로
                                }
    @return                     PCM information {'wav': pcm_file_path, 'rx': '', 'tx': ''}
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    global DELETE_FILE_LIST
    log_str = '''  3-1) Make WAV to PCM law file'''
    log.info(log_str)
    log.info('\t  --> Convert wav to pcm')
    conv_loc_path = job['CONV_LOC_PATH']
    conv_file_nm = job['CONV_FILE_NM']
    chn_cla_cd = job['CHN_CLA_CD']
    wav_file_path = os.path.join(conv_loc_path, conv_file_nm)
    # 녹취 파일 없음
    if not os.path.exists(wav_file_path):
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR04' # 녹취파일미탐지
        raise Exception("Not existed wav file [{0}]".format(wav_file_path))
    try:
        if job['REC_ENC_TP'] in ('PCM', 'GSM'):
            # 스테레오 파일일 경우
            if chn_cla_cd == 'S':
                rx_file_name, tx_file_name = separation_wav_file(log, wav_file_path)
                rx_file_path = os.path.join(conv_loc_path, rx_file_name)
                rx_pcm_file_path = os.path.join(conv_loc_path, rx_file_name[:-4] + '.pcm')
                tx_file_path = os.path.join(conv_loc_path, tx_file_name)
                tx_pcm_file_path = os.path.join(conv_loc_path, tx_file_name[:-4] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(rx_file_path, rx_pcm_file_path)
                log.info('\t  --> RX CONV_FILE_NM = {0}'.format(rx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(tx_file_path, tx_pcm_file_path)
                log.info('\t  --> TX CONV_FILE_NM = {0}'.format(tx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': '', 'rx': rx_pcm_file_path, 'tx': tx_pcm_file_path}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(rx_file_path)
                DELETE_FILE_LIST.append(rx_pcm_file_path)
                DELETE_FILE_LIST.append(tx_file_path)
                DELETE_FILE_LIST.append(tx_pcm_file_path)
            # 모노 파일일 경우
            else:
                pcm_file_path = os.path.join(conv_loc_path, os.path.splitext(conv_file_nm)[0] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(wav_file_path, pcm_file_path)
                log.info('\t  --> CONV_FILE_NM = {0}'.format(conv_file_nm))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': pcm_file_path, 'rx': '', 'tx': ''}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(pcm_file_path)
        elif  job['REC_ENC_TP'] in ('G723', ):
            log.info('\t  --> g723 wav file to gsm')
            # 스테레오 파일일 경우
            if chn_cla_cd == 'S':
                gsm_wav_path = os.path.join(conv_loc_path, 'gsm_' + conv_file_nm)
                cmd = "ffmpeg -y -hide_banner -i {0} -c:a gsm_ms -ar 8000 -b:a 13K {1}".format(
                    wav_file_path, gsm_wav_path)
                std_out, std_err = sub_process(cmd)
                if len(std_err) > 0:
                    log.info(std_err)
                rx_file_name, tx_file_name = separation_wav_file(log, gsm_wav_path)
                rx_file_path = os.path.join(conv_loc_path, rx_file_name)
                rx_pcm_file_path = os.path.join(conv_loc_path, rx_file_name[:-4] + '.pcm')
                tx_file_path = os.path.join(conv_loc_path, tx_file_name)
                tx_pcm_file_path = os.path.join(conv_loc_path, tx_file_name[:-4] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(rx_file_path, rx_pcm_file_path)
                log.info('\t  --> RX CONV_FILE_NM = {0}'.format(rx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(tx_file_path, tx_pcm_file_path)
                log.info('\t  --> TX CONV_FILE_NM = {0}'.format(tx_file_name))
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.debug(std_out)
                pcm_info_dict = {'wav': '', 'rx': rx_pcm_file_path, 'tx': tx_pcm_file_path}
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(gsm_wav_path)
                DELETE_FILE_LIST.append(rx_file_path)
                DELETE_FILE_LIST.append(rx_pcm_file_path)
                DELETE_FILE_LIST.append(tx_file_path)
                DELETE_FILE_LIST.append(tx_pcm_file_path)
            # 모노 파일일 경우
            else:
                gsm_wav_path = os.path.join(conv_loc_path, 'gsm_' + conv_file_nm)
                cmd = "ffmpeg -y -hide_banner -i {0} -c:a gsm_ms -ar 8000 -b:a 13K {1}".format(
                    wav_file_path, gsm_wav_path)
                std_out, std_err = sub_process(cmd)
                if len(std_err) > 0:
                    log.info(std_err)
                log.info('\t  --> Convert wav file to pcm')
                pcm_file_path = os.path.join(conv_loc_path, os.path.splitext(conv_file_nm)[0] + '.pcm')
                cmd = "sox -t wav {0} -r 8000 -b 16 -t raw {1}".format(gsm_wav_path, pcm_file_path)
                std_out, std_err = sub_process(cmd)
                if len(std_out) > 0:
                    log.info(std_out)
                pcm_info_dict = {'wav': pcm_file_path, 'rx': '', 'tx': ''}
                DELETE_FILE_LIST.append(gsm_wav_path)
                DELETE_FILE_LIST.append(wav_file_path)
                DELETE_FILE_LIST.append(pcm_file_path)
        else:
            PROG_STAT_CD = '19'
            PROG_STAT_DTL_CD = 'STTERR02'  # 지원하지않는코덱
            raise Exception("\t  --> Not support wav encoding. [REC_ENC_TP = {0}]".format(job['REC_ENC_TP']))
        log.info("\t  --> Success convert wav to pcm")
        return pcm_info_dict
    except Exception:
        PROG_STAT_DTL_CD = 'STTERR03' # PCM파일생성실패
        raise Exception(traceback.format_exc())


def convert_rec(log, job, target_file_path):
    """
    Convert record GSM to PCM wav
    @param      log:                    Logger Object
    @param      job:                    job{
                                            PROC_PRI          처리우선순위
                                            REC_KEY           녹취KEY
                                            REC_ID            녹취ID
                                            REC_FILE_NM       녹취파일명
                                            STT_REC_FILE_NM   실시간STT녹취파일명
                                            REC_COMP_CD       녹취업체구분코드
                                            CHN_CLA_CD        채널구분코드
                                            REC_ST_DT         녹취시작일자
                                            REC_ST_TM         녹취시작시간
                                            REC_ED_TM         녹취종료시간
                                            REC_DU_TM         녹취통화시간
                                            BROF_CD           지점코드
                                            EMP_NO            사원번호
                                            STEREO_SPLIT_YN   스테레오분할여부
                                            REC_BIZ_TYPE      녹취업무유형
                                            REC_TRANS_TM      녹취전송일시
                                            REC_FILE_EXT      녹취파일확장자
                                            REC_ENC_TP        녹취파일인코딩
                                            CONV_LOC_PATH     녹취변완위치경로
                                            CONV_FILE_NM      녹취변환파일명
                                            LIS_REC_FILE_NM   청취녹취파일명
                                            LIS_REC_LOC_PATH  청취녹취파일경로
                                        }
    @param      target_file_path:       Target record file path
    @return:                            LIS_REC_FILE_NM, LIS_REC_LOC_PATH
    """
    global DELETE_FILE_LIST
    year, month, day = job['REC_ST_DT'].strip().split('-')
    convert_dir_path = os.path.join(config.STTConfig.lis_tmp_dir_path, "{0}/{1}{2}".format(year, month, day))
    remote_dir_path = os.path.join(config.STTConfig.lis_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
    convert_file_path = os.path.join(convert_dir_path, job['REC_FILE_NM'].replace(" ", ""))
    DELETE_FILE_LIST.append(convert_file_path)
    remote_file_path = os.path.join(remote_dir_path, job['REC_FILE_NM'].replace(" ", ""))
    if not os.path.exists(convert_dir_path):
        os.makedirs(convert_dir_path)
    if job['REC_ENC_TP'] == 'G723':
        cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 1 {1}'.format(target_file_path, convert_file_path)
    else:
        cmd = 'sox -r 8000 -c 1 {0} -r 8000 -c 1 -e signed-integer {1}'.format(target_file_path, convert_file_path)
    std_out, std_err = sub_process(cmd)
    if len(std_out) > 0:
        log.info("\t  --> {0}".format(std_out.strip()))
    if len(std_err) > 0:
        log.info("\t  --> {0}".format(std_err.strip()))
    log.info("\t  --> Success convert wav file")
    flag = False
    log.info("\t  --> SFTP upload convert wav file")
    ssh = ''
    sftp = ''
    for svr_host in config.STTConfig.lis_rec_svr_list:
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.load_host_keys(os.path.expanduser(os.path.join('~', '.ssh', 'known_hosts')))
            ssh.connect(svr_host, username='minds', allow_agent=True, look_for_keys=True, timeout=10)
            sftp = ssh.open_sftp()
            try:
                ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                sftp.stat(remote_dir_path)
            except IOError:
                ssh.exec_command('mkdir -p {0}'.format(remote_dir_path))
                pass
            time.sleep(3)
            sftp.put(convert_file_path, remote_file_path)
            ssh.close()
            sftp.close()
            flag = True
            log.info("\t  --> Success SFTP upload convert wav file")
            break
        except Exception:
            log.error(traceback.format_exc())
            if ssh:
                ssh.close()
            if sftp:
                sftp.close()
            continue
    if not flag:
        return None, None
    return job['REC_FILE_NM'].replace(" ", ""), remote_dir_path


def extract_record_info_and_download_real(log, db, job):
    """
    Extract Real STT record information and download
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                }
    @return                     job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                    REC_BIZ_TYPE      녹취업무유형
                                    REC_TRANS_TM      녹취전송일시
                                    REC_FILE_EXT      녹취파일확장자
                                    REC_ENC_TP        녹취파일인코딩
                                    CONV_LOC_PATH     녹취변완위치경로
                                    CONV_FILE_NM      녹취변환파일명
                                    LIS_REC_FILE_NM   청취녹취파일명
                                    LIS_REC_LOC_PATH  청취녹취파일경로
                                }
    """
    try:
        global PROG_STAT_CD
        global DELETE_FILE_LIST
        global PROG_STAT_DTL_CD
        log.info('  2-1) Select Record information from CallTechSolution(통합녹취서버)')
        if job['REC_FILE_NM']:
            data = {
                "cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
                "data": [{'recdate': job['REC_ST_DT'], 'branchcd': job['BROF_CD'], 'recfilenm': job['REC_FILE_NM']}]
            }
            log.info("\t  --> Execute Get Record information from CallTechSolution(통합녹취서버)")
            result = requests.post(config.CTSConfig.stt_url, json=data)
            json_result = json.loads(result.text)
            if len(json_result['data']) < 1:
                raise Exception("\t  --> Can't get Record information from CallTechSolution(통합녹취서버)")
            log.info("\t  --> Response = {0}".format(json_result))
            item = json_result['data'][0]
            if not item['ftpipvoice']:
                raise Exception("\t  --> Can't get Record information from CallTechSolution(통합녹취서버)")
            log.info("\t  --> Success, Get Record information from CallTechSolution(통합녹취서버)")
            rec_st_tm = item['recstartdate']
            rec_ed_tm = item['recenddate']
            rec_comp_cd = '100'
            rec_biz_type = item['iocf']
        else:
            rec_st_tm = job['REC_ST_TM']
            rec_ed_tm = job['REC_ED_TM']
            rec_comp_cd = '100'
            rec_biz_type = 'O'
        log.info('  2-2) SFTP STT AP1,2(10.10.107.81, 10.10.107.82) download Record file')
        get_st_tm = datetime.now()
        payload = {
            'rec_st_dt': job['REC_ST_DT'],
            'srv_host': socket.gethostbyname(socket.gethostname()),
            'stt_rec_file_nm': job['STT_REC_FILE_NM']
        }
        log.info("\t  --> Execute Get Record information from STT AP1,2(10.10.107.81, 10.10.107.82)")
        log.info("\t  --> Execute Download ".format(job['STT_REC_FILE_NM']))
        random_cnt = random.randrange(0, 2)
        try:
            result = requests.post(config.STTConfig.tran_rec_svr_list[random_cnt], json=payload)
            local_dir_path = json.loads(result.text)
            if not local_dir_path:
                log.error("\t  --> Can't Download Record from STT AP1,2(10.10.107.81, 10.10.107.82)")
                return False
        except Exception:
            result = requests.post(config.STTConfig.tran_rec_svr_list[random_cnt - 1], json=payload)
            local_dir_path = json.loads(result.text)
            if not local_dir_path:
                log.error("\t  --> Can't Download Record from STT AP1,2(10.10.107.81, 10.10.107.82)")
                return False
        log.info("\t  --> Response = {0}".format(local_dir_path))
        local_file_path = os.path.join(local_dir_path, job['STT_REC_FILE_NM'])
        wav_file_nm = job['STT_REC_FILE_NM'].replace(".mp3", ".wav")
        wav_file_path = os.path.join(local_dir_path, wav_file_nm)
        DELETE_FILE_LIST.append(local_file_path)
        log.info("\t  --> Success, Download Record file")
        log.info('  2-3) Convert Record file [MP3 --> PCM WAV]')
        cmd = 'ffmpeg -y -i {0} -vn -ar 8000 -ac 2 -b:a 128k {1}'.format(local_file_path, wav_file_path)
        std_out, std_err = sub_process(cmd)
        if len(std_out) > 0:
            log.debug(std_out)
        if len(std_err) > 0:
            log.debug(std_err)
        log.info("\t  --> Success, Convert Record file")
        log.info('  2-4) Extract Record file Encoding, Channels, Duration')
        encoding = 'PCM'
        channel = 'S'
        record_duration = job['REC_DU_TM'] if job['REC_DU_TM'] else 0
        try:
            cmd = 'ffmpeg -i {0}'.format(os.path.join(local_dir_path, wav_file_nm))
            std_out, std_err = sub_process(cmd)
            log.info("\t  -->\n{0}".format(std_err.strip()))
            # Encoding
            encoding = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 30]
            if 'g723' in encoding:
                encoding = 'G723'
            elif 'gsm' in encoding:
                encoding = 'GSM'
            elif 'pcm' in encoding:
                encoding = 'PCM'
            else:
                encoding = encoding.replace("Audio:", "").strip().split()[0]
            log.info("\t  --> Encoding = {0}".format(encoding))
            # Channels
            info_str = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 100]
            if 'mono' in info_str:
                channel = 'M'
            elif '1 channel' in info_str:
                channel = 'M'
            elif 'stereo' in info_str:
                channel = 'S'
            elif '2 channel' in info_str:
                channel = 'S'
            else:
                channel = 'M'
            cha_str = channel + ' (스테레오)' if channel == 'S' else channel + ' (모노)'
            log.info("\t  --> Extract channels = {0}".format(cha_str))
            if channel != job['CHN_CLA_CD']:
                log.info("\t  --> Record information is wrong {0} -> {1}".format(job['CHN_CLA_CD'], channel))
                job['CHN_CLA_CD'] = channel
            # Duration
            du_st_idx = std_err.find('Duration:')
            duration = std_err[du_st_idx:du_st_idx + 30].split(",")[0].replace("Duration:", "").strip()
            tmp_list = duration.split(":")
            if len(tmp_list) == 3:
                duration_int = int(tmp_list[0]) * 3600
                duration_int += int(tmp_list[1]) * 60
                duration_int += int(float(tmp_list[2]))
                record_duration = duration_int
                log.info("\t  --> Extract duration = {0}(/s)".format(record_duration))
                try:
                    log.info("\t  --> Extract Real STT Dial time")
                    dial_time = util.select_stt_fir_line(db, job['REC_KEY'])
                    if dial_time:
                        tmp_list = dial_time.split(":")
                        dial_int = int(tmp_list[0]) * 3600
                        dial_int += int(tmp_list[1]) * 60
                        dial_int += int(float(tmp_list[2]))
                        log.info("\t  --> Extract Real STT Dial time = {0}(/s)".format(dial_int))
                        record_duration += dial_int
                    else:
                        log.info("\t  --> No Real STT reuslt")
                        log.info("\t  --> Extract Real STT Dial time = 0(/s)")
                except Exception:
                    log.error(traceback.format_exc())
                    pass
            if record_duration >= 99999:
                if encoding == 'G723':
                    try:
                        file_size = os.path.getsize(local_file_path)
                        record_duration = int(file_size) / 800
                        log.info("\t  --> Duration is too long change to {0}(/s)".format(record_duration))
                    except Exception:
                        log.error(traceback.format_exc())
                        record_duration = 0
                else:
                    log.info("\t  --> Duration is too long change to 0(/s)")
                    record_duration = 0
        except Exception:
            log.error(traceback.format_exc())
            pass
        #year, month, day = job['REC_ST_DT'].strip().split('-')
        #lis_rec_loc_path = os.path.join(config.STTConfig.lis_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
        job.update(
            {
                "REC_ST_TM": rec_st_tm,
                "REC_ED_TM": rec_ed_tm,
                "REC_DU_TM": record_duration,
                "REC_COMP_CD": rec_comp_cd,
                "REC_BIZ_TYPE": rec_biz_type,
                "REC_TRANS_TM": get_st_tm,
                "REC_FILE_EXT": 'mp3',
                "REC_ENC_TP": encoding,
                "CHN_CLA_CD": channel,
                "CONV_LOC_PATH": local_dir_path,
                "CONV_FILE_NM": wav_file_nm,
                "LIS_REC_FILE_NM": '',
                "LIS_REC_LOC_PATH": '',
                #"LIS_REC_FILE_NM": wav_file_nm,
                #"LIS_REC_LOC_PATH": lis_rec_loc_path,
            }
        )
        return job
    except Exception:
        PROG_STAT_CD = '18'
        PROG_STAT_DTL_CD = 'STTERR00' #녹취다운오류
        raise Exception(traceback.format_exc())


def extract_record_info_and_download(log, job):
    """
    Extract record information and download from CallTechSolution(통합녹취서버)
    @param      log:            Logger object
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                }
    @return                     job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                    REC_BIZ_TYPE      녹취업무유형
                                    REC_TRANS_TM      녹취전송일시
                                    REC_FILE_EXT      녹취파일확장자
                                    REC_ENC_TP        녹취파일인코딩
                                    CONV_LOC_PATH     녹취변완위치경로
                                    CONV_FILE_NM      녹취변환파일명
                                    LIS_REC_FILE_NM   청취녹취파일명
                                    LIS_REC_LOC_PATH  청취녹취파일경로
                                }
    """
    try:
        global PROG_STAT_CD
        global DELETE_FILE_LIST
        global PROG_STAT_DTL_CD
        aes_cipher = cipher.AESCipher(config.AESConfig)
        log.info('  2-1) Select Record information from CallTechSolution(통합녹취서버)')
        data = {
            "cmd": "06", "id": "", "med": "", "def": "", "dec": "", "rtm": "", "rvl": "", "datatype": "JSON",
            "data": [{'recdate': job['REC_ST_DT'], 'branchcd': job['BROF_CD'], 'recfilenm': job['REC_FILE_NM']}]
        }
        log.info("\t  --> Execute Get Record information from CallTechSolution(통합녹취서버)")
        result = requests.post(config.CTSConfig.stt_url, json=data)
        json_result = json.loads(result.text)
        if len(json_result['data']) < 1:
            raise Exception("\t  --> Can't get Record information from CallTechSolution(통합녹취서버)")
        log.info("\t  --> Response = {0}".format(json_result))
        item = json_result['data'][0]
        if not item['ftpipvoice']:
            raise Exception("\t  --> Can't get Record information from CallTechSolution(통합녹취서버)")
        log.info("\t  --> Success, Get Record information from CallTechSolution(통합녹취서버)")
        log.info('  2-2) SFTP CallTechSolution(통합녹취서버) download Record file')
        log.info("\t  --> Execute connecting CallTechSolution(통합녹취서버)")
        transport = paramiko.Transport(item['ftpipvoice'], item['ftppot'])
        transport.connect(username=item['ftpacc'], password=aes_cipher.decrypt_hk(item['ftppwd']))
        sftp = paramiko.SFTPClient.from_transport(transport)
        log.info("\t  --> Success, connect CallTechSolution(통합녹취서버)")
        source_file_path = os.path.join(item['recfilepath'].replace('\\', '/'), job['REC_FILE_NM'])
        year, month, day = job['REC_ST_DT'].strip().split('-')
        local_dir_path = os.path.join(config.STTConfig.get_rec_dir_path, "{0}/{1}{2}".format(year, month, day))
        local_file_path = os.path.join(local_dir_path, job['REC_FILE_NM'].replace(" ", ""))
        if not os.path.exists(local_dir_path):
            try:
                os.makedirs(local_dir_path)
            except OSError:
                pass
            except Exception:
                log.error(traceback.format_exc())
                raise Exception("\t  --> Can't make directory[{0}]".format(local_dir_path))
        get_st_tm = datetime.now()
        log.info("\t  --> Execute Download {0} --> {1}".format(source_file_path, local_file_path))
        sftp.get(source_file_path, local_file_path)
        DELETE_FILE_LIST.append(local_file_path)
        log.info("\t  --> Success, Download Record file")
        sftp.close()
        transport.close()
        log.info('  2-3) Extract Record file Encoding, Channels, Duration')
        encoding = 'GSM'
        channel = 'M'
        record_duration = item['tmptotss'] if item['tmptotss'] else 0
        try:
            cmd = 'ffmpeg -i {0}'.format(
                os.path.join(local_dir_path, job['REC_FILE_NM'].replace(" ", "").replace('(', '\(').replace(')', '\)')))
            std_out, std_err = sub_process(cmd)
            log.info("\t  -->\n{0}".format(std_err.strip()))
            # Encoding
            encoding = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 30]
            if 'g723' in encoding:
                encoding = 'G723'
            elif 'gsm' in encoding:
                encoding = 'GSM'
            elif 'pcm' in encoding:
                encoding = 'PCM'
            else:
                encoding = encoding.replace("Audio:", "").strip().split()[0]
            log.info("\t  --> Encoding = {0}".format(encoding))
            # Channels
            info_str = std_err[std_err.find('Audio:'):std_err.find('Audio:') + 100]
            if 'mono' in info_str:
                channel = 'M'
            elif '1 channel' in info_str:
                channel = 'M'
            elif 'stereo' in info_str:
                channel = 'S'
            elif '2 channel' in info_str:
                channel = 'S'
            else:
                channel = 'M'
            cha_str = channel + ' (스테레오)' if channel == 'S' else channel + ' (모노)'
            log.info("\t  --> Extract channels = {0}".format(cha_str))
            if channel != job['CHN_CLA_CD']:
                log.info("\t  --> Record information is wrong {0} -> {1}".format(job['CHN_CLA_CD'], channel))
                job['CHN_CLA_CD'] = channel
            # Duration
            du_st_idx = std_err.find('Duration:')
            duration = std_err[du_st_idx:du_st_idx + 30].split(",")[0].replace("Duration:", "").strip()
            tmp_list = duration.split(":")
            if len(tmp_list) == 3:
                duration_int = int(tmp_list[0]) * 3600
                duration_int += int(tmp_list[1]) * 60
                duration_int += int(float(tmp_list[2]))
                record_duration = duration_int
                log.info("\t  --> Extract duration = {0}(/s)".format(record_duration))
            if record_duration >= 99999:
                if encoding == 'G723':
                    try:
                        file_size = os.path.getsize(local_file_path)
                        record_duration = int(file_size) / 800
                        log.info("\t  --> Duration is too long change to {0}(/s)".format(record_duration))
                    except Exception:
                        log.error(traceback.format_exc())
                        record_duration = 0
                else:
                    log.info("\t  --> Duration is too long change to 0(/s)")
                    record_duration = 0
        except Exception:
            log.error(traceback.format_exc())
            pass
        job.update(
            {
                "REC_ST_TM": item['recstartdate'],
                "REC_ED_TM": item['recenddate'],
                "REC_DU_TM": record_duration,
                "REC_COMP_CD": item['recroute'],
                "REC_BIZ_TYPE": item['iocf'],
                "REC_TRANS_TM": get_st_tm,
                "REC_FILE_EXT": os.path.splitext(job['REC_FILE_NM'])[1].replace('.', ''),
                "REC_ENC_TP": encoding,
                "CHN_CLA_CD": channel,
                "CONV_LOC_PATH": local_dir_path,
                "CONV_FILE_NM": job['REC_FILE_NM'].replace(" ", ""),
                "LIS_REC_FILE_NM": '',
                "LIS_REC_LOC_PATH": '',
            }
        )
        #log.info('  2-4) Convert Record file Encoding GSM to PCM wav')
        #try:
        #    # Converting
        #    lis_rec_file_nm, lis_rec_loc_path = convert_rec(log, job, local_file_path)
        #    job['LIS_REC_FILE_NM'] = lis_rec_file_nm
        #    job['LIS_REC_LOC_PATH'] = lis_rec_loc_path
        #except Exception:
        #    log.error(traceback.format_exc())
        #    pass
        return job
    except Exception:
        PROG_STAT_CD = '18' #녹취오류
        PROG_STAT_DTL_CD = 'STTERR00' #녹취다운오류
        raise Exception(traceback.format_exc())


def update_stt_status_to_start(log, db, job):
    """
    Update STT status to start QA_CAL_INFO_TB(콜정보테이블)
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                }
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        log.info('  1-1) Update STT PROG_STAT_CD to 12(STT처리중)')
        util.update_stt_status_commit(
            db=db,
            host_nm=socket.gethostname(),
            prog_stat_cd='12', # STT처리중
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            rec_key=job['REC_KEY'],
            rec_id=job['REC_ID'],
            rec_file_nm=job['REC_FILE_NM'],
            rec_st_dt=job['REC_ST_DT']
        )
    except Exception:
        PROG_STAT_CD = '19'
        PROG_STAT_DTL_CD = 'STTERR01' # STT상태변환오류
        raise Exception(traceback.format_exc())
    log.info('\t  --> Done updated STT status')


def stt_process(log, db, job):
    """
    STT process
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                }
    """
    log.info('1. Update STT status to start QA_CAL_INFO_TB(콜정보테이블)')
    update_stt_status_to_start(log, db, job)
    if job['REC_COMP_CD'] == '100': # 실시간STT
        log.info('2. Extract Real STT record information and download wav file')
        updated_job = extract_record_info_and_download_real(log, db, job)
        if updated_job:
            job = updated_job
        else:
            log.info('Exception process) Mono STT Execute ..')
            if job['REC_FILE_NM']:
                job = extract_record_info_and_download(log, job)
            else:
                log.info("Exception process) Not existed Mono file Update PROG_STAT_CD = '13'")
                # Update QA_CAL_INFO_TB(콜정보테이블) status
                util.update_stt_status_commit(
                    db=db,
                    host_nm=socket.gethostname(),
                    prog_stat_cd='13',
                    prog_stat_dtl_cd=None,
                    rec_key=job['REC_KEY'],
                    rec_id=job['REC_ID'],
                    rec_file_nm=job['REC_FILE_NM'],
                    rec_st_dt=job['REC_ST_DT']
                )
                try:
                    db.conn.commit()
                    db.disconnect()
                except Exception:
                    pass
                del_file(log)
                log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
                sys.exit(0)
    else:
        log.info('2. Extract record information and download wav file')
        job = extract_record_info_and_download(log, job)
    log.info('3. Make pcm file')
    pcm_file_info = make_pcm_file(log, job)
    log.info('4. Execute STT')
    stt_result_info = execute_cnn_stt(log, db, job, pcm_file_info)
    log.info('5. Insert STT results')
    max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_list = insert_stt_result(log, db, job, stt_result_info)
    log.info('6. Update STT status to end and information to STTARECSTAT(STTA녹취상태)')
    update_stt_status_and_info(log, db, job, max_silence_time, spch_sped_rx, spch_sped_tx, stt_result_info)
    log.info('7. Execute Common Forbidden word HMD(공통금지어 PROD_CAT="L", PROD_CD="ZZZREC", PLN_CD="0")')
    output_list = execute_common_fwd_hmd(log, db, job, stt_result_list)
    log.info('8. Insert TA results')
    insert_ta_output(log, db, job['REC_KEY'], output_list)


def main(job):
    """
    This program that execute STT
    @param      job:            job{
                                    PROC_PRI          처리우선순위
                                    REC_KEY           녹취KEY
                                    REC_ID            녹취ID
                                    REC_FILE_NM       녹취파일명
                                    STT_REC_FILE_NM   실시간STT녹취파일명
                                    REC_COMP_CD       녹취업체구분코드
                                    CHN_CLA_CD        채널구분코드
                                    REC_ST_DT         녹취시작일자
                                    REC_ST_TM         녹취시작시간
                                    REC_ED_TM         녹취종료시간
                                    REC_DU_TM         녹취통화시간
                                    BROF_CD           지점코드
                                    EMP_NO            사원번호
                                    STEREO_SPLIT_YN   스테레오분할여부
                                }
    """
    # Ignore kill signal
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    # Set logger
    sysdate = datetime.now().strftime("%Y%m%d")
    base_dir_path = "{0}/{1}".format(sysdate[:4], sysdate[4:])
    if job['REC_COMP_CD'] == '100':
        log_file_name = '{0}.log'.format(job['STT_REC_FILE_NM'].replace(" ", ""))
    else:
        log_file_name = '{0}.log'.format(job['REC_FILE_NM'].replace(" ", ""))
    log = logger.set_logger(
        logger_name=config.STTConfig.logger_name,
        log_dir_path=os.path.join(config.STTConfig.log_dir_path, base_dir_path),
        log_file_name=log_file_name,
        log_level=config.STTConfig.log_level
    )
    # Execute STT
    log.info("[START] Execute STT ..")
    log_str = '''Record information
                                    PROC_PRI         처리우선순위 = {0}
                                    REC_KEY          녹취KEY = {1}
                                    REC_ID           녹취ID = {2}
                                    REC_FILE_NM      녹취파일명 = {3}
                                    STT_REC_FILE_NM  실시간STT녹취파일명 = {4}
                                    REC_COMP_CD      녹취업체구분코드 = {5}
                                    CHN_CLA_CD       채널구분코드 = {6}
                                    REC_ST_DT        녹취시작일자 = {7}
                                    REC_ST_TM        녹취시작시간 = {8}
                                    REC_ED_TM        녹취종료시간 = {9}
                                    REC_DU_TM        녹취시간(/s) = {10}
                                    BROF_CD          지점코드 = {11}
                                    EMP_NO           사원번호 = {12}
                                    STEREO_SPLIT_YN  스테레오분할여부 = {13}'''.format(
        job['PROC_PRI'], job['REC_KEY'], job['REC_ID'], job['REC_FILE_NM'], job['STT_REC_FILE_NM'], job['REC_COMP_CD'],
        job['CHN_CLA_CD'], job['REC_ST_DT'], job['REC_ST_TM'], job['REC_ED_TM'], job['REC_DU_TM'], job['BROF_CD'],
        job['EMP_NO'], job['STEREO_SPLIT_YN']
    )
    log.info(log_str)
    # Connect DB and Load Petra cipher API
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
        load_petra_api(log)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    # Execute STT
    try:
        stt_process(log, db, job)
    except Exception:
        log.error(traceback.format_exc())
        log_str_list = list()
        log_str_list.append('Update QA_CAL_INFO_TB(콜정보테이블) status to error code')
        log_str_list.append('[PROG_STAT_CD= {0}, PROG_STAT_DTL_CD= {1}]'.format( PROG_STAT_CD, PROG_STAT_DTL_CD))
        log.info(' '.join(log_str_list))
        # Update QA_CAL_INFO_TB(콜정보테이블) status
        util.update_stt_status_commit(
            db=db,
            host_nm=socket.gethostname(),
            prog_stat_cd=PROG_STAT_CD,
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            rec_key=job['REC_KEY'],
            rec_id=job['REC_ID'],
            rec_file_nm=job['REC_FILE_NM'],
            rec_st_dt=job['REC_ST_DT']
        )
    finally:
        try:
            db.conn.commit()
            db.disconnect()
        except Exception:
            pass
        del_file(log)
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
