#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2021-01-20, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import time
import json
import signal
import socket
import pprint
import requests
import traceback
import collections
from datetime import datetime, timedelta
from cfg import config
from lib import logger, util, db_connection, nlp, hmd, libpcpython

###########
# options #
###########
reload(sys)
sys.setdefaultencoding("utf-8")

#############
# constants #
#############
PETRA_SID = ''
PROG_STAT_CD = '23' # '23': QA변환완료
PROG_STAT_DTL_CD = ''
DTC_INFO_DICT = dict()
C_REC_INFO_DICT = dict()
DELETE_FILE_LIST = list()
SCRPT_HK_CD_DICT = dict()
ST_SECT_INFO_DICT = dict()
CUST_REPLY_INFO_DICT = dict()
TOTAL_CHK_ITM_CD_DICT = dict()
HOST_NM = socket.gethostname()
CUST_REPLY_NCSS_YN_DICT = dict()
PRO_ST_TM = datetime.fromtimestamp(time.time())


#######
# def #
#######
def elapsed_time(start_time):
    """
    Elapsed time
    @param          start_time:          date object
    @return                              Required time (type : datetime)
    """
    end_time = datetime.fromtimestamp(time.time())
    required_time = end_time - start_time
    return required_time


def del_file(log):
    """
    Delete file
    @param      log:    Logger object
    """
    for file_path in DELETE_FILE_LIST:
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't delete {0}".format(file_path))
            continue


def connect_db(log, db_type, db_conf):
    """
    Connect DB
    @param      log:            Logger Object
    @param      db_type:        DB type (Oracle, MSSQL, MYSQL)
    @param      db_conf:        DB config
    @return:                    DB object
    """
    # Connect DB (DB 접속시 오류가 발생할 경우 3번까지 재접속 시도)
    db = False
    for _ in range(0, 3):
        try:
            log.info('Try connecting to {0} DB ...'.format(db_type))
            if db_type.upper() == 'ORACLE':
                db = db_connection.Oracle(db_conf, failover=True, service_name=True)
            elif db_type.upper() == 'MSSQL':
                db = db_connection.MSSQL(db_conf)
            elif db_type.upper() == 'MYSQL':
                db = db_connection.MYSQL(db_conf)
            else:
                raise Exception('Not supported db ..(Oracle, MSSQL, MYSQL)')
            log.info('Success connect to {0} DB'.format(db_type))
            break
        except Exception:
            log.error(traceback.format_exc())
            log.error("Can't connect db")
            time.sleep(20)
    if not db:
        raise Exception("Can't connect db")
    return db


def extract_speech_time(seconds):
    """
    Extract speech time
    :param      seconds:    Seconds(/s)
    :return:                HH:MM:SS.FF
    """
    time_list = str(timedelta(seconds=float(seconds))).split(':')
    hh = ('0' + time_list[0])[-2:]
    mm = time_list[1]
    ss = '%.2f' % float(time_list[2])
    ss = ss.split('.')[0]
    ss = ss if len(ss) == 2 else '0' + ss
    return ':'.join([hh, mm, ss])


def extract_speech_speed(start_time, end_time, sent):
    """
    Extract speech speed
    @param      start_time:     Start time(/ms)
    @param      end_time:       End time(/ms)
    @param      sent:           Sentence (encoding='utf-8')
    @return:                    Speech speed
    """
    sntc_len = len(sent.replace(' ', ''))
    during_time = (end_time / 8000.0) - (start_time / 8000.0)
    if during_time > 0:
        speech_speed = round(float(sntc_len) / during_time, 2)
    else:
        speech_speed = 0
    if speech_speed > 999:
        speech_speed = 0.0
    return speech_speed


def make_target_line(end_idx, line_gap, dtc_line_list):
    """
    Make target line
    @param      end_idx:            End index
    @param      line_gap:           Line gap
    @param      dtc_line_list:      Start line number list
    @return:                        Output list
    """
    if '1' not in dtc_line_list:
        return list()
    dtc_list = list()
    start_line_list = dtc_line_list['1']
    for line_num in start_line_list:
        temp_dict = dict()
        temp_dict['1'] = [line_num]
        for idx in range(1, end_idx):
            temp_list = list()
            if str(idx) not in temp_dict:
                continue
            for num in temp_dict[str(idx)]:
                for cnt in range(0, line_gap + 1):
                    next_line_num = int(num) + cnt
                    if str(idx + 1) not in dtc_line_list:
                        return list()
                    if next_line_num in dtc_line_list[str(idx + 1)]:
                        if next_line_num not in temp_list:
                            temp_list.append(next_line_num)
                        temp_dict[str(idx + 1)] = temp_list
        if len(temp_dict) != end_idx:
            continue
        dtc_list.append(temp_dict)
    output_list = list()
    for item_dict in dtc_list:
        for line_list in item_dict.values():
            for line_number in line_list:
                if line_number not in output_list:
                    output_list.append(line_number)
    return output_list


def load_petra_api(log):
    """
    Load Petra cipher API
    @param      log:            Logger Object
    """
    global PETRA_SID
    log.info("Load Petra cipher API ...")
    rtn = libpcpython.PcAPI_initialize(config.PetraConfig.conf_file_path, '')
    log.info("  --> Petra initialize return is [{0}]".format(rtn))
    PETRA_SID = libpcpython.PcAPI_getSession('')
    log.info("  --> Petra getSession sid is [{0}]".format(rtn))


def requests_tlife_system(log, qa_result):
    """
    Requests for T-Life System
    @param      log:                Logger object
    @param      qa_result:          Heungkuk QA result
    """
    data = {
        "header":{
            "HDR_PACKET_SIZE":"00001530",
            "HDR_TRX_DATE":"",
            "HDR_TRX_CODE":"",
            "HDR_RESULT_CODE":"",
            "HDR_CHANNEL_SEQ_ID":"",
            "HDR_REQ_SYS_ID":"HKSTT",
            "HDR_DEST_SYS_ID":"HKTM",
            "HDR_REQ_SYS_SEQ_ID":"",
            "HDR_REQ_SYS_SEND_DATE": datetime.strftime(datetime.now(), "%Y%m%d%H%M%s")[:14],
            "HDR_DEST_SYS_RECV_DATE":"",
            "HDR_DEST_PROG":"CITMST04",
            "HDR_TRA_FLAG":"1",
            "HDR_OPERATION_FLAG":"",
            "HDR_CHANNEL_ERR_CODE":"",
            "HDR_DATA_FLAG":"R",
            "HDR_USER_ID":"",
            "HDR_CLIENT_IP":socket.gethostbyname(socket.gethostname()),
            "HDR_VERSION":"",
            "HDR_REPLY_FLAG":"",
            "HDR_EXTERNAL_FLAG":"N",
            "HDR_FILLER":""
        },
        "body":[
            qa_result
        ]
    }
    log.info("\t  --> Send HK QA result to T-life")
    result = ''
    flag = False
    for _ in range(0, 3):
        try:
            result = requests.post(config.QAConfig.tlife_host, json=data)
            flag = True
            break
        except Exception:
            log.error(traceback.format_exc())
            log.info("\t  --> Retry Send HK QA result to T-life")
            log.info('\t  --> Waiting 5 seconds ...')
            time.sleep(5)
            continue
    if not flag:
        raise Exception("\t  --> Can't send HK QA result to T-life")
    try:
        log.info("\t    : {0}".format(result))
    except Exception:
        pass


def extract_hk_qa_result(log, job, fd_kwd_dict, hk_scrpt_list, total_stt_results):
    """
    Extract Heungkuk QA results
    @param      log:                Logger object
    @param      job:                Job{
                                        NSPL_NO        계약번호
                                        TMS_INFO       회차정보
                                        PROD_CD        상품코드
                                        PROD_NM        상품명
                                        CONTRACT_DT    청약일자
                                        QA_REQ_TM      QA요청일자
                                        PROC_PRI       처리우선순위
                                        TMS_FILE_CNT   회차별녹취파일수
                                        PROD_CAT       보종구분
                                        PLN_CD         플랜코드
                                    }
    @param      fd_kwd_dict:            Forbidden keyword dictionary
    @param      hk_scrpt_list:          Heungkuk T-life script list
    @param      total_stt_results:      Total STT results
    @return:                            QA result, {CREC_VOIC_SP, CREC_DU_TM, SREC_VOIC_SP, SREC_DU_TM}
    """
    global C_REC_INFO_DICT
    global SCRPT_HK_CD_DICT
    log.info("\t  --> Extract data from T-life script")
    hk_ans_rst_dict = dict()
    hk_chk_rst_dict = dict()
    for sec_key, dtct_rst, ans_rst in hk_scrpt_list:
        sec_key_list = sec_key.split('_')
        if len(sec_key_list) == 4:
            chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd = sec_key_list
            if dtct_rst == '01':
                hk_sec_key = "{0}_{1}".format(scrp_lctg_cd, scrp_mctg_cd)
                if hk_sec_key in hk_chk_rst_dict:
                    hk_chk_rst_dict[hk_sec_key].append(scrp_sctg_cd)
                    hk_ans_rst_dict[hk_sec_key].append(ans_rst)
                else:
                    hk_chk_rst_dict[hk_sec_key] = [scrp_sctg_cd]
                    hk_ans_rst_dict[hk_sec_key] = [ans_rst]
        else:
            chk_itm_cd, cusl_cd, kwd_seq = sec_key_list
            hk_chk_rst_dict[cusl_cd] = True
    log.info("\t  --> Extract C_VOIC_SP, C_DU_TM, S_VOIC_SP, S_DU_TM")
    flag = 'START'
    tmp_rec_key = ''
    s_line_cnt = 0
    c_line_cnt = 0
    s_voic_sp = 0.0
    c_voic_sp = 0.0
    total_s_du_tm = 0.0
    total_c_du_tm = 0.0
    s_rec_dict = dict()
    c_rec_dict = dict()
    c_flag = True
    for item in total_stt_results:
        """
            item = {
                'TMS_INFO'          : 회차정보
                'REC_KEY'           : 녹취KEY
                'REC_ID'            : 녹취ID
                'REC_FILE_NM'       : 녹취파일명
                'REC_BIZ_CD'        : 녹취업무코드
                'REC_DU_TM'         : 통화시간
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
            }
        """
        try:
            st_hh, st_mm, st_ss = item['STMT_ST_TM'].split(':')
            ed_hh, ed_mm, ed_ss = item['STMT_ED_TM'].split(':')
            start = ((int(st_hh) * 60 * 60) + (int(st_mm) * 60) + int(st_ss)) * 8000
            end = ((int(ed_hh) * 60 * 60) + (int(ed_mm) * 60) + int(ed_ss)) * 8000
            if item['REC_BIZ_CD'] == 'S':
                s_rec_dict[item['REC_KEY']] = int(item['REC_DU_TM'])
                s_line_cnt += 1
                s_voic_sp += extract_speech_speed(start, end, item['STMT'])
            elif item['REC_BIZ_CD'] == 'C':
                st_stmt_no, stmt_st_tm = C_REC_INFO_DICT[item['REC_KEY']]['st_info']
                if st_stmt_no and not tmp_rec_key:
                    tmp_rec_key = item['REC_KEY']
                if tmp_rec_key != item['REC_KEY']:
                    c_flag = False
                if st_stmt_no and c_flag:
                    if not C_REC_INFO_DICT[item['REC_KEY']]['rec_du_tm']:
                        continue
                    if st_stmt_no == item['STMT_NO']:
                        flag = 'END'
                        rec_du_tm = C_REC_INFO_DICT[item['REC_KEY']]['rec_du_tm']
                        s_du_tm = ((int(st_hh) * 60 * 60) + (int(st_mm) * 60) + int(st_ss))
                        total_s_du_tm += s_du_tm
                        c_du_tm = int(rec_du_tm) - s_du_tm
                        total_c_du_tm += c_du_tm
                    if flag == 'START':
                        s_voic_sp += extract_speech_speed(start, end, item['STMT'])
                        s_line_cnt += 1
                    if flag == 'END':
                        c_voic_sp += extract_speech_speed(start, end, item['STMT'])
                        c_line_cnt += 1
                else:
                    c_rec_dict[item['REC_KEY']] = int(item['REC_DU_TM'])
                    c_line_cnt += 1
                    c_voic_sp += extract_speech_speed(start, end, item['STMT'])
            else:
                continue
        except Exception:
            log.error('\t  --> {0}'.format(traceback.format_exc()))
            continue
    for du_tm in s_rec_dict.values():
        total_s_du_tm += du_tm
    for du_tm in c_rec_dict.values():
        total_c_du_tm += du_tm
    srec_voic_sp = round(s_voic_sp / s_line_cnt, 1) if s_line_cnt else round(s_voic_sp / 1, 1)
    crec_voic_sp = round(c_voic_sp / c_line_cnt, 1) if c_line_cnt else round(c_voic_sp / 1, 1)
    result_info = {
        "srec_voic_sp": srec_voic_sp,
        "srec_du_tm": extract_speech_time(total_s_du_tm),
        "crec_voic_sp": crec_voic_sp,
        "crec_du_tm": extract_speech_time(total_c_du_tm)
    }
    log.info("\t  --> Extract SCRP_SCTG_CD, JUG_RST, ANS_RST")
    dtct_rst_cnt = 0
    qa_rst_info = list()
    for hk_sec_key, sctg_cd_list in SCRPT_HK_CD_DICT.items():
        scrp_lctg_cd, scrp_mctg_cd = hk_sec_key.split('_')
        rst_dict = {"SCRP_SCTG_CD": scrp_mctg_cd, "JUG_RST": "", "ANS_RST": ""}
        if hk_sec_key in hk_chk_rst_dict:
            if len(set(sctg_cd_list) - set(hk_chk_rst_dict[hk_sec_key])) < 1:
                dtct_rst_cnt += 1
                rst_dict["JUG_RST"] = '01'  # 탐지
            elif len(set(sctg_cd_list) & set(hk_chk_rst_dict[hk_sec_key])) > 0:
                rst_dict["JUG_RST"] = '02'  # 부분탐지
            if 'N' in list(set(hk_ans_rst_dict[hk_sec_key])):
                ans_rst = 'N'
            elif 'Y' in list(set(hk_ans_rst_dict[hk_sec_key])):
                ans_rst = 'Y'
            else:
                ans_rst = ''
            rst_dict["ANS_RST"] = ans_rst
        else:
            rst_dict["JUG_RST"] = '03'  # 미탐지
        qa_rst_info.append(rst_dict)
    log.info("\t  --> Set T-life send data")
    fd_kwd_list = list()
    for key, kwd_list in fd_kwd_dict.items():
        for dtct_dtc_no in kwd_list:
            if dtct_dtc_no not in fd_kwd_list:
                fd_kwd_list.append(dtct_dtc_no)
    fd_rst_cnt = len(fd_kwd_list)
    while len(qa_rst_info[:]) < 100:
        qa_rst_info.append({"SCRP_SCTG_CD": "", "JUG_RST": "", "ANS_RST": ""})
    c_speech_time = extract_speech_time(total_c_du_tm)
    hh, mm, ss = c_speech_time.split(':')
    if int(hh) > 0:
        mm = int(mm) + (int(hh) * 60)
    cnv_c_du_tm = '{0}:{1}'.format(mm, ss)
    s_speech_time = extract_speech_time(total_s_du_tm)
    hh, mm, ss = s_speech_time.split(':')
    if int(hh) > 0:
        mm = int(mm) + (int(hh) * 60)
    cnv_s_du_tm = '{0}:{1}'.format(mm, ss)
    if cnv_c_du_tm == '00:00':
        output_voic_sp = ""
        output_c_du_tm = ""
    else:
        output_voic_sp = "{0}({1})".format(cnv_s_du_tm, result_info.get('srec_voic_sp'))
        output_c_du_tm = "{0}({1})".format(cnv_c_du_tm, result_info.get('crec_voic_sp'))
    qa_result = {
        "NSPL_NO": job['NSPL_NO'],
        "DTCT_RST_CNT": str(dtct_rst_cnt),
        "TOTAL_DTCT_CNT": str(len(SCRPT_HK_CD_DICT)),
        "VOIC_SP": output_voic_sp,
        "C_DU_TM": output_c_du_tm,
        "FD_RST_CNT": fd_rst_cnt
    }
    log.info("\t    : \n{0}".format(pprint.pformat(qa_result)))
    qa_result['QA_RST_INFO'] = qa_rst_info
    return result_info, qa_result


def update_qa_ta_status_and_info(log, db, job, min_stt_st_dtm, max_stt_ed_dtm, result_info):
    """
    Update QA status and information to QA_CTR_LIST_TB(계약리스트테이블)
    @param      log:                Logger object
    @param      db:                 DB object
    @param      job:                Job{
                                        NSPL_NO        계약번호
                                        TMS_INFO       회차정보
                                        PROD_CD        상품코드
                                        PROD_NM        상품명
                                        CONTRACT_DT    청약일자
                                        QA_REQ_TM      QA요청일자
                                        PROC_PRI       처리우선순위
                                        TMS_FILE_CNT   회차별녹취파일수
                                        PROD_CAT       보종구분
                                        PLN_CD         플랜코드
                                    }
    @param      min_stt_st_dtm:     Minimum STT start time
    @param      max_stt_ed_dtm:     Maximum STT end time
    @param      result_info:        {CREC_VOIC_SP, CREC_DU_TM, SREC_VOIC_SP, SREC_DU_TM}
    @return:                        If start return TA process start time
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        util.update_qa_status_and_ta_info(
            db=db,
            prog_stat_cd=PROG_STAT_CD,
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            stt_st_dtm=min_stt_st_dtm,
            stt_ed_dtm=max_stt_ed_dtm,
            ta_st_dtm=PRO_ST_TM,
            ta_ed_dtm=datetime.fromtimestamp(time.time()),
            host_nm=HOST_NM,
            updated_tm=datetime.fromtimestamp(time.time()),
            nspl_no=job['NSPL_NO'],
            tms_info=job['TMS_INFO'],
            crec_voic_sp=result_info.get('crec_voic_sp'),
            crec_du_tm=result_info.get('crec_du_tm'),
            srec_voic_sp=result_info.get('srec_voic_sp'),
            srec_du_tm=result_info.get('srec_du_tm')
        )
        log.info('\t  --> Done updated QA status and information')
    except Exception:
        PROG_STAT_CD = '29'
        PROG_STAT_DTL_CD = 'QAERR00'
        raise Exception(traceback.format_exc())


def make_chk_rst_data(log, job, chk_app_ord):
    """
    Make QA_CHK_RST_TB(평가항목결과테이블) insert data
    @param      log:                Logger object
    @param      job:                Job{
                                        NSPL_NO        계약번호
                                        TMS_INFO       회차정보
                                        PROD_CD        상품코드
                                        PROD_NM        상품명
                                        CONTRACT_DT    청약일자
                                        QA_REQ_TM      QA요청일자
                                        PROC_PRI       처리우선순위
                                        TMS_FILE_CNT   회차별녹취파일수
                                        PROD_CAT       보종구분
                                        PLN_CD         플랜코드
                                    }
    @param      chk_app_ord:        CHK_APP_ORD(항목적용차수)
    @return:                        QA_CHK_RST_TB data
    """
    global DTC_INFO_DICT
    global TOTAL_CHK_ITM_CD_DICT
    hk_scrpt_list = list()
    qa_chk_bind_list = list()
    overlap_check_dict = dict()
    log.info("\t  --> Extract data from DTC_INFO_DICT")
    for sec_key, dtc_key_dict in DTC_INFO_DICT.items():
        """
        sec_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd)
        OR    
        sec_key = "{0}_{1}_{2}".format(chk_itm_cd, cusl_cd, kwd_seq)
        """
        ans_rst = ''
        chk_ctg_cd = ''
        cust_ans_yn = ''
        output_dict = dict()
        seq_list = list()
        sec_key_list = sec_key.split('_')
        if len(sec_key_list) == 4:
            chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd = sec_key_list
            for dtc_key, dtc_sec_info in dtc_key_dict.items():
                """
                dtc_key = '{0}_{1}_{2}_{3}_{4}'.format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq)
                OR    
                dtc_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, cusl_cd, kwd_seq)
                dtc_sec_info = {
                    'chk_ctg_cd': item['CHK_CTG_CD'],
                    'esty_scrp_yn': item['ESTY_SCRP_YN'],
                    'cust_ans_yn': item['CUST_ANS_YN'],
                    'dtc_list': [
                        {
                            'TMS_INFO'          : 회차정보
                            'REC_KEY'           : 녹취KEY
                            'REC_ID'            : 녹취ID
                            'REC_FILE_NM'       : 녹취파일명
                            'STMT_NO'           : 문장번호
                            'SPK_DIV_CD'        : 화자구분코드
                            'STMT_ST_TM'        : 문장시작시간
                            'STMT_ED_TM'        : 문장종료시간
                            'STMT'              : 원문
                            'tmp_line_no'       : 전체 STT 임시 문장번호
                            'DTCT_KWD'          : 탐지키워드
                            'ans_rst'           : 답변결과
                        }, ...
                    ]
                }
                """
                chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq = dtc_key.split('_')
                chk_ctg_cd = dtc_sec_info['chk_ctg_cd']
                esty_scrp_yn = dtc_sec_info['esty_scrp_yn']
                cust_ans_yn = dtc_sec_info['cust_ans_yn']
                if dtc_sec_info['dtc_list']:
                    for item in dtc_sec_info['dtc_list']:
                        if esty_scrp_yn.upper() == 'Y':
                            seq_list.append(stmt_seq)
                        if item['REC_KEY'] in output_dict:
                            if cust_ans_yn:
                                if cust_ans_yn.upper() == 'Y':
                                    output_dict[item['REC_KEY']]['ans_rst_list'].append(item['ans_rst'])
                            if output_dict[item['REC_KEY']]['st_tm'] > item['STMT_ST_TM']:
                                output_dict[item['REC_KEY']]['st_tm'] = item['STMT_ST_TM']
                            if output_dict[item['REC_KEY']]['ed_tm'] < item['STMT_ED_TM']:
                                output_dict[item['REC_KEY']]['ed_tm'] = item['STMT_ED_TM']
                        else:
                            output_dict[item['REC_KEY']] = {
                                'ans_rst_list': list(),
                                'st_tm': item['STMT_ST_TM'],
                                'ed_tm': item['STMT_ED_TM'],
                            }
                            if cust_ans_yn:
                                if cust_ans_yn.upper() == 'Y':
                                    output_dict[item['REC_KEY']]['ans_rst_list'].append(item['ans_rst'])
        else:
            chk_itm_cd, cusl_cd, kwd_seq = sec_key_list
            for dtc_key, dtc_sec_info in dtc_key_dict.items():
                chk_ctg_cd = dtc_sec_info['chk_ctg_cd']
                if dtc_sec_info['dtc_list']:
                    seq_list.append(kwd_seq)
                    for item in dtc_sec_info['dtc_list']:
                        if item['REC_KEY'] in output_dict:
                            if output_dict[item['REC_KEY']]['st_tm'] > item['STMT_ST_TM']:
                                output_dict[item['REC_KEY']]['st_tm'] = item['STMT_ST_TM']
                            if output_dict[item['REC_KEY']]['ed_tm'] < item['STMT_ED_TM']:
                                output_dict[item['REC_KEY']]['ed_tm'] = item['STMT_ED_TM']
                        else:
                            output_dict[item['REC_KEY']] = {
                                'ans_rst_list': [None],
                                'st_tm': item['STMT_ST_TM'],
                                'ed_tm': item['STMT_ED_TM'],
                            }
        if sec_key in TOTAL_CHK_ITM_CD_DICT:
            if len(set(TOTAL_CHK_ITM_CD_DICT[sec_key]) - set(seq_list)) < 1:
                dtct_rst = '01' # 탐지
            elif len(set(TOTAL_CHK_ITM_CD_DICT[sec_key]) & set(seq_list)) == 0:
                dtct_rst = '03' # 미탐지
            else:
                dtct_rst = '02' # 부분탐지
        else:
            dtct_rst = '03'  # 미탐지
        overlap_check_key = (chk_itm_cd, chk_ctg_cd)
        if overlap_check_key in overlap_check_dict:
            continue
        overlap_check_dict[overlap_check_key] = 1
        bind = (
            job['NSPL_NO'],
            job['TMS_INFO'],
            job['PROD_CAT'],
            chk_app_ord,
            chk_itm_cd,
            chk_ctg_cd,
            None,
            'STT',
            None,
            None,
            'N' if cust_ans_yn in ('y', 'Y') else None,
            dtct_rst,
            None,
            None,
            HOST_NM,
            datetime.fromtimestamp(time.time()),
            HOST_NM,
            datetime.fromtimestamp(time.time())
        )
        if output_dict:
            for rec_key, rec_info in output_dict.items():
                if 'Y' in rec_info['ans_rst_list']:
                    ans_rst = 'Y'
                elif 'N' in rec_info['ans_rst_list']:
                    ans_rst = 'N'
                else:
                    ans_rst = None
                bind = (
                    job['NSPL_NO'],
                    job['TMS_INFO'],
                    job['PROD_CAT'],
                    chk_app_ord,
                    chk_itm_cd,
                    chk_ctg_cd,
                    rec_key,
                    'STT',
                    None,
                    None,
                    ans_rst,
                    dtct_rst,
                    rec_info['st_tm'],
                    rec_info['ed_tm'],
                    HOST_NM,
                    datetime.fromtimestamp(time.time()),
                    HOST_NM,
                    datetime.fromtimestamp(time.time())
                )
        qa_chk_bind_list.append(bind)
        hk_scrpt_list.append((sec_key, dtct_rst, ans_rst))
    return qa_chk_bind_list, hk_scrpt_list


def extract_cust_reply_info(item, dtc_key, cust_reply_info):
    """
    Extract customer reply information
    @param          item:                   item = {
                                                'TMS_INFO'          : 회차정보
                                                'REC_KEY'           : 녹취KEY
                                                'REC_ID'            : 녹취ID
                                                'REC_FILE_NM'       : 녹취파일명
                                                'STMT_NO'           : 문장번호
                                                'SPK_DIV_CD'        : 화자구분코드
                                                'STMT_ST_TM'        : 문장시작시간
                                                'STMT_ED_TM'        : 문장종료시간
                                                'STMT'              : 원문
                                                'tmp_line_no'       : 전체 STT 임시 문장번호
                                                'DTCT_KWD'          : 탐지키워드
                                            }
    @param          dtc_key:                chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq
    @param          cust_reply_info:        customer reply information
    @return:                                customer reply information
    """
    cust_reply_yn = 'N'
    cust_reply_nor_yn = None
    cust_reply_stmt = ''
    cust_reply_st_tm = None
    cust_reply_ed_tm = None
    if item['SPK_DIV_CD'].upper() == 'A':
        for idx in range(1, config.QAConfig.s_cust_an_line + 1):
            # 스트레오 파일일 경우 고객 답변만 추가
            key = ('C', item['REC_FILE_NM'], str(int(item['STMT_NO'].strip()) + idx))
            if key in CUST_REPLY_INFO_DICT:
                cust_reply_yn = 'Y'
                stmt_st_tm, stmt_end_tm, stmt = CUST_REPLY_INFO_DICT[key]
                for an_str in CUST_REPLY_NCSS_YN_DICT[dtc_key]:
                    if an_str in stmt:
                        cust_reply_stmt += stmt.strip()
                        if not cust_reply_st_tm:
                            cust_reply_st_tm = stmt_st_tm
                        cust_reply_ed_tm = stmt_end_tm
                        cust_reply_nor_yn = 'Y'
                        break
    if item['SPK_DIV_CD'].upper() == 'M':
        for idx in range(1, config.QAConfig.m_cust_an_line + 1):
            # 모노 파일일 경우 고객 답변 리스트에 있는 단어가 문장에 있을 경우만 추가
            key = ('M', item['REC_FILE_NM'], str(int(item['STMT_NO'].strip()) + idx))
            if key in CUST_REPLY_INFO_DICT:
                stmt_st_tm, stmt_end_tm, stmt = CUST_REPLY_INFO_DICT[key]
                for an_str in CUST_REPLY_NCSS_YN_DICT[dtc_key]:
                    if an_str in stmt and len(stmt) < 10:
                        cust_reply_stmt += stmt.strip()
                        if not cust_reply_st_tm:
                            cust_reply_st_tm = stmt_st_tm
                        cust_reply_ed_tm = stmt_end_tm
                        cust_reply_yn = 'Y'
                        cust_reply_nor_yn = 'Y'
                        break
    if cust_reply_stmt:
        if len(cust_reply_stmt.strip()) == 0:
            cust_reply_stmt = None
    cust_reply_info['reply_yn'] = cust_reply_yn
    cust_reply_info['reply_nor_yn'] = cust_reply_nor_yn
    cust_reply_info['reply_stmt'] = cust_reply_stmt
    cust_reply_info['reply_st_tm'] = cust_reply_st_tm
    cust_reply_info['reply_ed_tm'] = cust_reply_ed_tm
    return cust_reply_info


def insert_qa_output(log, db, job, ctr_scrp_bind_list, scrp_dtct_bind_list, qa_chk_bind_list):
    """
    Insert QA output
    @param      log:                            Logger object
    @param      db:                             DB object
    @param      job:                            Job{
                                                    NSPL_NO        계약번호
                                                    TMS_INFO       회차정보
                                                    PROD_CD        상품코드
                                                    PROD_NM        상품명
                                                    CONTRACT_DT    청약일자
                                                    QA_REQ_TM      QA요청일자
                                                    PROC_PRI       처리우선순위
                                                    TMS_FILE_CNT   회차별녹취파일수
                                                    PROD_CAT       보종구분
                                                    PLN_CD         플랜코드
                                                }
    @param      ctr_scrp_bind_list:             Bind list
    @param      scrp_dtct_bind_list:            Bind list
    @param      qa_chk_bind_list:               Bind list
    @return:
    """
    log.info("\t  1) QA_CTR_SCRP_TB")
    log.info('\t    --> Check QA_CTR_SCRP_TB results')
    del_flag = util.select_qa_ctr_scrp_results_existed(db, job['NSPL_NO'], job['TMS_INFO'])
    if del_flag:
        log.info('\t    --> Already existed. Delete QA_CTR_SCRP_TB results')
        util.delete_qa_ctr_scrp_results(db, job['NSPL_NO'], job['TMS_INFO'])
        log.info('\t    --> Done delete QA_CTR_SCRP_TB results')
    log.info('\t    --> Insert QA_CTR_SCRP_TB results')
    util.insert_qa_ctr_scrp_results(db, ctr_scrp_bind_list)
    log.info("\t  2) QA_SCRP_DTCT_TB")
    log.info('\t    --> Check QA_SCRP_DTCT_TB results')
    del_flag = util.select_qa_scrp_dtct_results_existed(db, job['NSPL_NO'], job['TMS_INFO'])
    if del_flag:
        log.info('\t    --> Already existed. Delete QA_SCRP_DTCT_TB results')
        util.delete_qa_scrp_dtct_results(db, job['NSPL_NO'], job['TMS_INFO'])
        log.info('\t    --> Done delete QA_SCRP_DTCT_TB results')
    log.info('\t    --> Insert QA_SCRP_DTCT_TB results')
    util.insert_qa_scrp_dtct_results(db, scrp_dtct_bind_list)
    log.info("\t  3) QA_CHK_RST_TB")
    log.info('\t    --> Check QA_CHK_RST_TB results')
    del_flag = util.select_qa_chk_rst_results_existed(db, job['NSPL_NO'], job['TMS_INFO'])
    if del_flag:
        log.info('\t    --> Already existed. Delete QA_CHK_RST_TB results')
        util.delete_qa_chk_rst_results(db, job['NSPL_NO'], job['TMS_INFO'])
        log.info('\t    --> Done delete QA_CHK_RST_TB results')
    log.info('\t    --> Insert QA_CHK_RST_TB results')
    util.insert_qa_chk_rst_results(db, qa_chk_bind_list)


def make_scrp_dtct_data(log, job, chk_app_ord, dtc_sec_info_dict):
    """
    Make QA_SCRP_DTCT_RST_TB(표준스크립트탐지결과테이블) insert data
    @param      log:                            Logger object
    @param      job:                            Job{
                                                    NSPL_NO        계약번호
                                                    TMS_INFO       회차정보
                                                    PROD_CD        상품코드
                                                    PROD_NM        상품명
                                                    CONTRACT_DT    청약일자
                                                    QA_REQ_TM      QA요청일자
                                                    PROC_PRI       처리우선순위
                                                    TMS_FILE_CNT   회차별녹취파일수
                                                    PROD_CAT       보종구분
                                                    PLN_CD         플랜코드
                                                }
    @param      chk_app_ord:                    CHK_APP_ORD(항목적용차수)
    @param      dtc_sec_info_dict:              QA output list
    @return                                     QA_SCRP_DTCT_RST_TB data
    """
    global DTC_INFO_DICT
    global C_REC_INFO_DICT
    all_seq = 1
    fd_kwd_dict = dict()
    overlap_check_dict = dict()
    scrp_dtct_bind_list = list()
    log.info("\t  --> Extract data from QA output list")
    for sec_key, sec_info_list in dtc_sec_info_dict.items():
        """
        sec_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd)
        sec_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, cusl_cd, kwd_seq)
        """
        for item, dtc_cate in sec_info_list:
            """
            item = {
                'TMS_INFO'          : 회차정보
                'REC_KEY'           : 녹취KEY
                'REC_ID'            : 녹취ID
                'REC_FILE_NM'       : 녹취파일명
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'tmp_line_no'       : 전체 STT 임시 문장번호
                'DTCT_KWD'          : 탐지키워드
            }
            """
            scrp_lctg_cd = None
            scrp_mctg_cd = None
            scrp_sctg_cd = None
            scrp_app_ord = None
            cusl_cd = None
            cusl_app_ord = None
            cust_reply_info = {
                'reply_yn': None,
                'reply_nor_yn': None,
                'reply_stmt': None,
                'reply_st_tm': None,
                'reply_ed_tm': None
            }
            dtc_cate_list = dtc_cate.split('!@#$')
            chk_ctg_cd = dtc_cate_list[0]
            if chk_ctg_cd == 'S':  # 스크립트
                """
                dtc_cate_list = [
                    'CHK_CTG_CD',           평가구분코드
                    'CHK_ITM_CD',           평가항목코드
                    'SCRP_LCTG_CD',         스크립트대분류코드   
                    'SCRP_MCTG_CD',         스크립트중분류코드
                    'SCRP_SCTG_CD',         스크립트소분류코드
                    'SCRP_APP_ORD'          스크립트적용차수
                    'STMT_SEQ',             문장순서
                    'DTCT_DTC_NO',          탐지사전번호
                    'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                    'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                    'DTCT_DTC_ED_NO',       탐지사전끝번호
                ]
                """
                chk_itm_cd = dtc_cate_list[1]
                scrp_lctg_cd = dtc_cate_list[2]
                scrp_mctg_cd = dtc_cate_list[3]
                scrp_sctg_cd = dtc_cate_list[4]
                scrp_app_ord = dtc_cate_list[5]
                stmt_seq = dtc_cate_list[6]
                dtct_dtc_no = dtc_cate_list[7]
                # 구간 키
                sec_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd)
                # 문장 키
                dtc_key = '{0}_{1}_{2}_{3}_{4}'.format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq)
                # 고객답변정보
                if dtc_key in CUST_REPLY_NCSS_YN_DICT:
                    cust_reply_info = extract_cust_reply_info(item, dtc_key, cust_reply_info)
                # 평가항목결과테이블
                """
                    dtc_key: {
                        'chk_ctg_cd': item['CHK_CTG_CD'],
                        'esty_scrp_yn': item['ESTY_SCRP_YN'],
                        'cust_ans_yn': item['CUST_ANS_YN'],
                        'dtc_list': list()
                    }
                """
                if cust_reply_info['reply_yn'] == 'Y' and cust_reply_info['reply_nor_yn'] == 'Y':
                    ans_rst = 'Y'
                else:
                    ans_rst = 'N'
                item['ans_rst'] = ans_rst
                DTC_INFO_DICT[sec_key][dtc_key]['dtc_list'].append(item)
                # REC_BIZ_CD = C 청약콜
                if scrp_sctg_cd == 'S2001':
                    if item['REC_KEY'] in C_REC_INFO_DICT:
                        if C_REC_INFO_DICT[item['REC_KEY']]['st_info'][0]:
                            if int(C_REC_INFO_DICT[item['REC_KEY']]['st_info'][0]) > int(item['STMT_NO']):
                                C_REC_INFO_DICT[item['REC_KEY']]['st_info'] = (item['STMT_NO'], item['STMT_ST_TM'])
                        else:
                            C_REC_INFO_DICT[item['REC_KEY']]['st_info'] = (item['STMT_NO'], item['STMT_ST_TM'])
                if scrp_sctg_cd == 'S0102':
                    if item['REC_KEY'] in C_REC_INFO_DICT:
                        if C_REC_INFO_DICT[item['REC_KEY']]['ed_info'][0]:
                            if int(C_REC_INFO_DICT[item['REC_KEY']]['ed_info'][0]) < int(item['STMT_NO']):
                                C_REC_INFO_DICT[item['REC_KEY']]['ed_info'] = (item['STMT_NO'], item['STMT_ED_TM'])
                        else:
                            C_REC_INFO_DICT[item['REC_KEY']]['ed_info'] = (item['STMT_NO'], item['STMT_ED_TM'])
            elif chk_ctg_cd == 'C':  # 상담(금지어)
                """
                    dtc_cate_list = [
                        'CHK_CTG_CD',           평가구분코드
                        'CHK_ITM_CD',           평가항목코드
                        'CUSL_CD',              상담코드   
                        'CUSL_APP_ORD',         상담적용차수
                        'KWD_SEQ',              키워드순서
                        'DTCT_DTC_NO',          탐지사전번호
                        'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                        'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                        'DTCT_DTC_ED_NO',       탐지사전끝번호
                    ]
                """
                chk_itm_cd = dtc_cate_list[1]
                cusl_cd = dtc_cate_list[2]
                kwd_seq = dtc_cate_list[4]
                cusl_app_ord = dtc_cate_list[3]
                dtct_dtc_no = dtc_cate_list[5]
                # 문장 키
                dtc_key = "{0}_{1}_{2}".format(chk_itm_cd, cusl_cd, kwd_seq)
                # 평가항목결과테이블
                """
                    dtc_key: {
                        'chk_ctg_cd': item['CHK_CTG_CD'],
                        'esty_scrp_yn': item['ESTY_SCRP_YN'],
                        'cust_ans_yn': item['CUST_ANS_YN'],
                        'dtc_list': list()
                    }
                """
                item['ans_rst'] = None
                DTC_INFO_DICT[dtc_key][dtc_key]['dtc_list'].append(item)
                if cusl_cd == 'FD0002':
                    key = (item['REC_KEY'], item['STMT_NO'].strip())
                    if key in fd_kwd_dict:
                        if dtct_dtc_no not in fd_kwd_dict[key]:
                            fd_kwd_dict[key].append(dtct_dtc_no)
                    else:
                        fd_kwd_dict[key] = [dtct_dtc_no]
            else:
                continue
            check_key = (
                chk_ctg_cd, item['REC_KEY'], item['STMT_NO'],
                chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, cusl_cd
            )
            if check_key in overlap_check_dict:
                continue
            overlap_check_dict[check_key] = True
            if item['STMT']:
                enc_stmt = libpcpython.PcAPI_enc_id(
                    PETRA_SID, config.PetraConfig.enc_cold_id, item['STMT'], len(item['STMT']))
            else:
                enc_stmt = item['STMT']
            if cust_reply_info['reply_stmt']:
                enc_reply_stmt = libpcpython.PcAPI_enc_id(
                    PETRA_SID,
                    config.PetraConfig.enc_cold_id,
                    cust_reply_info['reply_stmt'],
                    len(cust_reply_info['reply_stmt'])
                )
            else:
                enc_reply_stmt = cust_reply_info['reply_stmt']
            bind = (
                job['NSPL_NO'],
                job['TMS_INFO'],
                chk_ctg_cd,
                item['REC_KEY'],
                item['STMT_NO'].strip(),
                all_seq,
                item['SPK_DIV_CD'],
                job['PROD_CAT'],
                chk_app_ord,
                chk_itm_cd,
                scrp_lctg_cd,
                scrp_mctg_cd,
                scrp_sctg_cd,
                scrp_app_ord,
                cusl_cd,
                cusl_app_ord,
                item['STMT_ST_TM'],
                item['STMT_ED_TM'],
                enc_stmt,
                cust_reply_info['reply_yn'],
                cust_reply_info['reply_nor_yn'],
                cust_reply_info['reply_st_tm'],
                cust_reply_info['reply_ed_tm'],
                enc_reply_stmt,
                dtct_dtc_no,
                item['DTCT_KWD'],
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                HOST_NM,
                datetime.fromtimestamp(time.time())
            )
            scrp_dtct_bind_list.append(bind)
            all_seq += 1
    return fd_kwd_dict, scrp_dtct_bind_list


def extract_qa_ta_output(log, hmd_output_list, cate_check_dict):
    """
    Extract data from QA TA output
    @param      log:                    Logger object
    @param      hmd_output_list:        HMD output list
    @param      cate_check_dict:        Category Dictionary
    @return:                            HMD output dictionary
    """
    # Modifying HMD output
    log.info("\t  --> Modifying HMD output")
    # 탐지 구간 정보 추출
    pre_sec_st_line_no = ''
    prev_record_file_name = ''
    pre_sec_list = list()
    tmp_sec_line_no = ''
    tmp_sec_list = list()
    dtc_sec_info_dict = collections.OrderedDict()
    for item, dtc_cat_dict in hmd_output_list:
        """
            item = {
                'TMS_INFO'          : 회차정보
                'REC_KEY'           : 녹취KEY
                'REC_ID'            : 녹취ID
                'REC_FILE_NM'       : 녹취파일명
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
        """
        # 고객 문장일 경우 Skip
        #if item['SPK_DIV_CD'] == 'C':
        #    continue
        # 구간 탐지 중 새로운 녹취 파일시 구간 정보 리셋
        if len(prev_record_file_name) < 1:
            prev_record_file_name = item['REC_FILE_NM']
        if prev_record_file_name != item['REC_FILE_NM']:
            pre_sec_st_line_no = ''
            prev_record_file_name = ''
            pre_sec_list = list()
            tmp_sec_line_no = ''
            tmp_sec_list = list()
        for dtc_cate, value_list in dtc_cat_dict.items():
            dtc_cate_list = dtc_cate.split('!@#$')
            chk_ctg_cd = dtc_cate_list[0]
            # 탐지 된 키워드
            dtct_kwd_list = list()
            for value in value_list:
                dtct_kwd = value[0]
                if dtct_kwd not in dtct_kwd_list:
                    dtct_kwd_list.append(dtct_kwd)
            tmp_dtct_kwd = ','.join(dtct_kwd_list)
            if len(tmp_dtct_kwd.encode('utf-8')) > 300:
                item['DTCT_KWD'] = dtct_kwd_list[0]
            else:
                item['DTCT_KWD'] = tmp_dtct_kwd
            if chk_ctg_cd == 'S':  # 스크립트
                """
                    dtc_cate_list = [
                        'CHK_CTG_CD',           평가구분코드
                        'CHK_ITM_CD',           평가항목코드
                        'SCRP_LCTG_CD',         스크립트대분류코드   
                        'SCRP_MCTG_CD',         스크립트중분류코드
                        'SCRP_SCTG_CD',         스크립트소분류코드
                        'SCRP_APP_ORD'          스크립트적용차수
                        'STMT_SEQ',             문장순서
                        'DTCT_DTC_NO',          탐지사전번호
                        'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                        'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                        'DTCT_DTC_ED_NO',       탐지사전끝번호
                    ]
                """
                if item['SPK_DIV_CD'] == 'C':
                    continue
                chk_itm_cd = dtc_cate_list[1]
                scrp_lctg_cd = dtc_cate_list[2]
                scrp_mctg_cd = dtc_cate_list[3]
                scrp_sctg_cd = dtc_cate_list[4]
                stmt_seq = dtc_cate_list[6]
                dtct_dtc_no = dtc_cate_list[7]
                dtct_dtc_grp_no = dtc_cate_list[8]
                # 구간 키
                sec_key = "{0}_{1}_{2}_{3}".format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd)
                # 문장 키
                dtc_key = "{0}_{1}_{2}_{3}_{4}".format(chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq)
                # 카테고리 키
                cate_key = (
                    chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq, dtct_dtc_no, dtct_dtc_grp_no
                )
                if cate_key in cate_check_dict:
                    if item['tmp_line_no'] in cate_check_dict[cate_key]:
                        # 문장이 시작 문장과 그 이전 구간에 동시에 탐지 될 경우
                        if sec_key in pre_sec_list and item['tmp_line_no'] == pre_sec_st_line_no:
                            prev_flag = True
                        else:
                            prev_flag = False
                        # 같은 구간일 경우
                        if sec_key in tmp_sec_list or prev_flag:
                            dtc_sec_info_dict[sec_key].append((dict(item), dtc_cate))
                        # 시작 문장일 경우
                        elif dtc_key in ST_SECT_INFO_DICT:
                            # 동일한 문장 번호일 경우
                            if item['tmp_line_no'] == tmp_sec_line_no:
                                tmp_sec_list.append(sec_key)
                                if sec_key not in dtc_sec_info_dict:
                                    dtc_sec_info_dict[sec_key] = [(dict(item), dtc_cate)]
                                else:
                                    dtc_sec_info_dict[sec_key].append((dict(item), dtc_cate))
                            # 새로운 문장 번호일 경우
                            else:
                                pre_sec_list = tmp_sec_list
                                pre_sec_st_line_no = item['tmp_line_no']
                                tmp_sec_list = list()
                                tmp_sec_list.append(sec_key)
                                tmp_sec_line_no = item['tmp_line_no']
                                if sec_key not in dtc_sec_info_dict:
                                    dtc_sec_info_dict[sec_key] = list()
                                dtc_sec_info_dict[sec_key].append((dict(item), dtc_cate))
                        else:
                            continue
            elif chk_ctg_cd == 'C':  # 상담(금지어)
                """
                    dtc_cate_list = [    
                        'CHK_CTG_CD',           평가구분코드
                        'CHK_ITM_CD',           평가항목코드
                        'CUSL_CD',              상담코드   
                        'CUSL_APP_ORD',         상담적용차수
                        'KWD_SEQ',              키워드순서
                        'DTCT_DTC_NO',          탐지사전번호
                        'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                        'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                        'DTCT_DTC_ED_NO',       탐지사전끝번호
                    ]
                """
                chk_itm_cd = dtc_cate_list[1]
                cusl_cd = dtc_cate_list[2]
                kwd_seq = dtc_cate_list[4]
                dtct_dtc_no = dtc_cate_list[5]
                dtct_dtc_grp_no = dtc_cate_list[6]
                # 구간 키
                sec_key = "{0}_{1}_{2}".format(chk_itm_cd, cusl_cd, kwd_seq)
                # 카테고리 키
                cate_key = (
                    chk_itm_cd, cusl_cd, kwd_seq, dtct_dtc_no, dtct_dtc_grp_no
                )
                if cate_key in cate_check_dict:
                    if item['tmp_line_no'] in cate_check_dict[cate_key]:
                        if sec_key not in dtc_sec_info_dict:
                            dtc_sec_info_dict[sec_key] = list()
                        dtc_sec_info_dict[sec_key].append((dict(item), dtc_cate))
            else:
                continue
    return dtc_sec_info_dict


def execute_nlp(log, nlp_client, stt_result_list):
    """
    Execute NLP
    @param      log:                    Logger
    @param      nlp_client:             NLP client
    @param      stt_result_list:        STT results list
    @return:                            NLP output list
    """
    try:
        log.info('\t  --> NLP analyzing ...')
        tmp_line_no = 1
        nlp_output_list = list()
        for item in stt_result_list:
            """
                item = {
                    'TMS_INFO'          : 회차정보
                    'REC_KEY'           : 녹취KEY
                    'REC_ID'            : 녹취ID
                    'REC_FILE_NM'       : 녹취파일명
                    'REC_BIZ_CD'        : 녹취업무코드
                    'STMT_NO'           : 문장번호
                    'SPK_DIV_CD'        : 화자구분코드
                    'STMT_ST_TM'        : 문장시작시간
                    'STMT_ED_TM'        : 문장종료시간
                    'STMT'              : 원문
                }
            """
            """
            item = {
                'ISP_TMS'           : 심사회차
                'REC_ID'            : 녹취ID
                'RECORD_FILE_NAME'  : 녹취파일명
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_END_TM'       : 문장종료시간
                'STMT'              : 원문
            }
            """
            try:
                result = nlp_client.analyze(item['STMT'])
                sent, nlp_sent, morph_sent = result
                item['nlp_sent'] = nlp_sent
                item['tmp_line_no'] = tmp_line_no
            except Exception:
                log.error("  Can't analyze NLP")
                log.error("  File name --> {0}".format(item['REC_FILE_NM']))
                log.error("  Sentence --> {0}".format(item['STMT']))
                log.error(traceback.format_exc())
                continue
            nlp_output_list.append(item)
            tmp_line_no += 1
        log.info('\t  --> Done NLP')
        return nlp_output_list
    except Exception:
        raise Exception(traceback.format_exc())


def execute_qa_hmd(log, hmd_client, hmd_model, total_stt_results):
    """
    Execute QA HMD
    @param      log:                    Logger
    @param      hmd_client:             HMD Client
    @param      hmd_model:              HMD model
    @param      total_stt_results:      Total STT results
    @return:                            HMD output list and Category dictionary
    """
    global CUST_REPLY_INFO_DICT
    log.info('\t  --> Load NLP client')
    nlp_client = nlp.NlpClient(config.QAConfig.nlp_engine)
    nlp_output_list = execute_nlp(log, nlp_client, total_stt_results)
    log.info("\t  --> HMD analyzing ...")
    hmd_output_list = list()
    for item in nlp_output_list:
        """
            item = {
                'TMS_INFO'          : 회차정보
                'REC_KEY'           : 녹취KEY
                'REC_ID'            : 녹취ID
                'REC_FILE_NM'       : 녹취파일명
                'REC_BIZ_CD'        : 녹취업무코드
                'REC_DU_TM'         : 통화시간
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'nlp_sent'          : 형태소 분석 문장 
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
        """
        try:
            # 고객 답변 정보
            if item['SPK_DIV_CD'].upper() == 'C':
                key = ('C', item['REC_FILE_NM'], item['STMT_NO'].strip())
                CUST_REPLY_INFO_DICT[key] = (item['STMT_ST_TM'], item['STMT_ED_TM'], item['STMT'])
            if item['SPK_DIV_CD'].upper() == 'M':
                key = ('M', item['REC_FILE_NM'], item['STMT_NO'].strip())
                CUST_REPLY_INFO_DICT[key] = (item['STMT_ST_TM'], item['STMT_ED_TM'], item['STMT'])
            dtc_cat_result = hmd_client.execute_hmd(item['STMT'], item['nlp_sent'], hmd_model)
        except Exception:
            log.error(traceback.format_exc())
            log.error("\t  --> Can't analyze HMD")
            log.error("\t  --> Sentence -> {0}".format(item['STMT']))
            continue
        hmd_output_list.append((item, dtc_cat_result))
    log.info('\t  --> Setup category dictionary')
    dtc_cate_dict = dict()
    cate_idx_dict = dict()
    for item, dtc_cat_result in hmd_output_list:
        """
            item = {
                'TMS_INFO'          : 회차정보
                'REC_KEY'           : 녹취KEY
                'REC_ID'            : 녹취ID
                'REC_FILE_NM'       : 녹취파일명
                'REC_BIZ_CD'        : 녹취업무코드
                'REC_DU_TM'         : 통화시간
                'STMT_NO'           : 문장번호
                'SPK_DIV_CD'        : 화자구분코드
                'STMT_ST_TM'        : 문장시작시간
                'STMT_ED_TM'        : 문장종료시간
                'STMT'              : 원문
                'nlp_sent'          : 형태소 분석 문장 
                'tmp_line_no'       : 전체 STT 임시 문장번호
            }
        """
        if dtc_cat_result:
            for dtc_cate, value_list in dtc_cat_result.items():
                dtc_cate_list = dtc_cate.split('!@#$')
                chk_ctg_cd = dtc_cate_list[0]
                if chk_ctg_cd == 'S': # 스크립트
                    """
                        dtc_cate_list = [
                            'CHK_CTG_CD',           평가구분코드
                            'CHK_ITM_CD',           평가항목코드
                            'SCRP_LCTG_CD',         스크립트대분류코드
                            'SCRP_MCTG_CD',         스크립트중분류코드
                            'SCRP_SCTG_CD',         스크립트소분류코드
                            'SCRP_APP_ORD'          스크립트적용차수
                            'STMT_SEQ',             문장순서
                            'DTCT_DTC_NO',          탐지사전번호
                            'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                            'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                            'DTCT_DTC_ED_NO',       탐지사전끝번호
                        ]
                    """
                    chk_itm_cd = dtc_cate_list[1]
                    scrp_lctg_cd = dtc_cate_list[2]
                    scrp_mctg_cd = dtc_cate_list[3]
                    scrp_sctg_cd = dtc_cate_list[4]
                    stmt_seq = dtc_cate_list[6]
                    dtct_dtc_no = dtc_cate_list[7]
                    dtct_dtc_grp_no = dtc_cate_list[8]
                    dtct_dtc_grp_in_no = dtc_cate_list[9]
                    dtct_dtc_grp_ed_no = dtc_cate_list[10]
                    # 카테고리 키
                    cate_key = (
                        chk_itm_cd, scrp_lctg_cd, scrp_mctg_cd, scrp_sctg_cd, stmt_seq, dtct_dtc_no, dtct_dtc_grp_no
                    )
                    if cate_key not in cate_idx_dict:
                        cate_idx_dict[cate_key] = (dtct_dtc_grp_ed_no, [dtct_dtc_grp_in_no])
                    else:
                        cate_idx_dict[cate_key][1].append(dtct_dtc_grp_in_no)
                    if cate_key in dtc_cate_dict:
                        if dtct_dtc_grp_in_no not in dtc_cate_dict[cate_key]:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no] = [item['tmp_line_no']]
                        else:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no].append(item['tmp_line_no'])
                    else:
                        dtc_cate_dict[cate_key] = {dtct_dtc_grp_in_no: [item['tmp_line_no']]}
                elif chk_ctg_cd == 'C': # 상담(금지어)
                    """
                        dtc_cate_list = [
                            'CHK_CTG_CD',           평가구분코드
                            'CHK_ITM_CD',           평가항목코드
                            'CUSL_CD',              상담코드   
                            'CUSL_APP_ORD',         상담적용차수
                            'KWD_SEQ',              키워드순서
                            'DTCT_DTC_NO',          탐지사전번호
                            'DTCT_DTC_GRP_NO',      탐지사전그룹번호
                            'DTCT_DTC_GRP_IN_NO',   탐지사전그룹내순서
                            'DTCT_DTC_ED_NO',       탐지사전끝번호
                        ]
                    """
                    chk_itm_cd = dtc_cate_list[1]
                    cusl_cd = dtc_cate_list[2]
                    kwd_seq = dtc_cate_list[4]
                    dtct_dtc_no = dtc_cate_list[5]
                    dtct_dtc_grp_no = dtc_cate_list[6]
                    dtct_dtc_grp_in_no = dtc_cate_list[7]
                    dtct_dtc_grp_ed_no = dtc_cate_list[8]
                    # 카테고리 키
                    cate_key = (chk_itm_cd, cusl_cd, kwd_seq, dtct_dtc_no, dtct_dtc_grp_no)
                    if cate_key not in cate_idx_dict:
                        cate_idx_dict[cate_key] = (dtct_dtc_grp_ed_no, [dtct_dtc_grp_in_no])
                    else:
                        cate_idx_dict[cate_key][1].append(dtct_dtc_grp_in_no)
                    if cate_key in dtc_cate_dict:
                        if dtct_dtc_grp_in_no not in dtc_cate_dict[cate_key]:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no] = [item['tmp_line_no']]
                        else:
                            dtc_cate_dict[cate_key][dtct_dtc_grp_in_no].append(item['tmp_line_no'])
                    else:
                        dtc_cate_dict[cate_key] = {dtct_dtc_grp_in_no: [item['tmp_line_no']]}
                else:
                    continue
    log.info('\t  --> Check category')
    cate_check_dict = dict()
    for cate_key, value in cate_idx_dict.items():
        flag = True
        end_idx = int(value[0])
        idx_list = value[1]
        for cnt in range(1, end_idx + 1):
            if str(cnt) in idx_list:
                continue
            else:
                flag = False
        if flag:
            output_list = make_target_line(end_idx, config.QAConfig.hmd_line_gap, dtc_cate_dict[cate_key])
            cate_check_dict[cate_key] = output_list
    return hmd_output_list, cate_check_dict


def make_qa_hmd_model(log, db, job, chk_app_ord, hmd_client, cate_info_results):
    """
    Make HMD Tree dictionary
    @param      log:                        Logger object
    @param      db:                         DB object
    @param      job:                        Job{
                                                NSPL_NO        계약번호
                                                TMS_INFO       회차정보
                                                PROD_CD        상품코드
                                                PROD_NM        상품명
                                                CONTRACT_DT    청약일자
                                                QA_REQ_TM      QA요청일자
                                                PROC_PRI       처리우선순위
                                                TMS_FILE_CNT   회차별녹취파일수
                                                PROD_CAT       보종구분
                                                PLN_CD         플랜코드
                                            }
    @param      chk_app_ord:                CHK_APP_ORD(항목적용차수)
    @param      hmd_client:                 HMD Client
    @param      cate_info_results:          Category information Results
    @return:                                HMD model, QA_CTR_SCRP_TB insert bind list
    """
    global DTC_INFO_DICT
    global DELETE_FILE_LIST
    global SCRPT_HK_CD_DICT
    global ST_SECT_INFO_DICT
    global TOTAL_CHK_ITM_CD_DICT
    global CUST_REPLY_NCSS_YN_DICT
    log.info("\t  --> Make HMD category and rule ..")
    hmd_model_list = list()
    ctr_scrp_bind_list = list()
    overlap_check_dict = dict()
    all_seq = 1
    for item in cate_info_results:
        """
        item = {
            CHK_CTG_CD          평가구분코드
            CHK_ITM_CD          평가항목코드
            SCRP_LCTG_CD        스크립트대분류코드
            SCRP_MCTG_CD        스크립트중분류코드
            SCRP_SCTG_CD        스크립트중분류코드
            SCRP_APP_ORD        스크립트적용차수
            ST_SCRP_YN          시작문장여부
            ESTY_SCRP_YN        필수문장여부
            STMT_SEQ            문장순서
            DTCT_DTC_NO         탐지사전번호
            DTCT_DTC_GRP_NO     탐지사전그룹번호
            DTCT_DTC_GRP_IN_NO  탐지사전그룹내순서
            DTCT_DTC_ED_NO      탐지사전끝번호
            DTCT_DTC_CON        탐지사전내용
            GUD_STMT            가이드문장
            SCRP                스크립트
            ANS_CD              답변코드
            CUST_ANS_YN         고객답변필요여부
        }
        OR
        item = {
            CHK_CTG_CD          평가구분코드
            CHK_ITM_CD          평가항목코드
            CUSL_CD             상담코드
            CUSL_APP_ORD        상담적용차수
            KWD_SEQ             키워드순서
            DTCT_DTC_NO         탐지사전번호
            DTCT_DTC_GRP_NO     탐지사전그룹번호
            DTCT_DTC_GRP_IN_NO  탐지사전그룹내순서
            DTCT_DTC_ED_NO      탐지사전끝번호
            DTCT_DTC_CON        탐지사전내용
            GUD_STMT            가이드문장
            SCRP                스크립트
            SNS_YN              SNS전송여부
        }
        """
        for key, value in item.items():
            item[key] = str(value).strip()
        if item['CHK_CTG_CD'] == 'S': # 스크립트
            category = [
                str(item['CHK_CTG_CD']).strip(),
                str(item['CHK_ITM_CD']).strip(),
                str(item['SCRP_LCTG_CD']).strip(),
                str(item['SCRP_MCTG_CD']).strip(),
                str(item['SCRP_SCTG_CD']).strip(),
                str(item['SCRP_APP_ORD']).strip(),
                str(item['STMT_SEQ']).strip(),
                str(item['DTCT_DTC_NO']).strip(),
                str(item['DTCT_DTC_GRP_NO']).strip(),
                str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                str(item['DTCT_DTC_ED_NO']).strip()
            ]
            # Heungkuk T-life HMD
            hk_sec_key = "{0}_{1}".format(str(item['SCRP_LCTG_CD']).strip(), str(item['SCRP_MCTG_CD']).strip())
            if hk_sec_key in SCRPT_HK_CD_DICT:
                SCRPT_HK_CD_DICT[hk_sec_key].append(str(item['SCRP_SCTG_CD']).strip())
            else:
                SCRPT_HK_CD_DICT[hk_sec_key] = [str(item['SCRP_SCTG_CD']).strip()]
            if 'None' in category:
                log.error("\t    --> Category is Not NULL -> {0}".format(category))
                continue
            hmd_model_list.append((category, item['DTCT_DTC_CON']))
            # 구간 키
            sec_key = "{0}_{1}_{2}_{3}".format(
                item['CHK_ITM_CD'], item['SCRP_LCTG_CD'], item['SCRP_MCTG_CD'], item['SCRP_SCTG_CD'])
            # 문장 키
            dtc_key = "{0}_{1}_{2}_{3}_{4}".format(
                item['CHK_ITM_CD'], item['SCRP_LCTG_CD'], item['SCRP_MCTG_CD'], item['SCRP_SCTG_CD'], item['STMT_SEQ'])
            # 평가항목필수문장 저장
            if sec_key in TOTAL_CHK_ITM_CD_DICT:
                if item['ESTY_SCRP_YN'].upper() == 'Y':
                    TOTAL_CHK_ITM_CD_DICT[sec_key].append(item['STMT_SEQ'])
            else:
                if item['ESTY_SCRP_YN'].upper() == 'Y':
                    TOTAL_CHK_ITM_CD_DICT[sec_key] = [item['STMT_SEQ']]
            # 탐지항목정보 저장
            tmp_dtc_info = {
                'chk_ctg_cd': item['CHK_CTG_CD'],
                'esty_scrp_yn': item['ESTY_SCRP_YN'],
                'cust_ans_yn': item['CUST_ANS_YN'],
                'dtc_list': list()
            }
            if sec_key in DTC_INFO_DICT:
                if dtc_key in DTC_INFO_DICT[sec_key]:
                    DTC_INFO_DICT[sec_key].update({dtc_key:tmp_dtc_info})
                else:
                    DTC_INFO_DICT[sec_key][dtc_key] = tmp_dtc_info
            else:
                DTC_INFO_DICT[sec_key] = {dtc_key:tmp_dtc_info}
            # 구간 시작 문장
            if item['ST_SCRP_YN'].upper() == 'Y':
                ST_SECT_INFO_DICT[dtc_key] = True
            # 고객 답변 필수 문장
            if item['CUST_ANS_YN'].upper() == 'Y':
                result = util.select_ans_cont(db, item['ANS_CD'])
                ans_cont_list = list()
                for ans_cont in result:
                    ans_cont_list.append(ans_cont['ANS_CONT'])
                CUST_REPLY_NCSS_YN_DICT[dtc_key] = ans_cont_list
            # QA_CTR_SCRP_TB 중복제거
            check_key = (item['CHK_CTG_CD'], item['CHK_ITM_CD'], item['DTCT_DTC_NO'])
            if check_key in overlap_check_dict:
                continue
            overlap_check_dict[check_key] = True
            bind = (
                job['NSPL_NO'],
                job['TMS_INFO'],
                item['CHK_CTG_CD'],
                all_seq,
                job['PROD_CAT'],
                chk_app_ord,
                item['CHK_ITM_CD'],
                item['DTCT_DTC_NO'],
                item['GUD_STMT'],
                item['SCRP'],
                item['STMT_SEQ'],
                item['ESTY_SCRP_YN'],
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                HOST_NM,
                datetime.fromtimestamp(time.time())
            )
            ctr_scrp_bind_list.append(bind)
        elif item['CHK_CTG_CD'] == 'C':  # 상담(금지어)
            category = [
                str(item['CHK_CTG_CD']).strip(),
                str(item['CHK_ITM_CD']).strip(),
                str(item['CUSL_CD']).strip(),
                str(item['CUSL_APP_ORD']).strip(),
                str(item['KWD_SEQ']).strip(),
                str(item['DTCT_DTC_NO']).strip(),
                str(item['DTCT_DTC_GRP_NO']).strip(),
                str(item['DTCT_DTC_GRP_IN_NO']).strip(),
                str(item['DTCT_DTC_ED_NO']).strip()
            ]
            if 'None' in category:
                log.error("\t    --> Category is Not NULL -> {0}".format(category))
                continue
            hmd_model_list.append((category, item['DTCT_DTC_CON']))
            # 구간 키
            sec_key = "{0}_{1}_{2}".format(item['CHK_ITM_CD'], item['CUSL_CD'], item['KWD_SEQ'])
            # 문장 키
            dtc_key = "{0}_{1}_{2}".format(item['CHK_ITM_CD'], item['CUSL_CD'], item['KWD_SEQ'])
            # 평가항목필수문장 저장
            if sec_key in TOTAL_CHK_ITM_CD_DICT:
                TOTAL_CHK_ITM_CD_DICT[sec_key].append(item['KWD_SEQ'])
            else:
                TOTAL_CHK_ITM_CD_DICT[sec_key] = [item['KWD_SEQ']]
            if dtc_key in DTC_INFO_DICT:
                pass
            else:
                DTC_INFO_DICT[dtc_key] = {
                    dtc_key:{
                        'chk_ctg_cd': item['CHK_CTG_CD'],
                        'esty_scrp_yn': None,
                        'cust_ans_yn': None,
                        'dtc_list': list()
                    }
                }
            # QA_CTR_SCRP_TB 중복제거
            check_key = (item['CHK_CTG_CD'], item['CHK_ITM_CD'], item['DTCT_DTC_NO'])
            if check_key in overlap_check_dict:
                continue
            overlap_check_dict[check_key] = True
            bind = (
                job['NSPL_NO'],
                job['TMS_INFO'],
                item['CHK_CTG_CD'],
                all_seq,
                job['PROD_CAT'],
                chk_app_ord,
                item['CHK_ITM_CD'],
                item['DTCT_DTC_NO'],
                item['GUD_STMT'],
                item['SCRP'],
                item['KWD_SEQ'],
                None,
                HOST_NM,
                datetime.fromtimestamp(time.time()),
                HOST_NM,
                datetime.fromtimestamp(time.time())
            )
            ctr_scrp_bind_list.append(bind)
        else:
            log.error("\t  --> CHK_CTG_CD(평가구분코드) C(상담) or S(스크립트) not {0} ..".format(item['CHK_CTG_CD']))
            continue
        all_seq += 1
    model_name = '{0}_{1}_qa'.format(job['NSPL_NO'], job['TMS_INFO'])
    hmd_model = False
    for _ in range(0, 3):
        try:
            log.info("\t  --> Make HMD model ..")
            hmd_client.set_model(model_name, hmd_model_list)
            log.info("\t  --> Load HMD model ..")
            hmd_model = hmd_client.load_hmd_model("!@#$", model_name)
            break
        except Exception:
            log.error(traceback.format_exc())
            log.info("\t  --> Retry make HMD model ..")
            log.info('\t  --> Waiting 20 seconds ...')
            time.sleep(20)
            continue
    log.info("\t  --> Make QA_CRT_SCRP_TB insert data")
    DELETE_FILE_LIST.append(hmd_client.hmd_model_path(model_name))
    if not hmd_model:
        raise Exception("Can't make HMD model")
    return hmd_model, ctr_scrp_bind_list


def select_qa_hmd_info(log, db, job):
    """
    Select CHK_APP_ORD(항목적용차수) and QA hmd
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            Job{
                                    NSPL_NO        계약번호
                                    TMS_INFO       회차정보
                                    PROD_CD        상품코드
                                    PROD_NM        상품명
                                    CONTRACT_DT    청약일자
                                    QA_REQ_TM      QA요청일자
                                    PROC_PRI       처리우선순위
                                    TMS_FILE_CNT   회차별녹취파일수
                                    PROD_CAT       보종구분
                                    PLN_CD         플랜코드
                                }
    @return:                    CHK_APP_ORD(항목적용차수), QA HMD
    """
    try:
        log.info("\t  --> Select CHK_APP_ORD(항목적용차수)")
        chk_app_ord = util.select_qa_chk_app_ord(db, job['PROD_CAT'], job['PROD_CD'], job['CONTRACT_DT'])
        log.info("\t    : CHK_APP_ORD(항목적용차수)= {0}".format(chk_app_ord))
        log.info("\t  --> Select QA script hmd")
        cate_info_results = util.select_qa_script_hmd(db, job['PROD_CAT'], job['PROD_CD'], job['PLN_CD'], chk_app_ord)
        log.info("\t  --> Select QA cusl hmd")
        cate_info_results += util.select_qa_cusl_hmd(db, job['PROD_CAT'], job['PROD_CD'], job['PLN_CD'], chk_app_ord)
    except Exception:
        log.error("\t  --> Can't select HMD information")
        raise Exception(traceback.format_exc())
    if not cate_info_results:
        raise Exception("No HMD QA data")
    log.info("\t  --> Success selected HMD")
    return chk_app_ord, cate_info_results


def execute_qa_ta(log, db, job, total_stt_results):
    """
    Execute QA TA
    @param      log:                    Logger object
    @param      db:                     DB object
    @param      job:                    Job{
                                            NSPL_NO        계약번호
                                            TMS_INFO       회차정보
                                            PROD_CD        상품코드
                                            PROD_NM        상품명
                                            CONTRACT_DT    청약일자
                                            QA_REQ_TM      QA요청일자
                                            PROC_PRI       처리우선순위
                                            TMS_FILE_CNT   회차별녹취파일수
                                            PROD_CAT       보종구분
                                            PLN_CD         플랜코드
                                        }
    @param      total_stt_results:      STT result {
                                            'TMS_INFO'          : 회차정보
                                            'REC_KEY'           : 녹취KEY
                                            'REC_ID'            : 녹취ID
                                            'REC_FILE_NM'       : 녹취파일명
                                            'REC_BIZ_CD'        : 녹취업무코드
                                            'REC_DU_TM'         : 통화시간
                                            'STMT_NO'           : 문장번호
                                            'SPK_DIV_CD'        : 화자구분코드
                                            'STMT_ST_TM'        : 문장시작시간
                                            'STMT_ED_TM'        : 문장종료시간
                                            'STMT'              : 원문
                                        }
    @return:                            {CREC_VOIC_SP, CREC_DU_TM, SREC_VOIC_SP, SREC_DU_TM}
    """
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    tmp_err_code = ''
    try:
        # QA 관련 HMD 사전 조회
        tmp_err_code = '03'
        log.info("  3-1) Select HMD information")
        chk_app_ord, cate_info_results = select_qa_hmd_info(log, db, job)
        # QA HMD 사전 생성과 QA_CRT_SCRP_TB insert data 생성
        tmp_err_code = '04'
        log.info("  3-2) Make QA HMD model and Make QA_CRT_SCRP_TB insert data")
        log.info("\t  --> Load HMD Client .. ")
        hmd_client = hmd.HmdClient()
        hmd_model, ctr_scrp_bind_list = make_qa_hmd_model(log, db, job, chk_app_ord, hmd_client, cate_info_results)
        # QA HMD 수행
        tmp_err_code = '05'
        log.info("  3-3) Execute QA HMD")
        hmd_output_list, cate_check_dict = execute_qa_hmd(log, hmd_client, hmd_model, total_stt_results)
        # 탐지된 HMD 결과 추출
        tmp_err_code = '06'
        log.info("  3-4) Extract QA HMD result")
        dtc_sec_info_dict = extract_qa_ta_output(log, hmd_output_list, cate_check_dict)
        # QA_SCRP_DTCT_RST_TB insert data 생성
        tmp_err_code = '07'
        log.info("  3-5) Make QA_SCRP_DTCT_RST_TB insert data")
        fd_kwd_dict, scrp_dtct_bind_list = make_scrp_dtct_data(log, job, chk_app_ord, dtc_sec_info_dict)
        # QA_CHK_RST_TB insert data 생성
        tmp_err_code = '08'
        log.info("  3-6) Make QA_CHK_RST_TB insert data")
        qa_chk_bind_list, hk_scrpt_list = make_chk_rst_data(log, job, chk_app_ord)
        # QA 결과 insert DB
        tmp_err_code = '09'
        log.info("  3-7) Insert QA output")
        insert_qa_output(log, db, job, ctr_scrp_bind_list, scrp_dtct_bind_list, qa_chk_bind_list)
        # T-life 전송 data extract
        tmp_err_code = '10'
        log.info("  3-8) Extract T-life QA output")
        result_info, qa_result = extract_hk_qa_result(log, job, fd_kwd_dict, hk_scrpt_list, total_stt_results)
        # T-life QA 결과 send T-life
        tmp_err_code = '11'
        log.info("  3-9) Send T-life QA output")
        requests_tlife_system(log, qa_result)
        return result_info
    except Exception:
        PROG_STAT_CD = '29'
        PROG_STAT_DTL_CD = 'QAERR' + tmp_err_code
        raise Exception(traceback.format_exc())


def select_stt_results(log, db, job):
    """
    Select STT all results
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            Job{
                                    NSPL_NO        계약번호
                                    TMS_INFO       회차정보
                                    PROD_CD        상품코드
                                    PROD_NM        상품명
                                    CONTRACT_DT    청약일자
                                    QA_REQ_TM      QA요청일자
                                    PROC_PRI       처리우선순위
                                    TMS_FILE_CNT   회차별녹취파일수
                                    PROD_CAT       보종구분
                                    PLN_CD         플랜코드
                                }
    @return:                    Minimum STT start time, Maximum STT end time, Total STT results, STT running Y/N
    """
    global C_REC_INFO_DICT
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        ctr_cal_rel_bind_list = list()
        stt_prog_stat_cd_info = util.select_stt_prog_stat_cd_info(db, job['NSPL_NO'], job['TMS_INFO'])
        # NSPL_NO(계약번호) 기준으로 STT 결과 조회 실패
        if not stt_prog_stat_cd_info:
            PROG_STAT_CD = '29' # QA오류
            PROG_STAT_DTL_CD = 'QAERR02' # STT변환오류
            log.info("\t  --> No STT results data [QA_CAL_INFO_TB(콜정보테이블), QA_CTR_CAL_REL_TB(계약콜관계테이블)]")
            return None, None, False, False
        del_target = dict()
        log.info("  2-1) Check overlap record file")
        target_stt_dict = dict()
        for item in stt_prog_stat_cd_info:
            log_str_list = list()
            log_str_list.append("\t  --> PROG_STAT_CD= {0}({1}),".format(item['PROG_STAT_CD'], item['CD_NM']))
            log_str_list.append("TMS_INFO= {0},".format(item['TMS_INFO']))
            log_str_list.append("TL_CD= {0},".format(item['TL_CD']))
            log_str_list.append("REC_BIZ_CD= {0},".format(item['REC_BIZ_CD']))
            log_str_list.append("REC_COMP_CD= {0},".format(item['REC_COMP_CD']))
            log_str_list.append("REC_FILE_NM= {0},".format(item['REC_FILE_NM']))
            log_str_list.append("REC_DU_TM= {0},".format(item['REC_DU_TM']))
            log_str_list.append("REC_KEY= {0}".format(item['REC_KEY']))
            log.info(' '.join(log_str_list))
            target_stt_dict[item['REC_KEY']] = True
            if item['PROG_STAT_CD'] == '14':
                log.info("\t  --> Real STT PROG_STAT_CD(14 실시간STT진행)")
                log.info("\t  --> Delete Real STT REC_KEY= {0}".format(item['REC_KEY']))
                util.delete_qa_cal_info(db, item['REC_KEY'])
                util.delete_stt_results(db, item['REC_KEY'])
                del target_stt_dict[item['REC_KEY']]
                continue
            key = (item['REC_ID'], item['REC_FILE_NM'])
            if key in del_target:
                if item['REC_COMP_CD'] == '100':
                    rec_key = del_target[key]['rec_key']
                    rec_biz_cd = del_target[key]['rec_biz_cd']
                    tl_cd = del_target[key]['tl_cd']
                else:
                    rec_key = item['REC_KEY']
                    rec_biz_cd = item['REC_BIZ_CD']
                    tl_cd = item['TL_CD']
                log.info("\t\t1) Overlap REC_ID= {0}, REC_FILE_NM= {1}".format(item['REC_ID'], item['REC_FILE_NM']))
                del target_stt_dict[rec_key]
                log.info("\t\t2) Update REC_ID= {0}, REC_FILE_NM= {1} --> REC_BIZ_CD= {2}, TL_CD= {3}".format(
                    item['REC_ID'], item['REC_FILE_NM'], rec_biz_cd, tl_cd))
                util.update_qa_cal_info(db, item['REC_ID'], item['REC_FILE_NM'], rec_biz_cd, tl_cd)
            del_target[key] = {
                "rec_key": item['REC_KEY'],
                "rec_biz_cd": item['REC_BIZ_CD'],
                "tl_cd": item['TL_CD']
            }
        log.info("  2-2) Check record STT status")
        flag = False
        tmp_cd = 100
        for item in stt_prog_stat_cd_info:
            """
            item = {
                'TMS_INFO'         : 회차정보
                'REC_KEY'          : 녹취KEY
                'REC_ID'           : 녹취ID
                'REC_FILE_NM'      : 녹취파일명
                'REC_BIZ_CD'       : 녹취업무코드
                'REC_COMP_CD'      : 녹취업체구분코드
                'PROG_STAT_CD'     : 진행상태코드
                'CD_NM'            : 코드명
                'STT_ST_TM'        : STT시작일시
                'STT_COM_TM'       : STT종료일시
                'REC_DU_TM'        : 통화시간
            }
            """
            if item['REC_KEY'] not in target_stt_dict:
                log.info("\t  --> Skip Overlap Record REC_KEY={0}, REC_ID= {1}, REC_FILE_NM= {2}".format(
                    item['REC_KEY'], item['REC_ID'], item['REC_FILE_NM']))
                continue
            log_str_list = list()
            log_str_list.append("\t  --> PROG_STAT_CD= {0}({1}),".format(item['PROG_STAT_CD'], item['CD_NM']))
            log_str_list.append("TMS_INFO= {0},".format(item['TMS_INFO']))
            log_str_list.append("TL_CD= {0},".format(item['TL_CD']))
            log_str_list.append("REC_BIZ_CD= {0},".format(item['REC_BIZ_CD']))
            log_str_list.append("REC_COMP_CD= {0},".format(item['REC_COMP_CD']))
            log_str_list.append("REC_FILE_NM= {0},".format(item['REC_FILE_NM']))
            log_str_list.append("REC_DU_TM= {0},".format(item['REC_DU_TM']))
            log_str_list.append("REC_KEY= {0}".format(item['REC_KEY']))
            log.info(' '.join(log_str_list))
            bind = (
                item['REC_KEY'],
                job['NSPL_NO'],
                job['TMS_INFO'],
                item['REC_ID'],
                item['REC_FILE_NM']
            )
            ctr_cal_rel_bind_list.append(bind)
            if item['REC_BIZ_CD'] == 'C':
                C_REC_INFO_DICT[item['REC_KEY']] = {
                    'st_info': ('', ''), 'ed_info': ('', ''), 'rec_du_tm': item['REC_DU_TM']
                }
            if item['PROG_STAT_CD'] == '13': # STT완료
                continue
            if int(item['PROG_STAT_CD']) < tmp_cd:
                tmp_cd = int(item['PROG_STAT_CD'])
            if item['PROG_STAT_CD'] in ('10', '11', '12'):
                flag = True
        if flag:
            log.info("\t  --> STT running... update PROG_STAT_CD to 20(QA점검등록)")
            util.update_qa_status_commit(
                db=db,
                prog_stat_cd='20',  # QA점검등록
                prog_stat_dtl_cd=PROG_STAT_DTL_CD,
                host_nm=HOST_NM,
                updated_tm=datetime.fromtimestamp(time.time()),
                nspl_no=job['NSPL_NO'],
                tms_info=job['TMS_INFO']
            )
            return None, None, False, True
        try:
            log.info("  2-3) Update REC_KEY(녹취KEY) QA_CTR_CAL_REL_TB(계약콜관계테이블)")
            log.info("\t  --> Update REC_KEY")
            util.update_ctr_cal_rel_info(db, ctr_cal_rel_bind_list)
        except Exception:
            log.error("\t  --> No STT results data [QA_CAL_INFO_TB(콜정보테이블), QA_CTR_CAL_REL_TB(계약콜관계테이블)]")
            pass
        #if tmp_cd in (19, ):
        #    PROG_STAT_CD = '29' # QA오류
        #    PROG_STAT_DTL_CD = 'QAERR01' # STT변환오류
        #    return None, None, False, False
        # STT 최초 시작시간 및 최종 종료시간 구하기
        log.info("  2-4) Extract STT process start and end time")
        fir_flag = True
        min_stt_st_dtm = ''
        max_stt_ed_dtm = ''
        for item in stt_prog_stat_cd_info:
            if not item['STT_ST_TM'] or not item['STT_COM_TM']:
                continue
            if item['TMS_INFO'] == job['TMS_INFO'] and item['PROG_STAT_CD'] == '13': #STT완료
                if fir_flag:
                    fir_flag = False
                    min_stt_st_dtm = item['STT_ST_TM']
                    max_stt_ed_dtm = item['STT_COM_TM']
                else:
                    if min_stt_st_dtm > item['STT_ST_TM']:
                        min_stt_st_dtm = item['STT_ST_TM']
                    if max_stt_ed_dtm < item['STT_COM_TM']:
                        max_stt_ed_dtm = item['STT_COM_TM']
        log.info("  2-5) Select STT results from QA_STT_RST_TB(STT결과테이블)")
        total_stt_results = util.select_stt_results(db, job['NSPL_NO'], job['TMS_INFO'])
        # NSPL_NO(계약번호) 기준으로 STT 결과 조회 실패
        if not total_stt_results:
            PROG_STAT_CD = '29' # QA오류
            PROG_STAT_DTL_CD = 'QAERR02' # STT변환오류
            log.info("\t  --> No STT results data [QA_CAL_INFO_TB(콜정보테이블), QA_CTR_CAL_REL_TB(계약콜관계테이블)]")
            return None, None, False, False
        for item in total_stt_results:
            """
                item = {
                    'TMS_INFO'          : 회차정보
                    'REC_KEY'           : 녹취KEY
                    'REC_ID'            : 녹취ID
                    'REC_FILE_NM'       : 녹취파일명
                    'REC_BIZ_CD'        : 녹취업무코드
                    'REC_DU_TM'         : 통화시간
                    'STMT_NO'           : 문장번호
                    'SPK_DIV_CD'        : 화자구분코드
                    'STMT_ST_TM'        : 문장시작시간
                    'STMT_ED_TM'        : 문장종료시간
                    'STMT'              : 원문
                }
            """
            if item['STMT']:
                dec_stmt = libpcpython.PcAPI_dec_id(
                    PETRA_SID, config.PetraConfig.enc_cold_id, item['STMT'], len(item['STMT']))
            else:
                dec_stmt = ''
            item['STMT'] = dec_stmt
        log.info("\t  --> Success selected STT results")
        return min_stt_st_dtm, max_stt_ed_dtm, total_stt_results, False
    except Exception:
        PROG_STAT_DTL_CD = 'QAERR02' # STT결과조회오류
        raise Exception(traceback.format_exc())


def update_qa_status_to_start(log, db, job):
    """
    Update QA status to start QA_CTR_LIST_TB(계약리스트테이블)
    @param      log:              Logger object
    @param      db:               DB object
    @param      job:              Job{
                                    NSPL_NO        계약번호
                                    TMS_INFO       회차정보
                                    PROD_CD        상품코드
                                    PROD_NM        상품명
                                    CONTRACT_DT    청약일자
                                    QA_REQ_TM      QA요청일자
                                    PROC_PRI       처리우선순위
                                    TMS_FILE_CNT   회차별녹취파일수
                                    PROD_CAT       보종구분
                                    PLN_CD         플랜코드
                                }
    """
    global HOST_NM
    global PROG_STAT_CD
    global PROG_STAT_DTL_CD
    try:
        log.info('  1-1) Update QA status to start(22)')
        util.update_qa_status_commit(
            db=db,
            prog_stat_cd='22', #QA진행
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            host_nm=HOST_NM,
            updated_tm=datetime.fromtimestamp(time.time()),
            nspl_no=job['NSPL_NO'],
            tms_info=job['TMS_INFO']
        )
    except Exception:
        PROG_STAT_CD = '29'
        PROG_STAT_DTL_CD = 'QAERR00'
        raise Exception(traceback.format_exc())
    log.info('\t  --> Done updated QA status')


def qa_process(log, db, job):
    """
    QA process
    @param      log:            Logger object
    @param      db:             DB object
    @param      job:            Job{
                                    NSPL_NO        계약번호
                                    TMS_INFO       회차정보
                                    PROD_CD        상품코드
                                    PROD_NM        상품명
                                    CONTRACT_DT    청약일자
                                    QA_REQ_TM      QA요청일자
                                    PROC_PRI       처리우선순위
                                    TMS_FILE_CNT   회차별녹취파일수
                                    PROD_CAT       보종구분
                                    PLN_CD         플랜코드
                                }
    """
    log.info('1. Update QA status to start QA_CTR_LIST_TB(계약리스트테이블)')
    update_qa_status_to_start(log, db, job)
    log.info('2. Select STT results')
    min_stt_st_dtm, max_stt_ed_dtm, total_stt_results, stt_running_flag = select_stt_results(log, db, job)
    if not stt_running_flag:
        if not total_stt_results:
            log.info('3. Update QA status to end and information to QA_CTR_LIST_TB(계약리스트테이블)')
            update_qa_ta_status_and_info(log, db, job, min_stt_st_dtm, max_stt_ed_dtm, dict())
        else:
            log.info('3. Execute QA TA')
            result_info = execute_qa_ta(log, db, job, total_stt_results)
            log.info('4. Update TA status to end and information to QA_CTR_LIST_TB(계약리스트테이블)')
            update_qa_ta_status_and_info(log, db, job, min_stt_st_dtm, max_stt_ed_dtm, result_info)


def main(job):
    """
    This program that execute QA
    @param      job:            Job{
                                    NSPL_NO        계약번호
                                    TMS_INFO       회차정보
                                    PROD_CD        상품코드
                                    PROD_NM        상품명
                                    CONTRACT_DT    청약일자
                                    QA_REQ_TM      QA요청일자
                                    PROC_PRI       처리우선순위
                                    TMS_FILE_CNT   회차별녹취파일수
                                    PROD_CAT       보종구분
                                    PLN_CD         플랜코드
                                }
    """
    # Ignore kill signal
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    # Set logger
    base_dir_path = "{0}/{1}".format(job['CONTRACT_DT'][:4], job['CONTRACT_DT'][4:8])
    log = logger.set_logger(
        logger_name=config.QAConfig.logger_name,
        log_dir_path=os.path.join(config.QAConfig.log_dir_path, base_dir_path),
        log_file_name='{0}_{1}.log'.format(job['NSPL_NO'], job['TMS_INFO']),
        log_level=config.QAConfig.log_level
    )
    # Execute QA
    log.info("[START] Execute QA ..")
    log_str = '''QA information
                                    PROC_PRI                처리우선순위 = {0}
                                    PROD_CAT                    보종구분 = {1}
                                    PLN_CD                      플랜코드 = {2}
                                    QA_REQ_TM                 QA요청일자 = {3}
                                    CONTRACT_DT                 청약일자 = {4}
                                    NSPL_NO                     계약번호 = {5}
                                    TMS_INFO                    회차정보 = {6}
                                    PROD_CD                     상품코드 = {7}
                                    PROD_NM                       상품명 = {8}
                                    TMS_FILE_CNT          회차별녹취파일수 = {9}'''.format(
        job['PROC_PRI'], job['PLN_CD'], job['PROD_CAT'], job['QA_REQ_TM'], job['CONTRACT_DT'], job['NSPL_NO'],
        job['TMS_INFO'], job['PROD_CD'], job['PROD_NM'], job['TMS_FILE_CNT']
    )
    job['PROD_CAT'] = 'L'
    job['PROD_CD'] = 'ZZZSCR'
    job['PLN_CD'] = '0'
    log.info(log_str)
    log.info(" * Change PROD_CAT= 'L', PROD_CD= 'ZZZSCR', PLN_CD= '0' * ")
    # Connect DB and Load Petra cipher API
    try:
        db = connect_db(log, 'ORACLE', config.OracleConfig)
        load_petra_api(log)
    except Exception:
        log.error(traceback.format_exc())
        log.error("[FAIL] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
        sys.exit(1)
    # Execute QA
    try:
        qa_process(log, db, job)
    except Exception:
        log.error(traceback.format_exc())
        log.info('Update QA_CTR_LIST_TB(계약리스트테이블) status to error code')
        # Update QA_CTR_LIST_TB(계약리스트테이블) status
        util.update_qa_status_commit(
            db=db,
            prog_stat_cd='29',
            prog_stat_dtl_cd=PROG_STAT_DTL_CD,
            host_nm=HOST_NM,
            updated_tm=datetime.fromtimestamp(time.time()),
            nspl_no=job['NSPL_NO'],
            tms_info=job['TMS_INFO']
        )
    finally:
        try:
            db.conn.commit()
            db.disconnect()
        except Exception:
            pass
        del_file(log)
        log.info("[E N D] Start time = {0}, The time required = {1}".format(PRO_ST_TM, elapsed_time(PRO_ST_TM)))
