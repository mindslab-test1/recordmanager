## BASE IMAGE
FROM docker.maum.ai:443/recm:6

## IMPORT SOURCE CODE
COPY ./record_collector/pcap_collector.py /root/recm/record_collector/pcap_collector.py
COPY ./record_manager/src /root/recm/record_manager/src/
RUN cd /root/recm/record_manager/src && sh ./build_proto.sh \
  && chmod +x version.py \
  && cd build \
  && cmake .. && make -j \
  && cp /root/recm/record_manager/src/build/RECMd /srv/maum/bin/  

## EXECUTE COMMAND
#ENTRYPOINT ["./entrypoint.sh"]

