#!/bin/bash
PATH=/srv/maum/bin:$PATH

# Start pcap collector with python
python /root/recm/record_collector/pcap_collector.py -c /root/recm/record_collector/pcap_collector.cfg &

# Start record manager 
/srv/maum/bin/RECMd -c /srv/maum/etc/recm.yaml &

# Wait for any process to exit
wait -n
  
# Exit with status of process that exited first
exit $?
