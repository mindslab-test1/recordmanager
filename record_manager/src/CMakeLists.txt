cmake_minimum_required(VERSION 2.8.9)

project (record_manager)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wfatal-errors")
set(EXE RECMd)
set(SOURCE_FILES
  app-variable.cpp
  maum/brain/w2l/w2l.grpc.pb.cc
  maum/brain/w2l/w2l.pb.cc
  stt-client.cpp
  voice-channel.cpp
  main.cpp
  session_pool.cpp
  record.pb.cc
)

set(CMAKE_CXX_STANDARD 11)

include_directories(${PROTOBUF_DIR}/include)
include_directories(${CMAKE_INSTALL_PREFIX}/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

link_directories(${PROTOBUF_DIR}/lib)
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

add_executable(${EXE}
  ${SOURCE_FILES}
)

add_custom_target(recm_version
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/version.py
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    VERBATIM
)
add_dependencies(${EXE} recm_version)

find_package(spdlog REQUIRED)

target_link_libraries(${EXE}
  protobuf
  zmq
  pthread
  spdlog::spdlog
  yaml-cpp
  grpc++
)
