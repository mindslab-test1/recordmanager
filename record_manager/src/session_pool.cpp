#include <sys/time.h>
#include "zmq.h"
#include "app-variable.h"
#include "session_pool.h"
#include "safe-queue.h"

#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/prettywriter.h"
#include "include/rapidjson/allocators.h"

void *Session::cmd_sender_;
std::mutex Session::cmd_lock_;

using namespace rapidjson;

Session::Session() {
    state_ = ST_IDLE;
    payload_cnt_ = 0;
}

Session::~Session() {
}

void Session::Run() {
  while (true) {
    Record record;
    record_q_.wait_and_pop(record);
    if (record.has_event()) {
      handle_event(record.event());
    } else if (record.has_voice()) {
      handle_voice(record.voice());
    }
  }
}

void Session::handle_event(Event event) {
  switch (state_) {
  case ST_IDLE: {
    if (event.type() == Event_EventType::Event_EventType_PREPARE) {
      record_key_ = event.record_key();
      voice_key_ = event.voice_key();
      info_.key = record_key_;
      info_.agent = event.from_user();
      state_ = ST_PREPARE;
      publish_event("CALLING");
      g_var.logger->debug("session prepared [idle->prepared], rec key: {}, voice key: {}", event.record_key(), voice_key_);
    } else {
      g_var.logger->debug("session state is [idle], ignore event type: {}", event.type());
    }
    break;
  }
  case ST_PREPARE: {
    if (event.type() == Event_EventType::Event_EventType_START) {
      state_ = ST_START;
      //info_.extra = event.extra();
      info_.agent = event.from_user();
      channels.emplace_back(new VoiceChannel(0));
      channels.emplace_back(new VoiceChannel(1));
      publish_event("CONNECTED");
      g_var.logger->debug("session started [prepared->started], rec key: {}, voice key: {}", event.record_key(), voice_key_);
    } else {
      g_var.logger->debug("session state is [prepared], ignore event type: {}", event.type());
    }
    break;
  }
  case ST_START: {
    if (event.type() == Event_EventType::Event_EventType_STOP) {
      state_ = ST_STOP;
	  g_var.logger->debug("session stopped, rec key: {}, voice key: {}, voice pkt_cnt: {}",
		event.record_key(), voice_key_, payload_cnt_);
     
	  //release();
	  for (auto &chan : channels) {
		chan->Stop();
	  }
	  channels.clear();

      // request release
      Command cmd;
      cmd.set_type(Command_CommandType::Command_CommandType_RELEASE);
      cmd.set_record_key(event.record_key());
      cmd.set_voice_key(event.voice_key());
      auto data = cmd.SerializeAsString();
      std::unique_lock<std::mutex> lock(cmd_lock_);
      zmq_send(cmd_sender_, data.c_str(), data.size(), 0);
    } else {
	  g_var.logger->debug("session state is [started], ignore event type: {}", event.type());
    }
    break;
  }
  case ST_STOP: {
	g_var.logger->debug("session state is [stopped], ignore event type: {}", event.type());
    break;
  }
  default:
	g_var.logger->debug("session state is [unknown], ignore event type: {}", event.type());
    break;
  }
}

void Session::handle_voice(Voice voice) {
  switch (state_) {
  case ST_START: {
    payload_cnt_++;
    // TODO: do stt
    //printf("channel No. = %d\n", voice.channel_no());
    //assert(record.voice().channel_no())
    //assert(record.voice().payload())
    if (channels.size() > voice.channel_no()) {
      channels[voice.channel_no()]->DoSTT(voice.payload(), info_);
    }
    break;
  }
  default:
    break;
  }
}

void Session::Start() {
  // thrd_ = std::thread(&Session::Run, this, foo);
  thrd_ = std::thread(&Session::Run, this);
}

void Session::Stop() {
}

void Session::DispatchRecord(Record record) {
  record_q_.push(record);
}

void Session::DispatchEvent(Event event)
{
  //     session_q.push();
  if (event.type() == Event_EventType::Event_EventType_STOP) {
    Command cmd;
    cmd.set_type(Command_CommandType::Command_CommandType_RELEASE);
    cmd.set_record_key(event.record_key());
    cmd.set_voice_key(event.voice_key());
    auto data = cmd.SerializeAsString();
    zmq_send(cmd_sender_, data.c_str(), data.size(), ZMQ_DONTWAIT);
  }
}

//zmq 데이터 전송
void Session::publish_event(std::string call_status){
  
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  document.AddMember("EventType", "CALL", allocator);
  document.AddMember("Event", "status", allocator);
  document.AddMember("Status", Value(call_status.c_str(), allocator).Move(), allocator);
  document.AddMember("Agent", Value(info_.agent.c_str(), allocator).Move(), allocator);
  document.AddMember("CALL_ID", Value(info_.key.c_str(), allocator).Move(), allocator);
  //document.AddMember("Extra", Value(info_.extra.c_str(). allocator).Move(), allocator);
  
  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  if(g_var.publisher_){
    std::string buf = strbuf.GetString();
    g_var.push_lock.lock();
    zmq_send(g_var.publisher_, buf.data(), buf.size(), ZMQ_NOBLOCK);
    g_var.push_lock.unlock();
    LOGGER()->debug(">>> call event publish: {}", buf);
  }
}


void Session::DispatchVoice(Voice voice, int channel_no)
{
}

SessionPool::SessionPool(int session_num)
{
  for (int i = 0; i < session_num; i++) {
    Session *session = new Session();
    session->Start();
    session_list_.push_back(session);
    idle_sessions_.push(session);
  }
  Initialize();
}

SessionPool::~SessionPool() {
  for (auto s : session_list_) {
    s->Stop();
    delete s;
  }
}

void SessionPool::Initialize() {
  context_ = zmq_ctx_new();

  char endpoint[128];
  evt_receiver_ = zmq_socket(context_, ZMQ_PULL);
  //sprintf(endpoint, "tcp://127.0.0.1:%d", g_var.event_port);
  sprintf(endpoint, "tcp://0.0.0.0:%d", g_var.event_port);
  if (zmq_bind(evt_receiver_, endpoint) != 0) {
  }

  cmd_receiver_ = zmq_socket(context_, ZMQ_PULL);
  //sprintf(endpoint, "tcp://127.0.0.1:%d", g_var.cmd_port);
  sprintf(endpoint, "tcp://0.0.0.0:%d", g_var.cmd_port);
  if (zmq_bind(cmd_receiver_, endpoint) != 0) {
  }

  Session::cmd_sender_ = zmq_socket(context_, ZMQ_PUSH);
  if (zmq_connect(Session::cmd_sender_, endpoint) != 0) {
  }

  qry_receiver_ = zmq_socket(context_, ZMQ_REP);
  //sprintf(endpoint, "tcp://0.0.0.0:%d", g_var.qry_port);
  sprintf(endpoint, "tcp://0.0.0.0:%d", g_var.qry_port);
  if (zmq_bind(qry_receiver_, endpoint) != 0) {
    printf("error\n");
  }
}

void SessionPool::Start()
{
  // zeromq loop
  char buf[1024];
  int  size;
  timeval st, et, diff;
  gettimeofday(&st, NULL);
  int pkt_cnt = 0;
  
  while (true) {
    zmq_pollitem_t items [] = {
      { evt_receiver_, 0, ZMQ_POLLIN, 0 },
      { cmd_receiver_, 0, ZMQ_POLLIN, 0 },
      { qry_receiver_, 0, ZMQ_POLLIN, 0 }
    };
    zmq_poll(items, 3, -1 /* no timeout */);

    if (items[0].revents & ZMQ_POLLIN) {
      int size = zmq_recv(evt_receiver_, buf, 1023, 0);
      if (size != -1) {
        handle_record(buf, size);
      }
    }
    if (items[1].revents & ZMQ_POLLIN) {
      int size = zmq_recv(cmd_receiver_, buf, 1023, 0);
      if (size != -1) {
        handle_command(buf, size);
      }
    }
    if (items[2].revents & ZMQ_POLLIN) {
      int size = zmq_recv(qry_receiver_, buf, 1023, 0);
      if (size != -1) {
        handle_query(buf, size);
      }
    }

    // test
    pkt_cnt++;
    if (pkt_cnt % 10000 == 0) {
      gettimeofday(&et, NULL);
      timersub(&et, &st, &diff);
      //printf("per 10000 - %ld.%03ld\n", diff.tv_sec, diff.tv_usec / 1000);
      gettimeofday(&st, NULL);
    }
  }
}

void SessionPool::handle_command(char* buf, int size) {
  Command cmd;
  if (!cmd.ParseFromArray(buf, size)) {
    printf("error\n");
  }
  if (cmd.type() == Command_CommandType::Command_CommandType_RELEASE) {
    ReleaseSession(cmd.record_key(), cmd.voice_key());
  }
}

void SessionPool::handle_query(char* buf, int size) {
  Query qry;
  if (!qry.ParseFromArray(buf, size)) {
    printf("error\n");
  }
  Query answer;
  answer.set_message("hi: " + std::to_string(session_map_.size()));
  char msg[1024];
  answer.SerializeToArray(msg, sizeof(msg));
  zmq_send(qry_receiver_, msg, answer.ByteSizeLong(), ZMQ_DONTWAIT);
}


/**
 * @brief 
 *   RecordCollector로부터 수신한 메시지를 처리한다.
 * 
 * 
 * @param buf : pointer of buffer
 * @param size : buffer size
 */
void SessionPool::handle_record(char* buf, int size) {
  Record record;

  buf[size] = 0;
  std::string msg(buf);
  if (!record.ParseFromArray(buf, size)) {
    printf("error\n");
  }
  if (record.has_event()) {
    //g_var.logger->debug("got record event, rec key: {}, type: {}", record.event().record_key().c_str(), record.event().type());
    //printf("got record event reckey: [%s]\n", record.event().record_key().c_str());
    Session *session;
    session = this->FindSession(record.event().record_key());
    if (!session) {
        if (record.event().type() == Event_EventType::Event_EventType_PREPARE) {
	    //printf("GetSession(): [%s]\n", record.event().record_key().c_str());
            session = this->GetSession(record.event().record_key());
        }
        if (!session) return;
        this->SetVoiceKey(session, record.event().voice_key());
    }

    session->DispatchRecord(record);
    // find session
    // if not session: make session
    // else: send event
    // printf("%s\n", record.event().record_key().c_str());
  } else if (record.has_voice()) {
    Session *session;
    int channel_no = 0;
    for (auto voice_key : record.voice().voice_key_candidates()) {
      //printf("got voice event voice_key: [%s]\n", voice_key.c_str());
      session = this->FindSessionFromVoice(voice_key);
      if (session) {
          record.mutable_voice()->set_channel_no(channel_no);
          session->DispatchRecord(record);
      } else {
          //printf("can't find %s\n", voice_key.c_str());
      }
      channel_no++;
    }
  }
}

void SessionPool::Stop()
{
}

Session *SessionPool::FindSession(const std::string& key)
{
  auto session_iter = session_map_.find(key);
  if (session_iter == session_map_.end()) {
    return NULL;
  }
  return session_iter->second;
}

Session *SessionPool::FindSessionFromVoice(const std::string& key)
{
  auto session_iter = session_map2_.find(key);
  if (session_iter == session_map2_.end()) {
    return NULL;
  }
  return session_iter->second;
}

Session *SessionPool::GetSession(const std::string& key)
{
  if (idle_sessions_.empty()) {
      printf("no idle session\n");
    return NULL;
  }
  Session * session;
  session = idle_sessions_.front();
  idle_sessions_.pop();
  session_map_[key] = session;

  return session;
}

void SessionPool::ReleaseSession(const std::string& rkey, const std::string& vkey)
{
  auto it = session_map_.find(rkey);
  if (it == session_map_.end()) {
    return;
  }
  it->second->state_ = ST_IDLE;
  idle_sessions_.push(it->second);
  it->second->publish_event("DISCONNECTED");
  session_map_.erase(it);
  session_map2_.erase(vkey);
  g_var.logger->info("idle session count is {}", idle_sessions_.size());
}

void SessionPool::SetVoiceKey(Session* session, const std::string& key)
{
  session_map2_[key] = session;
}
