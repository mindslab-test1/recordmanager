#ifndef APP_VARIABLE_H
#define APP_VARIABLE_H

#include <string>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

struct APP_VARIABLE {
  APP_VARIABLE();
  void GetArgs(int argc, char *argv[]);
  void GetConfig(std::string file_name);
  void Initialize();

  std::string cfg_file;

  int event_port;
  int cmd_port;
  int qry_port;

  int pull_timeout;
  std::string push_addr;
  int max_session_cnt;

  bool save_pcm;
  bool save_result;
  std::string sttd_addr;

  // for zeromq
  void *context_;
  void *publisher_;
  std::mutex push_lock;

  std::shared_ptr<spdlog::logger> logger;
  std::shared_ptr<spdlog::logger> console;
};

extern APP_VARIABLE g_var;

#define LOGGER() g_var.logger

#endif /* APP_VARIABLE_H */
