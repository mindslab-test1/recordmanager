#ifndef SESSION_POOL_H
#define SESSION_POOL_H

#include <vector>
#include <queue>
#include <unordered_map>
#include <thread>
#include "record.pb.h"
#include "safe-queue.h"
#include "voice-channel.h"
#include "session-info.h"

#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/prettywriter.h"
#include "include/rapidjson/allocators.h"


enum SessionState {
    ST_IDLE,
    ST_PREPARE,
    ST_START,
    ST_STOP
};

class Session {
public:
  Session();
  ~Session();

  void Start();
  void Stop();
  void DispatchEvent(Event event);
  void DispatchVoice(Voice voice, int channel_no);
  void DispatchRecord(Record record);

private:
  void Run();

  void handle_event(Event event);
  void handle_voice(Voice voice);
  void send_release();
  
  void publish_event(std::string call_status);
  int channel_cnt_;
  int payload_cnt_;
  
  std::string record_key_;
  std::string voice_key_;
  SESSION_INFO info_;

  std::thread thrd_;
  SafeQueue<Record> record_q_;
  SessionState state_;

  std::vector<std::unique_ptr<VoiceChannel> > channels;

  static void *cmd_sender_;
  static std::mutex cmd_lock_;
  static std::mutex publisher_lock_;
 
  friend class SessionPool;
};

class SessionPool {
public:
  SessionPool(int session_num);
  ~SessionPool();

  void Initialize();
  void Start();
  void Stop();

  Session* FindSession(const std::string &key);
  Session* FindSessionFromVoice(const std::string &key);
  Session* GetSession(const std::string &key);
  void ReleaseSession(const std::string &rkey, const std::string &vkey);

  void SetVoiceKey(Session* session, const std::string &key);

private:
  void handle_record(char *buf, int size);
  void handle_command(char *buf, int size);
  void handle_query(char *buf, int size);

  void *context_;
  void *evt_receiver_;
  void *cmd_receiver_;
//   void *cmd_sender_;
  void *qry_receiver_;
  void *publisher_;

  std::vector<Session*> session_list_;
  // 셀프 체크가 가능하므로 사용하지 않음
  //std::queue<Session*> active_sessions_;
  std::queue<Session*> idle_sessions_;

  // from record key (ex: SIP CALL-ID)
  std::unordered_map<std::string, Session*> session_map_;
  // from channel key (ex: RTP src ip + port)
  std::unordered_map<std::string, Session*> session_map2_;
};

#endif /* SESSION_POOL_H */
