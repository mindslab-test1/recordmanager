from concurrent import futures
import time
import math
import logging

import grpc

from maum.brain.w2l import w2l_pb2
from maum.brain.w2l import w2l_pb2_grpc


class SpeechToTextServicer(w2l_pb2_grpc.SpeechToTextServicer):
    """Provides methods that implement functionality of route guide server."""

    def __init__(self):
        pass

    def StreamRecognize(self, request_iterator, context):
        seq = 1
        total_len = 0
        pcm_len = 0
        ts = w2l_pb2.TextSegment()
        ts.start = 1
        ts.end = 2
        ts.txt = '테스트 시작합니다'
        yield ts

        for speech in request_iterator:
            pcm_len += len(speech.bin)
            total_len += pcm_len
            # 2초 지날 때마다 결과 전송
            if pcm_len >= (8000 * 2) * 2:
                ts = w2l_pb2.TextSegment()
                ts.start = 1
                ts.end = 2
                ts.txt = '안녕하세요'
                pcm_len = 0
                yield ts

        ts = w2l_pb2.TextSegment()
        ts.start = 1
        ts.end = 2
        ts.txt = '테스트 종료합니다. TOTAL_PCM_LEN = %d' % total_len
        yield ts


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    w2l_pb2_grpc.add_SpeechToTextServicer_to_server(
        SpeechToTextServicer(), server)
    server.add_insecure_port('[::]:17051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
