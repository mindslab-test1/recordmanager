#include "app-variable.h"
#include "voice-channel.h"

#define BYTES_PER_SAMPLE 2

VoiceChannel::VoiceChannel(int channel_no) 
  : channel_no_(channel_no), stt_client_(nullptr) {
}

VoiceChannel::~VoiceChannel() {
}

void VoiceChannel::Stop() {
  if (stt_client_) {
    timeval now;
    stt_client_->Stop(now);
  }
}

bool VoiceChannel::ConnectSTT(SESSION_INFO info) {
  auto channel = grpc::CreateChannel(g_var.sttd_addr, grpc::InsecureChannelCredentials());
  channel->GetState(true);
  LOGGER()->info("try to connect stt");
  if (channel->WaitForConnected(std::chrono::system_clock::now() + std::chrono::milliseconds(10))) {
    stt_client_.reset(new SttClient(channel)); 
    if (stt_client_->Start("baseline", info, channel_no_) == false) {
      stt_client_.reset();
      return false;
    }
    return true;
  }
  return false;
}

void VoiceChannel::DoSTT(const std::string &buf, SESSION_INFO info) {
  timeval now;
  if (!stt_client_) {
    if (!ConnectSTT(info)) {
      return;
    }
  }

  if (stt_client_->Write(buf.data(), buf.size(), now) == false) {
    stt_client_.reset();
  }
}

void VoiceChannel::DoSTT(short *buf, size_t len, SESSION_INFO info) {
  timeval now;
  if (!stt_client_) {
    if (!ConnectSTT(info)) {
      return;
    }
  }

  if (stt_client_->Write((char *)buf, len * BYTES_PER_SAMPLE, now) == false) {
    stt_client_.reset();
  }
}
