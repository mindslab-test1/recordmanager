#include <getopt.h>

#include "app-variable.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "yaml-cpp/yaml.h"
#include "version.h"
#include "zmq.h"

#define OPT_CFG    1000
#define OPT_ONCE   1001
#define OPT_DOMAIN 1002
#define OPT_DIRECT 1004

#define CFG_FILENAME "recm.yaml"

void version()
{
  printf("Version: %s\n", git_version);
  exit(0);
}

void help(char *argv)
{
  printf("Usage: %s options\n", argv);
  printf("  -c, --cfg      [config file path]\n"
         "  -v, --version\n"
         "  -h, --help\n");
  exit(0);
}

APP_VARIABLE::APP_VARIABLE() {
  spdlog::flush_on(spdlog::level::trace);
  console = spdlog::stdout_color_mt("console");
  cfg_file = CFG_FILENAME;
}

void APP_VARIABLE::GetArgs(int argc, char *argv[]) {
  int c;
  while (true) {
    static struct option long_options[] = {
      {"cfg",         required_argument, 0,           'c'},
      {"help",        no_argument,       0,           'h'},
      {"version",     no_argument,       0,           'v'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "c:hv", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:
        break;

      case 'c':
	cfg_file = optarg;
        break;

      case 'h':
        help(argv[0]);
        break;

      case 'v':
        version();
        break;

      default:
        help(argv[0]);
    }
  }
}

template<typename T>
T get_value(YAML::Node node, std::string key) {
  if (!node[key].IsDefined()) {
    g_var.console->error("{}: {} is not found", node.Tag(), key);
    exit(1);
  }
  return node[key].as<T>();
}

template<typename T>
T get_value_or_default(YAML::Node node, std::string key, T value) {
  if (!node[key].IsDefined()) {
    return value;
  }
  return node[key].as<T>();
}

void APP_VARIABLE::GetConfig(std::string file_name) {
  try {
    YAML::Node config = YAML::LoadFile(file_name);

    auto log_level = get_value<std::string>(config["log"], "level");
    auto log_dir   = get_value<std::string>(config["log"], "directory");
    auto max_size  = get_value<int>(config["log"], "max.size.mbytes") * 1000 * 1000;
    auto max_files = get_value<int>(config["log"], "max.files");
    logger = spdlog::rotating_logger_mt("APP", log_dir + "/recm.log", max_size, max_files);
    logger->set_level(spdlog::level::from_str(log_level));
    logger->info("APP process started...");
    logger->trace("Logger is created...");
    logger->debug("Log level is {}", log_level);

	// STT 서버 gRPC 주소
	sttd_addr = get_value<std::string>(config["stt"], "address");

	event_port = get_value<int>(config["zmq"], "event.port");
	cmd_port   = get_value<int>(config["zmq"], "command.port");
	qry_port   = get_value<int>(config["zmq"], "query.port");
	logger->debug("Zmq event listen port is {}", event_port);
	logger->debug("Zmq command listen port is {}", cmd_port);
	logger->debug("Zmq query listen port is {}", qry_port);
	
	//zmq로 데이터 전송
	pull_timeout = get_value<int>(config["common"], "pull.timeout");
	push_addr    = get_value<std::string>(config["common"], "push.addr");
	max_session_cnt = get_value<int>(config["common"],"max.session");

  } catch (YAML::Exception &e) {
    console->error("Config parser error: {}", e.what());
    exit(1);
  }
}

void APP_VARIABLE::Initialize() {
  // zeromq publish
  int rc;
  context_ = zmq_ctx_new();
  publisher_ = zmq_socket(context_, ZMQ_PUSH);
  
  if ( (rc = zmq_connect(publisher_, push_addr.c_str())) != 0) {
    console->error("zmq bind error");
    exit(1);
  }
}

APP_VARIABLE g_var;
