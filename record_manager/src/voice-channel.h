#ifndef VOICE_CHANNEL_H
#define VOICE_CHANNEL_H

#include <memory>
#include "voice-channel.h"
#include "stt-client.h"
#include "session-info.h"

class VoiceChannel {
 public:
  VoiceChannel(int channel_no = 0);
  virtual ~VoiceChannel();

  virtual void Stop();

  void DoSTT(const std::string &buf, SESSION_INFO info);
  void DoSTT(short *buf, size_t len, SESSION_INFO info);

private:

  bool ConnectSTT(SESSION_INFO info);
  SESSION_INFO info_;
  std::unique_ptr<SttClient> stt_client_;
  int channel_no_;
};

#endif /* VOICE_CHANNEL_H */
