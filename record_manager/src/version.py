#!/usr/bin/env python

import os
import subprocess

HDR_TMP = '''\
#ifndef GIT_VERSION_H
#define GIT_VERSION_H

const char git_version[] = "{}";

#endif /* GIT_VERSION_H */
'''

FNULL = open(os.devnull, 'w')

def main():
    p = subprocess.Popen(["git", "describe", "--always", "--abbrev=5", "--tags", "--long"],
                         stdout=subprocess.PIPE, stderr=FNULL)
    git_version, err = p.communicate()
    git_version = git_version.decode('utf-8').strip()

    if git_version == '':
        git_version = 'unknown'
    elif len(git_version.split('-')) < 2:
        git_version = 'unknown-' + git_version
        
    p = subprocess.Popen(["git", "diff"], stdout=subprocess.PIPE, stderr=FNULL)
    modified, err = p.communicate()
    if len(modified) > 0:
        git_version += '-modified'
    print(git_version)

    new_text = HDR_TMP.format(git_version)
    try:
        with open('version.h') as f:
            old_text = f.read()
            if old_text == new_text:
                return
    except:
        pass

    f = open('version.h', 'w')
    f.write(new_text)
    f.close()

if __name__ == '__main__':
    main()
