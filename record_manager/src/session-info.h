#ifndef SESSION_INFO_H
#define SESSION_INFO_H

#include <string>

struct SESSION_INFO {
	std::string key;
	std::string agent;
	std::string extra;
	std::string record_dir;
};


#endif
