#include "app-variable.h"
#include "stt-client.h"
//#include <boost/algorithm/string.hpp>
#include <sys/time.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <zmq.h>

#include "include/rapidjson/document.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/prettywriter.h"
#include "include/rapidjson/allocators.h"

using namespace rapidjson;

std::thread SttClient::monitor_thrd_;

SttClient::SttClient(std::shared_ptr<Channel> channel)
    : stub_(SpeechToText::NewStub(channel)) {
  pcm_fd_ = 0;
  result_fd_ = 0;
  last_result_time_ = 0;
}

SttClient::~SttClient() {
  if (result_thrd_.joinable()) {
    result_thrd_.join();
  }

  if (pcm_fd_ > 0) close(pcm_fd_);
  if (result_fd_ > 0) close(result_fd_);
}

void SttClient::ReadStream(ClientReaderWriter<Speech, TextSegment>* stream) {
  TextSegment segment;
  std::string buf;
  while (stream->Read(&segment)) {
    g_var.console->debug("result: {}", segment.txt());
    Publish(segment.start(), segment.end(), call_id_, segment.txt());
    last_result_time_ = segment.end();
    buf += segment.txt();

    // io 성능을 위해 데이터가 모이면 쓰자
    if (g_var.save_result && result_fd_ > 0 && buf.length() >= 1024) {
      write(result_fd_, buf.c_str(), buf.length());
      buf.clear();
    }
  }

  if (g_var.save_result && result_fd_ > 0 && !buf.empty()) {
    write(result_fd_, buf.c_str(), buf.length());
  }

  long int end;
  timeval tv_diff;
  timeval now;
  timersub(&stop_time_, &start_time_, &tv_diff);
  //LOGGER()->info("[SttClient] time: {} {}", tv_diff.tv_sec, tv_diff.tv_usec);
  // The unit of end is 10 millisec
  end = tv_diff.tv_sec * 100 + tv_diff.tv_usec / 10000;
  end = end * 80;
  Publish(last_result_time_, end, call_id_, "stop");
}

void SttClient::Publish(int start, int end, std::string call_id, std::string txt) {
    //char buf[4096];
    //sprintf(buf, "%s,%.2f,%.2f,%s",
    //        call_id.c_str(),
    //        start / 100.0,
    //        end / 100.0,
    //        txt.c_str());

    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    char str_start[64];
    char str_end[64];
    char str_chan[2];
    
    sprintf(str_start, "%.2f", start / 8000.0);
    sprintf(str_end, "%.2f", end / 8000.0);
    sprintf(str_chan, "%d", channel_no_);

    document.AddMember("EventType", "STT", allocator);
    document.AddMember("Event", "result", allocator);
    document.AddMember("Start", StringRef(str_start), allocator);
    document.AddMember("End", StringRef(str_end), allocator);
    document.AddMember("Text", StringRef(txt.c_str()), allocator);
    document.AddMember("Agent", Value(info_.agent.c_str(), allocator).Move(), allocator);
    document.AddMember("CALL_ID", Value(info_.key.c_str(), allocator).Move(), allocator);
    document.AddMember("ChannelNo", StringRef(str_chan), allocator);
    //document.AddMember("Extra", Value(info_.extra.c_str(), allocator).Move, allocator);
    
    StringBuffer strbuf;
    Writer<StringBuffer> writer(strbuf);
    document.Accept(writer);

    if (g_var.publisher_){
      std::string buf = strbuf.GetString();
      g_var.push_lock.lock();
      zmq_send(g_var.publisher_, buf.data(), buf.size(), ZMQ_NOBLOCK);
      g_var.push_lock.unlock();
      LOGGER()->debug(">>> stt result publish: {}", buf);
  }

  // call_id, start_time, end_time, text
  LOGGER()->debug("result: {}", txt);
}

bool SttClient::Start(std::string model, SESSION_INFO info, int channel_no) {  //int channel_no 추가하기
  info_ = info;
  call_id_ = info_.key;
  channel_no_ = channel_no;
  //start_time_ = start_time;
  gettimeofday(&start_time_, NULL);
  //LOGGER()->info("[SttClient] time: {} {}", start_time_.tv_sec, start_time_.tv_usec);

#if 0
  if (g_var.save_pcm) {
    pcm_tempf_ = g_var.call_temp_path + "/" + call_id_ + ".tmp";
    pcm_fd_ = open(pcm_tempf_.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);
    LOGGER()->debug("PCM temp file_name is {}", pcm_tempf_);
  }
  if (g_var.save_result) {
    result_tempf_ = g_var.call_temp_path + "/" + call_id_ + ".dat";
    result_fd_ = open(result_tempf_.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);
    LOGGER()->debug("STT Result temp file_name is {}", result_tempf_);
  }
#endif

  resp.Clear();
  ctx_.reset(new ClientContext);
  // meta data example
  // ctx_->AddMetadata("in.lang", g_var.sttd_lang);

  stream_ = stub_->StreamRecognize(ctx_.get());
  result_thrd_ = std::thread(&SttClient::ReadStream, this, stream_.get());

  g_var.console->info("SttClient started... ");
  Publish(0, 0, call_id_, "start");
  return true;
}

std::string to_date(timeval tv) {
  struct tm *info;
  char tmp[20];
  char buffer[80];
  info = localtime(&tv.tv_sec);

  strftime(tmp, sizeof(tmp) , "%H:%M:%S", info);
  snprintf(buffer, sizeof(buffer), "%s.%03ld", tmp, tv.tv_usec / 1000);
  return std::string(buffer);
}

void SttClient::Stop(timeval end_time) {
  g_var.console->info("SttClient stopped... ");
  gettimeofday(&stop_time_, NULL);

  if (sndbuf_.size() > 0) {
    Speech speech;
    speech.set_bin(sndbuf_);
#if 0
    if (g_var.save_pcm) {
      WritePCM(sndbuf_, true);
    }
#endif
    sndbuf_.clear();
    if (stream_->Write(speech) == false) {
      LOGGER()->error("stt client write fail {}", phone_number_);
    }
  }
  stream_->WritesDone();
  if (result_thrd_.joinable()) {
    result_thrd_.join();
  }
  auto status = stream_->Finish();
  if (status.ok()) {
    LOGGER()->info("[SttClient] finished: {}", status.error_message());
  } else {
    LOGGER()->info("[SttClient]failed: {}", status.error_message());
  }

#if 0
  // 녹취 파일 정리
  if (g_var.save_pcm && pcm_fd_ > 0) {
    close(pcm_fd_);
    pcm_fd_ = 0;
    std::string pcm_final = g_var.call_final_path + "/" + call_id_ + ".pcm";
    if (rename(pcm_tempf_.c_str(), pcm_final.c_str()) != 0) {
      LOGGER()->error("Move PCM file error : {} -> {}, {}",
                      pcm_tempf_, pcm_final, strerror(errno));
    }
  }

  // STT Result 파일 정리
  if (g_var.save_result && result_fd_ > 0) {
    close(result_fd_);
    result_fd_ = 0;
    std::string result_final = g_var.call_final_path + "/" + call_id_ + ".result";
    if (rename(result_tempf_.c_str(), result_final.c_str()) != 0) {
      LOGGER()->error("Move Result file error : {} -> {}, {}",
                      result_tempf_, result_final, strerror(errno));
    }
  }
#endif
}

void SttClient::WritePCM(std::string &buf, bool save_all) {
  audio_buf_.append(buf);
  if (audio_buf_.size() >= 1024 || save_all) {
    if (write(pcm_fd_, audio_buf_.data(), audio_buf_.size()) == -1) {
      LOGGER()->error("File Writing is failed, {}", strerror(errno));
    }
    audio_buf_.clear();
  }
}

bool SttClient::Write(const char* buf, size_t size, timeval timestamp) {
  sndbuf_.append(buf, size);
  if (sndbuf_.size() < 160 * 20) {
    return true;
  }

  Speech speech;
  //speech.set_bin(buf, size);
  speech.set_bin(sndbuf_);

  if (g_var.save_pcm) {
    WritePCM(sndbuf_);
  }
  sndbuf_.clear();

  if (stream_->Write(speech) == false) {
    LOGGER()->error("stt client write fail {}", call_id_);
    return false;
  }

  return true;
}

bool SttClient::Ping(std::string addr) {
#if 0
  // Find STT Child Process
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SttModelResolver::Stub> stub(SttModelResolver::NewStub(channel));

  ClientContext ctx;
  ServerStatus server_status;

  Model model;
  model.set_model(g_var.sttd_model);
  model.set_sample_rate(8000);
  model.set_lang(LangCode::kor);

  grpc::Status status = stub->Find(&ctx, model, &server_status);
  if (status.error_code() != grpc::StatusCode::OK) {
    g_var.console->error("GRPC Find({}) error is {}", addr, status.error_message());
    return false;
  }

  // Ping to STT Child Process
  auto real_channel =
    grpc::CreateChannel(server_status.server_address(), grpc::InsecureChannelCredentials());
  auto real_stub = SttRealService::NewStub(real_channel);

  ClientContext real_ctx;
  ServerStatus real_server_status;
  grpc::Status real_status = real_stub->Ping(&real_ctx, model, &real_server_status);
  if (real_server_status.running() != true) {
    g_var.console->error("GRPC Ping({}) status is {}:{}",
                        addr, real_server_status.running(), real_server_status.invoked_by());
    return false;
  }
#endif
  return true;
}

void SttClient::StartMonitor() {
  monitor_thrd_ = std::thread(SttClient::CheckAlive);
}

void SttClient::StopMonitor() {
  if (monitor_thrd_.joinable()) {
    monitor_thrd_.join();
  }
}

void SttClient::CheckAlive() {
}

bool SttClient::Ping(std::string addr, std::string &real_addr) {
#if 0
  // Find STT Child Process
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SttModelResolver::Stub> stub(SttModelResolver::NewStub(channel));

  ClientContext ctx;
  ServerStatus server_status;

  Model model;
  model.set_model(g_var.sttd_model);
  model.set_sample_rate(8000);
  model.set_lang(LangCode::kor);

  grpc::Status status = stub->Find(&ctx, model, &server_status);
  if (status.error_code() != grpc::StatusCode::OK) {
    g_var.console->error("GRPC Find({}) error is {}", addr, status.error_message());
    return false;
  }

  // Ping to STT Child Process
  auto real_channel =
    grpc::CreateChannel(server_status.server_address(), grpc::InsecureChannelCredentials());
  auto real_stub = SttRealService::NewStub(real_channel);

  ClientContext real_ctx;
  ServerStatus real_server_status;
  grpc::Status real_status = real_stub->Ping(&real_ctx, model, &real_server_status);
  if (real_server_status.running() != true) {
    g_var.console->error("GRPC Ping({}) status is {}:{}",
                        addr, real_server_status.running(), real_server_status.invoked_by());
    return false;
  }
  real_addr = server_status.server_address();
  //g_var.console->info("real server is {}, {}", real_addr, addr);
#endif
  return true;
}
