#ifndef STT_CLIENT_H
#define STT_CLIENT_H

#include <thread>
#include <string>
#include <grpc++/grpc++.h>
#include "maum/brain/w2l/w2l.grpc.pb.h"

#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/prettywriter.h"
#include "include/rapidjson/allocators.h"
#include "session-info.h"
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using maum::brain::w2l::SpeechToText;
using maum::brain::w2l::Speech;
using maum::brain::w2l::TextSegment;

using std::string;


class SttClient
{
public:

  SttClient(std::shared_ptr<Channel> channel);
  ~SttClient();

//  bool Start(std::string model, std::string call_id, timeval start_time);
  bool Start(std::string model, SESSION_INFO info, int channel_no);
  void Stop(timeval timestamp);

  bool Write(const char* buf, size_t size, timeval timestamp);
  void Publish(int start, int end, std::string call_id, std::string txt);

  static bool Ping(std::string addr);
  static bool Ping(std::string addr, std::string &real_addr);
  static void StartMonitor();
  static void StopMonitor();
  static void CheckAlive();

public:

  TextSegment resp;
  std::string addr;

private:

  void ReadStream(ClientReaderWriter<Speech, TextSegment>* stream);
  void WritePCM(std::string &buf, bool save_all = false);

  std::unique_ptr<ClientContext> ctx_;
  std::unique_ptr<SpeechToText::Stub> stub_;
  std::vector<int16_t> sample_list_;
  std::unique_ptr<grpc::ClientReaderWriter<Speech, TextSegment> > stream_;

  int pcm_fd_;
  int result_fd_;

  std::string call_id_;
  std::thread result_thrd_;

  std::string phone_number_;

  timeval start_time_;
  timeval stop_time_;
  timeval result_time_;

  /**
   * @brief STT segment 결과 중 마지막으로 받은 segment.end 값
   *
   */
  int last_result_time_;

  string sndbuf_;
  string audio_buf_;
  string pcm_tempf_;
  string result_tempf_;
  
  SESSION_INFO info_;
  int channel_no_;

  static std::thread monitor_thrd_;
};


#endif /* STT_CLIENT_H */
