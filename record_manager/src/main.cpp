#include <string>
#include <string.h>
#include <sys/time.h>
#include <zmq.h>
#include "app-variable.h"
#include "session_pool.h"
#include "record.pb.h"

int main(int argc, char *argv[])
{
  g_var.GetArgs(argc, argv);
  g_var.GetConfig(g_var.cfg_file);
  g_var.Initialize();

  // read config
  // if active/standby
  //   start heartbeat module
  // start session pool
  SessionPool sp(200);
  sp.Start();

  return 0;
}
