#!/bin/sh
SRC_DIR=../../protos
DST_DIR=.
protoc -I=$SRC_DIR --cpp_out=$DST_DIR $SRC_DIR/record.proto
#protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/record.proto

# w2l
protoc -I . --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` maum/brain/w2l/w2l.proto
protoc -I . --cpp_out=. maum/brain/w2l/w2l.proto

# w2l mock server
python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. maum/brain/w2l/w2l.proto
