# RecordManager
## Record Manager의 구성
---
RecordManager는 두 개의 서비스로 구성
+ [pcap_collector.py] 
  - libpcap의 python wrapper 라이브러리인 pypcap을 사용하여SIP packet을 capture
  - 캡처된 SIP 패킷 포함된 RTP정보를 분석하고 payload를 zmq를 이용하여 RECMd로 전송
+ [RECMd]
  - Record manager 데몬으로 zmq로 수신된 RTP payload를 기반으로 voice session을 생성하고 관리
  - 생성된 voice session별로 grpc 연동으로 STT로 음성패킷을 전송하고 음성인식 결과를 수신 


## Record Manager 도커
---
### 컨테이너 내에서 다수의 서버스를 실행할 수 있는 방법 
자세한 내용은 Dockerfile을 참고. entrypoint.sh를 아래의 링크를 참고하여 작성 \ 
[Docker 링크](https://docs.docker.com/config/containers/multi-service_container/)
```
#!/bin/bash
PATH=/srv/maum/bin:$PATH

# Start pcap collector with python
python /root/recm/record_collector/pcap_collector.py -c /root/recm/record_collector/pcap_collector.cfg &

# Start record manager 
/srv/maum/bin/RECMd -c /srv/maum/etc/recm.yaml &

# Wait for any process to exit
wait -n
  
# Exit with status of process that exited first
exit $?
```
